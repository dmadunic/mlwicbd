# mlwicbd - devhub deploy

This readme explains how to manually deploy mlwicbd application on AG04 devhub server.

We are going to deploy two applications:

* mlwicbd-rest - rest api packaged as war, deployed on tomcat
* mlwicbd-ng2-app - angular2 spa application packaged as zip and deployed on nginx

NOTE: in all code snippets replace dmadunic with your user account on devhub.

 
## Deploy applications to devhub
 
### Build

First perform standard application build by running the following command, from the root of the project

``` bash
rm -rf mlwicbd-ng2-app/{node_modules,dist} # Not needed in all cases
./gradlew clean build
```

For more details on project setup see README.md

#### Copy necessary files to your ${HOME} folder on devhub.ag04.com

```
scp mlwicbd-rest/build/libs/mlwicbd-rest-${version}.war dmadunic@devhub.ag04.com:.
 
scp mlwicbd-ng2-app/build/distributions/mlwicbd-ng2-app-${version}.zip dmadunic@devhub.ag04.com:.
    
```

After all transfers have completed:
```
ssh dmadunic@devhub.ag04.com 
mv mlwicbd-rest-${version}.war mlwicbd-rest.war
sudo su
```

#### Deploy mlwicbd-rest war to tomcat

```
mv mlwicbd-rest.war /srv/tomcat/webapps/.
```

There should be no need to restart tomcat.

If there is a need to restart Tomcat:

``` bash
service tomcat stop;
rm -rf /srv/tomcat/webapps/mlwicbd-rest;
service tomcat start;
```

Then start the application inside https://devhub.ag04.com/manager/.

You can check if rest-api is deployed by opening the following url in your browser:
 
https://devhub.ag04.com/mlwicbd-rest/env

#### Deploy angular2 spa to nginx

If the folder mlwicbd/ already exists in your ${HOME} folder delete it, otherwise skip this step.

```
rm -fR mlwicbd
```

Now unzip angular2 app, and prepare it:

```
unzip mlwicbd-ng2-app-${version}.zip
mv mlwicbd-ng2-app-${version} mlwicbd
```

And finally deploy angular2 app to nginx

```
rm -fR /srv/nginx/mlwicbd
mv mlwicbd /srv/nginx/.

```

If all went well you should have angular2 application up and running at:

https://devhub.ag04.com/mlwicbd/

Good luck!

If you notice any errors in this readme.md PLEASE CORRECT THEM!

Thx