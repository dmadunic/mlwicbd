var replace = require('replace-in-file');
var package = require("./package.json");
var buildVersion = package.version;
var buildDate = new Date();
const optionsVersion = {
  files: 'src/environments/environment.prod.ts',
  from: /buildVersion: '(.+)'/g,
  to: "buildVersion: '"+ buildVersion + "'",
  allowEmptyPaths: false,
};
const optionsDate = {
  files: 'src/environments/environment.prod.ts',
  from: /buildDate: '(.+)'/g,
  to: "buildDate: '"+ buildDate + "'",
  allowEmptyPaths: false,
};
console.log("Writing version into environments.prod.ts");
try {
  var changedFilesVersion = replace.sync(optionsVersion);
  var changedFilesDate = replace.sync(optionsDate)
  console.log('v:' + changedFilesVersion + '...@:' + changedFilesDate);
  if (changedFilesVersion == 0 && changedFilesDate == 0) {
    throw "Please make sure that file '" + optionsVersion.files + "' has \"buildVersion: ''\"";
  }
  if (changedFilesDate == 0){
    throw "Please make sure that file '" + optionsDate.files + "' has \"buildDate: ''\"";

  }
  console.log('Build version set: ' + buildVersion);
  console.log('Build time set: ' + buildDate)
}
catch (error) {
  console.error('Error occurred:', error);
  throw error
}
