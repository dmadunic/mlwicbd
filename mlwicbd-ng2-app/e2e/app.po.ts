import { browser, element, by } from 'protractor';

export class Ag04Ng2Bs3SbAdmin2Page {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('app-root h1')).getText();
  }
}
