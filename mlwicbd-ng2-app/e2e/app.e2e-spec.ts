import { Ag04Ng2Bs3SbAdmin2Page } from './app.po';

describe('ag04-ng2-bs3-sb-admin-2 App', function() {
  let page: Ag04Ng2Bs3SbAdmin2Page;

  beforeEach(() => {
    page = new Ag04Ng2Bs3SbAdmin2Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
