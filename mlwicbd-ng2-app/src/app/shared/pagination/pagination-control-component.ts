import { Component, Output, EventEmitter, Input } from "@angular/core";
import { GlobalConstants } from "../../common/global.constants";

@Component({
  selector: 'app-pagination-control',
  templateUrl: './pagination-control-component.html'
})
export class PaginationControlComponent {

  @Output() submitChangeSize = new EventEmitter<number>();

  @Output() submitPageChange = new EventEmitter<number>();

  @Input() firstRecord: number;

  @Input() lastRecord: number;

  @Input() numOfRecords: number;

  @Input() pageNumber: number;

  @Input() initPageSize: number;

  pageSizes = GlobalConstants.PAGE_SIZES;

  changeSize(size: number) {
    this.submitChangeSize.emit(size);
  }

  newPage(page) {
    this.submitPageChange.emit(page);
  }

}
