import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { PaginationControlComponent } from "./pagination/pagination-control-component";
import { FieldValidationErrorWidget } from "../common/widget/field-validation-error.widget";
import { TranslateService } from '@ngx-translate/core';
import { GravatarComponent } from "../common/gravatar.component";
import {UserFormComponent} from "./user-form/user-form.component";
import {BlankComponent} from "./blank/blank.component";
import {AlertModule} from "ngx-bootstrap";
import {NgxPaginationModule} from "ngx-pagination";

/**
 * Do not specify providers for modules that might be imported by a lazy loaded module.
 */
@NgModule({
  imports:      [
    CommonModule,
    NgxPaginationModule,
    TranslateModule,
    AlertModule.forRoot(),
  ],
  declarations: [ PaginationControlComponent, FieldValidationErrorWidget, GravatarComponent, BlankComponent],
  exports:      [
    CommonModule,
    NgxPaginationModule,
    TranslateModule,
    AlertModule,
    PaginationControlComponent,
    FieldValidationErrorWidget,
    GravatarComponent,
    BlankComponent
  ]
})
export class SharedModule {
  //
}
