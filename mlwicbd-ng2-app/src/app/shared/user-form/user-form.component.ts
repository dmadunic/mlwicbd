import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import { FormGroup } from '@angular/forms';
import {CountryService} from "../../common/service/country-service";
import {Country} from "../../common/dto/country.dto";
import {UserFormSettings} from "../../common/form/user-form.settings";

@Component({
  selector: 'user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {
  countries: Country[];
  @Input() countryCode?: string;
  @Input() formSettings: UserFormSettings;
  @Output() notify: EventEmitter<FormGroup> = new EventEmitter<FormGroup>();
  public userForm: FormGroup;

  @Input()
  set _userForm(userForm: FormGroup){
    this.userForm = userForm || null;
  }

  constructor(
    private countryService: CountryService,
  ) { }

  ngOnInit() {
    this.countryService.fetchAll().subscribe(
      countries => {
        this.countries = countries.content;
        this.userForm.patchValue({'countryCode': this.findCountry(this.countries)});
      },
      err => {
        console.log("FETCH countries failed: " + err);
      }
    );
  }

  public submit() : void {
    this.notify.emit(this.userForm);
  }

  findCountry(countries: Country[]): any {
    if (!this.countryCode){
      return null;
    }

    for (let country of countries){
      if (country.a2code == this.countryCode)
        return country.code;
    }
  }


}
