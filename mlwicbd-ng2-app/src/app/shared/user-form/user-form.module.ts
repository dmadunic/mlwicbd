/**
 * Created by tgreblicki on 6/27/17.
 */
import {NgModule} from "@angular/core";
import {UserFormComponent} from "./user-form.component";
import {ReactiveFormsModule} from "@angular/forms";
import {SharedModule} from "../shared.module";

@NgModule({
  imports: [
    SharedModule,
    ReactiveFormsModule
  ],
  declarations: [UserFormComponent],
  exports: [UserFormComponent]
})

export class UserFormModule {}
