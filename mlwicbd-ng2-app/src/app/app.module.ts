import { BrowserModule } from "@angular/platform-browser";
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";
import { LocationStrategy, HashLocationStrategy } from "@angular/common";
import { HttpModule, Http } from '@angular/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { LocalStorageModule } from 'angular-2-local-storage';
import { SlimLoadingBarModule } from "ng2-slim-loading-bar";
import { AppComponent } from "./app.component";
import { AppRoutingModule } from "./app-routing.module";
import { LoginComponent } from "./login/login.component";
import { MainModule } from "./main/main.module";
import { HttpWrapperService } from "./http-wrapper.service";
import { CoreModule } from "./core/core.module";
import { GlobalConstants } from "./common/global.constants";
import { AlertModule, Ng2BootstrapModule } from "ngx-bootstrap";

export function HttpLoaderFactory(http: Http) {
    let prefix = GlobalConstants.APP_BASE + 'assets/i18n/';
    return new TranslateHttpLoader(http, prefix);
}

@NgModule({
  imports: [
    Ng2BootstrapModule,
    BrowserModule,
    CoreModule,
    ReactiveFormsModule,
    AlertModule.forRoot(),
    SlimLoadingBarModule.forRoot(),
    LocalStorageModule.withConfig({
      prefix: 'mlwicbd',
      storageType: 'sessionStorage'
    }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [Http]
      }
    }),
    MainModule,
    AppRoutingModule
  ],
  declarations: [
    AppComponent,
    LoginComponent
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
  providers: [
    HttpWrapperService,
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
