import {NgModule, SkipSelf, Optional, APP_INITIALIZER} from "@angular/core";
import {AuthService} from "./auth.service";
import {AppInitService} from "./app-init.service";
import {AuthGuard} from "./auth-guard.service";
import {AuthHttp, AuthConfig} from "angular2-jwt";
import {RequestOptions, Http, HttpModule} from "@angular/http";
import {environment} from "../../environments/environment";
import {TokenUtilsService} from "./oauth2/token.utils";
import {LocalStoreManager} from "./oauth2/local-store-manager.service";
import {RoleGuard} from "./role-guard.service";
import {ToastyModule} from "ng2-toasty";

/**
 * Do initing of services that are required before app loads
 * NOTE: this factory needs to return a function (that then returns a promise)
 */

//TODO: provjeriti dali ovdje treba auth service?!
export function init_app(appInitService: AppInitService, authService: AuthService) {
  return () => appInitService.init()
}

/**
 * Angular2-JWT configuration
 */
export function ng2JwtConfig(http: Http, options: RequestOptions, authService: AuthService) {
  return new AuthHttp(new AuthConfig({
    tokenName: environment.token_storage.jwt_object['access_token'],
    tokenGetter: (() => {
      let token = authService.token;
      if (token == null)
        return null;
      else return token.access_token;
    })
  }), http, options);
}

@NgModule({
  imports: [
    HttpModule,
    ToastyModule.forRoot()
  ],
  /**
   * Global singleton services injected inside AppModule.
   * Do not reprovide them in another component/module
   * unless you want an another instance.
   */
  providers: [
    LocalStoreManager,
    TokenUtilsService,
    AuthService,
    AppInitService,
    {
      provide: APP_INITIALIZER,
      //TODO: ako u metodi ne treba auth service izbaciti ga iz deps-a
      useFactory: init_app,
      // AppInitService has to be put first or it throws exception
      deps: [AppInitService, AuthService],
      multi: true
    },
    AuthGuard,
    RoleGuard,
    {
      provide: AuthHttp,
      useFactory: ng2JwtConfig,
      deps: [Http, RequestOptions, AuthService]
    },
  ],
  exports: [ToastyModule]
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule) {
      throw new Error('CoreModule is already loaded. Import it in the AppModule only');
    }
  }

  /**
   * Optionally configure core module
   * https://angular.io/docs/ts/latest/guide/ngmodule.html#!#core-for-root
   */
  /*
  static forRoot(config: TodoConfig): ModuleWithProviders {
    return {
      ngModule: CoreModule,
      providers: [
        {provide: TodoConfig, useValue: config }
      ]
    };
  }
  */
}
