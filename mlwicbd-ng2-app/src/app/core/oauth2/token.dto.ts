/**
 * Created by jdolanski on 10.01.17..
 */
export interface TokenDto {
  access_token: string;
  token_type: string;
  refresh_token: string;
  expires_in: number;
  scope: string;
  fnm: string;
  lnm: string;
}
