import {Http, Headers, Response} from "@angular/http";
import {Observable} from "rxjs";
import {TokenDto} from "./token.dto";
import {environment} from "../../../environments/environment";
import {LocalStoreManager} from "./local-store-manager.service";
import {Injectable} from "@angular/core";

/**
 * Created by jdolanski on 10.01.17..
 */
@Injectable()
export class TokenUtilsService {
  private readonly authHeaders: Headers;

  constructor(private http: Http, private manager: LocalStoreManager) {
    console.log('Initializing storage sync listener...');
    /*
     This is the first thing you have to call before you use any functionality in this library.
     You can call this on Page Load. This hooks tab synchronization up.
     */
    this.manager.initialiseStorageSyncListener();

    this.authHeaders = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': `Basic ${btoa(environment.client.id + ":" + environment.client.secret)}`
    });
  }

  public requestAccessToken(username: string, password: string, rememberMe?: boolean): Observable<TokenDto> {
    let data = `username=${encodeURI(username)}&password=${encodeURI(password)}`;

    return this.http.post(
      `${environment.token_provider}/oauth/token?grant_type=password`,
      data, {headers: this.authHeaders}
    ).map((response: Response) => this.storeTokenDto(response, rememberMe))
      .catch(error => Observable.throw(error || "Token service not available at the moment"));
  }

  public requestRefreshToken(): Observable<TokenDto> {
    let data = `refresh_token=${this.getTokenDto().refresh_token}`;

    return this.http.post(
      `${environment.token_provider}/oauth/token?grant_type=refresh_token`,
      data, {headers: this.authHeaders}
    ).map((response: Response) => this.storeTokenDto(response))
      .catch((error: any) =>  Observable.throw(error || "Token service not available at the moment"));
  }

  public storeTokenDto(response: Response, rememberMe?: boolean): TokenDto {
    if (rememberMe == null) {
      let storedRememberMe = this.manager.getDataObject<boolean>(environment.token_storage.remember_me);
      if (storedRememberMe != null && storedRememberMe === true) {
        rememberMe = true;
      } else {
        rememberMe = false;
      }
    }

    let tokenDto: TokenDto = response.json() as TokenDto;

    if (rememberMe === true) {
      this.manager.savePermanentData(JSON.stringify(tokenDto), environment.token_storage.jwt_object);
    } else {
      this.manager.saveSyncedSessionData(JSON.stringify(tokenDto), environment.token_storage.jwt_object);
    }

    this.manager.savePermanentData(JSON.stringify(rememberMe), environment.token_storage.remember_me);
    return tokenDto;
  }

  public getTokenDto(): TokenDto {
    return this.manager.getDataObject<TokenDto>(environment.token_storage.jwt_object);
  }

  public clear(): void {
    this.manager.deleteData(environment.token_storage.jwt_object);
    this.manager.deleteData(environment.token_storage.remember_me);
  }
}
