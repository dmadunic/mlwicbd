import {Injectable} from "@angular/core";
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanActivateChild} from "@angular/router";
import {Observable} from "rxjs";
import {AuthService} from "../core/auth.service";

/**
 * Created by jdolanski on 11.01.17..
 */
@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild {

  constructor(private auth: AuthService, private router: Router) {

  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.auth.authState.first()
      .map(authenticated => {
        if (authenticated == false) {
          console.log('AuthGuard#redirecting');
          this.router.navigate(['/login'], {queryParams: {returnUrl: state.url}});
        }

        return authenticated;
      })
      .catch(error => {
        console.log(error);
        this.router.navigate(['/login'], {queryParams: {returnUrl: state.url}});
        return Observable.of(false);
      });
  }

  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.canActivate(childRoute, state);
  }
}


