import {Injectable} from "@angular/core";
import {CanActivate, CanActivateChild, Router, ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {AuthService} from "./auth.service";
import {JwtHelper} from "angular2-jwt";
import {AuthGuard} from "./auth-guard.service";
import {Observable} from "rxjs";

/**
 * Created by jdolanski on 19.01.17..
 */
@Injectable()
export class RoleGuard implements CanActivate, CanActivateChild {
  jwtHelp: JwtHelper;

  constructor(private authGuard: AuthGuard,
              private router: Router,
              private authService: AuthService) {
    this.jwtHelp = new JwtHelper();
  }

  /**
   * This guard depends on the result of AuthGuard
   * route.data['roles'] are roles that are allowed to access the route
   * route.data['roles'] is set in the routing configuration
   */
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.authGuard.canActivate(route, state).first()
      .map(authed => {
        if (!authed)
          return false;

        if (this.getUserRoles().some(r => route.data['roles'].indexOf(r) > -1)) {
          return true;
        } else {
          // Maybe show message & redirect if unauthorized
          this.router.navigate(['dashboard', 'unauthorized']);
          return false
        }
      });
  }

  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.canActivateChild(childRoute, state);
  }

  private getUserRoles(): string[] {
    let decoded = this.jwtHelp.decodeToken(this.authService.token.access_token);
    if (decoded != null)
      return decoded['authorities'];
    return null;
  }
}
