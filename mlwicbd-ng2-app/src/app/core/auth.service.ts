import {Injectable} from "@angular/core";
import {TokenDto} from "./oauth2/token.dto";
import {Observable, BehaviorSubject, Subject} from "rxjs";
import {TokenUtilsService} from "./oauth2/token.utils";
import {DecodedJwtDto} from "../common/dto/decoded-jwt.dto";
import {Response} from "@angular/http";

/**
 * Created by jdolanski on 10.01.17..
 */
@Injectable()
export class AuthService {
  private authenticatedSource = new BehaviorSubject<boolean>(true);

  constructor(private tokenUtil: TokenUtilsService) {

  }

  public login(username: string, password: string, rememberMe?: boolean): Observable<TokenDto> {
    return this.tokenUtil.requestAccessToken(username, password, rememberMe)
      .do(
        (token: TokenDto) => console.log(token),
        error => Observable.throw(error),
        () => this.authenticatedSource.next(true)
      );
  }

  public unauthenticate(): void {
    this.authenticatedSource.next(false);
    this.tokenUtil.clear();
  }

  public refreshToken(): Observable<TokenDto> {
    return this.tokenUtil.requestRefreshToken()
      .do(
        (token: TokenDto) => console.log(token),
        error => Observable.throw(error),
        () => this.authenticatedSource.next(true)
      );
  }

  public get token(): TokenDto {
    return this.tokenUtil.getTokenDto();
  }

  public get authState(): Observable<boolean> {
    return this.authenticatedSource.asObservable();
  }

  public get loginConfirmer(): Observable<boolean> {
    return this.authenticatedSource.asObservable().filter(auth => auth == true);
  }

  public get logoutConfirmer(): Observable<boolean> {
    return this.authenticatedSource.asObservable().filter(auth => auth == false);
  }
}
