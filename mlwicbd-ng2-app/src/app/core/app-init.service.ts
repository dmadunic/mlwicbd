import {Injectable} from "@angular/core";
import {AuthService} from "./auth.service";
import {TokenDto} from "./oauth2/token.dto";
import {environment} from "../../environments/environment";

/**
 * Created by jdolanski on 11.01.17..
 */
@Injectable()
export class AppInitService {
  private readonly max_waiting_time: number = 1000;

  constructor(private auth: AuthService) {

  }

  public init(): Promise<any> {
    console.log('Initializing app...');
    let token = this.auth.token;

    if (this.isTokenGoodEnough(token)) {
      return Promise.resolve();
    } else {
      // RememberMe is false if token was saved to session storage rather than local storage
      let rememberMe = JSON.parse(localStorage.getItem(environment.token_storage.remember_me)) as boolean;
      if (rememberMe === false) {
        // If rememberMe === false, check if a tab with user already logged in exists
        return this.syncSessionStorageBetweenTabs();
      } else {
        this.auth.unauthenticate();
        return Promise.resolve();
      }
    }
  }

  /**
   * This method waits until the token from session storage from active tab gets copied to new tab before it checks token.
   * If max_waiting_time has passed without the token getting copied to new tab, user gets logged out.
   * This happens if user (that has rememberMe === false) closed the last remaining tab while logged in.
   * @returns {Promise<TResult>}
   */
  private syncSessionStorageBetweenTabs(): Promise<any> {
    return new Promise(resolve => {
      let token: TokenDto = null;
      let eventAttached: boolean = false;

      let sessionSyncListener = (ev: StorageEvent) => {
        // Key used by local storage manager service to transfer values from one session storage to another
        if (ev.key === 'setSessionStorage') {
          token = JSON.parse(JSON.parse(ev.newValue)[environment.token_storage.jwt_object]);

          window.removeEventListener('storage', sessionSyncListener);
          eventAttached = false;

          resolve(token);
        }
      };

      window.addEventListener('storage', sessionSyncListener);
      eventAttached = true;

      setTimeout(() => {
        if (eventAttached) {
          console.log('Token doesnt exist or sync took too long');
          window.removeEventListener('storage', sessionSyncListener);
          eventAttached = false;
          this.auth.unauthenticate();
          resolve();
        }
      }, this.max_waiting_time);
    }).then((token: TokenDto) => {
      if (!this.isTokenGoodEnough(token)) {
        this.auth.unauthenticate();
      }

      return Promise.resolve();
    });
  }

  private isTokenGoodEnough(token: TokenDto) {
    return token != null && token.access_token != null && token.refresh_token != null;
  }
}
