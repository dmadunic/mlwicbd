export interface WizardStep {
    url: RegExp,
    step: number
}

export interface WizardStepMessage {
  [step:number] : string
}
