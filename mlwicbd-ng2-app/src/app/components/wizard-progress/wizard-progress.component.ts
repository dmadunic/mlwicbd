import {Component, Input, OnInit} from '@angular/core';
import {WizardStep, WizardStepMessage} from "./wizard-step";

@Component({
  selector: 'app-wizard-progress',
  templateUrl: './wizard-progress.component.html',
  styleUrls: ['./wizard-progress.component.css']
})
export class WizardProgressComponent implements OnInit {

  @Input() messages: WizardStepMessage;
  @Input() startingStep = 1;
  currentStep;
  maxSteps;
  currentStepMessage: string;

  stepKeys: any[];
  constructor() { }

  ngOnInit() {
    this.currentStep = this.startingStep;
    this.stepKeys = Object.keys(this.messages);
    this.maxSteps = Object.keys(this.messages).length + this.startingStep - 1;
    this.currentStepMessage = this.messages[this.startingStep];
  }

  public nextStep() {
    if (this.currentStep + 1 <= this.maxSteps) {
      this.currentStep++;
      this.currentStepMessage = this.messages[this.currentStep];
    }
  }

  public prevStep(){
    if (this.currentStep - 1 >= this.startingStep) {
      this.currentStep--;
      this.currentStepMessage = this.messages[this.currentStep];
    }
  }

  public step(index) {
    if (this.messages){
      this.currentStep = index;
      this.currentStepMessage = this.messages[this.currentStep];
    }
  }

}
