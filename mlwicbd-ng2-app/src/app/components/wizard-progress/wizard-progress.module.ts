import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {WizardProgressComponent} from "./wizard-progress.component";

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    WizardProgressComponent
  ],
  exports: [
    WizardProgressComponent
  ]
})
export class WizardProgressModule { }
