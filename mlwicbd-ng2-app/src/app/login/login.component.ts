import {Component, OnInit} from "@angular/core";
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import {Router, ActivatedRoute} from "@angular/router";
import {AuthService} from "../core/auth.service";
import {SlimLoadingBarService} from "ng2-slim-loading-bar";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public loginForm: FormGroup;
  public loginErrorResponse: {error: string, error_description: string};
  public returnUrl: string;

  constructor(private fb: FormBuilder,
              private auth: AuthService,
              private route: ActivatedRoute,
              private router: Router,
              private loadingBarService: SlimLoadingBarService
  ) {

  }

  ngOnInit() {
    // Reset login status
    this.auth.unauthenticate();
    // Get return url from route parameters or default to ''
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '';

    this.loginForm = this.fb.group({
      username: ['',
        Validators.compose([
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(32)
        ])
      ],
      password: ['',
        Validators.compose([
          Validators.required,
          Validators.minLength(5)
        ])
      ],
      rememberMe: [false]
    });
  }

  login(): void {
    this.loginErrorResponse = null;
    this.loadingBarService.start(() => console.log("Login successful"));
    this.auth.login(this.loginForm.value.username, this.loginForm.value.password, this.loginForm.value.rememberMe)
      .subscribe(
        () => this.router.navigate([this.returnUrl]),
        error => {
          this.loadingBarService.complete();
          this.parseError(error);
        }
      );
  }

  parseError(error: any) {
    if (error.status == 0){
      this.loginErrorResponse = {
        error: "ERR_CONNECTION_REFUSED",
        error_description: "Server is not available, please try again later"
      };
    }else {
      this.loginErrorResponse = JSON.parse(error['_body']);
    }
  }
}
