import {Component} from "@angular/core";
import * as md5 from "blueimp-md5";

@Component({
    selector: "gravatar",
    inputs: ["defaultStyle", "email", "size"],
    template: `
    <img class="my-gravatar"
        width="{{size || DEFAULT_SIZE}}" height="{{size || DEFAULT_SIZE}}"
        src='{{gravatarUrl(defaultStyle, email, size)}}'>`
})

export class GravatarComponent {
    public DEFAULT_SIZE: number = 80;
    public size: any;
    public email: any;
    public defaultStyle: any;

    public gravatarUrl(
            defaultStyle: string = "monsterid",
            email: string,
            size: number = this.DEFAULT_SIZE
        ) {
        //
        defaultStyle = encodeURIComponent(defaultStyle);
        return `https://www.gravatar.com/avatar/${md5(email.toLowerCase().trim())}?d=${defaultStyle}&s=${size}`.replace(/\s/g, "");
    }
}
