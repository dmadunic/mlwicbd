import {environment} from "../../environments/environment";

export const GlobalConstants = {
  APP_BASE: `${environment.app_base}`,
  API_ENDPOINT: `${environment.api.url}/${environment.api.version}`,
  API_TIMEOUT: 30000,
  DEFAULT_LANG: 'en',
  PAGE_SIZES: [
    {size: 15, selected: true},
    {size: 25, selected: false},
    {size: 50, selected: false},
    {size: 100, selected: false}
  ],
  CODES_MAX_PGSIZE: 100,
  DEFAULT_PAGE_SIZE : 15,
  TERRITORY_CODE_MILITARY_FRONTIER: 'CS-MIL-FR',
  UNIT_TYPE_REGIMENT: 'REGIMENT',
  YEAR_MONTHS: [
    {code: "JANUARY", value: 1}, 
    {code: "FEBRUARY", value: 2},
    {code: "MARCH", value: 3},
    {code: "APRIL", value: 4},
    {code: "MAY", value: 5},
    {code: "JUNE", value: 6},
    {code: "JULY", value: 7},
    {code: "AUGUST", value: 8},
    {code: "SEPTEMBER", value: 9},
    {code: "OCTOBER", value: 10},
    {code: "NOVEMBER", value: 11},
    {code: "DECEMBER", value: 12}
  ],
  MARTIAL_STATUS: [
    {id: null, code: "-"},
    {id: 1, code: "SINGLE"},
    {id: 2, code: "MARRIED"},
    {id: 3, code: "WIDOWED"}
  ],
  CHILDREN_GENDER_CODES: [
    "SON", "DAUGHTER"
  ]
};
