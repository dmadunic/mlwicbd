import { TimeoutError } from 'rxjs';
import { BaseError } from './dto/error-base.dto';
import { EntityConflictError } from './dto/error-entity-conflict.dto';
import { EntityError } from './dto/error-entity.dto';
import { FieldError } from './dto/error-field.dto';
import { MultipleErrorsResponse } from './dto/error-mulitple.dto';

/**
 * 
 * @param error
 * @author dmadunic 
 */
export function isGeneralServerError(error: any):boolean {
    if (error instanceof TimeoutError) {
        return true;
    } 
    //if (error instanceof Response) {
        if (error.status === 404 || error.status === 500) {
            return true;
        }
    //}
    return false;
}

/**
 * This function converts parsed JSON response which is simple Object into specific error classes:
 * <ul>
 *  <li>BaseError</li>
 *  <li>EntityError</li>
 *  <li>EntityConflictError</li>
 *  <li>MultipleErrorsResponse with an array of FieldError(s)</li>
 * </ul>
 * 
 * @param error (http error)
 * @author dmadunic 
 */
export function handleRestApiErrorResponse(errorResponse:any):BaseError {
    let serverErr:any = null;
    
    if (errorResponse['_body'] != null) {
        serverErr = JSON.parse(errorResponse['_body']);
    }

    // either url was not found or request was processed but entity was not found 
    if (errorResponse.status == 404) {
        let error:BaseError = null;
        if (serverErr != null) {
            error = new EntityError(serverErr.code, serverErr.message, serverErr.entityId);
        }
        return error;
    }
    
    // concurrency error ...
    if (errorResponse.status == 409) {
        let error:EntityConflictError = new EntityConflictError(serverErr.code, serverErr.message, serverErr.lastModifier, serverErr.currentVersion, serverErr.version);
        return error;
    } 
    
    // validation failed ...
    if (errorResponse.status == 422) {
        let error:MultipleErrorsResponse = new MultipleErrorsResponse(serverErr.code, serverErr.message);
        for (var fe of serverErr.errors) {
            var fieldError:FieldError = new FieldError(fe.code, fe.message, fe.field, fe.value);
            error.addError(fieldError);
        }
        return error;
    }
    
    return null;
}