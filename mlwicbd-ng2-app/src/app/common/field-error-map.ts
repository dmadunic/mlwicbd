import { FieldError } from "./dto/error-field.dto";

/**
 * 
 * 
 */
export class FieldErrorsMap {
    errorsMap: { [fieldName: string]: FieldError[] } = {};

    constructor(fieldErrors:FieldError[]) {
        for (let fe of fieldErrors) {
            let errors:FieldError[] = this.errorsMap[fe.field];
            if (errors == null) {
                errors = [];
            }
            errors.push(fe);
            this.errorsMap[fe.field] = errors;
        }
    }

    public get(fieldName:string):FieldError[] {
        return this.errorsMap[fieldName];
    }
    
}