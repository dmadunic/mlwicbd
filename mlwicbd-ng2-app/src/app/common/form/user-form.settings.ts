export class UserFormSettings{
  controlVisible: UserFormControlsVisibility = new UserFormControlsVisibility();
  controlDisabled: UserFormControlsDisabled = new UserFormControlsDisabled();
  submitBtnText: string = "Submit";
  submitBtnIcon: string = null;
  submitButtonDisabled: boolean = false;
}

export class UserFormControlsVisibility {
  username: boolean = true;
  firstName: boolean = true;
  lastName: boolean = true;
  address: boolean = true;
  postalCode: boolean = true;
  place: boolean = true;
  countryCode: boolean = true;
  email: boolean = true;
  mobile: boolean = true;
  phone: boolean = true;
  password: boolean = true;
}

export class UserFormControlsDisabled {
  username: boolean = false;
  firstName: boolean = false;
  lastName: boolean = false;
  address: boolean = false;
  postalCode: boolean = false;
  place: boolean = false;
  countryCode: boolean = false;
  email: boolean = false;
  mobile: boolean = false;
  phone: boolean = false;
  password: boolean = false;
}
