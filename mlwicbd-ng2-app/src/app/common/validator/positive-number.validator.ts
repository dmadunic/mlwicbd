import { FormControl } from '@angular/forms';

/**
 * Custom Validator that checks for non positive values ( < 0).
 * It allows for a value to be null. 
 * Validator will only check if control.value is positive Number in case if control.value is NOT null.
 * 
 * @param c 
 * @author: dmadunic
 */
export function positiveNumber(c: FormControl) {
  if (c.value == null) {
    return null;
  }

  if (Number(c.value) < 0) {
    return { positiveNumber: true };
  } else {
    return null;
  }
}