import {Observable} from 'rxjs';
import {FormGroup, FormBuilder} from "@angular/forms";
import {GlobalConstants} from "./global.constants";
import {PagedResponse} from "./paged-response";

/**
 * Abstract class representing angular2 component that consists of: 
 * 1) SearchForm, 
 * 2) PagedResponse<> and 
 * 3) Pagination bar
 * 
 * 
 * Created by dmadunic on 20.02.17.
 */
export abstract class PagedResultComponent<T> {
    pageNumber: number = 1;
    pageSize = GlobalConstants.DEFAULT_PAGE_SIZE;
    pageSizes = GlobalConstants.PAGE_SIZES;
    sortAttribute: string = '';

    searchForm: FormGroup;
    pagedResult: PagedResponse<T>;

    sortBy(sortAttribute) {
        if (this.sortAttribute.charAt(0) != '-') {
            if (this.sortAttribute == sortAttribute) {
                this.sortAttribute = '-' + sortAttribute;
            } else {
                this.sortAttribute = sortAttribute;
            }
        } else {
            this.sortAttribute = sortAttribute;
        }
        this.searchForm.patchValue({'sort': this.sortAttribute});
        this.search();
    }

    //--- pagination control functions ----------------------------------------

    changeSize(pageSize) {
        this.searchForm.patchValue({'size': pageSize});
        this.pageSize = pageSize;
        this.search();
    }

    firstRecord(): number {
        return (this.pagedResult.number * this.pagedResult.size) + 1;
    }

    lastRecord(): number {
        return (this.pagedResult.number * this.pagedResult.size) + this.pagedResult.numberOfElements;
    }

    //--- each subclass component needs to implement these methods ------------

    abstract search(page?: number): void;

}