import { Injectable } from "@angular/core";
import { Response, URLSearchParams, RequestOptions, Headers } from "@angular/http";
import { FormGroup } from "@angular/forms";
import { Observable } from "rxjs";
import { Person } from "../../common/dto/person.dto";
import { PagedResponse } from "../../common/paged-response";
import { HttpWrapperService } from "../../http-wrapper.service";
import { GlobalConstants } from "../../common/global.constants";
import * as _ from "lodash";

@Injectable()
export class PersonService {

  constructor(private http: HttpWrapperService) { }

  public get(id: number): Observable<Person> {
    let url = `${GlobalConstants.API_ENDPOINT}/person/${id}`;

    return this.http
      .authGet(url)
      .map((res: Response) => res.json())
      .catch((error:any) => Observable.throw(error || 'Unknown Server error'));
  }

  public search(params): Observable<PagedResponse<Person>> {
    let url = `${GlobalConstants.API_ENDPOINT}/person`;

    let searchParams = new URLSearchParams();

    if (!_.isEmpty(params.firstName)) {
        searchParams.set('firstName', params.firstName);
    }
    if (!_.isEmpty(params.lastName)) {
        searchParams.set('lastName', params.lastName);
    }
    if (!_.isEmpty(params.placeOfBirth)) {
        searchParams.set('placeOfBirth', params.placeOfBirth);
    }

    searchParams.set('page', params.page);
    searchParams.set('size', params.size);
    searchParams.set('sort', params.sort);

    let options = new RequestOptions({
      search: searchParams
    });

    return this.http
      .authGet(url, options)
      .map((r: Response) => r.json() as PagedResponse<Person>)
      .catch((error:any) => Observable.throw(error || 'Unknown Server error'));
  }

  public save(person:any): Observable<Person> {
    if (person.id == null) {
      // save NEW record ...
      return this.create(person);
    } else {
      // update EXISTING record ...
      return this.update(person);
    }
  }

  public create(person:any): Observable<Person> {
    let url = `${GlobalConstants.API_ENDPOINT}/person/`;
    let options = new RequestOptions({
      headers: new Headers({
        'Content-Type': 'application/json'
      })
    });

    return this.http.authPost(url, person, options)
        .map((r: Response) => r.json())
        .catch((error: any) => Observable.throw(error || 'Server error'));
  }

  public update(person:any): Observable<Person> {
    let url = `${GlobalConstants.API_ENDPOINT}/person/${person.id}`;
    let options = new RequestOptions({
      headers: new Headers({
        'Content-Type': 'application/json'
      })
    });

    return this.http.authPut(url, person, options)
      .map((r: Response) => r.json())
      .catch((error: any) => Observable.throw(error || 'Server error'));
  }

  public mergePersons(sourceId:number,targetId:number): Observable<boolean> {
    let url = `${GlobalConstants.API_ENDPOINT}/person/${sourceId}/${targetId}`;
    return this.http.authGet(url)
      .map((r: Response) => r.ok)
      .catch((error: any) => Observable.throw(error || 'Server error'));
  }


}
