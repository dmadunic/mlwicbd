import {Injectable} from "@angular/core";
import {Response, URLSearchParams, RequestOptions, Headers} from "@angular/http";
import {Observable} from "rxjs";
import {HttpWrapperService} from "../../http-wrapper.service";
import {PagedResponse} from "../paged-response";
import {MilitaryUnit} from "../dto/military-unit.dto";
import {GlobalConstants} from "../global.constants";
import * as _ from "lodash";

@Injectable()
export class MilitaryUnitService {

  constructor(private http: HttpWrapperService) { }

  public none(territoryCode:string):MilitaryUnit {
    var unit = new MilitaryUnit();
    unit.id = null;
    unit.name = "-";
    unit.description = 'None selected';
    unit.unitType = null;
    unit.territoryCode=territoryCode;
    unit.parentUnitId = null;
    unit.parentUnitName=null;
    return unit;
  }

  public getRegimentsForTerritory(territoryCode: string): Observable<PagedResponse<MilitaryUnit>> {
      var params:any = {};
      params.territory = territoryCode;
      params.unitType = GlobalConstants.UNIT_TYPE_REGIMENT;
      return this.search(params);
  }

  public fetchUnitsForTerritory(territoryCode: string, pageSize?: number): Observable<PagedResponse<MilitaryUnit>> {
    var params:any = {};
    params.territory = territoryCode;
    if (pageSize != null) {
      params.pgsize = pageSize;
    }
    params.sort='name,unitType';
    return this.search(params);

  }
  /**
   * Generic search method for MilitaryUnits.
   */
  public search(params): Observable<PagedResponse<MilitaryUnit>> {
    let url = `${GlobalConstants.API_ENDPOINT}/militaryunits`;

    let searchParams = new URLSearchParams();
    if (!_.isEmpty(params.referenceCode)) {
        searchParams.set('referenceCode', params.referenceCode);
    }
    
    searchParams.set('unitType', params.unitType);
    searchParams.set('territory', params.territory);
    searchParams.set('searchParam', params.searchParam);
    searchParams.set('parentUnitId', params.parentUnitId);
    searchParams.set('page', params.page);
    searchParams.set('size', params.pgsize);
    searchParams.set('sort', params.sort);

    let options = new RequestOptions({
      search: searchParams
    });

    return this.http
      .authGet(url, options)
      .map((r: Response) => r.json() as PagedResponse<MilitaryUnit>)
      .catch((error:any) => Observable.throw(error || 'Unknown Server error'));
  }

}  