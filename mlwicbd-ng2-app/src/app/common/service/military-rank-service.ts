import {Injectable} from "@angular/core";
import {Response, URLSearchParams, RequestOptions, Headers} from "@angular/http";
import {Observable} from "rxjs";
import {HttpWrapperService} from "../../http-wrapper.service";
import {PagedResponse} from "../paged-response";
import {MilitaryRank} from "../dto/military-rank.dto";
import {GlobalConstants} from "../global.constants";
import * as _ from "lodash";

@Injectable()
export class MilitaryRankService {

  constructor(private http: HttpWrapperService) { }

  public none():MilitaryRank {
    var mr = new MilitaryRank();
    mr.id = null;
    mr.code = "-";
    return mr;
  }

  public getRanksForTerritory(territoryCode: string, pageSize?: number): Observable<PagedResponse<MilitaryRank>> {
      var params:any = {};
      params.territoryCode = territoryCode;
      if (pageSize != null) {
          params.pgsize = pageSize;
      }
      return this.search(params);
  }

  /**
   * Search method for MilitaryRanks.
   */
  public search(params): Observable<PagedResponse<MilitaryRank>> {
    let url = `${GlobalConstants.API_ENDPOINT}/militaryranks`;

    let searchParams = new URLSearchParams();
    if (!_.isEmpty(params.referenceCode)) {
        searchParams.set('referenceCode', params.referenceCode);
    }
    
    searchParams.set('rankType', params.rankType);
    searchParams.set('territoryCode', params.territoryCode);
    searchParams.set('page', params.page);
    searchParams.set('size', params.pgsize);
    searchParams.set('sort', params.sort);

    let options = new RequestOptions({
      search: searchParams
    });

    return this.http
      .authGet(url, options)
      .map((r: Response) => r.json() as PagedResponse<MilitaryRank>)
      .catch((error:any) => Observable.throw(error || 'Unknown Server error'));
  }
}