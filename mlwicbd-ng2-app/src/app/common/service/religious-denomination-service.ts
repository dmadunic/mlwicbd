import {Injectable} from "@angular/core";
import {Response, URLSearchParams, RequestOptions, Headers} from "@angular/http";
import {Observable} from "rxjs";
import {HttpWrapperService} from "../../http-wrapper.service";
import {PagedResponse} from "../paged-response";
import {ReligiousDenomination} from "../dto/religious-denomination.dto";
import {GlobalConstants} from "../global.constants";
import * as _ from "lodash";

@Injectable()
export class ReligiousDenominationService {

    constructor(private http: HttpWrapperService) {
        //empty constructor
     }

     public none():ReligiousDenomination {
        var rel = new ReligiousDenomination();
        rel.id = null;
        rel.code = "-";
        return rel;
     }

    public fetchAll(): Observable<PagedResponse<ReligiousDenomination>> {
        return this.fetch(null);
    }

    public fetch(params): Observable<PagedResponse<ReligiousDenomination>> {
        let url = `${GlobalConstants.API_ENDPOINT}/denominations`;

        let searchParams = new URLSearchParams();
        if (params != null) {
            searchParams.set('page', params.page);
            searchParams.set('size', params.pgsize);
            searchParams.set('sort', params.sort);
        }

        let options = new RequestOptions({
            search: searchParams
        });

        return this.http
            .authGet(url, options)
            .map((r: Response) => r.json() as PagedResponse<ReligiousDenomination>)
            .catch((error:any) => Observable.throw(error || 'Unknown Server error'));
    }

}    