import {Injectable} from "@angular/core";
import {Response} from "@angular/http";
import {Observable} from "rxjs";
import {HttpWrapperService} from "../../http-wrapper.service";
import {GlobalConstants} from "../global.constants";
import * as _ from "lodash";

@Injectable()
export class AppConfigService {

  constructor(private http: HttpWrapperService) {
    //empty constructor
  }


  public fetch(): Observable<any> {
    let url = `${GlobalConstants.API_ENDPOINT}/configs/build`;


    return this.http.get(url)
      .map((r: Response) => r.json())
      .catch((error:any) => Observable.throw(error || 'Unknown Server error'));
  }

}
