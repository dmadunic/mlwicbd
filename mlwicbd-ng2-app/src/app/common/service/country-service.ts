import {Injectable} from "@angular/core";
import {Response, URLSearchParams, RequestOptions, Headers} from "@angular/http";
import {Observable} from "rxjs";
import {HttpWrapperService} from "../../http-wrapper.service";
import {PagedResponse} from "../paged-response";
import {Country} from "../dto/country.dto";
import {GlobalConstants} from "../global.constants";
import * as _ from "lodash";

@Injectable()
export class CountryService {

    constructor(private http: HttpWrapperService) {
        //empty constructor
     }

     public none():Country {
        var country = new Country();
        country.id = null;
        country.name = "-";
        return country;
     }

    public fetchAll(): Observable<PagedResponse<Country>> {
        return this.fetch(null);
    }

    public fetch(params): Observable<PagedResponse<Country>> {
        let url = `${GlobalConstants.API_ENDPOINT}/countries`;

        let searchParams = new URLSearchParams();
        if (params != null) {
            searchParams.set('page', params.page);
            searchParams.set('size', params.pgsize);
            searchParams.set('sort', params.sort);
        }

        let options = new RequestOptions({
            search: searchParams
        });

        return this.http.get(url, options)
            .map((r: Response) => r.json() as PagedResponse<Country>)
            .catch((error:any) => Observable.throw(error || 'Unknown Server error'));
    }

}    