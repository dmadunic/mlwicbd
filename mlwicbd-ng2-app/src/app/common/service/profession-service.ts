import {Injectable} from "@angular/core";
import {URLSearchParams, RequestOptions, Headers, Response} from "@angular/http";
import {Observable} from "rxjs";
import {HttpWrapperService} from "../../http-wrapper.service";
import {PagedResponse} from "../paged-response";
import {Profession} from "../dto/profession.dto";
import {GlobalConstants} from "../global.constants";
import * as _ from "lodash";

@Injectable()
export class ProfessionService {

  constructor(private http: HttpWrapperService) {
    //empty constructor
  }

  public none(): Profession {
    var profession = new Profession();
    profession.id = null;
    profession.code = "-";
    return profession;
  }

  public fetchAll(): Observable<PagedResponse<Profession>> {
    return this.fetch(null);
  }

  public fetch(params): Observable<PagedResponse<Profession>> {
    let url = `${GlobalConstants.API_ENDPOINT}/professions`;

    let searchParams = new URLSearchParams();
    if (params != null) {
      searchParams.set('page', params.page);
      searchParams.set('size', params.size);
      searchParams.set('sort', params.sort);

      if (!_.isEmpty(params.name)) {
        searchParams.set('name', params.name);
      }
      if (!_.isEmpty(params.code)) {
        searchParams.set('code', params.code);
      }
    }


    let options = new RequestOptions({
      search: searchParams
    });

    return this.http
      .authGet(url, options)
      .map((r: Response) => r.json() as PagedResponse<Profession>)
      .catch((error: any) => Observable.throw(error || 'Unknown Server error'));
  }


  public
  search(params): Observable<PagedResponse<Profession>> {

    return this.fetch(params);
  }

  public save(params): Observable<Profession> {
    if (_.isEmpty(params.id)
    ) {
      return this.create(params)
    }

    return this.update(params);
  }

  private
  create(params): Observable<Profession> {
    let url = `${GlobalConstants.API_ENDPOINT}/professions`;
    let options = new RequestOptions({
      headers: new Headers({
        'Content-Type': 'application/json'
      })
    });

    return this.http.authPost(url, params, options)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error || "Server error"));
  }

  private update(params): Observable<Profession> {
    let url = `${GlobalConstants.API_ENDPOINT}/professions/${params.id}`;
    let options = new RequestOptions({
      headers: new Headers({
        'Content-Type': 'application/json'
      })
    });

    return this.http.authPut(url, params, options)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error || "Server error"))
  }

}

