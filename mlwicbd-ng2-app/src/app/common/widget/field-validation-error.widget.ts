
import { Component, Input } from '@angular/core';
import { FieldError } from "../dto/error-field.dto";

@Component({
  selector: 'field-validation-error',
  templateUrl: './field-validation-error.widget.html'
})
export class FieldValidationErrorWidget {

    @Input() fieldErrors: FieldError[];

}