/**
 * Created by jdolanski on 27.01.17..
 */
export class BaseAuditableDto {
  createdAt: Date;
  modifiedAt: Date;
  createdBy: string;
  modifiedBy: string;
}