import {BaseAuditableDto} from "./base-auditable.dto";
/**
 *
 * Created by dmadunic on 18.02.17.
 */
export class Profession extends BaseAuditableDto{
  id: number;
  name: string;
  code: string;
  version: number;
}
