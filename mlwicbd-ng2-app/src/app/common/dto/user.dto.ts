import { Role } from "./role.dto";
/**
 *
 * Created by dmadunic on 05.04.2017.
 */
export class User {
    id: number;
    username: string;
    firstName: string;
    lastName: string;
    email: string;
    address: string;
    postalCode: string;
    place: string;
    countryCode: string;
    countryName: string;
    phone: string;
    mobile: string;
    enabled: boolean;
    roles: Role[];

}
