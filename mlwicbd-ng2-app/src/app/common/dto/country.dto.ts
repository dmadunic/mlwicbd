/**
 * 
 * Created by dmadunic on 05.04.17.
 */
export class Country {
  id: number;
  name: string;
  code: string;
  a3code: string;
  a2code: string;
  active:boolean;

}