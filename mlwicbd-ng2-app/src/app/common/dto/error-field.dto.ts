import { BaseError } from "./error-base.dto";

/**
 * 
 */
export class FieldError extends BaseError {
    field:string;
    value:string;

    constructor(code:string, message:string, field:string, value?:string) {
        super(code, message);
        this.field = field;
        if (value != null) {
            this.value = value;
        }
    }
}