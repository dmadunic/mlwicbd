/**
 * 
 * Created by dmadunic on 20.02.17.
 */
export class ReligiousDenomination {
  id: number;
  code: string;
  name: string;

}