/**
 * 
 * Created by dmadunic on 20.02.17.
 */
export class ServiceRecord {
  id: number;
  description: string;
  durationYears: number;
  durationMonths: number;
  durationDays: number;
  
}