/**
 * Created by jdolanski on 27.12.16..
 */
export class DecodedJwtDto {
  exp: number;
  user_name: string;
  authorities: string [];
  jti: string;
  client_id: string;
  scope: string [];
  fnm: string;
  lnm: string;
}
