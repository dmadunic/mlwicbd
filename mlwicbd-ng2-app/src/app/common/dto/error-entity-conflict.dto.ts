import { BaseError } from "./error-base.dto";

/**
 * 
 */
export class EntityConflictError extends BaseError {
    version:number;
    currentVersion:number;
    lastModifier:string;

    constructor(code:string, message:string, lastModifier:string, currentVersion:number, version:number) {
        super(code, message);
        this.lastModifier = lastModifier;
        this.currentVersion = currentVersion;
        this.version = version;
    }

}