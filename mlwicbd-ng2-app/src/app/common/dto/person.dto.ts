/**
 *
 * Created by dmadunic on 20.02.17.
 */
export class Person {
  id: number;
  version: number;
  firstName: string;
  lastName: string;
  sourcesFirstNames: string;
  sourcesLastNames: string;
  placeOfBirth: string;
  note: string;
  firstMusterList :{
    id : number,
    mlYear: number,
    mlMonth: number
  };
  lastMusterList: {
    id: number,
    mlYear: number,
    mlMonth: number
  };
}
