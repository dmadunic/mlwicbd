import { MilitaryRank } from "./military-rank.dto";
import { Person } from "./person.dto";
import { ReligiousDenomination } from "./religious-denomination.dto";
import { Profession } from "./profession.dto";
import { Child } from "./child.dto";
import { BaseAuditableDto } from './base-auditable.dto';
import { ServiceRecord } from "./service-record.dto";

/**
 * 
 * Created by dmadunic on 18.02.2017.
 */
export class MusterListEntry extends BaseAuditableDto {
    id: number = null;
    version: number = 0;
    listNumber: number;
    rank: MilitaryRank;
    person: Person;
    firstName: string;
    lastName: string;
    placeOfBirth: string;
    location: string;
    houseNumber: string;
    age: number;
    religion: ReligiousDenomination;
    maritalStatus: number;
    wifePresent: boolean;
    profession: Profession;
    sourceProfession: string;
    unit1: number;
    unit2: number;
    unit3: number;
    children: Child[];
    serviceRecords: ServiceRecord[];
    totalYearsServed: number;
    totalMonthsServed: number;
    musterListId: number;

    constructor() {
        // empty
        super();
    }

}