/**
 * 
 * Created by dmadunic on 18.02.17.
 */
export class MilitaryUnit {
  id: number;
  name: string;
  description: string;
  unitType: string;
  territoryCode: string;
  parentUnitId: number;
  parentUnitName: string;

}