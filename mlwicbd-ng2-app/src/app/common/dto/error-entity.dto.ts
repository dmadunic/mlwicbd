import { BaseError } from "./error-base.dto";

/**
 * 
 */
export class EntityError extends BaseError {
    entityId:string;

    constructor(code:string, message:string, entityId:string) {
        super(code, message);
        this.entityId = entityId;
    }
}