/**
 * 
 * Created by dmadunic on 20.02.17.
 */
export class Child {
  id: number;
  name: string;
  age: number;
  gender: string;

}