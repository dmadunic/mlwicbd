
/**
 * Base server side error.
 * 
 */
export class BaseError {
    code:string;
    message:string;

    constructor(code:string, message:string) {
        this.code = code;
        this.message = message;
    }
}
