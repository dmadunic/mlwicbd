import { BaseError } from "./error-base.dto";
import { FieldError } from "./error-field.dto";

/**
 * 
 */
export class MultipleErrorsResponse extends BaseError {
    errors:FieldError[];

    public addError(error:FieldError):void {
        if (this.errors == null) {
            this.errors = [];
        }
        this.errors.push(error);
    }
    
}