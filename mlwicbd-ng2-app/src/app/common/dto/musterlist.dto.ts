import {BaseAuditableDto} from "./base-auditable.dto";
/**
 * 
 * Created by dmadunic on 15.02.17.
 */
export class MusterList extends BaseAuditableDto {
  id: number;
  version: number;
  title: string;
  referenceCode: string;
  year: number;
  month: number;
  unitId: number;
  unitName: string;
  unitType: string;
  parentUnitId: number;
  parentUnitName: string;
  parentUnitType: string;
  entriesCount: number;
  
}