import {Injectable} from "@angular/core";
import {Http, RequestOptionsArgs, Response, RequestOptions, RequestMethod} from "@angular/http";
import {AuthHttp, AuthHttpError} from "angular2-jwt";
import {AuthService} from "./core/auth.service";
import {Observable, TimeoutError} from "rxjs";
import 'rxjs/add/operator/timeout';
import { TranslateService } from '@ngx-translate/core';
import { ToastyService } from "ng2-toasty";
import {Router} from "@angular/router";
import {GlobalConstants} from "./common/global.constants";
import {SlimLoadingBarService} from "ng2-slim-loading-bar";

/**
 * Created by jdolanski on 12.01.17..
 */
@Injectable()
export class HttpWrapperService {
  constructor(private http: Http,
              private authHttp: AuthHttp,
              private authService: AuthService,
              private toastyService: ToastyService,
              private translateService: TranslateService,
              private router: Router,
              private loader: SlimLoadingBarService,) {
    this.loader.color = '#36B1BF';
  }

  // ---------- Regular requests ----------

  public get(url: string, options?: RequestOptionsArgs): Observable<Response> {
    let method: RequestOptions = new RequestOptions({method: RequestMethod.Get});
    return this.request(url, method.merge(options));
  }

  public post(url: string, body: any, options?: RequestOptionsArgs): Observable<Response> {
    let method: RequestOptions = new RequestOptions({method: RequestMethod.Post});
    return this.request(url, method.merge(options), body);
  }

  public put(url: string, body: any, options?: RequestOptionsArgs): Observable<Response> {
    let method: RequestOptions = new RequestOptions({method: RequestMethod.Put});
    return this.request(url, method.merge(options), body);
  }

  public delete(url: string, options?: RequestOptionsArgs): Observable<Response> {
    let method: RequestOptions = new RequestOptions({method: RequestMethod.Delete});
    return this.request(url, method.merge(options));
  }

  // ---------- Requests with JWT in header ----------

  public authGet(url: string, options?: RequestOptionsArgs): Observable<Response> {
    let method: RequestOptions = new RequestOptions({method: RequestMethod.Get});
    return this.authRequest(url, method.merge(options));
  }

  public authPost(url: string, body: any, options?: RequestOptionsArgs): Observable<Response> {
    let method: RequestOptions = new RequestOptions({method: RequestMethod.Post});
    return this.authRequest(url, method.merge(options), body);
  }

  public authPut(url: string, body: any, options?: RequestOptionsArgs): Observable<Response> {
    let method: RequestOptions = new RequestOptions({method: RequestMethod.Put});
    return this.authRequest(url, method.merge(options), body);
  }

  public authDelete(url: string, options?: RequestOptionsArgs): Observable<Response> {
    let method: RequestOptions = new RequestOptions({method: RequestMethod.Delete});
    return this.authRequest(url, method.merge(options));
  }


  /*
   All regular requests should call this function
   */
  private request(url: string, options: RequestOptionsArgs, data?: Object): Observable<Response> {
    this.loader.start();

    if (data != null) {
      options.body = JSON.stringify(data);
    }

    return this.http.request(url, options)
      .timeout(GlobalConstants.API_TIMEOUT)
      .do(
        () => {/* Nothing yet */},
        error => this.loader.complete(),
        () => this.loader.complete()
      )
      .catch(error => {
        console.log("FAILED http authRequest: " + error.constructor.name + "/" + error.status);
        if (error instanceof TimeoutError) {
          this.translatedToastyMessage('error.server.timeout');
        } else if (error.status === 404) {
          this.translatedToastyMessage('error.server.err_404');
        } else if (error.status === 500) {
          this.translatedToastyMessage('error.server.err_500');
        }
        return Observable.throw(error);
      }
      );
  }

  /**
   * All requests with JWT should call this function
   * @param retry Flag for avoiding infinite recursion. Defaults to true
   */
  private authRequest(url: string, options: RequestOptionsArgs, data?: Object, retry = true): Observable<Response> {
    this.loader.start();

    if (data != null) {
      options.body = JSON.stringify(data);
    }

    if (this.authService.token == null) {
      this.redirectToLoginPage();
      return Observable.throw('No token in storage!');
    } else {
      return this.authHttp.request(url, options)
        .timeout(GlobalConstants.API_TIMEOUT)
        .do(
          () => {/* Nothing yet */},
          error => this.loader.complete(),
          () => this.loader.complete()
        )
        .catch(error => {
          /*
           Angular2-JWT automatically checks for token validity on each request.
           AuthHttpError gets thrown if token is invalid, null, or expired.
           Http status 401 gets returned if token was valid locally but not on the back-end.
           */
          if (error instanceof AuthHttpError || error.status === 401) {
            return this.authService.refreshToken()
              .flatMap(() => {
                if (retry != null && retry === false) {
                  return Observable.empty();
                }
                // Retry the original request once
                return this.authRequest(url, options, data, false);
              })
              .catch(error => {
                console.log("Error while retrying an URL after token refresh: " + error);
                if (error.status == 401) {
                  this.redirectToLoginPage();
                }
                return Observable.throw(error);
              });
          } else {
            // Something other than invalid/non-existing access token
            console.log("FAILED http authRequest: " + error + "/" + error.status);
            if (error instanceof TimeoutError) {
              this.translatedToastyMessage('error.server.timeout');
            } else if (error.status === 404) {
              this.translatedToastyMessage('error.server.err_404');
            } else if (error.status === 500) {
              this.translatedToastyMessage('error.server.err_500');
            }
            return Observable.throw(error);
          }
        });
    }
  }

  //--- util methods ----------------------------------------------------------

  private translatedToastyMessage(message: string): void {
    this.translateService.get(message).subscribe((translatedMsg: string) => {
      this.toastyService.error(translatedMsg);
    });
  }

  // In cases token is missing during AuthHTTP request
  private redirectToLoginPage(): void {
    console.log('---> TTPWrapper redirecting to /login');
    this.router.navigate(['/login']);
  }
}
