import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "../../shared/shared.module";
import { MfRoutingModule } from "./military-frontier-routing.module";
import { MusterListService } from "./musterlists/musterlist.service";
import { MusterListEntryService } from "./mlentry/musterlist-entry.service";
import { MilitaryUnitService } from "../../common/service/military-unit-service";
import { MilitaryRankService } from "../../common/service/military-rank-service";
import { ReligiousDenominationService } from "../../common/service/religious-denomination-service";
import { ProfessionService } from "../../common/service/profession-service";
import { PersonService } from "../../common/service/person.service";
import { MfMusterListsSearchComponent } from "./musterlists/ml-search.component";
import { MfMusterListDetailsComponent } from "./musterlists/ml-details.component";
import { MfMusterListFormComponent } from './musterlists/ml-form-component';
import { MfMlEntryFormComponent } from "./mlentry/ml-entry-form.component";
import { ChildFormModal } from "./mlentry/child-form.modal";
import { ServiceRecordFormModal } from "./mlentry/servicerecord-form.modal";
import { MfMonathTableSearchComponent } from './monath-table/mt-search.component';
import { MfParishRegistersSearchComponent } from './parish-registers/pr-search.component';
import { MfStandesTableSearchComponent } from './standes-table/st-search';
import {NgxPaginationModule} from "ngx-pagination";
import {ModalModule} from "ngx-bootstrap";
import {MfMleCopyWizardWrapperComponent} from "./mle-copy-wizard/mle-copy-wizard-wrapper.component";
import {WizardProgressModule} from "../../components/wizard-progress/wizard-progress.module";

/**
 * Definition of MilitaryFrontier module.
 *
 */
@NgModule({
  imports: [
    ReactiveFormsModule,
    NgxPaginationModule,
    SharedModule,
    ModalModule.forRoot(),
    MfRoutingModule,
    WizardProgressModule
  ],
  declarations: [
    MfMusterListsSearchComponent,
    MfMusterListDetailsComponent,
    MfMlEntryFormComponent,
    MfMusterListFormComponent,
    ChildFormModal,
    ServiceRecordFormModal,
    MfMonathTableSearchComponent,
    MfParishRegistersSearchComponent,
    MfStandesTableSearchComponent,
    MfMleCopyWizardWrapperComponent
  ],
  providers: [
    MusterListService,
    MusterListEntryService,
    MilitaryUnitService,
    MilitaryRankService,
    ReligiousDenominationService,
    ProfessionService,
    PersonService
  ]
})
export class MilitaryFrontierModule { }
