import { Routes, RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { MfMusterListsSearchComponent } from "./musterlists/ml-search.component";
import { MfMusterListDetailsComponent } from "./musterlists/ml-details.component";
import { MfMusterListFormComponent } from './musterlists/ml-form-component';
import { MfMlEntryFormComponent } from "./mlentry/ml-entry-form.component";
import {MfMonathTableSearchComponent} from "./monath-table/mt-search.component";
import {MfParishRegistersSearchComponent} from "./parish-registers/pr-search.component";
import {MfStandesTableSearchComponent} from "./standes-table/st-search";
import {MfMleCopyWizardWrapperComponent} from "./mle-copy-wizard/mle-copy-wizard-wrapper.component";

/**
 * Routing definitions for MilitaryFrontier module.
 *
 * Created by dmadunic on 12.02.2017.
 */
const mfRoutes: Routes = [
  { path: 'musterlists', component: MfMusterListsSearchComponent },
  { path: 'musterlists/form', component: MfMusterListFormComponent },
  { path: 'musterlists/form/:id', component: MfMusterListFormComponent },
  { path: 'musterlists/:id', component: MfMusterListDetailsComponent },
  { path: 'musterlists/:id/entry-new', component: MfMlEntryFormComponent },
  { path: 'musterlists/:id/entry/:eid', component: MfMlEntryFormComponent },
  { path: 'monathtable', component: MfMonathTableSearchComponent},
  { path: 'partishregister', component: MfParishRegistersSearchComponent},
  { path: 'standestable', component: MfStandesTableSearchComponent},
  { path: 'copy', component: MfMleCopyWizardWrapperComponent, children: [
    { path:'musterlists', component: MfMusterListsSearchComponent },
    { path:'musterlists/:id/entry-new', component: MfMlEntryFormComponent}
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(mfRoutes)],
  exports: [RouterModule]
})
export class MfRoutingModule {

}
