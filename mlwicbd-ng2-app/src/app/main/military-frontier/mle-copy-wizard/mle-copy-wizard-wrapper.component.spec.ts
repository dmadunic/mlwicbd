import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MfMleCopyWizardWrapperComponent } from './mle-copy-wizard-wrapper.component';

describe('MfMleCopyWizardWrapperComponent', () => {
  let component: MfMleCopyWizardWrapperComponent;
  let fixture: ComponentFixture<MfMleCopyWizardWrapperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MfMleCopyWizardWrapperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MfMleCopyWizardWrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
