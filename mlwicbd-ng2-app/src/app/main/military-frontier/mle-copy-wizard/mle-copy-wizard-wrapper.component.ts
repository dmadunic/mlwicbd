import {Component, OnInit, ViewChild} from '@angular/core';
import {NavigationEnd, Router} from "@angular/router";
import {WizardStep, WizardStepMessage} from "../../../components/wizard-progress/wizard-step";
import {WizardProgressComponent} from "../../../components/wizard-progress/wizard-progress.component";

@Component({
  selector: 'app-mle-copy-wizard-wrapper',
  templateUrl: './mle-copy-wizard-wrapper.component.html',
  styleUrls: ['./mle-copy-wizard-wrapper.component.css']
})
export class MfMleCopyWizardWrapperComponent implements OnInit {
  @ViewChild(WizardProgressComponent) wizard;

  messages: WizardStepMessage;
  steps: WizardStep[] = new Array();

  constructor(private router: Router) {

    this.steps.push({
      url: new RegExp('^/main/military-frontier/copy/musterlists$'),
      step: 1
    });
    this.steps.push({
      url: new RegExp('^/main/military-frontier/copy/musterlists/[\\d]+/entry-new?.*$'),
      step: 2
    });

    this.messages = {
      1: "Select muster list into which you want to copy entry",
      2: "Save data"
    };

    this.router.events.subscribe(event => this.navigationInterceptor(event));
  }

  ngOnInit() {
  }

  private navigationInterceptor(event) {
    if (event instanceof NavigationEnd) {
      let index = this.steps.find((value, index, obj) => value.url.test(event.url));
      if (index != null)
        this.wizard.step(index.step);
    }
  }

}
