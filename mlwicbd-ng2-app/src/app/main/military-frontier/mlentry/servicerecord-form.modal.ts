import { Component, Output, EventEmitter, ViewChild } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { ServiceRecord } from "../../../common/dto/service-record.dto";
import { positiveNumber } from '../../../common/validator/positive-number.validator';
import {ModalDirective} from "ngx-bootstrap";

/**
 * Modal Dialog for entering service record data.
 *
 * @author dmadunic
 */
@Component({
  selector: 'mlentry-servicerecord-form',
  templateUrl: './servicerecord-form.modal.html'
})
export class ServiceRecordFormModal {

  @Output() recordEntered = new EventEmitter();
  @ViewChild('serviceRecordForm') public serviceRecordForm: ModalDirective;

  srform: FormGroup;
  serviceRecord:ServiceRecord;

  constructor(
      private fb: FormBuilder
    ) {
      //
  }

  public showModal(serviceRecord:ServiceRecord):void {
    this.serviceRecord = serviceRecord;
    this.buildForm(serviceRecord);
    this.serviceRecordForm.show();
  }

  public submit():void {
    // 1. copy form to serviceRecord ...
    this.serviceRecord.id = this.srform.value.id;
    this.serviceRecord.description = this.srform.value.description;
    this.serviceRecord.durationYears = this.srform.value.durationYears;
    this.serviceRecord.durationMonths = this.srform.value.durationMonths;
    this.serviceRecord.durationDays = this.srform.value.durationDays;

    this.recordEntered.emit(this.serviceRecord);
    this.serviceRecordForm.hide();
  }

  public hide():void {
    this.serviceRecordForm.hide();
  }

  //--- form functions ---------------------------

  buildForm(serviceRecord?: ServiceRecord):void {
      this.srform = this.fb.group({
        id: [ serviceRecord.id ],
        description: [ serviceRecord.description, [ Validators.required, Validators.maxLength(255) ]],
        durationYears: [serviceRecord.durationYears ? serviceRecord.durationYears : 0, [ Validators.required, positiveNumber ]],
        durationMonths: [serviceRecord.durationMonths ? serviceRecord.durationMonths : 0, [ Validators.required ]],
        durationDays: [serviceRecord.durationDays ? serviceRecord.durationDays : 0, [ Validators.required, Validators.maxLength(10) ]]
      });
  }

}
