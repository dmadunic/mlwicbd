import {Injectable} from "@angular/core";
import {Response, URLSearchParams, RequestOptions, Headers} from "@angular/http";
import {Observable} from "rxjs";
import {PagedResponse} from "../../../common/paged-response";
import {HttpWrapperService} from "../../../http-wrapper.service";
import {MusterListEntry} from "../../../common/dto/musterlist-entry.dto";
import {GlobalConstants} from "../../../common/global.constants";
import * as _ from "lodash";

/**
 * [MilitaryFrontier] MusterList Entry Service.
 *
 * Created by dmadunic on 25.02.2017.
 */
@Injectable()
export class MusterListEntryService {

  constructor(private http: HttpWrapperService) { }

/**
 * Fetches single musterListEntry from server.
 *
 * @param mlId
 * @param entryId
 */
  public get(mlId: number, entryId: number): Observable<MusterListEntry> {
    let url = `${GlobalConstants.API_ENDPOINT}/musterlists/${mlId}/entries/${entryId}`;

    return this.http
      .authGet(url)
      .map((res: Response) => res.json())
      .catch((error:any) => Observable.throw(error || 'Unknown Server error'));
  }

  /**
  *
  * @param params
  */
  public search(params): Observable<PagedResponse<MusterListEntry>> {
    let url = `${GlobalConstants.API_ENDPOINT}/musterlists/${params.id}/entries`;
    return this.searchInternal(params, url);
  }

  public searchEntries(params): Observable<PagedResponse<MusterListEntry>> {
    let url = `${GlobalConstants.API_ENDPOINT}/musterlists/entries`;
    return this.searchInternal(params, url);
  }

  public searchMergedEntries(params): Observable<PagedResponse<MusterListEntry>> {

    let url = `${GlobalConstants.API_ENDPOINT}/musterlists/entries/merged`;
    return this.searchInternal(params, url);
  }

  private searchInternal(params, url:string): Observable<PagedResponse<MusterListEntry>> {
    let searchParams = new URLSearchParams();

    if (!_.isEmpty(params.personId)) {
        searchParams.set('personId', params.personId);
    }
    //TODO: add other options ...
    if (!_.isEmpty(params.personId1)){
      searchParams.set('personId1', params.personId1);

    }
    if (!_.isEmpty(params.personId2)){
      searchParams.set('personId2', params.personId2);

    }

    searchParams.set('page', params.page);
    searchParams.set('size', params.size);
    searchParams.set('sort', params.sort);

    let options = new RequestOptions({
      search: searchParams
    });

    return this.http
      .authGet(url, options)
      .map((r: Response) => r.json() as PagedResponse<MusterListEntry>)
      .catch((error:any) => Observable.throw(error || 'Unknown Server error'));
  }

  /**
   *
   * @param mlId
   * @param mlEntry
   * @param id
   */
  public save(mlId: number, mlEntry, id?: number): Observable<MusterListEntry> {
    if (mlEntry.id == null) {
      // save NEW record ...
      return this.create(mlId, mlEntry);
    } else {
      // update EXISTING record ...
      return this.update(mlId, mlEntry, id);
    }

  }

  public create(mlId: number, mlEntry): Observable<MusterListEntry> {
    let url = `${GlobalConstants.API_ENDPOINT}/musterlists/${mlId}/entries`;
    let options = new RequestOptions({
      headers: new Headers({
        'Content-Type': 'application/json'
      })
    });

    return this.http.authPost(url, mlEntry, options)
        .map((r: Response) => r.json())
        .catch((error: any) => Observable.throw(error || 'Server error'));
  }

  public update(mlId: number, mlEntry, id: number): Observable<MusterListEntry> {
    let url = `${GlobalConstants.API_ENDPOINT}/musterlists/${mlId}/entries/${id}`;
    let options = new RequestOptions({
      headers: new Headers({
        'Content-Type': 'application/json'
      })
    });

    return this.http.authPut(url, mlEntry, options)
      .map((r: Response) => r.json())
      .catch((error: any) => Observable.throw(error || 'Server error'));
  }

  public delete(mlId: number, mleId: number): Observable<boolean> {
    let url = `${GlobalConstants.API_ENDPOINT}/musterlists/${mlId}/entries/${mleId}`;
    return this.http.authDelete(url)
      .map((r: Response) => r.ok)
      .catch((error: any) => Observable.throw(error || 'Server error'));
  }

}
