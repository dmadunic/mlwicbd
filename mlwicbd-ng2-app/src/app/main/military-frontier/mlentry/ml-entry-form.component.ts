import { Component, ViewChild, OnInit } from '@angular/core';
import { Observable, Subscription, TimeoutError } from 'rxjs';
import { GlobalConstants } from "../../../common/global.constants";
import { ActivatedRoute, Params, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { TranslateService } from '@ngx-translate/core';
import { ToastyService } from "ng2-toasty";
import { MusterList } from "../../../common/dto/musterlist.dto";
import { MusterListEntry } from "../../../common/dto/musterlist-entry.dto";
import { MilitaryRank } from "../../../common/dto/military-rank.dto";
import { ReligiousDenomination } from "../../../common/dto/religious-denomination.dto";
import { Profession } from "../../../common/dto/profession.dto";
import { Person } from "../../../common/dto/person.dto";
import { Child } from "../../../common/dto/child.dto";
import { ServiceRecord } from "../../../common/dto/service-record.dto";
import { BaseError } from "../../../common/dto/error-base.dto";
import { MultipleErrorsResponse } from "../../../common/dto/error-mulitple.dto";
import { FieldError } from "../../../common/dto/error-field.dto";
import { FieldErrorsMap } from "../../../common/field-error-map";
import { MusterListService } from "../musterlists/musterlist.service";
import { MusterListEntryService } from "./musterlist-entry.service";
import { MilitaryRankService } from "../../../common/service/military-rank-service";
import { ReligiousDenominationService } from "../../../common/service/religious-denomination-service";
import { PersonService } from "../../../common/service/person.service";
import { ProfessionService } from "../../../common/service/profession-service";
import { positiveNumber } from '../../../common/validator/positive-number.validator';
import { ChildFormModal } from './child-form.modal';
import { ServiceRecordFormModal } from './servicerecord-form.modal';
import { FieldValidationErrorWidget } from "../../../common/widget/field-validation-error.widget";
import * as errorUtils from '../../../common/http-error-utils';
import {LocalStorageService} from "angular-2-local-storage";

/**
 * Component for both insert/update of MusterListEntry.
 *
 * @author Domagoj Madunic
 */
@Component({
  selector: 'mf-mlentry-form',
  templateUrl: './ml-entry-form.component.html',
})
export class MfMlEntryFormComponent implements OnInit {

    @ViewChild(ChildFormModal) childForm;
    @ViewChild(ServiceRecordFormModal) serviceRecordForm;

    mlId: number;
    entryId: number;

    musterList: MusterList = null;
    mlEntry: MusterListEntry;

    militaryRanks: MilitaryRank[];
    denominations: ReligiousDenomination[];
    professions: Profession[];
    entryForm: FormGroup = null;
    linkedPerson: Person = null;
    maritalStatuses = GlobalConstants.MARTIAL_STATUS;

    serverErrors:FieldErrorsMap = null;
    errorAlertMessage:string = null;

    formStorageKey: string = "entryForm";

    // button controls
    saveDisabled: boolean = false;
    copy: boolean= false;

    constructor(
      private router: Router,
      private route: ActivatedRoute,
      private fb: FormBuilder,
      private musterListService: MusterListService,
      private musterListEntryService: MusterListEntryService,
      private militaryRankService: MilitaryRankService,
      private religiousDenominationService: ReligiousDenominationService,
      private professionService: ProfessionService,
      private personService :PersonService,
      private translateService: TranslateService,
      private toastyService: ToastyService,
      private localStorageService: LocalStorageService
    ) {
        //
    }

    ngOnInit() {
        this.mlId = this.route.snapshot.params['id'];
        this.entryId = this.route.snapshot.params['eid'];
        this.copy = this.route.snapshot.queryParams['c'];
        if (this.copy) {
          this.entryId = this.route.snapshot.queryParams['eid'];
        }
        let pid:number = this.route.snapshot.queryParams['pid'];
        let profid:number = this.route.snapshot.queryParams['profid'];

        Observable.forkJoin([
            this.musterListService.get(this.mlId),
            this.fetchMlEntry(this.mlId, this.entryId),
            this.militaryRankService.getRanksForTerritory(
                GlobalConstants.TERRITORY_CODE_MILITARY_FRONTIER,
                GlobalConstants.CODES_MAX_PGSIZE
            ),
            this.religiousDenominationService.fetchAll(),
            this.professionService.fetchAll(),
            this.fetchPerson(pid),
        ]).subscribe(
            aggregateData => {
                this.musterList = aggregateData[0];
                this.mlEntry = aggregateData[1];

                this.militaryRanks = aggregateData[2].content;
                this.militaryRanks.unshift(this.militaryRankService.none());

                this.denominations = aggregateData[3].content;
                this.denominations.unshift(this.religiousDenominationService.none());

                this.professions = aggregateData[4].content;
                this.professions.unshift(this.professionService.none());

                let selectedPerson:Person = aggregateData[5];
                if (selectedPerson.id != null) {
                  this.linkedPerson = selectedPerson;
                } else {
                  this.linkedPerson = this.mlEntry.person;
                }
                this.buildForm(this.mlEntry, this.linkedPerson);
                this.initView();
                if (profid) {
                  this.entryForm.patchValue({'professionId': profid});
                }
            }
        );
    }

    //--- button action functions ---------------------------------------------

  public registerNewProfession(): void {
      let qp = { mlid: this.mlId };
      if (this.mlEntry.id != null) {
        qp['eid'] = this.mlEntry.id;
      }

    this.localStorageService.set(this.formStorageKey, this.entryForm.value);
    this.localStorageService.set("children", this.mlEntry.children);
    this.localStorageService.set("serviceRecords", this.mlEntry.serviceRecords);
    this.router.navigate(['/main/professions/search'], { queryParams: qp });
  }

    public linkPerson():void {
      let qp = { mlid: this.mlId };
      if (this.mlEntry.id != null) {
        qp['eid'] = this.mlEntry.id;
      }

      this.localStorageService.set(this.formStorageKey, this.entryForm.value);
      this.localStorageService.set("children", this.mlEntry.children);
      this.localStorageService.set("serviceRecords", this.mlEntry.serviceRecords);
      this.router.navigate(['/main/persons/search'], { queryParams: qp });
    }

    // Save button action function
    public save():void {
      this.saveDisabled = true;

      // 1. prepare entry form for submission ...
      var entryForm:any = this.entryForm.value;
      if (!this.copy){
        entryForm.id = this.entryId;
      }
      entryForm.version = this.mlEntry.version;
      entryForm.children = this.mlEntry.children;
      entryForm.serviceRecords = this.mlEntry.serviceRecords;

      //2. save entry and redirect to details page
      this.musterListEntryService.save(this.mlId, entryForm, entryForm.id).subscribe(
            savedEntry => {
              this.saveDisabled = false;
              this.router.navigate(['/main/military-frontier/musterlists/', this.mlId]);
              this.translateService.get("mlEntry.saved.success").subscribe((translatedMsg: string) => {
                this.toastyService.success(translatedMsg);
              });
            },
            err => {
              console.log(err);
              this.saveDisabled = false;

              if (!errorUtils.isGeneralServerError(err)) {
                this.translateService.get("mlEntry.save.failed").subscribe((translatedMsg: string) => {
                  this.toastyService.warning(translatedMsg);
                });
                let error = errorUtils.handleRestApiErrorResponse(err);
                if (error instanceof MultipleErrorsResponse) {
                  // izvuci field errors ...
                  this.serverErrors = new FieldErrorsMap(error.errors);
                }
                this.errorAlertMessage = error.message;
              }
            }
        );
    }

    private fetchMlEntry(mlid:number, eid: number): Observable<MusterListEntry> {
        let result: Observable<MusterListEntry> = null;

        if (eid == null) {
          result = Observable.create( observer => {
            observer.next(this.defaultMusterListEntry(mlid));
            observer.complete();
          });
        } else {
          result = this.musterListEntryService.get(this.mlId, eid);
        }
        return result;
    }

    private fetchPerson(pid:number):Observable<Person> {
      let result: Observable<Person> = null;

      if (pid == null) {
        result = Observable.create( observer => {
          observer.next(new Person());
          observer.complete();
        });
      } else {
        result = this.personService.get(pid);
      }
      return result;
    }

    //--- child ---

    public showAddChildModal() {
      // mark it as new Child record
      let newChild:Child = new Child();
      newChild.id = -1;
      this.childForm.showModal(newChild);
    }

    public showEditChildModal(child: Child):void {
      this.childForm.showModal(child);
    }

    public processChildEntry(child:Child):void {
      if (child.id == -1) {
        if (this.mlEntry.children == null) {
            this.mlEntry.children = [];
        }
        console.log("Adding new child recrd to array");
        child.id = null;
        this.mlEntry.children.push(child);
      } else {
        console.log("Replacing child in array");
      }
    }

    public removeChildRecord(index:number):void {
      console.log("Removing child element at index=" + index);
      this.mlEntry.children.splice(index, 1);
    }

    //--- service record ---

    public showAddServiceRecordModal() {
      // mark it as new ServiceRecord record
      let newServiceRecord: ServiceRecord = new ServiceRecord();
      newServiceRecord.id = -1;
      this.serviceRecordForm.showModal(newServiceRecord);
    }

    public showEditServiceRecordModal(serviceRecord: ServiceRecord):void {
      this.serviceRecordForm.showModal(serviceRecord);
    }

    public processServiceRecord(serviceRecord:ServiceRecord):void {
      if (serviceRecord.id == -1) {
        if (this.mlEntry.serviceRecords == null) {
            this.mlEntry.serviceRecords = [];
        }
        console.log("Adding new serviceRecord to array");
        serviceRecord.id = null;
        this.mlEntry.serviceRecords.push(serviceRecord);
      } else {
        console.log("Replacing serviceRecord in array");
      }
    }

    public removeServiceRecord(index:number):void {
      console.log("Removing serviceRecord element at index=" + index);
      this.mlEntry.serviceRecords.splice(index, 1);
    }

    //--- form validation -----------------------------------------------------

    buildForm(entry: MusterListEntry, linkedPerson:Person):void {
      this.entryForm = this.fb.group({
        musterListId: [this.mlId],
        listNumber: [ entry.listNumber, [ Validators.required, positiveNumber ]],
        personId: [linkedPerson ? linkedPerson.id : null, []],
        firstName: [entry.firstName, [
          Validators.required,
          Validators.maxLength(40)
        ]],
        lastName: [entry.lastName, [
          Validators.required,
          Validators.maxLength(40)
        ]],
        placeOfBirth: [entry.placeOfBirth, [
          Validators.required,
          Validators.maxLength(80)
        ]],
        rankId: [entry.rank!.id, []],
        location: [entry.location],
        religionId: [entry.religion ? entry.religion.id : null, [ Validators.required ]],
        sourceProfession: [entry.sourceProfession, []],
        professionId: [entry.profession ? entry.profession.id : null, []],
        houseNumber: [entry.houseNumber, [ ]],
        age: [entry.age, [ Validators.required, positiveNumber ]],
        maritalStatus: [entry.maritalStatus, [ Validators.required ]],
        wifePresent: [`${entry.wifePresent}`, []],
        unit1: [entry.unit1, [ positiveNumber ]],
        unit2: [entry.unit2, [ positiveNumber ]],
        unit3: [entry.unit3, [ positiveNumber ]],
        totalYearsServed: [entry.totalYearsServed, [ Validators.required, positiveNumber ]],
        totalMonthsServed: [entry.totalMonthsServed, [ Validators.required, positiveNumber ]]
      });
  }

  private defaultMusterListEntry(mlid: number): MusterListEntry {
    let mlEntry = new MusterListEntry();

    mlEntry.musterListId = mlid;
    mlEntry.listNumber = 0;
    mlEntry.rank = new MilitaryRank();
    mlEntry.person = new Person();
    mlEntry.firstName = '';
    mlEntry.lastName = '';
    mlEntry.placeOfBirth = '';
    mlEntry.location = '';
    mlEntry.houseNumber = '';
    mlEntry.age = 0;
    mlEntry.religion = new ReligiousDenomination();
    mlEntry.maritalStatus = null;
    mlEntry.wifePresent = false;
    mlEntry.profession = new Profession();
    mlEntry.sourceProfession = '';
    mlEntry.unit1 = 0;
    mlEntry.unit2 = 0;
    mlEntry.unit3 = 0;
    //children: Child[];
    //serviceRecords: ServiceRecord[];
    mlEntry.totalYearsServed = 0;
    mlEntry.totalMonthsServed = 0;
    return mlEntry;
  }

  initView(): boolean {

      let sf: any = this.localStorageService.get(this.formStorageKey);

      let s_child: any = this.localStorageService.get("children");
      let s_serviceRecords: any = this.localStorageService.get("serviceRecords");

      if (sf != null) {

        this.entryForm.patchValue({'listNumber': sf.listNumber});
        //this.entryForm.patchValue({'personId': sf.personId});
        this.entryForm.patchValue({'firstName': sf.firstName});
        this.entryForm.patchValue({'lastName': sf.lastName});
        this.entryForm.patchValue({'placeOfBirth': sf.placeOfBirth});
        this.entryForm.patchValue({'rankId': sf.rankId});
        this.entryForm.patchValue({'location': sf.location});
        this.entryForm.patchValue({'religionId': sf.religionId});
        this.entryForm.patchValue({'sourceProfession': sf.sourceProfession});
        this.entryForm.patchValue({'professionId': sf.professionId});
        this.entryForm.patchValue({'houseNumber': sf.houseNumber});
        this.entryForm.patchValue({'age': sf.age});
        this.entryForm.patchValue({'maritalStatus': sf.maritalStatus});
        this.entryForm.patchValue({'wifePresent': `${sf.wifePresent}`});
        this.entryForm.patchValue({'unit1': sf.unit1});
        this.entryForm.patchValue({'unit2': sf.unit2});
        this.entryForm.patchValue({'unit3': sf.unit3});
        this.entryForm.patchValue({'totalYearsServed': sf.totalYearsServed});
        this.entryForm.patchValue({'totalMonthsServed': sf.totalMonthsServed});

        this.localStorageService.remove(this.formStorageKey);
      }

      if(s_child != null) {
        this.mlEntry.children = s_child;

        this.localStorageService.remove("children");
      }

      if (s_serviceRecords != null){
        this.mlEntry.serviceRecords = s_serviceRecords;

        this.localStorageService.remove("serviceRecords");
      }

      return true;
  }

}
