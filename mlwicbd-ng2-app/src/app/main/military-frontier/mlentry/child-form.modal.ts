import { Component, Output, EventEmitter, ViewChild } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Child } from "../../../common/dto/child.dto";
import { GlobalConstants } from "../../../common/global.constants";
import { positiveNumber } from '../../../common/validator/positive-number.validator';
import {ModalDirective} from "ngx-bootstrap";

/**
 * Modal Dialog for entering children data.
 *
 * @author dmadunic
 */
@Component({
  selector: 'mlentry-child-form',
  templateUrl: './child-form.modal.html'
})
export class ChildFormModal {

  @Output() childEntered = new EventEmitter();
  @ViewChild('childForm') public childForm: ModalDirective;

  cform: FormGroup;
  child:Child;
  genderList:string[] = GlobalConstants.CHILDREN_GENDER_CODES;

  constructor(
      private fb: FormBuilder
    ) {
      //
  }

  public showModal(child: Child):void {
    this.child = child;
    this.buildForm(child);
    this.childForm.show();
  }

  public submit():void {
    // 1. copy form to child ...
    this.child.id = this.cform.value.id;
    this.child.name = this.cform.value.cname;
    this.child.age = this.cform.value.age;
    this.child.gender = this.cform.value.gender;

    this.childEntered.emit(this.child);
    this.childForm.hide();
  }

  public hide():void {
    this.childForm.hide();
  }

  //--- form functions ---------------------------

  buildForm(child?: Child):void {
      this.cform = this.fb.group({
        id: [ child.id ],
        cname: [ child.name, [ Validators.required, Validators.maxLength(40) ]],
        age: [child.age ? child.age : 0, [ Validators.required, positiveNumber ]],
        gender: [child.gender ? child.gender : 'SON', [ Validators.required ]]
      });
  }

}
