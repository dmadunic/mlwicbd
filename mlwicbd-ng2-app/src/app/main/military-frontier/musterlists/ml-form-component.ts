import { Component, OnInit } from '@angular/core';
import { Observable, Subscription, TimeoutError } from 'rxjs';
import { GlobalConstants } from "../../../common/global.constants";
import { ActivatedRoute, Params, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { CustomValidators } from 'ng2-validation';
import { TranslateService } from '@ngx-translate/core';
import { ToastyService } from "ng2-toasty";
import { MusterList } from "../../../common/dto/musterlist.dto";
import { MilitaryUnit } from "../../../common/dto/military-unit.dto";
import { BaseError } from "../../../common/dto/error-base.dto";
import { MultipleErrorsResponse } from "../../../common/dto/error-mulitple.dto";
import { FieldError } from "../../../common/dto/error-field.dto";
import { FieldErrorsMap } from "../../../common/field-error-map";
import { MusterListService } from "../musterlists/musterlist.service";
import { MilitaryUnitService } from "../../../common/service/military-unit-service";
import { positiveNumber } from '../../../common/validator/positive-number.validator';
import * as errorUtils from '../../../common/http-error-utils';

/**
 * Muster list form component. This component is used for both: create and edit operations.
 * 
 * It returns either to:
 * - MusterList search screen
 * - MusterList details screen
 *  
 * @author: Domagoj Madunic
 */
@Component({
  selector: 'mf-musterlist-form',
  templateUrl: './ml-form.component.html'
})
export class MfMusterListFormComponent implements OnInit {

    id: number;
    musterList: MusterList = null;
    units: MilitaryUnit[] = null;
    monthYears = GlobalConstants.YEAR_MONTHS;
    
    mlForm: FormGroup;

    serverErrors:FieldErrorsMap = null;
    errorAlertMessage:string = null;

    // buton controls 
    saveDisabled: boolean = false;

    constructor(
      private router: Router,
      private route: ActivatedRoute,
      private fb: FormBuilder,
      private musterListService: MusterListService,
      private militaryUnitService: MilitaryUnitService,
      private translateService: TranslateService,
      private toastyService: ToastyService,
    ) { 
        //
    }

    ngOnInit() {
        this.id = this.route.snapshot.params['id'];
        Observable.forkJoin([
            this.fetchMusterList(this.id),
            this.militaryUnitService.fetchUnitsForTerritory(
                GlobalConstants.TERRITORY_CODE_MILITARY_FRONTIER, 
                GlobalConstants.CODES_MAX_PGSIZE
            ),
        ]).subscribe(
            aggregateData => {
                this.musterList = aggregateData[0];
                
                this.units = aggregateData[1].content;
                this.units.unshift(this.militaryUnitService.none(GlobalConstants.TERRITORY_CODE_MILITARY_FRONTIER));
                
                this.buildForm(this.musterList);
            }
        );
        var noMonth:{value:number, code:string} = {value: null, code: 'NONE_SELECTED'};
        this.monthYears.push(noMonth);
    }

    private fetchMusterList(mlid:number): Observable<MusterList> {
        let result: Observable<MusterList>  = null;

        if (mlid == null) {
          result = Observable.create( observer => {
            observer.next(this.defaultMusterList());
            observer.complete();
          });
        } else {
          result = this.musterListService.get(mlid);
        }
        return result;
    }

    private defaultMusterList():MusterList {
      let ml:MusterList = new MusterList();
      ml.id = null;
      ml.version=null;
      ml.referenceCode = null;
      ml.title = null;
      ml.year = null;
      ml.month = null;
      ml.unitId = null;
      return ml;
    }

    //--- form validation -----------------------------------------------------

    buildForm(musterList?: MusterList):void {
      this.mlForm = this.fb.group({
        id: [this.id],
        referenceCode: [ musterList.referenceCode, [ Validators.required, Validators.maxLength(80) ]],
        title: [musterList.title, [ Validators.required, Validators.maxLength(200)
        ]],
        unitId: [musterList.unitId, [Validators.required]],
        year: [musterList.year, [ Validators.required, CustomValidators.range([1680, 1882])]],
        month: [musterList.month, [ Validators.required ]]
      });
  }

  //--- form button controls --------------------------------------------------

  public goBack():void {
    if (this.id == null) {
      this.router.navigate(['/main/military-frontier/musterlists']);
    } else {
      this.router.navigate(['/main/military-frontier/musterlists/', this.id]);
    }
  }

  public save():void {
    this.saveDisabled = true;
      
    // 0. prepare musterList form for submission ...
    var mlForm:any = this.mlForm.value;
    mlForm.id = this.id;
    mlForm.version = this.musterList.version;

    //1. save entity
    this.musterListService.save(mlForm).subscribe(
      savedMusterList => {
        this.saveDisabled = false;
        //2. navigate to musterList details ...
        this.router.navigate(['/main/military-frontier/musterlists/', savedMusterList.id]);
        this.translateService.get("mlEntry.saved.success").subscribe((translatedMsg: string) => {
          this.toastyService.success(translatedMsg);
        });
      },
      err => {
        console.log(err);
        this.saveDisabled = false;
              
        if (!errorUtils.isGeneralServerError(err)) {
          this.translateService.get("musterList.save.failed").subscribe((translatedMsg: string) => {
            this.toastyService.warning(translatedMsg);
          });
          let error = errorUtils.handleRestApiErrorResponse(err);
          if (error instanceof MultipleErrorsResponse) {
            // izvuci field errors ...
            this.serverErrors = new FieldErrorsMap(error.errors);
          }
          this.errorAlertMessage = error.message;
        }
      });
  }

}    