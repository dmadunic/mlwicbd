import {Injectable} from "@angular/core";
import {Response, URLSearchParams, RequestOptions, Headers, ResponseContentType} from "@angular/http";
import {Observable} from "rxjs";
import {PagedResponse} from "../../../common/paged-response";
import {HttpWrapperService} from "../../../http-wrapper.service";
import {MusterList} from "../../../common/dto/musterlist.dto";
import {GlobalConstants} from "../../../common/global.constants";
import * as _ from "lodash";
import {MusterListEntry} from "../../../common/dto/musterlist-entry.dto";

@Injectable()
export class MusterListService {

  constructor(private http: HttpWrapperService) { }

  public get(id: number): Observable<MusterList> {
    let url = `${GlobalConstants.API_ENDPOINT}/musterlists/${id}`;

    return this.http
      .authGet(url)
      .map((res: Response) => res.json())
      .catch((error:any) => Observable.throw(error || 'Unknown Server error'));
  }

  public search(params): Observable<PagedResponse<MusterList>> {
    let url = `${GlobalConstants.API_ENDPOINT}/musterlists`;

    let searchParams = new URLSearchParams();
    if (!_.isEmpty(params.referenceCode)) {
        searchParams.set('referenceCode', params.referenceCode);
    }

    searchParams.set('year', params.year);
    searchParams.set('month', params.month);
    searchParams.set('unitId', params.unitId);
    searchParams.set('page', params.page);
    searchParams.set('size', params.size);
    searchParams.set('sort', params.sort);

    let options = new RequestOptions({
      search: searchParams
    });

    return this.http
      .authGet(url, options)
      .map((r: Response) => r.json() as PagedResponse<MusterList>)
      .catch((error:any) => Observable.throw(error || 'Unknown Server error'));
  }

  public save(musterList:any): Observable<MusterList> {
    if (musterList.id == null) {
      // save NEW record ...
      return this.create(musterList);
    } else {
      // update EXISTING record ...
      return this.update(musterList);
    }
  }

  public create(mmusterList): Observable<MusterList> {
    let url = `${GlobalConstants.API_ENDPOINT}/musterlists/`;
    let options = new RequestOptions({
      headers: new Headers({
        'Content-Type': 'application/json'
      })
    });

    return this.http.authPost(url, mmusterList, options)
        .map((r: Response) => r.json())
        .catch((error: any) => Observable.throw(error || 'Server error'));
  }

  public update(musterList): Observable<MusterList> {
    let url = `${GlobalConstants.API_ENDPOINT}/musterlists/${musterList.id}`;
    let options = new RequestOptions({
      headers: new Headers({
        'Content-Type': 'application/json'
      })
    });

    return this.http.authPut(url, musterList, options)
      .map((r: Response) => r.json())
      .catch((error: any) => Observable.throw(error || 'Server error'));
  }

  public downloadCsv(musterList){
    let url = `${GlobalConstants.API_ENDPOINT}/musterlists/${musterList.id}/downloadCsv`;
    let options = new RequestOptions({
      headers: new Headers({
        'Content-Type': 'application/json',
        'responseType': ResponseContentType.Blob,
      })
    });
    let headers = new Headers({
      'Content-Type': 'application/csv',
      'Accept': 'application/csv'
    });
    console.log("calling service...");
    return this.http.authGet(url, {
      headers: headers,
      responseType: ResponseContentType.Blob
    })
      .map((r: Response) => r.blob(), {type: 'application/csv'})
      .catch((error: any) => Observable.throw(error || 'Server error'));
  }

  public isPersonRecorded(musterListId, personId): Observable<boolean> {
    let url = `${GlobalConstants.API_ENDPOINT}/musterlists/${musterListId}/entries/person/${personId}`;
    return this.http.authGet(url)
      .map((res : Response) => res.json())
      .catch((error : any) => Observable.throw(error));
  }

}
