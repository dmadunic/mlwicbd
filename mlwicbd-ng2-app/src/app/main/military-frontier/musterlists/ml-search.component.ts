import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { FormBuilder } from "@angular/forms";
import { GlobalConstants } from "../../../common/global.constants";
import { MusterListService } from "./musterlist.service";
import { MilitaryUnitService } from "../../../common/service/military-unit-service"
import { MusterList } from "../../../common/dto/musterlist.dto";
import { MilitaryUnit } from "../../../common/dto/military-unit.dto";
import { PagedResultComponent } from "../../../common/paged-result-component";
import {LocalStoreManager} from "../../../core/oauth2/local-store-manager.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'mf-musterlists-search',
  templateUrl: './ml-search.component.html',
  styleUrls: ['./ml-search.component.css']
})
export class MfMusterListsSearchComponent extends PagedResultComponent<MusterList> implements OnInit {
  searchFormEnabled: boolean = false;
  months = GlobalConstants.YEAR_MONTHS;

  regiments: MilitaryUnit[];

  mleId: number;
  mlid: number;

  globalAlertMessage: string = null;

  constructor(
    private musterListService: MusterListService,
    private militaryUnitService: MilitaryUnitService,
    private fb: FormBuilder,
    private localStorage: LocalStoreManager,
    private route: ActivatedRoute,
    private router: Router
  ) {
    //
    super();
  }

  ngOnInit() {
    this.mleId = this.route.snapshot.queryParams['eid'];
    this.mlid = this.route.snapshot.queryParams['mlid'];
    this.searchFormEnabled = false;
    this.resetSearchForm();
    Observable.forkJoin([
      this.militaryUnitService.getRegimentsForTerritory(GlobalConstants.TERRITORY_CODE_MILITARY_FRONTIER),
      this.musterListService.search(this.searchForm.value),
    ]).subscribe(
      aggregateData => {
        this.regiments = aggregateData[0].content;
        this.pagedResult = aggregateData[1];
        this.searchFormEnabled = true;
        this.pageNumber = this.pagedResult.number +1;
      },
      err => {
        this.searchFormEnabled = true;
      }
    );

  }

  //-- button controls --------------------------------------------------------

  selectForCopy(mlId) {
    let pid = this.route.snapshot.queryParams['pid'];
    let qp = {eid: this.mleId, c: this.route.snapshot.queryParams['c']};
    this.musterListService.isPersonRecorded(mlId, pid).subscribe(
      data => {
        if (!data) {
          this.router.navigate([`/main/military-frontier/copy/musterlists/${mlId}/entry-new`], {queryParams: qp});
        }
        else {
          this.globalAlertMessage = "person.record.exists";
        }
      },
          error => console.log(error)
    );

  }

  //--- implemented abstract methods ------------------------------------------

  search(page?: number) {

    this.searchFormEnabled = false;
    if(page != null) {
      this.searchForm.patchValue({'page': page - 1});
    } else {
      this.searchForm.patchValue({'page': 0});
    }
    this.localStorage.saveSessionData(JSON.stringify(this.searchForm.value), "ml-search-form");
    this.musterListService.search(this.searchForm.value).subscribe(
      musterLists => {
        this.pagedResult = musterLists;
        this.pageNumber = musterLists.number +1;
        this.searchFormEnabled = true;
      },
      err => {
        this.searchFormEnabled = true;
      }
    );
  }

  //--- component private methods ---------------------------------------------

  private resetSearchForm():void {
    let searchFormInfo = this.localStorage.getDataObject<any>("ml-search-form");
    if (searchFormInfo == null){
      this.searchForm = this.fb.group({
        year: null,
        unitId: null,
        month: null,
        page: 0,
        size: [this.pageSize],
        sort: [this.sortAttribute]
      });
    }
    else {
      this.searchForm = this.fb.group({
        year: searchFormInfo['year'],
        unitId: searchFormInfo['unitId'],
        month: searchFormInfo['month'],
        page: searchFormInfo['page'],
        size: searchFormInfo['size'],
        sort: searchFormInfo['sort']
      });
    }
  }

  resetAlert() {
    this.globalAlertMessage = null;
  }

}
