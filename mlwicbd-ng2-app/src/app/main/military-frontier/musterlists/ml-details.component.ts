import { Component, OnInit } from '@angular/core';
import { FormBuilder } from "@angular/forms";
import { Router, ActivatedRoute} from '@angular/router';
import { Location } from '@angular/common';
import 'rxjs/add/operator/switchMap';
import 'rxjs/Rx' ;
import { GlobalConstants } from "../../../common/global.constants";
import { MusterListService } from "./musterlist.service";
import { MusterListEntryService } from "../mlentry/musterlist-entry.service";
import { MusterList } from "../../../common/dto/musterlist.dto";
import { MusterListEntry } from "../../../common/dto/musterlist-entry.dto";
import { PagedResultComponent } from "../../../common/paged-result-component";

import * as FileSaver from "file-saver";
import {TranslateService} from "@ngx-translate/core";
import {ToastyService} from "ng2-toasty";

@Component({
  selector: 'mf-musterlists-details',
  templateUrl: './ml-details.component.html'
})
export class MfMusterListDetailsComponent extends PagedResultComponent<MusterListEntry> implements OnInit {

  mlId: number;
  mlDetails: MusterList;
  yearMonths = GlobalConstants.YEAR_MONTHS;

  constructor(
      private musterListService: MusterListService,
      private musterListEntryService: MusterListEntryService,
      private fb: FormBuilder,
      private router: Router,
      private route: ActivatedRoute,
      private location: Location,
      private translateService: TranslateService,
      private toastyService: ToastyService
    ) {
    super();
  }

  ngOnInit() {
    this.sortAttribute = 'listNumber';
    this.mlId = this.route.snapshot.params['id'];

    this.musterListService.get(this.mlId).subscribe(
        result => {
          this.mlDetails = result;
          this.initSearchForm(this.mlDetails.id);
          this.musterListEntryService.search(this.searchForm.value).subscribe(
            entries => {
              this.pagedResult = entries;
              this.pageNumber = entries.number +1;
            },
            err => {
              console.error("Failed to fetch mlEntries:" + err);
              //TODO: error
            }
          );
        }
    );

  }

  //---

  public downloadAsCsv() {
    console.log("Starting csv download...");
    this.musterListService.downloadCsv(this.mlDetails)
      .subscribe(data => //console.log("data came") ,
        this.downloadFile(data),
        error => console.log("Error downloading file:" + error),
        ()=>console.log("Downloading file"));

  }
  downloadFile(data: any){
    FileSaver.saveAs(data, `muster-list(${this.mlDetails.referenceCode}).csv`);
  }

  editSelectedEntry(eId: number) {
    let targetRoute = '/main/military-frontier/musterlists/' + this.mlId + '/entry/' + eId;
    this.router.navigate([targetRoute]);
  }

  deleteSelectedEntry(eId: number) {
    console.log("Deleting entry with id=" + eId);
    this.musterListEntryService.delete(this.mlId,eId).subscribe(
      data => {
        this.translateService.get("mlEntry.delete.success").subscribe((translatedMsg: string) =>
          this.toastyService.success(translatedMsg));
        this.musterListEntryService.search(this.searchForm.value).subscribe(
          data => {
            this.pagedResult = data;
            this.pageNumber = data.number + 1;
          },
          error => {
            console.log(error);
          }
        )

      },
      error => {
        console.error("Failed to delete mle: " +error);
        this.translateService.get("mlEntry.delete.fail").subscribe((translatedMsg: string) =>
          this.toastyService.success(translatedMsg))

      }
    )
  }

  //--- TODO: implement role based logic here ....

  isEditDisabled():boolean {
    return false;
  }
  isNewEntryDisabled():boolean {
    return false;
  }
  isDisabledEntryEdit():boolean {
    return false;
  }
  isDisabledEntryDelete():boolean {
    return false;
  }

  //--- implemented abstract methods ------------------------------------------

  search(page?: number) {
    if(page != null) {
      this.searchForm.patchValue({'page': page - 1});
    } else {
      this.searchForm.patchValue({'page': 0});
    }

    this.musterListEntryService.search(this.searchForm.value).subscribe(
      entries => {
        this.pagedResult = entries;
        this.pageNumber = entries.number +1;
      },
      err => {
        //TODO:
      }
    );
  }

  private initSearchForm(mlId: number):void {
    this.searchForm = this.fb.group({
      id: mlId,
      page: 0,
      size: [this.pageSize],
      sort: [this.sortAttribute]
    });
  }

}
