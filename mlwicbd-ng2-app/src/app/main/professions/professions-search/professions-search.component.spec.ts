import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfessionsSearchComponent } from './professions-search.component';

describe('ProfessionsSearchComponent', () => {
  let component: ProfessionsSearchComponent;
  let fixture: ComponentFixture<ProfessionsSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfessionsSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfessionsSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
