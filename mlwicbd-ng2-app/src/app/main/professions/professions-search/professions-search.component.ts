import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {LocalStorageService} from "angular-2-local-storage";
import {FormBuilder, FormGroup} from "@angular/forms";
import {Profession} from "../../../common/dto/profession.dto";
import {PagedResultComponent} from "../../../common/paged-result-component";
import {ProfessionService} from "../../../common/service/profession-service";
import * as _ from "lodash";
import {ProfessionFormComponent} from "../profession-form/profession-form.component";


@Component({
  selector: 'app-professions-search',
  templateUrl: './professions-search.component.html',
  styleUrls: ['./professions-search.component.css']
})
export class ProfessionsSearchComponent extends PagedResultComponent<Profession> implements OnInit {

  @ViewChild(ProfessionFormComponent) formModal;

   private readonly INIT_PAGE_SIZE: number = 25;

  mleId: number = null;
  mlId: number = null;

  // buttons
  searchFormEnabled: boolean = true;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private professionService: ProfessionService,
    private localStorageService:LocalStorageService,
    private fb: FormBuilder
  ) {
    super();
  }

  ngOnInit() {
    this.mlId = this.route.snapshot.queryParams['mlid'];
    this.mleId = this.route.snapshot.queryParams['eid'];
    let autoLoadData:boolean = this.initSearchForm();
    if (autoLoadData) {
      this.search();
    }else {
      this.changeSize(this.INIT_PAGE_SIZE);
    }
  }

  onNotify(profession: Profession){
    this.search(this.pageNumber);
  }

  //--- button controls --------------------------------------------------------

  public backToMlEntryForm():void {
    let targetRoute = this.getMlEntryTargetUrl();
    this.router.navigate([targetRoute]);
  }

  public selectProfession(profession: Profession):void {
    let targetRoute = this.getMlEntryTargetUrl();
    this.router.navigate([targetRoute], { queryParams: { profid: profession.id }});
  }

  public showDetails(professionId:number):void {
    let targetRoute = '/main/professions/details/' + professionId;
    let queryparams = this.getQueryParams();
    this.router.navigate([targetRoute], { queryParams: queryparams});
  }

  public showEditModal(profession :Profession):void {
    this.formModal.showModal(profession);
  }

  //TODO: duplicate method
  private getMlEntryTargetUrl():string {
    let targetRoute = '/main/military-frontier/musterlists/';
    if (this.mleId == null) {
      return targetRoute + this.mlId + '/entry-new';
    } else {
      return targetRoute + this.mlId + '/entry/' + this.mleId;
    }
  }

  //TODO: duplicate method
  private getQueryParams():{} {
    let qp = {};
    if (this.mlId != null) {
      qp['mlid']= this.mlId;
    }
    if (this.mleId != null) {
      qp['eid'] = this.mleId;
    }
    return qp;
  }

  //--- implemented abstract methods ------------------------------------------

  search(page?: number):void {
    this.searchFormEnabled = false;

    if(page != null) {
      this.searchForm.patchValue({'page': page - 1});
    } else {
      this.searchForm.patchValue({'page': 0});
    }

    this.professionService.search(this.searchForm.value).subscribe(
      persons => {
        this.pagedResult = persons;
        this.pageNumber = persons.number +1;
        this.searchFormEnabled = true;
        this.localStorageService.set('professionsSearchForm', this.searchForm.value);
      },
      err => {
        console.log("SEARCH failed: " + err);
        this.searchFormEnabled = true;
      }
    );
  }

//--- component private methods ---------------------------------------------

  private initSearchForm():boolean {
    this.searchForm = this.emptySearchForm();

    let sf:any = this.localStorageService.get('professionsSearchForm');
    if (sf == null) {
      return false;
    }
    if (!_.isEmpty(sf.code)) {
      this.searchForm.patchValue({'code': sf.code});
      this.searchForm.patchValue({'name': sf.name});
    }
    //now merge data from sessionStorage to searchForm ...
    this.searchForm.patchValue({'page': sf.page});
    this.searchForm.patchValue({'size': sf.size});
    this.searchForm.patchValue({'sortAttribute': sf.sortAttribute});

    return true;
  }

  private emptySearchForm():FormGroup {
    let searchForm:FormGroup = this.fb.group({
      code: [''],
      name: [''],
      page: 0,
      size: [this.pageSize],
      sort: [this.sortAttribute]
    });
    return searchForm;
  }



}
