import {Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {Profession} from "../../../common/dto/profession.dto";
import {ProfessionService} from "../../../common/service/profession-service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ToastyService} from "ng2-toasty";
import {FieldErrorsMap} from "../../../common/field-error-map";
import {TranslateService} from "@ngx-translate/core";
import {MultipleErrorsResponse} from "../../../common/dto/error-mulitple.dto";
import * as errorUtils from '../../../common/http-error-utils';
import {ModalDirective} from "ngx-bootstrap";
import {DateFormatPipe, MomentModule} from "angular2-moment";

@Component({
  selector: 'app-profession-form',
  templateUrl: './profession-form.component.html',
  styleUrls: ['./profession-form.component.css']
})
export class ProfessionFormComponent implements OnInit {

  @ViewChild('formModal') public formModal: ModalDirective;

  profession: Profession;
  professionForm: FormGroup;

  btnDisabled: boolean = false;

  @Output() notify: EventEmitter<Profession> = new EventEmitter<Profession>();

  serverErrors: FieldErrorsMap = null;
  errorAlertMessage:string = null;

  constructor(private professionService: ProfessionService,
              private fb: FormBuilder,
              private translateService: TranslateService,
              private toastyService: ToastyService,
  ) { }

  ngOnInit() {
  }

  public showModal(profession: Profession): void {
    this.serverErrors = null;
    this.errorAlertMessage = null;
    this.profession = profession;
    this.buildForm(this.profession);
    this.formModal.show();
  }

  public hide():void {
    this.formModal.hide();
  }

  buildForm(profession ?: Profession) {
    if(profession != null) {
      console.log(profession);
      this.professionForm = this.fb.group({
        id: [profession.id],
        name: [profession.name, [Validators.required, Validators.maxLength(255)]],
        code: [profession.code, [Validators.required, Validators.maxLength(40)]],
        createdBy: [profession.createdBy],
        createdDate: [profession.createdAt],
        modifiedBy: [profession.modifiedBy],
        modifiedDate: [profession.modifiedAt],
        version: [profession.version]
      })
    }else {
      this.professionForm = this.fb.group({
        id: [null],
        name: ['', [Validators.required, Validators.maxLength(255)]],
        code: ['', [Validators.required, Validators.maxLength(40)]],
        version:[null]
      })
    }
  }

  submit() {
    this.btnDisabled = true;
      this.professionService.save(this.professionForm.value).subscribe(
        data => {
          console.log(data);
          this.hide();
          this.btnDisabled = false;
          this.notify.emit(data);
          this.translateService.get("profession.save.success").subscribe((translatedMsg: string) => {
            this.toastyService.success(translatedMsg);
          });
        },
        err => {
          console.log(err);
          this.btnDisabled = false;

          if (!errorUtils.isGeneralServerError(err)) {
            this.translateService.get("professions.save.failed").subscribe((translatedMsg: string) => {
              this.toastyService.warning(translatedMsg);
            });
            let error = errorUtils.handleRestApiErrorResponse(err);
            if (error instanceof MultipleErrorsResponse) {
              // izvuci field errors ...
              this.serverErrors = new FieldErrorsMap(error.errors);
            }
            this.errorAlertMessage = error.message;
          }
        });
  }

}
