/**
 * Created by tgreblicki on 05.05.17..
 */

import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {ProfessionsSearchComponent} from "./professions-search/professions-search.component";
import {ProfessionFormComponent} from "./profession-form/profession-form.component";

const  professionsRoutes: Routes = [
  {path: '', redirectTo: 'search', pathMatch: 'full'},
  {path: 'search', component: ProfessionsSearchComponent},
  {path: 'details/:id', component: ProfessionFormComponent}
];
@NgModule({
  imports: [RouterModule.forChild(professionsRoutes)],
  exports: [RouterModule]

})

export class ProfessionsRoutingModule {}
