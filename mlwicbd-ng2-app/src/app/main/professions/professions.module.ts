import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfessionsSearchComponent } from './professions-search/professions-search.component';
import { ProfessionFormComponent } from './profession-form/profession-form.component';
import {ProfessionService} from "../../common/service/profession-service";
import {ProfessionsRoutingModule} from "./professions-routing.module";
import {SharedModule} from "../../shared/shared.module";
import {ReactiveFormsModule} from "@angular/forms";
import {ModalModule} from "ngx-bootstrap";
import {MomentModule} from "angular2-moment";

@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    ProfessionsRoutingModule,
    ReactiveFormsModule,
    ModalModule,
    MomentModule
  ],
  declarations: [ProfessionsSearchComponent, ProfessionFormComponent],
  providers: [ProfessionService]
})
export class ProfessionsModule { }
