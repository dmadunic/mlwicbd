import { Component, OnInit } from '@angular/core';
import {environment} from "../../../environments/environment";
import {AppConfigService} from "../../common/service/app-config.service";

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css'],
})
export class FooterComponent implements OnInit {
  buildVersion: string = environment.build_info.buildVersion;
  buildDate: string = environment.build_info.buildDate;

  restBuildTime: string;
  restBuildVersion: string;
  constructor( private appConfig: AppConfigService) {
  }

  ngOnInit() {
    this.getConfiguration();
  }

  private getConfiguration(){
    this.appConfig.fetch().subscribe(data =>{
      this.restBuildTime = data["buildTime"];
      this.restBuildVersion = data["buildVersion"];
      console.log(data);
    },
    error => {
      console.log(error);
    })
  }

}
