import {Component, OnInit} from "@angular/core";
import {loadMorrisData} from "../../common/morris.data";

@Component({
  selector: 'app-home',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
    loadMorrisData();

  }
}
