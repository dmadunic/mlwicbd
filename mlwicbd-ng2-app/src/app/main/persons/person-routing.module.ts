import { Routes, RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { PersonSearchComponent } from "./person-search.component";
import { PersonDetailsComponent } from "./person-details.component";
import {PersonMergeComponent} from "./person-merge/person-merge.component";
import {PersonMergeWrapperComponent} from "./person-merge/person-merge-wrapper.component";
/**
 * Routing definitions for Persons module.
 *
 * Created by dmadunic on 12.02.2017.
 */
const personRoutes: Routes = [
  { path: '', redirectTo: 'search', pathMatch: 'full' },
  { path: 'search', component: PersonSearchComponent },
  { path: 'details/:pid', component: PersonDetailsComponent },
  { path: 'merge', component: PersonMergeWrapperComponent, children: [
    {path: '', redirectTo: 'search', pathMatch: 'full'},
    {path: 'search', component: PersonSearchComponent},
    {path: 'details/:pid', component: PersonDetailsComponent},
    {path: ':tpid/:spid', component: PersonMergeComponent}

  ]}
];

@NgModule({
  imports: [RouterModule.forChild(personRoutes)],
  exports: [RouterModule]
})
export class PersonRoutingModule {

}

