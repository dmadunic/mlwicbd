import { Component, ViewChild, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { FormBuilder } from "@angular/forms";
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';
import { ToastyService } from "ng2-toasty";
import 'rxjs/add/operator/switchMap';
import { PersonService } from "../../common/service/person.service";
import { MusterListEntryService } from "../military-frontier/mlentry/musterlist-entry.service";
import { Person } from "../../common/dto/person.dto";
import { MusterListEntry } from "../../common/dto/musterlist-entry.dto";
import { PagedResultComponent } from "../../common/paged-result-component";
import { PersonFormModal } from './person-form.modal';

@Component({
  selector: 'person-details',
  templateUrl: './person-details.component.html'
})
export class PersonDetailsComponent extends PagedResultComponent<MusterListEntry> implements OnInit {
    @ViewChild(PersonFormModal) personFormModal;

    pid: number;
    person: Person;
    mlId:number = null;
    mleId:number = null;
    tpid:number = null;

    constructor(
      private personService: PersonService,
      private musterListEntryService: MusterListEntryService,
      private fb: FormBuilder,
      private router: Router,
      private route: ActivatedRoute,
      private location: Location,
      private translateService: TranslateService,
      private toastyService: ToastyService,
    ) {
      super();
      this.pid = this.route.snapshot.params['pid'];
      this.mlId = this.route.snapshot.queryParams['mlid'];
      this.mleId = this.route.snapshot.queryParams['eid'];
      this.tpid = this.route.snapshot.queryParams['tpid'];
  }

  ngOnInit() {

    this.sortAttribute='musterList.year,musterList.month';
    this.initSearchForm(this.pid);
    Observable.forkJoin([
        this.personService.get(this.pid),
        this.musterListEntryService.searchEntries(this.searchForm.value)
    ]).subscribe(
        aggregateData => {
            this.person = aggregateData[0];
            this.pagedResult = aggregateData[1];
            this.pageNumber = aggregateData[1].number +1;
        },
        err => {
            console.error("Failed to fetch mlEntries:" + err);
            //TODO: error
        }
    );
  }

  //--- button controls -------------------------------------------------------

  private mergePerson(): void {
    if (this.tpid == null) {
      let qp = {tpid: this.person.id};
      this.router.navigate(['/main/persons/merge/search'], {queryParams: qp});
    }else {
      this.router.navigate(['/main/persons/merge',this.tpid, this.person.id]);
    }
  }
  private cancelMerge(){
    this.router.navigate(['/main/persons/search']);
  }

  private copyMle(mle: MusterListEntry): void {
    let qp = {eid : mle.id, mlid: mle.musterListId, pid: this.pid, c: true };
    this.router.navigate(['/main/military-frontier/copy/musterlists'], {queryParams: qp});
  }

  //--- implemented abstract methods ------------------------------------------

  search(page?: number) {
    if(page != null) {
      this.searchForm.patchValue({'page': page - 1});
    } else {
      this.searchForm.patchValue({'page': 0});
    }

    this.musterListEntryService.searchEntries(this.searchForm.value).subscribe(
      entries => {
        this.pagedResult = entries;
        this.pageNumber = entries.number +1;
      },
      err => {
        console.error("Failed to fetch mlEntries:" + err);
        //TODO:
      }
    );
  }

  //---

  public backToMlEntryForm():void {
    let targetRoute = this.getMlEntryTargetUrl();
    this.router.navigate([targetRoute]);
  }

  public selectPerson(person:Person):void {
    let targetRoute = this.getMlEntryTargetUrl();
    this.router.navigate([targetRoute], { queryParams: { pid: person.id }});
  }

  public backToPersonSearch():void {
    let targetRoute = '/main/persons/search';
    let queryparams = this.getQueryParams();
    this.router.navigate([targetRoute], { queryParams: queryparams});
  }

  public downloadAsCsv():void {

  }

  public showPersonEditModal():void {
    this.personFormModal.showModal(this.person);
  }

  public processPersonSave(person:Person):void {
    //TODO: implement save ....
    this.person = person;
    // display notification ...
    this.translateService.get("person.saved.success").subscribe((translatedMsg: string) => {
      this.toastyService.success(translatedMsg);
    });

    this.personFormModal.hide();

  }

  //TODO: duplicate method
  private getMlEntryTargetUrl():string {
    let targetRoute = '/main/military-frontier/musterlists/';
    if (this.mleId == null) {
      return targetRoute + this.mlId + '/entry-new';
    } else {
      return targetRoute + this.mlId + '/entry/' + this.mleId;
    }
  }

  //TODO: duplicate method
  private getQueryParams():{} {
    let qp = {};
    if (this.mlId != null) {
      qp['mlid']= this.mlId;
    }
    if (this.mleId != null) {
      qp['eid'] = this.mleId;
    }
    if (this.tpid != null) {
      qp['tpid'] = this.tpid
    }

    return qp;
  }

  //---

  private initSearchForm(pid:number):void {
    this.searchForm = this.fb.group({
      personId: pid,
      page: 0,
      size: [this.pageSize],
      sort: [this.sortAttribute]
    });
  }

}
