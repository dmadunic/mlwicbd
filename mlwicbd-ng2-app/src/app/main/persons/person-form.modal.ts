import { Component, Output, EventEmitter, ViewChild } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { TranslateService } from '@ngx-translate/core';
import { ToastyService } from "ng2-toasty";
import { PersonService } from "../../common/service/person.service";
import { Person } from "../../common/dto/person.dto";
import { BaseError } from "../../common/dto/error-base.dto";
import { MultipleErrorsResponse } from "../../common/dto/error-mulitple.dto";
import { FieldError } from "../../common/dto/error-field.dto";
import { FieldErrorsMap } from "../../common/field-error-map";
import * as errorUtils from '../../common/http-error-utils';
import { FieldValidationErrorWidget } from "../../common/widget/field-validation-error.widget";
import {ModalDirective} from "ngx-bootstrap";

/**
 * Modal Dialog for entering Person entity data.
 *
 * @author dmadunic
 */
@Component({
  selector: 'person-form',
  templateUrl: './person-form.modal.html'
})
export class PersonFormModal {

  @Output() personEntered = new EventEmitter();
  @ViewChild('personFormModal') public personFormModal: ModalDirective;

  personForm: FormGroup;
  person:Person;

  serverErrors:FieldErrorsMap = null;
  errorAlertMessage:string = null;

  constructor(
      private personService: PersonService,
      private fb: FormBuilder,
      private translateService: TranslateService,
      private toastyService: ToastyService,
    ) {
      //
  }

  public showModal(person:Person):void {
    this.person = person;
    this.buildForm(person);
    this.personFormModal.show();
  }

  public submit():void {
    // 1. copy form to person entity ...
    this.person.id = this.personForm.value.id;
    this.person.version = this.personForm.value.version;
    this.person.firstName = this.personForm.value.firstName;
    this.person.lastName = this.personForm.value.lastName;
    this.person.placeOfBirth = this.personForm.value.placeOfBirth;
    this.person.sourcesFirstNames = this.personForm.value.sourcesFirstNames;
    this.person.sourcesLastNames = this.personForm.value.sourcesLastNames;
    this.person.note = this.personForm.value.note;

    console.log("Saving person=" + this.person);
    this.personService.save(this.person).subscribe(
      result => {
            this.person = result;
        },
        err => {
            console.error("Failed to save Person err=" + err);
            if (!errorUtils.isGeneralServerError(err)) {
                this.translateService.get("mlEntry.save.failed").subscribe((translatedMsg: string) => {
                  this.toastyService.warning(translatedMsg);
                });
                let error = errorUtils.handleRestApiErrorResponse(err);
                if (error instanceof MultipleErrorsResponse) {
                  // izvuci field errors ...
                  this.serverErrors = new FieldErrorsMap(error.errors);
                }
                this.errorAlertMessage = error.message;
            }
        }
    );

    this.personEntered.emit(this.person);
  }

  public hide():void {
    this.personFormModal.hide();
  }

  //--- form functions ---------------------------

  buildForm(person?: Person):void {
      this.personForm = this.fb.group({
        id: [ person.id ],
        firstName: [ person.firstName, [ Validators.required, Validators.maxLength(40) ]],
        lastName: [person.lastName, [ Validators.required, Validators.maxLength(40) ]],
        placeOfBirth: [person.placeOfBirth, [ Validators.required, Validators.maxLength(80) ]],
        note: [person.note, [ Validators.maxLength(255) ]],
        sourcesFirstNames: [person.sourcesFirstNames, [ Validators.required, Validators.maxLength(255) ]],
        sourcesLastNames: [person.sourcesLastNames, [ Validators.required, Validators.maxLength(255) ]],
        version: [person.version]
      });
  }

}
