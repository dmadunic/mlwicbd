import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonMergeComponent } from './person-merge.component';

describe('PersonMergeComponent', () => {
  let component: PersonMergeComponent;
  let fixture: ComponentFixture<PersonMergeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonMergeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonMergeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
