/**
 * Created by tgreblicki on 24.05.17..
 */


import {Component, OnInit, ViewChild} from "@angular/core";
import {Router, NavigationEnd} from "@angular/router";
import {WizardProgressComponent} from "../../../components/wizard-progress/wizard-progress.component";
import {WizardStep, WizardStepMessage} from "../../../components/wizard-progress/wizard-step";
@Component({
  selector: 'person-merge-wrrapper',
  templateUrl: './person-merge-wrapper.component.html'
})
export class  PersonMergeWrapperComponent implements OnInit {
  @ViewChild(WizardProgressComponent) wizard;
  steps: WizardStep[] = new Array<WizardStep>();
  messages: WizardStepMessage;

  constructor(private router: Router) {
    this.messages = {
      1: 'Select another person',
      2: 'Merge persons'
    };
    this.steps.push({
      url: new RegExp('/main/persons/merge/search.+'),
      step: 1
    });
    this.steps.push({
      url: new RegExp('main/persons/merge/[\\d]+/[\\d]+'),
      step: 2
    });
    this.steps.push({
      url: new RegExp('main/persons/merge/details/[\\d]+'),
      step: 1
    });
    this.router.events.subscribe(event => this.navigationInterceptor(event))
  }

  ngOnInit(): void {
  }

  private navigationInterceptor(event) {
    if (event instanceof NavigationEnd) {
      let index = this.steps.find((value, index, obj) => value.url.test(event.url));
      if (index != null)
        this.wizard.step(index.step);
    }
  }
}
