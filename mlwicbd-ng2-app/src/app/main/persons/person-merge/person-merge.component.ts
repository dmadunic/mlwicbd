import {Component, OnInit, ViewChild} from '@angular/core';
import {PagedResultComponent} from "../../../common/paged-result-component";
import {MusterListEntry} from "../../../common/dto/musterlist-entry.dto";
import {ActivatedRoute, Router} from "@angular/router";
import {PersonService} from "../../../common/service/person.service";
import {ToastyService} from "ng2-toasty";
import {Observable} from "rxjs";
import {MusterListEntryService} from "../../military-frontier/mlentry/musterlist-entry.service";
import {FormBuilder} from "@angular/forms";
import {Person} from "../../../common/dto/person.dto";
import {ConfirmMergeModalComponent} from "./confirm-merge-modal/confirm-merge-modal.component";
import {PersonMleModalComponent} from "./person-mle-modal/person-mle-modal.component";

@Component({
  selector: 'app-person-merge',
  templateUrl: './person-merge.component.html',
  styleUrls: ['./person-merge.component.css']
})
export class PersonMergeComponent extends PagedResultComponent<MusterListEntry> implements OnInit {
  @ViewChild(ConfirmMergeModalComponent) mergeModal;
  @ViewChild(PersonMleModalComponent) mleModal;

  tpid: number;
  spid: number;

  person1: Person;
  person2: Person;

  tFullName: string;
  sFullName: string;
  constructor(private route: ActivatedRoute,
              private router: Router,
              private personService: PersonService,
              private musterListEntryService: MusterListEntryService,
              private fb: FormBuilder) {
    super();
    this.tpid = this.route.snapshot.params['tpid'];
    this.spid = this.route.snapshot.params['spid'];
  }

  ngOnInit() {
    this.sortAttribute='musterList.year,musterList.month';
    this.initSearchForm(this.spid, this.tpid);
    Observable.forkJoin([
      this.personService.get(this.spid),
      this.personService.get(this.tpid),
      this.musterListEntryService.searchMergedEntries(this.searchForm.value)
    ]).subscribe(
      aggregateData => {
        this.person1 = aggregateData[0];
        this.person2 = aggregateData[1];
        this.pagedResult = aggregateData[2];
        this.pageNumber = aggregateData[2].number +1;
      },
      err => {
        console.error("Failed to fetch mlEntries:" + err);
        //TODO: error
      }
    );
  }

  public onNotify(personInfo) {
    let id = personInfo['id'];
    let fullName = personInfo['fullName'];
    if (id == this.tpid) {
      this.tFullName = fullName;
    }
    if (id == this.spid) {
      this.sFullName = fullName;
    }
  }

  // --- button controls ------------------------------------------------------
  private cancelMerge(){
    this.router.navigate(['/main/persons/search']);
  }

  private mergePersons(sourcePerson: Person, targetPerson: Person){
    this.mergeModal.show(sourcePerson, targetPerson);
  }

  private showPersonsMle(person: Person) {
    this.mleModal.show(person);
  }

  // --- implement abstract method --------------------------------------------
  search(page?: number) {
    if(page != null) {
      this.searchForm.patchValue({'page': page - 1});
    } else {
      this.searchForm.patchValue({'page': 0});
    }
    this.musterListEntryService.searchMergedEntries(this.searchForm.value).subscribe(
      entries => {
        this.pagedResult = entries;
        this.pageNumber = entries.number +1;
      },
      err => {
        console.error("Failed to fetch mlEntries:" + err);
        //TODO:
      }
    );
  }
  // ---
  private initSearchForm(spid:number, tpid:number):void {
    this.searchForm = this.fb.group({
      personId1: spid,
      personId2: tpid,
      page: 0,
      size: [this.pageSize],
      sort: [this.sortAttribute]
    });
  }
}
