import {Component, OnInit, ViewChild} from '@angular/core';
import {Person} from "../../../../common/dto/person.dto";
import {MusterListEntryService} from "../../../military-frontier/mlentry/musterlist-entry.service";
import {PagedResultComponent} from "../../../../common/paged-result-component";
import {MusterListEntry} from "../../../../common/dto/musterlist-entry.dto";
import {FormBuilder} from "@angular/forms";

@Component({
  selector: 'app-person-mle-modal',
  templateUrl: './person-mle-modal.component.html',
  styleUrls: ['./person-mle-modal.component.css']
})
export class PersonMleModalComponent extends PagedResultComponent<MusterListEntry> implements OnInit {
  @ViewChild('mleModal') modal;
  constructor(
    private musterListEntryService: MusterListEntryService,
    private fb: FormBuilder) {
    super();
    this.sortAttribute='musterList.year,musterList.month';
  }

  person: Person;

  ngOnInit(){

  }

  // --- implement abstract method --------------------------------------------
  search(page?: number) {
    if(page != null) {
      this.searchForm.patchValue({'page': page - 1});
    } else {
      this.searchForm.patchValue({'page': 0});
    }
    this.musterListEntryService.searchEntries(this.searchForm.value).subscribe(
      entries => {
        this.pagedResult = entries;
        this.pageNumber = entries.number +1;
      },
      err => {
        console.error("Failed to fetch mlEntries:" + err);
        //TODO:
      }
    );
  }

  // ---
  private initSearchForm(pid:number):void {
    this.searchForm = this.fb.group({
      personId: `${pid}`,
      page: 0,
      size: [this.pageSize],
      sort: [this.sortAttribute]
    });
  }

  public show(person: Person) {
    this.person = person;
    this.initSearchForm(this.person.id);
    this.search();
    this.modal.show();
  }

  public hide(){
    this.modal.hide();
  }
}
