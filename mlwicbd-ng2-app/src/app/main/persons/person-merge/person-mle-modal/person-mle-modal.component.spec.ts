import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonMleModalComponent } from './person-mle-modal.component';

describe('PersonMleModalComponent', () => {
  let component: PersonMleModalComponent;
  let fixture: ComponentFixture<PersonMleModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonMleModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonMleModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
