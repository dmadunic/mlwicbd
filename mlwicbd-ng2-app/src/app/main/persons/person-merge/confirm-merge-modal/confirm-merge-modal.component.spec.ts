import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmMergeModalComponent } from './confirm-merge-modal.component';

describe('ConfirmMergeModalComponent', () => {
  let component: ConfirmMergeModalComponent;
  let fixture: ComponentFixture<ConfirmMergeModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfirmMergeModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmMergeModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
