import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {Person} from "../../../../common/dto/person.dto";
import {ModalDirective} from "ngx-bootstrap";
import {PersonService} from "../../../../common/service/person.service";
import {Router} from "@angular/router";
import {ToastyService} from "ng2-toasty";

@Component({
  selector: 'app-confirm-merge-modal',
  templateUrl: './confirm-merge-modal.component.html',
  styleUrls: ['./confirm-merge-modal.component.css']
})
export class ConfirmMergeModalComponent implements OnInit {
  @ViewChild('confirmModal') modal: ModalDirective;

  @Input() sourcePerson: Person;
  @Input() targetPerson: Person;

  constructor(private personService: PersonService,
    private router: Router,
    private toastyService: ToastyService) { }

  ngOnInit() {
  }

  cancel(){
    this.modal.hide();
  }

  confirm(){
    this.personService.mergePersons(this.sourcePerson.id, this.targetPerson.id).subscribe(
      data => {
        if (data == true) {
          this.router.navigate(['/main/persons/details', this.targetPerson.id]);
          this.toastyService.success("person.merge.success")
        }
      },
      error=> {
        console.log(error)
      }
    );
    this.modal.hide();
  }

  public show(sourcePerson: Person, targetPerson: Person) {
    this.sourcePerson = sourcePerson;
    this.targetPerson = targetPerson;
    this.modal.show();
  }

}
