import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "../../shared/shared.module";
import { PersonRoutingModule } from "./person-routing.module";
import { PersonService } from "../../common/service/person.service";
import { MusterListEntryService } from "../military-frontier/mlentry/musterlist-entry.service";
import { PersonSearchComponent } from "./person-search.component";
import { PersonDetailsComponent } from "./person-details.component";
import { PersonFormModal } from './person-form.modal';
import { PersonMergeComponent } from './person-merge/person-merge.component';
import {PersonMergeWrapperComponent} from "./person-merge/person-merge-wrapper.component";
import {WizardProgressModule} from "../../components/wizard-progress/wizard-progress.module";
import { ConfirmMergeModalComponent } from './person-merge/confirm-merge-modal/confirm-merge-modal.component';
import { PersonMleModalComponent } from './person-merge/person-mle-modal/person-mle-modal.component';
import {NgxPaginationModule} from "ngx-pagination";
import {ModalModule} from "ngx-bootstrap";
import {MusterListService} from "../military-frontier/musterlists/musterlist.service";

/**
 * Definition of Person module.
 *
 */
@NgModule({
  imports: [
    ReactiveFormsModule,
    NgxPaginationModule,
    SharedModule,
    ModalModule.forRoot(),
    PersonRoutingModule,
    WizardProgressModule
  ],
  declarations: [
    PersonSearchComponent,
    PersonDetailsComponent,
    PersonFormModal,
    PersonMergeComponent,
    PersonMergeWrapperComponent,
    ConfirmMergeModalComponent,
    PersonMleModalComponent
  ],
  providers: [
    PersonService,
    MusterListEntryService,
    MusterListService
  ],
  exports: [
    PersonSearchComponent,
    PersonDetailsComponent
  ]
})
export class PersonsModule { }
