import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { FormGroup, FormBuilder } from "@angular/forms";
import { LocalStorageService } from 'angular-2-local-storage';
import { PersonService } from "../../common/service/person.service";
import { Person } from "../../common/dto/person.dto";
import { PagedResponse } from "../../common/paged-response";
import { PagedResultComponent } from "../../common/paged-result-component";
import * as _ from "lodash";
import {MusterListService} from "../military-frontier/musterlists/musterlist.service";

@Component({
  selector: 'person-search',
  templateUrl: './person-search.component.html',
  styleUrls: ['./person-search.component.css']
})
export class PersonSearchComponent extends PagedResultComponent<Person> implements OnInit {
    mlId:number = null;
    mleId:number = null;
    tpid:number  = null;

    // buttons
    searchFormEnabled: boolean = true;

    globalAlertMessage: string = null;


    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private personService: PersonService,
        private localStorageService:LocalStorageService,
        private fb: FormBuilder,
        private musterListService: MusterListService
    ) {
        super();
    }

    ngOnInit() {
      this.mlId = this.route.snapshot.queryParams['mlid'];
      this.mleId = this.route.snapshot.queryParams['eid'];
      this.tpid = this.route.snapshot.queryParams['tpid'];
      let autoLoadData:boolean = this.initSearchForm();
      if (autoLoadData) {
        this.search();
      }
    }

    //--- button controls --------------------------------------------------------

    public backToMlEntryForm():void {
      let targetRoute = this.getMlEntryTargetUrl();
      this.router.navigate([targetRoute]);
    }

    public selectPerson(person:Person):void {
      let targetRoute = this.getMlEntryTargetUrl();
      this.musterListService.isPersonRecorded(this.mlId, person.id).subscribe(
        data => {
          if (data == true) {
            this.globalAlertMessage = "person.record.exists";
          }
          else {
            this.router.navigate([targetRoute], { queryParams: { pid: person.id }});
          }
        },
        error => console.log(error)
      );
    }

    public showDetails(personId:number):void {
      let targetRoute;
      if (this.tpid != null)
        targetRoute = '/main/persons/merge/details/' + personId;
      else
        targetRoute = '/main/persons/details/' + personId;
      let queryparams = this.getQueryParams();
        this.router.navigate([targetRoute], { queryParams: queryparams});
    }

    //TODO: duplicate method
    private getMlEntryTargetUrl():string {
      let targetRoute = '/main/military-frontier/musterlists/';
      if (this.mleId == null) {
        return targetRoute + this.mlId + '/entry-new';
      } else {
        return targetRoute + this.mlId + '/entry/' + this.mleId;
      }
    }

    //TODO: duplicate method
    private getQueryParams():{} {
      let qp = {};
      if (this.mlId != null) {
        qp['mlid']= this.mlId;
      }
      if (this.mleId != null) {
        qp['eid'] = this.mleId;
      }
      if (this.tpid != null){
        qp['tpid'] = this.tpid
      }
      return qp;
    }

    //--- implemented abstract methods ------------------------------------------

  search(page?: number):void {
    this.searchFormEnabled = false;

    if(page != null) {
      this.searchForm.patchValue({'page': page - 1});
    } else {
      this.searchForm.patchValue({'page': 0});
    }

    this.personService.search(this.searchForm.value).subscribe(
      persons => {
        this.pagedResult = persons;
        this.pageNumber = persons.number +1;
        this.searchFormEnabled = true;
        this.localStorageService.set('personSearchForm', this.searchForm.value);
      },
      err => {
        console.log("SEARCH failed: " + err);
        this.searchFormEnabled = true;
      }
    );
  }

  //--- component private methods ---------------------------------------------

  private initSearchForm():boolean {
    this.searchForm = this.emptySearchForm();

    let sf:any = this.localStorageService.get('personSearchForm');
    if (sf == null) {
      return false;
    }
    if (!_.isEmpty(sf.firstName)) {
        this.searchForm.patchValue({'firstName': sf.firstName});
    }
    if (!_.isEmpty(sf.lastName)) {
        this.searchForm.patchValue({'lastName': sf.lastName});
    }
    //now merge data from sessionStorage to searchForm ...
    this.searchForm.patchValue({'page': sf.page});
    this.searchForm.patchValue({'size': sf.size});
    this.searchForm.patchValue({'sortAttribute': sf.sortAttribute});

    return true;
  }

  private emptySearchForm():FormGroup {
    let searchForm:FormGroup = this.fb.group({
      firstName: [''],
      lastName: [''],
      page: 0,
      size: [this.pageSize],
      sort: [this.sortAttribute]
    });
    return searchForm;
  }

  private mergePerson(personId: number ){
      this.router.navigate(['/main/persons/merge',this.tpid, personId]);
  }

  private cancelMerge(){
      this.tpid = null;
    this.router.navigate(['/main/persons/search']);
  }

  resetAlert() {
    this.globalAlertMessage = null;
  }

}
