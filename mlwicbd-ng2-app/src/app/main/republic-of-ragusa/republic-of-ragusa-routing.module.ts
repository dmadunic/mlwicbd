import {RouterModule, Routes} from "@angular/router";
import {RorMusterListSearchComponent} from "./musterlists/ml-search.component";
import {RorParishRegistersSearchComponent} from "./parish-registers/pr-search.component";
import {NgModule} from "@angular/core";

const rorRoutes: Routes = [
  { path: 'musterlists', component: RorMusterListSearchComponent },
  { path: 'parishregisters', component: RorParishRegistersSearchComponent},
];

@NgModule({
  imports: [RouterModule.forChild(rorRoutes)],
  exports: [RouterModule]
})
export class RorRoutingModule {

}
