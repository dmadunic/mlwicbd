import { NgModule } from '@angular/core';
import {ReactiveFormsModule} from "@angular/forms";
import {SharedModule} from "../../shared/shared.module";
import {RorRoutingModule} from "./republic-of-ragusa-routing.module";
import {RorMusterListSearchComponent} from "./musterlists/ml-search.component";
import {RorParishRegistersSearchComponent} from "./parish-registers/pr-search.component";
import {ModalModule} from "ngx-bootstrap";
import {NgxPaginationModule} from "ngx-pagination";

@NgModule({
  imports: [
    ReactiveFormsModule,
    NgxPaginationModule,
    SharedModule,
    ModalModule.forRoot(),
    RorRoutingModule
  ],
  declarations: [RorMusterListSearchComponent, RorParishRegistersSearchComponent]
})
export class RorModule { }
