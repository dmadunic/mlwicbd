import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {VdaMusterListSearchComponent} from "./musterlists/ml-search.component";
import {VdaParishRegistersSearchComponent} from "./parish-registers/pr-search.component";
import {ReactiveFormsModule} from "@angular/forms";
import {SharedModule} from "../../shared/shared.module";
import {VdaRoutingModule} from "./venetian-dal-alb-routing.module";
import {NgxPaginationModule} from "ngx-pagination";
import {ModalModule} from "ngx-bootstrap";

@NgModule({
  imports: [
    ReactiveFormsModule,
    NgxPaginationModule,
    SharedModule,
    ModalModule.forRoot(),
    VdaRoutingModule
  ],
  declarations: [VdaMusterListSearchComponent, VdaParishRegistersSearchComponent]
})
export class VdaModule { }
