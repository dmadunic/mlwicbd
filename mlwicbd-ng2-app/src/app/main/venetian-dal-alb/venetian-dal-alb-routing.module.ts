import {RouterModule, Routes} from "@angular/router";
import {VdaMusterListSearchComponent} from "./musterlists/ml-search.component";
import {VdaParishRegistersSearchComponent} from "./parish-registers/pr-search.component";
import {NgModule} from "@angular/core";

const vdaRoutes: Routes = [
  { path: 'musterlists', component: VdaMusterListSearchComponent },
  { path: 'parishregisters', component: VdaParishRegistersSearchComponent},
];

@NgModule({
  imports: [RouterModule.forChild(vdaRoutes)],
  exports: [RouterModule]
})
export class VdaRoutingModule {

}
