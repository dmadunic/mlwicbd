import {NgModule} from "@angular/core";
import {Routes, RouterModule} from "@angular/router";
import {MainComponent} from "./main.component";
import {BlankComponent} from "../shared/blank/blank.component";
import {DashboardComponent} from "./dashboard/dashboard.component";
import {RegistrationComponent} from "./registration/registration.component";
import {MfMusterListsSearchComponent} from "./military-frontier/musterlists/ml-search.component";
//import {AuthGuard} from "../core/auth-guard.service";

const mainRoutes: Routes = [
  { path: 'main', component: MainComponent,
    children: [
      {
        path: '',
        //canActivateChild: [AuthGuard],
        children: [
          { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
          { path: 'dashboard', component: DashboardComponent },
          { path: 'registration', component: RegistrationComponent },
          /**
           * Lazy loaded modules
           * Before modifying routing please read:
           * https://angular.io/docs/ts/latest/guide/router.html
           * https://angular.io/docs/ts/latest/cookbook/ngmodule-faq.html
           */
          { path: 'military-frontier', loadChildren: './military-frontier/military-frontier.module#MilitaryFrontierModule' },
          { path: 'venetian-dalmatia-and-albania', loadChildren: './venetian-dal-alb/venetian-dal-alb.module#VdaModule'},
          { path: 'republic-of-ragusa', loadChildren: './republic-of-ragusa/republic-of-ragusa.module#RorModule'},
          { path: 'persons', loadChildren: './persons/persons.module#PersonsModule'},
          { path: 'users', loadChildren: './users/users.module#UsersModule'},
          { path: 'professions', loadChildren: './professions/professions.module#ProfessionsModule'},
          { path: 'blank', component: BlankComponent}
        ]
      }
    ]
  }
//  , { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forChild(mainRoutes)],
  exports: [RouterModule]
})
export class MainRoutingModule {
}
