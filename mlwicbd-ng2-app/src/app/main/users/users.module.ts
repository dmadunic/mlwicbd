import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "../../shared/shared.module";
import { UsersRoutingModule } from "./users-routing.module";
import { UserService } from "./users.service";
import { UserProfileComponent } from "./user-profile.component";
import {UserUpdateComponent} from "./user-update/user-update.component";
import {UserFormComponent} from "../../shared/user-form/user-form.component";
import {UserSettingsComponent} from "./user-settings/user-settings.component";
import {ModalModule} from "ngx-bootstrap";
import {NgxPaginationModule} from "ngx-pagination";
import {UserFormModule} from "../../shared/user-form/user-form.module";

/**
 * Definition of Users module.
 *
 */
@NgModule({
  imports: [
    NgxPaginationModule,
    SharedModule,
    ModalModule.forRoot(),
    UsersRoutingModule,
    ReactiveFormsModule,
    UserFormModule
  ],
  declarations: [
    UserProfileComponent,
    UserUpdateComponent,
    UserSettingsComponent,
  ],
  providers: [
    UserService
  ]
})
export class UsersModule { }
