import { Routes, RouterModule } from "@angular/router";
import {NgModule, OnInit} from "@angular/core";
import { UserProfileComponent } from "./user-profile.component";
import {UserUpdateComponent} from "./user-update/user-update.component";
import {UserSettingsComponent} from "./user-settings/user-settings.component";
/**
 * Routing definitions for Persons module.
 *
 * Created by dmadunic on 06.04.2017.
 */
const usersRoutes: Routes = [
  {path: '', redirectTo: 'profile', pathMatch: 'full'},
  {path: 'profile', component: UserProfileComponent},
  {path: 'profile/edit', component: UserUpdateComponent},
  {path: 'settings', component: UserSettingsComponent}
];

@NgModule({
  imports: [RouterModule.forChild(usersRoutes)],
  exports: [RouterModule]
})
export class UsersRoutingModule implements OnInit{
  constructor(){}
  ngOnInit() {}
}
