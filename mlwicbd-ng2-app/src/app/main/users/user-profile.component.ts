import {Component, OnInit, ViewChild} from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ToastyService } from "ng2-toasty";
import { UserService } from "./users.service";
import { User } from "../../common/dto/user.dto";
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import * as errorUtils from '../../common/http-error-utils';
import {MultipleErrorsResponse} from "../../common/dto/error-mulitple.dto";
import {FieldErrorsMap} from "../../common/field-error-map";
import {ModalDirective} from "ngx-bootstrap";



@Component({
  selector: 'user-profile',
  templateUrl: './user-profile.component.html'
})
export class UserProfileComponent implements OnInit {
    user:User;

    @ViewChild('passwordModal') passwordModal: ModalDirective;

    passwordForm: FormGroup;
    btnDisabled:boolean =false;

    serverErrors: FieldErrorsMap = null;
    errorAlertMessage:string = null;

    constructor(
        private userService: UserService,
        private translateService: TranslateService,
        private toastyService: ToastyService,
        private fb: FormBuilder
    ) {
        //
    }

    ngOnInit() {
        this.userService.getUser().subscribe(
            user => {
                this.user = user;
            },
            err => {
                console.log("FETCH user details failed: " + err);
            }
        );
    }

    // --- password form modal & options --------------------------------------

    passwordRegex: any = '(?=.*[A-Z])(?=.*[a-z])(?=.*\\d)[A-Za-z\\d]{6,70}';

    showModal(){
      this.buildForm();
      this.passwordModal.show();
    }

    hideModal(){
      this.passwordModal.hide();
    }

    buildForm() {
      this.passwordForm = this.fb.group({
        oldPassword: ['', [Validators.required, Validators.maxLength(70), Validators.minLength(5)]],
        password: ['', [Validators.required, Validators.maxLength(70), Validators.minLength(6), Validators.pattern(this.passwordRegex)]],
        password2: ['', [Validators.required, Validators.maxLength(70),Validators.minLength(6), Validators.pattern(this.passwordRegex)]],
      }, {validator: this.matchingPasswords('password', 'password2')});
    }

  matchingPasswords(passwordKey: string, passwordConfirmationKey: string) {
    return (group: FormGroup) => {
      let passwordInput = group.controls[passwordKey];
      let passwordConfirmationInput = group.controls[passwordConfirmationKey];
      if (passwordInput.value !== passwordConfirmationInput.value) {
        return passwordConfirmationInput.setErrors({notEquivalent: true})
      }
      else{
        return null;
      }
    }
  }

  submit() {
      this.btnDisabled = true;
      this.userService.update(this.passwordForm.value, this.user).subscribe(
        data => {
          this.user = data;
          this.hideModal();
          this.btnDisabled = false;
          this.translateService.get("user.password.edit.success").subscribe((translatedMsg: string) => {
            this.toastyService.success(translatedMsg);
          });
        },
        err => {
          console.log(err);
          this.btnDisabled = false;

          if (!errorUtils.isGeneralServerError(err)) {
            this.translateService.get("professions.save.failed").subscribe((translatedMsg: string) => {
              this.toastyService.warning(translatedMsg);
            });
            let error = errorUtils.handleRestApiErrorResponse(err);
            if (error instanceof MultipleErrorsResponse) {
              // izvuci field errors ...
              this.serverErrors = new FieldErrorsMap(error.errors);
            }
            this.errorAlertMessage = error.message;
          }
        });
  }


}
