import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {CustomValidators} from "ng2-validation";

import { User } from "../../../common/dto/user.dto";
import {UserService} from "../users.service";
import {MultipleErrorsResponse} from "../../../common/dto/error-mulitple.dto";
import {FieldErrorsMap} from "../../../common/field-error-map";
import * as errorUtils from '../../../common/http-error-utils';
import {ToastyService} from "ng2-toasty";
import {TranslateService} from "@ngx-translate/core";
import {Router} from "@angular/router";
import {UserFormSettings} from "../../../common/form/user-form.settings";
import {AuthService} from "../../../core/auth.service";



@Component({
  selector: 'user-update',
  templateUrl: './user-update.component.html',
  styleUrls: ['./user-update.component.css']
})
export class UserUpdateComponent implements OnInit {
  user: User;
  editForm: FormGroup;
  showForm: boolean;
  countryCode: string;
  serverErrors:FieldErrorsMap = null;
  errorAlertMessage:string = null;
  formSettings: UserFormSettings = new UserFormSettings();



  constructor(
    private userService: UserService,
    private fb: FormBuilder,
    private toastyService: ToastyService,
    private translateService: TranslateService,
    private router: Router,
    private auth:AuthService
  ) {
    this.formSettings.submitBtnText="Save";
    this.formSettings.submitBtnIcon="fa fa-floppy-o";
    this.formSettings.controlDisabled.username=true;
  }

  ngOnInit() {
    this.userService.getUser().subscribe(
      user => {
        this.user = user;
        this.countryCode = user.countryCode;
        this.buildForm(this.user);
      },
      err => {
        console.log("FETCH user details failed: " + err);
      }
    );
  }

  buildForm(user?: User) {
    this.editForm = this.fb.group({
      username: [user.username,  [ Validators.required, Validators.maxLength(30), Validators.minLength(5) ],  ],
      firstName: [user.firstName, [ Validators.required, Validators.maxLength(40) ] ],
      lastName: [user.lastName, [ Validators.required, Validators.maxLength(40) ] ],
      address: [user.address, [ Validators.required, Validators.maxLength(80) ] ],
      postalCode: [user.postalCode, [ Validators.required, Validators.maxLength(20) ] ],
      place: [user.place, [ Validators.required, Validators.maxLength(80) ] ],
      countryCode: [user.countryCode, [ Validators.required ]],
      email: [ user.email, [ Validators.required, CustomValidators.email ]],
      mobile: [user.mobile, [ Validators.required ]],
      phone: [user.phone, [ Validators.required ]]
    });
  }

  public onNotify(form: FormGroup): void {
    this.editForm = form;
    this.submitEdit();
  }

  public submitEdit(): void {
    this.formSettings.submitButtonDisabled = true;
    console.log("editing user: " + this.editForm.value);
    this.userService.update(this.editForm.value, this.user).subscribe(
      result => {
        this.user = result;
        this.formSettings.submitButtonDisabled = false;
        this.showForm = false;
        this.toastyService.success("Profile successfully updates!");
        this.router.navigate(['/main/users/profile']);
        this.userService.syncUser(this.user);
        console.log(this.auth.refreshToken().subscribe());

      },
      err => {
        this.formSettings.submitButtonDisabled = false;
        console.error("Failed to update User err=" + err);
        if (!errorUtils.isGeneralServerError(err)) {
          this.translateService.get("mlEntry.save.failed").subscribe((translatedMsg: string) => {
            this.toastyService.warning(translatedMsg);
          });
          let error = errorUtils.handleRestApiErrorResponse(err);
          if (error instanceof MultipleErrorsResponse) {
            // izvuci field errors ...
            this.serverErrors = new FieldErrorsMap(error.errors);
          }
          this.errorAlertMessage = error.message;
        }
      }
    );

  }

}
