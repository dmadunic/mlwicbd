import { Injectable } from "@angular/core";
import { Response, RequestOptions, Headers } from "@angular/http";
import {Observable, Subject} from "rxjs";
import { User } from "../../common/dto/user.dto";
import { HttpWrapperService } from "../../http-wrapper.service";
import { GlobalConstants } from "../../common/global.constants";
import * as _ from "lodash";

@Injectable()
export class UserService {

    private userObs = new Subject();
    constructor(private http: HttpWrapperService) { }

    public register(userRegistrationForm:any): Observable<User> {
        let url = `${GlobalConstants.API_ENDPOINT}/users`;
        let options = new RequestOptions({
        headers: new Headers({
            'Content-Type': 'application/json'
            })
        });

        return this.http.post(url, userRegistrationForm, options)
            .map((r: Response) => r.json())
            .catch((error: any) => Observable.throw(error || 'Server error'));
    }

    public getUser():Observable<User> {
        let url = `${GlobalConstants.API_ENDPOINT}/users/me`;

        return this.http.authGet(url)
            .map((res: Response) => res.json())
            .catch((error:any) => Observable.throw(error || 'Unknown Server error'));
    }

    public update(userUpdateForm:any, user: User): Observable<User> {
      let url = `${GlobalConstants.API_ENDPOINT}/users/${user.id}`;
      let options = new RequestOptions({
        headers: new Headers({
          'Content-Type': 'application/json'
        })
      });
      return this.http.authPut(url, userUpdateForm, options)
        .map((r: Response) => r.json())
        .catch((error: any) => Observable.throw(error || 'Server error'));
    }

    syncUser(user: User) {
      this.userObs.next(user);
    }

    getSyncUser(): Subject<User> {
      return this.userObs;
    }

}
