import { Component, OnInit } from "@angular/core";
import { AuthService } from "../core/auth.service";
import { DecodedJwtDto } from "../common/dto/decoded-jwt.dto";
import { Router, ActivatedRoute } from "@angular/router";
import { JwtHelper } from "angular2-jwt";
import { TranslateService } from '@ngx-translate/core';
import {AppConfigService} from "../common/service/app-config.service";

@Component({
  selector: 'app-dashboard',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  token: DecodedJwtDto;
  currentLang: string = 'en';

  constructor(private auth: AuthService, private router: Router, private translateService: TranslateService, ) {
    let helper = new JwtHelper();

    auth.loginConfirmer.subscribe(() => {
      this.token = helper.decodeToken(auth.token.access_token);
    });

    auth.logoutConfirmer.subscribe(() => {
      console.log('Token cleared');
      this.token = null;
    });
  }

  ngOnInit() {
    //this.initializeMetisMenu();
  }

  public logout(): void {
    this.auth.unauthenticate();
    this.router.navigate(['/main']);
  }

  changeLang( lang:string ):void {
    this.currentLang = lang;
    this.translateService.use(this.currentLang);
  }


}
