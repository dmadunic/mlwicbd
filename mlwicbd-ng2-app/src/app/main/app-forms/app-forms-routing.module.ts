import {Routes, RouterModule} from "@angular/router";
import {NgModule} from "@angular/core";
import {AppFormsComponent} from "./app-forms.component";

/**
 * Created by jdolanski on 24.01.17..
 */
const appFormsRoutes: Routes = [
  {path: '', component: AppFormsComponent}
];

@NgModule({
  imports: [RouterModule.forChild(appFormsRoutes)],
  exports: [RouterModule]
})
export class AppFormsRoutingModule {

}
