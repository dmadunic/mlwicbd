import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppFormsComponent } from './app-forms.component';
import {AppFormsRoutingModule} from "./app-forms-routing.module";

@NgModule({
  imports: [
    CommonModule,
    AppFormsRoutingModule
  ],
  declarations: [AppFormsComponent]
})
export class AppFormsModule { }
