import { Component, ViewChild, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { TranslateService } from '@ngx-translate/core';
import { ToastyService } from "ng2-toasty";
import { CustomValidators } from 'ng2-validation';
import { Country } from "../../common/dto/country.dto";
import { User } from "../../common/dto/user.dto";
import { FieldError } from "../../common/dto/error-field.dto";
import { FieldErrorsMap } from "../../common/field-error-map";
import { MultipleErrorsResponse } from "../../common/dto/error-mulitple.dto";
import { FieldValidationErrorWidget } from "../../common/widget/field-validation-error.widget";
import * as errorUtils from '../../common/http-error-utils';
import { CountryService } from "../../common/service/country-service";
import { UserService } from "../users/users.service";
import { TermsOfServiceModal } from "./terms-of-service.modal";
import {UserFormComponent} from "../../shared/user-form/user-form.component";
import {UserFormSettings} from "../../common/form/user-form.settings";

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css'],

})
export class RegistrationComponent implements OnInit {

  @ViewChild(TermsOfServiceModal) tosModal;

  formSettings: UserFormSettings = new UserFormSettings();

  userForm: FormGroup;
  countries: Country[];
  registredUser: User = null;
  showForm:boolean = true;

  serverErrors:FieldErrorsMap = null;
  errorAlertMessage:string = null;

  constructor(
    private fb: FormBuilder,
    private countryService: CountryService,
    private userService: UserService,
    private translateService: TranslateService,
    private toastyService: ToastyService,
  ) {
    this.formSettings.submitBtnText="Sign In";
    this.formSettings.submitBtnIcon="fa fa-pencil";
    this.formSettings.submitButtonDisabled=false;
    this.formSettings.controlVisible.password=false;
  }

  ngOnInit() {
    this.buildForm();
    this.countryService.fetchAll().subscribe(
      countries => {
        this.countries = countries.content;
      },
      err => {
        console.log("FETCH countries failed: " + err);
      }
    );

  }

  buildForm():void {
      this.userForm = this.fb.group({
        username: ['', [ Validators.required, Validators.maxLength(30), Validators.minLength(5) ] ],
        firstName: ['', [ Validators.required, Validators.maxLength(40) ] ],
        lastName: ['', [ Validators.required, Validators.maxLength(40) ] ],
        address: ['', [ Validators.required, Validators.maxLength(80) ] ],
        postalCode: ['', [ Validators.required, Validators.maxLength(20) ] ],
        place: ['', [ Validators.required, Validators.maxLength(80) ] ],
        countryCode: [null, [ Validators.required ]],
        email: ['', [ Validators.required, CustomValidators.email ]],
        mobile: ['', [ Validators.required ]],
        phone: ['', [ Validators.required ]]
      });
  }

  public onNotify(form: FormGroup){
    this.userForm = form;
    this.submitRegistration();
  }

  public submitRegistration():void {
    console.log("registering new user=" + this.userForm.value);
    this.formSettings.submitButtonDisabled = true;
    this.userService.register(this.userForm.value).subscribe(
      result => {
            this.registredUser = result;
            this.formSettings.submitButtonDisabled = false;
            this.showForm = false;
        },
        err => {
            this.formSettings.submitButtonDisabled = false;
            console.error("Failed to register new User err=" + err);
            if (!errorUtils.isGeneralServerError(err)) {
                this.translateService.get("mlEntry.save.failed").subscribe((translatedMsg: string) => {
                  this.toastyService.warning(translatedMsg);
                });
                let error = errorUtils.handleRestApiErrorResponse(err);
                if (error instanceof MultipleErrorsResponse) {
                  // izvuci field errors ...
                  this.serverErrors = new FieldErrorsMap(error.errors);
                }
                this.errorAlertMessage = error.message;
            }
        }
    );
  }

  public showTermsOfService():void {
    console.log("Terms of service modal!");
    this.tosModal.showModal();
  }

}
