import { Component, OnInit, Input } from '@angular/core';
import { Country } from "../../common/dto/country.dto";
import { User } from "../../common/dto/user.dto";

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html'
})
export class UserDetailsComponent implements OnInit {
    
  @Input() user:User;

  constructor() { 
    //
  }

  ngOnInit() {
    // so far nothing ....
  }

}    