import { Component, ViewChild } from "@angular/core";
import {ModalDirective} from "ngx-bootstrap";

/**
 * Modal Dialog for display of Terms of Service.
 *
 * @author dmadunic
 */
@Component({
  selector: 'terms-of-service',
  templateUrl: './terms-of-service.modal.html'
})
export class TermsOfServiceModal {

  @ViewChild('tosModal') public tosModal: ModalDirective;

  constructor(
    ) {
      //
  }

  public showModal():void {
    this.tosModal.show();
  }

  public hide():void {
    this.tosModal.hide();
  }

}
