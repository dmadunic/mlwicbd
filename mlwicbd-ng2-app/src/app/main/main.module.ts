import { NgModule } from "@angular/core";
import { SharedModule } from "../shared/shared.module";
import { MainComponent } from "./main.component";
import { MainRoutingModule } from "./main-routing.module";
import { SidebarComponent } from "./layout/sidebar.component"
import { BlankComponent } from "../shared/blank/blank.component";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { RegistrationComponent } from "./registration/registration.component";
import { UserDetailsComponent } from "./registration/user-details.component";
import { TermsOfServiceModal } from "./registration/terms-of-service.modal";
import { CountryService } from "../common/service/country-service";
import { UserService } from "./users/users.service";
import {AppConfigService} from "../common/service/app-config.service";
import { FooterComponent } from './footer/footer.component';
import {MomentModule} from "angular2-moment";
import {BsDropdownModule, ModalModule} from "ngx-bootstrap";
import {UserFormModule} from "../shared/user-form/user-form.module";

@NgModule({
  imports: [
    SharedModule,
    BsDropdownModule.forRoot(),
    ModalModule.forRoot(),
    MainRoutingModule,
    MomentModule,
    UserFormModule
  ],
  declarations: [
    MainComponent,
    SidebarComponent,
    DashboardComponent,
    RegistrationComponent,
    UserDetailsComponent,
    TermsOfServiceModal,
    FooterComponent
  ],
   providers: [
    CountryService,
    UserService,
    AppConfigService
   ]
})
export class MainModule { }
