import { Component } from "@angular/core";
import { Router, NavigationStart, NavigationEnd, NavigationCancel, NavigationError } from "@angular/router";
import { SlimLoadingBarService } from "ng2-slim-loading-bar";
import { TranslateService } from '@ngx-translate/core';
import { GlobalConstants } from "./common/global.constants";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(
    private router: Router, 
    private loader: SlimLoadingBarService,
    private translate: TranslateService
    ) {
    //
    router.events.subscribe(event => {
      this.navigationInterceptor(event);
    });

    // this language will be used as a fallback when a translation isn't found in the current language
    translate.setDefaultLang(GlobalConstants.DEFAULT_LANG);

    // the lang to use, if the lang isn't available, it will use the current loader to get them
    translate.use(GlobalConstants.DEFAULT_LANG);

   }

   private navigationInterceptor(event): void {
    if (event instanceof NavigationStart) {
      this.loader.start();
    }
    if (event instanceof NavigationEnd) {
      this.loader.complete();
    }

    if (event instanceof NavigationCancel) {
      this.loader.reset();
    }
    if (event instanceof NavigationError) {
      this.loader.reset();
    }
  } 

}
