// https://devhub.ag04.com/mlwicbd-rest

export const environment = {
  production: true,
  app_base: '/mlwicbd/',
  api: {
    url: 'https://devhub.ag04.com/mlwicbd-rest',
    version: 'v1'
  },
  token_provider: 'https://devhub.ag04.com/mlwicbd-rest',
  loadExtraConfiguration : true,
  client: {
    id: 'mlwicbd_app',
    secret: 'secretXxxKey'
  },
  token_storage: {
    jwt_object: 'mlwicbd-ng2-jwt-obj',
    remember_me: 'mlwicbd-ng2-remember_me'
  },
  build_info: {
    buildVersion: '0.2.0',
    buildDate: 'Mon Jun 26 2017 10:23:09 GMT+0200 (CEST)'
  }
};
