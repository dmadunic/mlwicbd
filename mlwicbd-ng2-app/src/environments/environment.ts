// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  app_base: '/',
  api: {
    url: 'http://localhost:8080/mlwicbd-rest',
    version: 'v1'
  },
  token_provider: 'http://localhost:8080/mlwicbd-rest',
  loadExtraConfiguration : true,
  client: {
    id: 'mlwicbd_app',
    secret: 'secretXxxKey'
  },
  token_storage: {
    jwt_object: 'mlwicbd-ng2-dev-jwt-obj',
    remember_me: 'mlwicbd-ng2-dev-remember_me'
  },
  build_info: {
    buildVersion: 'DEV',
    buildDate: '2017-03-25T15:30:21'
  }
};
