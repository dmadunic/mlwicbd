// https://devhub.ag04.com/mlwicbd-rest

export const environment = {
  production: true,
  app_base: '/mlwicbd/',
  api: {
    url: 'https://devhub.ag04.com/mlwicbd-rest',
    version: 'v1'
  },
  token_provider: 'https://devhub.ag04.com/mlwicbd-rest',
  loadExtraConfiguration : true,
  client: {
    id: 'mlwicbd_app',
    secret: 'secretXxxKey'
  },
  token_storage: {
    jwt_object: 'mlwicbd-ng2-test-jwt-obj',
    remember_me: 'mlwicbd-ng2-test-remember_me'
  },
  build_info: {
    buildVersion: '0.2.0',
    buildDate: 'Fri May 05 2017 14:23:46 GMT+0200 (CEST)'
  }
};
