package org.mlwicbd.rest.mapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mlwicbd.dao.repository.CountryRepository;
import org.mlwicbd.model.CountryStubFactory;
import org.mlwicbd.model.User;
import org.mlwicbd.model.UserStubFactory;
import org.mlwicbd.rest.domain.form.user.UpdateUserForm;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.HashMap;
import java.util.Map;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.when;

/**
 * Created by dmadunic on 10/04/16.
 */
@RunWith(MockitoJUnitRunner.class)
public class UpdateUserFormMapperTest {

    @InjectMocks
    private UpdateUserFormMapper mapper =  new UpdateUserFormMapper();

    @Mock
    PasswordEncoder passwordEncoder;

    @Mock
    CountryRepository countryRepository;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
        when(countryRepository.findByCode("191")).thenReturn(CountryStubFactory.countryHr());
        when(countryRepository.findByCode("276")).thenReturn(CountryStubFactory.countryDe());
        when(passwordEncoder.encode("secret1")).thenReturn("$2a$10$.oQmL2hIYJ7OA.P4MH1ZE.XbzQSKeKLLglioxBUJ7njAa.HBW06Yi");
    }

    @Test
    public void mapWhenEntireFormIsEmpty_thenNoPropertyIsChanged() {
        // arrange ...
        UpdateUserForm form = new UpdateUserForm();
        User user = UserStubFactory.dmadunic();
        Map<String, Object> configs = new HashMap<String, Object>();
        configs.put(mapper.USER_KEY, user);

        // act ...
        User mappedUser = mapper.mapToEntity(form, null, configs);

        // assert ...
        assertThat(mappedUser.getUsername()).isEqualTo("domagoj.madunic");
        assertThat(mappedUser.getEmail()).isEqualTo("domagoj.madunic@gmail.com");
        assertThat(mappedUser.getPassword()).isEqualTo(UserStubFactory.DEFAULT_PASSWORD);
        assertThat(mappedUser.getFirstName()).isEqualTo("Domagoj");
        assertThat(mappedUser.getLastName()).isEqualTo("Madunić");
        assertThat(mappedUser.getUserProfile()).isNotNull();
        assertThat(mappedUser.getUserProfile().getPlace()).isEqualTo("Zagreb");
        assertThat(mappedUser.getUserProfile().getPostalCode()).isEqualTo("10000");
        assertThat(mappedUser.getUserProfile().getPhone()).isEqualTo("01 4616 121");
        assertThat(mappedUser.getUserProfile().getMobile()).isEqualTo("+385 91 182 1572");
        assertThat(mappedUser.getUserProfile().getAddress()).isEqualTo("A. Bauera 29");
        assertThat(mappedUser.getUserProfile().getCountry().getCode()).isEqualTo("191");
    }

    @Test
    public void mapWhenEntireFormIsSet_thenAllPropertiesAreChanged() {
        // arrange ...
        UpdateUserForm form = new UpdateUserForm();
        form.setUsername("dmadunic");
        form.setPassword("secret1");
        form.setPassword2("secret1");
        form.setMobile("+385 99 199 2000");
        form.setPhone("01 111 2222");
        form.setPlace("Dortmund");
        form.setCountryCode("276");
        form.setPostalCode("A-10000");
        form.setAddress("Hindenburg strasse 23");
        form.setEmail("domagoj.madunic2@gmail.com");
        form.setFirstName("Domagoj2");
        form.setLastName("Madunić2");

        User user = UserStubFactory.dmadunic();
        Map<String, Object> configs = new HashMap<String, Object>();
        configs.put(mapper.USER_KEY, user);

        // act ...
        User mappedUser = mapper.mapToEntity(form, null, configs);

        // assert ...
        assertThat(mappedUser.getUsername()).isEqualTo("dmadunic");
        assertThat(mappedUser.getEmail()).isEqualTo("domagoj.madunic2@gmail.com");
        assertThat(mappedUser.getPassword()).isEqualTo("$2a$10$.oQmL2hIYJ7OA.P4MH1ZE.XbzQSKeKLLglioxBUJ7njAa.HBW06Yi");
        assertThat(mappedUser.getFirstName()).isEqualTo("Domagoj2");
        assertThat(mappedUser.getLastName()).isEqualTo("Madunić2");
        assertThat(mappedUser.getUserProfile()).isNotNull();
        assertThat(mappedUser.getUserProfile().getPlace()).isEqualTo("Dortmund");
        assertThat(mappedUser.getUserProfile().getPostalCode()).isEqualTo("A-10000");
        assertThat(mappedUser.getUserProfile().getPhone()).isEqualTo("01 111 2222");
        assertThat(mappedUser.getUserProfile().getMobile()).isEqualTo("+385 99 199 2000");
        assertThat(mappedUser.getUserProfile().getAddress()).isEqualTo("Hindenburg strasse 23");
        assertThat(mappedUser.getUserProfile().getCountry().getName()).isEqualToIgnoringCase("GERMANY");
    }

}
