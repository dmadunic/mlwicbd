package org.mlwicbd.rest.mapper.dto;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mlwicbd.model.MilitaryUnit;
import org.mlwicbd.model.Territory;
import org.mlwicbd.model.UnitType;
import org.mlwicbd.rest.domain.dto.MilitaryUnitDto;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.fest.assertions.Assertions.assertThat;

/**
 * Created by dmadunic on 17/04/16.
 */
public class MilitaryUnitMapperTest {

    MilitaryUnitMapper mapper = new MilitaryUnitMapperImpl();

    @Test
    public void shouldApplyConversions() {
        MilitaryUnit unit = new MilitaryUnit();
        unit.setId(1L);
        unit.setName("Unit name");
        unit.setDescription("Unit description");
        unit.setUnitType(unitType());
        unit.setTerritory(territory());

        MilitaryUnit unitParent = new MilitaryUnit();
        unitParent.setId(2L);
        unitParent.setName("Parent unit name");
        unitParent.setDescription("Parent Unit description");
        unitParent.setUnitType(unitType());
        unitParent.setTerritory(territory());

        unit.setParentUnit(unitParent);

        //MilitaryUnitDto target = MilitaryUnitMapper.INSTANCE.militaryUnitToMilitaryUnitDto(unit);

        MilitaryUnitDto target = mapper.militaryUnitToMilitaryUnitDto(unit);

        assertThat(target).isNotNull();
        assertThat(target.getId()).isEqualTo(1L);
        assertThat(target.getName()).isEqualTo("Unit name");
        assertThat(target.getDescription()).isEqualTo("Unit description");
        assertThat(target.getParentUnitId()).isEqualTo(2L);
        assertThat(target.getTerritoryCode()).isEqualTo("CS_ML_FR");
        assertThat(target.getUnitType()).isEqualTo("REGIMENT");
    }

    private UnitType unitType() {
        UnitType type = new UnitType();
        type.setId(11L);
        type.setName("REGIMENT");
        type.setDescription("Regiment type description");
        return type;
    }

    private Territory territory() {
        Territory territory = new Territory();
        territory.setId(22L);
        territory.setName("Croatian-Slavonian Military Frontier");
        territory.setCode("CS_ML_FR");
        return territory;
    }
}
