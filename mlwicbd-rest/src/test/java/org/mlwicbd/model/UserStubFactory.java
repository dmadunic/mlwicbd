package org.mlwicbd.model;

/**
 * Created by dmadunic on 10/04/16.
 */
public class UserStubFactory {
    public static final String DEFAULT_PASSWORD = "$2a$08$OX/aKKb08DCChvx0HWM6xuRu4BPmH42SuJyoyBW2FNGtEwRIPHhP.";

    public static User dmadunic() {
        User user = new User();
        user.setId(1L);
        user.setUsername("domagoj.madunic");
        user.setFirstName("Domagoj");
        user.setLastName("Madunić");
        user.setEnabled(true);
        user.setPassword("$2a$08$OX/aKKb08DCChvx0HWM6xuRu4BPmH42SuJyoyBW2FNGtEwRIPHhP.");
        user.setEmail("domagoj.madunic@gmail.com");

        UserProfile userProfile = new UserProfile();
        userProfile.setAddress("A. Bauera 29");
        userProfile.setCountry(CountryStubFactory.countryHr());
        userProfile.setPostalCode("10000");
        userProfile.setPlace("Zagreb");
        userProfile.setMobile("+385 91 182 1572");
        userProfile.setPhone("01 4616 121");
        userProfile.setUser(user);
        user.setUserProfile(userProfile);

        return user;
    }
}
