package org.mlwicbd.model;

/**
 * Created by dmadunic on 10/04/16.
 */
public class CountryStubFactory {

    public static Country countryHr() {
        Country country = new Country();
        country.setA2code("HR");
        country.setA3code("HRV");
        country.setCode("191");
        country.setName("HRVATSKA");
        return country;
    }

    public static Country countryDe() {
        Country country = new Country();
        country.setA2code("DE");
        country.setA3code("DEU");
        country.setCode("276");
        country.setName("GERMANY");
        return country;
    }

}
