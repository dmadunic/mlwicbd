package org.mlwicbd.rest.validator;

import org.mlwicbd.dao.repository.MilitaryRankRepository;
import org.mlwicbd.dao.repository.PersonRepository;
import org.mlwicbd.dao.repository.ProfessionRepository;
import org.mlwicbd.dao.repository.ReligiousDenominationRepository;
import org.mlwicbd.rest.domain.form.musterlist.MusterListEntryForm;
import org.mlwicbd.service.MilitaryRankService;
import org.mlwicbd.service.MusterListEntryService;
import org.mlwicbd.service.ReligiousDenominationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


/**
 * @author idekic
 * @since 04-Aug-16.
 */
@Component
public class MusterListEntryFormValidator implements Validator {

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private MilitaryRankService militaryRankService;

    @Autowired
    private ReligiousDenominationService religiousDenominationService;

    //@Autowired
    //private MusterListEntryService musterListEntryService;

    @Override
    public boolean supports(Class<?> clazz) {
        return MusterListEntryForm.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        MusterListEntryForm form = (MusterListEntryForm) target;

        if (form.getPersonId() != null && !personRepository.exists(form.getPersonId())) {
            errors.rejectValue("personId", "mlentry.person-id.invalid", "No Person with that ID exists.");
        }

        if (form.getFirstName() == null) {
            errors.rejectValue("firstName", "mlentry.firstName.null", "First name may not be null.");
        }

        if (form.getLastName() == null) {
            errors.rejectValue("lastName", "mlentry.lastName.null", "Last name may not be null.");
        }

        if (form.getRankId() == null) {
            errors.rejectValue("rankId", "mlentry.rank-id.null", "MilitaryRank ID may not be null.");
        } else if (!militaryRankService.exists(form.getRankId())) {
            errors.rejectValue("rankId", "mlentry.rank-id.invalid", "No MilitaryRank with that ID exists.");
        }

        if (form.getReligionId() == null) {
            errors.rejectValue("religionId", "mlentry.religion-id.null", "Religion Id may not be null.");
        } else if (!religiousDenominationService.exists(form.getReligionId())) {
            errors.rejectValue("religiousDenominationCode", "mlentry.religion-id.invalid", "No ReligiousDenomination with supplied ID exists.");
        }

        if (form.getMaritalStatus() == null) {
            errors.rejectValue("maritalStatus", "mlentry.marital-status.null", "MaritalStatus may not be null.");
        }
    }
}
