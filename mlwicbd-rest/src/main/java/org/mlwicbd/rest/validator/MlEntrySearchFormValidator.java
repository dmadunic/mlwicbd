package org.mlwicbd.rest.validator;

import org.mlwicbd.rest.domain.form.musterlist.MlEntrySearchForm;
import org.mlwicbd.rest.domain.form.musterlist.MusterListEntryForm;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * Created by dmadunic on 05/10/2016.
 */
@Component
public class MlEntrySearchFormValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return MlEntrySearchForm.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        MlEntrySearchForm form = (MlEntrySearchForm) target;
        if (form.isEmpty()) {
            errors.reject("empty.form", "Form may not be empty at least one search parameter must be set.");
            return;
        }
    }
}
