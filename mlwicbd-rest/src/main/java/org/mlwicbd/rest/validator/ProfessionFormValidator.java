package org.mlwicbd.rest.validator;

import org.mlwicbd.model.person.Profession;
import org.mlwicbd.rest.domain.form.ProfessionForm;
import org.mlwicbd.service.ProfessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * Created by tgreblicki on 08.05.17..
 */
@Component
public class ProfessionFormValidator implements Validator {
    @Autowired
    private ProfessionService professionService;

    @Override
    public boolean supports(Class<?> clazz) {
        return ProfessionForm.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ProfessionForm professionForm = (ProfessionForm) target;
        // validate
        validateCode(professionForm, errors);
    }

    private void validateCode(ProfessionForm professionForm, Errors errors) {
        Profession profession = professionService.findByCode(professionForm.getCode());
        if ( profession != null && !profession.getId().equals(professionForm.getId())){
            errors.rejectValue("code", "prof.code.not.unique", "Profession code must be unique!");
        }
    }
}
