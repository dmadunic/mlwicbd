package org.mlwicbd.rest.validator;

import org.mlwicbd.rest.domain.form.musterlist.MusterListForm;
import org.mlwicbd.service.MilitaryUnitService;
import org.mlwicbd.service.MusterListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Created by dmadunic on 19/09/2016.
 */
@Component
public class MusterListFormValidator implements Validator {

    public static final int MIN_YEAR = 1527;
    public static final int MAX_YEAR = 1883;

    @Autowired
    MilitaryUnitService unitService;

    @Autowired
    MusterListService musterListService;

    @Override
    public boolean supports(Class<?> clazz) {
        return MusterListForm.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        MusterListForm form = (MusterListForm) target;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "title", "ml.title.empty", "MusterList title may not be empty.");

        validateReferenceCode(form, errors);
        validateIfYearIsInRange(form, errors);
        validateIfMonthIsInRange(form, errors);
        validateUnitId(form, errors);
        validateIfUnitYearMonthUnique(form, errors);
    }

    public void validateIfUnitYearMonthUnique(MusterListForm form, Errors errors) {
        Long id = musterListService.exists(form.getUnitId(), form.getYear(), form.getMonth());
        if (id == null) {
            return;
        }
        if (!id.equals(form.getId())) {
            errors.rejectValue("unitId", "ml.unitId-year-month.not-unique", "MusterList with this combination of: unitId, year and month already exists?!");
        }
    }

    private void validateIfYearIsInRange(MusterListForm form, Errors errors) {
        //
    }

    private void validateIfMonthIsInRange(MusterListForm form, Errors errors) {
        //
    }

    private void validateReferenceCode(MusterListForm form, Errors errors) {
        if (!StringUtils.hasText(form.getReferenceCode())) {
            errors.rejectValue("referenceCode", "ml.referenceCode.empty", "ReferenceCode may not be empty.");
            return;
        }
        Long id = musterListService.exists(form.getReferenceCode());
        if (id == null) {
            return;
        }
        if (!id.equals(form.getId())) {
            errors.rejectValue("referenceCode", "ml.referenceCode.not-unique", "MusterList with that referenceCode already exists?!");
        }
    }

    private void validateUnitId(MusterListForm form, Errors errors) {
        if (form.getUnitId() == null) {
            errors.rejectValue("unitId", "ml.unitId.null", "UnitId may NOT be NULL.");
            return;
        }
        if (!unitService.exists(form.getUnitId())) {
            errors.rejectValue("unitId", "ml.unitId.unkonwn", "Unit with supplied unitId does not exists?!");
        }
    }

}
