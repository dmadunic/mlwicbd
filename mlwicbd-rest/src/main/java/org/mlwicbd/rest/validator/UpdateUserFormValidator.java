package org.mlwicbd.rest.validator;

import org.mlwicbd.model.User;
import org.mlwicbd.rest.domain.form.user.UpdateUserForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;

/**
 *
 * Created by dmadunic on 10/04/16.
 */
@Component
public class UpdateUserFormValidator extends CreateUserFormValidator {

    @Autowired
    PasswordEncoder passwordEncoder;

    @Override
    public void validate(Object target, Errors errors) {
        UpdateUserForm form = (UpdateUserForm) target;

        // 1. username ...
        if (form.getUsername() != null) {
            validateUsername(form.getUsername(), errors);
        }

        // 2. email ...
        if (form.getEmail() != null) {
            validateEmail(form.getEmail(), errors);
        }

        // 3. firstName
        if (form.getFirstName() != null) {
            validateFirstName(form.getFirstName(), errors);
        }

        // 4. lastName
        if (form.getLastName() != null) {
            validateLastName(form.getLastName(), errors);
        }

        // 5. phone ...
        if (form.getPhone() != null) {
            validatePhone(form.getPhone(), errors);
        }

        // 6. mobile ...
        if (StringUtils.hasText(form.getMobile())) {
            validateMobile(form.getMobile(), errors);
        }

        // 7. address
        if (form.getAddress() != null) {
            validateAddress(form.getAddress(), errors);
        }

        // 8. postalCode
        if (form.getPostalCode() != null) {
            validatePostalCode(form.getPostalCode(), errors);
        }

        // 9. place
        if (form.getPlace() != null) {
            validatePlace(form.getPlace(), errors);
        }

        // 10. countryCode
        if (form.getCountryCode() != null) {
            validateCountryCode(form.getCountryCode(), errors);
        }

        // 11. passwords ...
        if (form.getPassword() != null || form.getPassword2() != null) {
            validatePasswords(form.getPassword(), form.getPassword2(), errors);
        }
    }

    public void extraValidations(Long id, UpdateUserForm form, Errors errors) {
        if (StringUtils.hasText(form.getUsername())) {
            validateUsernameUnique(id, form.getUsername(), errors);
        }

        if (StringUtils.hasText(form.getEmail())) {
            validateEmailUnique(id, form.getEmail(), errors);
        }

        if (form.getOldPassword() != null){
            validateOldUserPassword(id, form.getOldPassword(), errors);
        }
    }

    //--- private methods ----------------------------------

    private void validatePasswords(String password, String password2, Errors errors) {
        if (!StringUtils.hasText(password)) {
            errors.rejectValue("password", "password.empty", "Password may NOT be empty.");
            return;
        }
        if (!password.equals(password2)) {
            errors.rejectValue("password2", "password.not-same", "Entered passwords do NOT match.");
            return;
        }

        if (password.length() < 6) {
            errors.rejectValue("password", "password.too-short", "Password must be at least 6 characters long.");
            return;
        }

        boolean upper = false;
        boolean lower = false;
        boolean number = false;
        for (char c : password.toCharArray()) {
            if (Character.isUpperCase(c)) {
                upper = true;
            } else if (Character.isLowerCase(c)) {
                lower = true;
            } else if (Character.isDigit(c)) {
                number = true;
            }
        }
        if (!upper) {
            errors.rejectValue("password", "password.uppercase", "Password must contain at least one uppercase character.");
        }
        if (!lower) {
            errors.rejectValue("password", "password.lowercase", "Password must contain at least one lowercase character.");
        }
        if (!number) {
            errors.rejectValue("password", "password.number", "Password must contain at least one number.");
        }
    }

    private void validateUsernameUnique(Long id, String username, Errors errors) {
        User user = userRepository.findByUsername(username);
        if (user == null) {
            return;
        }

        if (!user.getId().equals(id)) {
            errors.rejectValue("username", "username.not-unique", "User with that username already exists");
        }
    }

    private void validateEmailUnique(Long id, String email, Errors errors) {
        User user = userRepository.findByEmail(email);
        if (user == null) {
            return;
        }

        if (!user.getId().equals(id)) {
            errors.rejectValue("email", "email.not-unique", "User with that email already exists");
        }
    }

    private void validateOldUserPassword(Long id, String oldPassword, Errors errors) {
        User user = userRepository.findOne(id);
        if (!passwordEncoder.matches(oldPassword,user.getPassword())){
            errors.rejectValue("oldPassword", "password.dont-match", "You have entered wrong password");
        }
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return UpdateUserForm.class.equals(clazz);
    }

}
