package org.mlwicbd.rest.validator;

import org.mlwicbd.rest.domain.form.PersonForm;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


/**
 * @author idekic
 * @since 04-Aug-16.
 */
@Component
public class PersonFormValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return PersonForm.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        PersonForm form = (PersonForm) target;

        //validate
    }
}
