package org.mlwicbd.rest.validator;

import org.mlwicbd.rest.domain.form.musterlist.MlEntryMergeSearchForm;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * Created by tgreblicki on 22.05.17..
 */
@Component
public class MlEntryMergeSearchFormValidator implements Validator{

    @Override
    public boolean supports(Class<?> clazz) {
        return MlEntryMergeSearchForm.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        MlEntryMergeSearchForm form = (MlEntryMergeSearchForm) target;
        if (form.isEmpty()) {
            errors.reject("empty.form", "Form may not be empty at least one search parameter must be set.");
            return;
        }
    }
}
