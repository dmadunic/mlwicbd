package org.mlwicbd.rest.validator;

import org.mlwicbd.dao.repository.CountryRepository;
import org.mlwicbd.dao.repository.UserRepository;
import org.mlwicbd.rest.domain.form.user.CreateUserForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by dmadunic on 03/04/16.
 */
@Component
public class CreateUserFormValidator implements Validator {
    private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    @Autowired
    protected UserRepository userRepository;

    @Autowired
    protected CountryRepository countryRepository;

    private Pattern emailPattern;

    /**
     * Validate the supplied {@code target} object, which must be
     * of a {@link Class} for which the {@link #supports(Class)} method
     * typically has (or would) return {@code true}.
     * <p>The supplied {@link Errors errors} instance can be used to report
     * any resulting validation errors.
     *
     * @param target the object that is to be validated (can be {@code null})
     * @param errors contextual state about the validation process (never {@code null})
     *
     */
    @Override
    public void validate(Object target, Errors errors) {
        CreateUserForm form = (CreateUserForm) target;

        // 1. username ...
        validateUsername(form.getUsername(), errors);
        if (StringUtils.hasText(form.getUsername())) {
            validateUsernameUniqe(form.getUsername(), errors);
        }

        // 2. email ...
        validateEmail(form.getEmail(), errors);
        if (StringUtils.hasText(form.getEmail())) {
            validateEmailUnique(form.getEmail(), errors);
        }

        // 3. firstName
        validateFirstName(form.getFirstName(), errors);

        // 4. lastName
        validateLastName(form.getLastName(), errors);

        // 5. phone ...
        validatePhone(form.getPhone(), errors);

        // 6. mobile ...
        validateMobile(form.getMobile(), errors);

        // 7. address
        validateAddress(form.getAddress(), errors);

        // 8. postalCode
        validatePostalCode(form.getPostalCode(), errors);

        // 9. place
        validatePlace(form.getPlace(), errors);

        // 10. countryCode
        validateCountryCode(form.getCountryCode(), errors);

    }

    protected void validateUsername(final String username, final Errors errors) {
        if (!StringUtils.hasText(username)) {
            errors.rejectValue("username", "username.empty", "Username may NOT be empty.");
            return;
        }
        // 1. validate username length ...
        if (username.length() < 5) {
            errors.rejectValue("username", "username.too-short", "Username must be at least five characters long.");
            return;
        }
        if (username.length() > 30) {
            errors.rejectValue("username", "username.too-long", "Username may not be longer than 30 characters.");
            return;
        }
    }

    protected void validateUsernameUniqe(final String username, final Errors errors) {
        // 1. validate username uniqueness ...
        int count = userRepository.countByUsername(username);
        if (count > 0) {
            errors.rejectValue("username", "username.not-unique", "User with that username already exists");
            return;
        }
    }

    protected void validateEmail(final String email, final Errors errors) {
        if (!StringUtils.hasText(email)) {
            errors.rejectValue("email", "email.empty", "Email may NOT be empty.");
            return;
        }
        if (email.length() > 80) {
            errors.rejectValue("email", "email.too-long", "email may not be longer than 80 characters.");
        }
        Pattern pattern = getEmailPattern();
        Matcher matcher = pattern.matcher(email);
        if (!matcher.matches()) {
            errors.rejectValue("email", "email.invalid", "Invalid value for email address");
            return;
        }
    }

    protected void validateEmailUnique(final String email, final Errors errors) {
        int count = userRepository.countByEmail(email);
        if (count > 0) {
            errors.rejectValue("email", "email.not-unique", "User with that email already exists");
            return;
        }
    }

    protected void validateCountryCode(final String countryCode, final Errors errors) {
        if (!StringUtils.hasText(countryCode)) {
            errors.rejectValue("countryCode", "countryCode.empty", "Country Code may NOT be empty.");
            return;
        }
        int count = countryRepository.countByCode(countryCode);
        if (count < 1) {
            errors.rejectValue("countryCode", "countryCode.unknown", "Unknown Country Code.");
            return;
        }
    }

    protected void validateFirstName(final String firstName, final Errors errors) {
        if (!StringUtils.hasText(firstName)) {
            errors.rejectValue("firstName", "firstName.empty", "First name may NOT be empty.");
            return;
        }
        if (firstName.length() > 40) {
            errors.rejectValue("firstName", "firstName.too-long", "First name may not be longer than 40 characters.");
        }
    }

    protected void validateLastName(final String lastName, final Errors errors) {
        if (!StringUtils.hasText(lastName)) {
            errors.rejectValue("lastName", "lastName.empty", "Last name may NOT be empty.");
            return;
        }
        if (lastName.length() > 40) {
            errors.rejectValue("lastName", "lastName.too-long", "Last name may not be longer than 40 characters.");
        }
    }

    protected void validateAddress(final String address, final Errors errors) {
        if (!StringUtils.hasText(address)) {
            errors.rejectValue("address", "address.empty", "Address may NOT be empty.");
            return;
        }
        if (address.length() > 80) {
            errors.rejectValue("address", "address.too-long", "Address name may not be longer than 80 characters.");
        }
    }

    protected void validatePostalCode(final String postalCode, final Errors errors) {
        if (!StringUtils.hasText(postalCode)) {
            errors.rejectValue("postalCode", "postalCode.empty", "Postal Code may NOT be empty.");
            return;
        }
        if (postalCode.length() > 40) {
            errors.rejectValue("postalCode", "postalCode.too-long", "Postal Code name may not be longer than 40 characters.");
        }
    }

    protected void validatePlace(final String place, final Errors errors) {
        if (!StringUtils.hasText(place)) {
            errors.rejectValue("place", "place.empty", "Place may NOT be empty.");
            return;
        }
        if (place.length() > 80) {
            errors.rejectValue("place", "place.too-long", "Place name may not be longer than 80 characters.");
        }
    }

    protected void validatePhone(final String phone, final Errors errors) {
        if (!StringUtils.hasText(phone)) {
            errors.rejectValue("phone", "phone.empty", "Phone number may NOT be empty.");
            return;
        }
        if (phone.length() > 30) {
            errors.rejectValue("phone", "phone.too-long", "Phone number may not be longer than 30 characters.");
        }
    }

    protected void validateMobile(final String mobile, final Errors errors) {
        if (!StringUtils.hasText(mobile)) {
            errors.rejectValue("mobile", "mobile.empty", "Mobile number may NOT be empty.");
            return;
        }
        if (mobile.length() > 30) {
            errors.rejectValue("mobile", "mobile.too-long", "Mobile number may not be longer than 30 characters.");
        }
    }

    //--- util methods --------------------------------------------------------

    protected Pattern getEmailPattern() {
        if (emailPattern == null) {
            emailPattern = Pattern.compile(EMAIL_PATTERN);
        }
        return emailPattern;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return CreateUserForm.class.equals(clazz);
    }
}
