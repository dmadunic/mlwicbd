package org.mlwicbd.rest;

import com.ag04.common.exception.ConcurrencyException;
import com.ag04.common.exception.EntityNotFoundException;
import com.ag04.common.exception.ValidationFailedException;
import com.ag04.common.web.domain.dto.BaseError;
import com.ag04.common.web.domain.dto.EntityConflictError;
import com.ag04.common.web.domain.dto.EntityError;
import com.ag04.common.web.domain.dto.MultipleErrorsResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.data.mapping.PropertyReferenceException;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dmadunic on 22/03/16.
 */
@ControllerAdvice
@Order(Ordered.LOWEST_PRECEDENCE)
public class GeneralRestExceptionHandler {
    private static final Logger LOG = LoggerFactory.getLogger(GeneralRestExceptionHandler.class);

    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    @ExceptionHandler(ValidationFailedException.class)
    @ResponseBody
    public MultipleErrorsResponse handleValidationFailedException(final Exception exception) {
        ValidationFailedException ex = (ValidationFailedException) exception;
        BindingResult errors = ex.getErrors();

        MultipleErrorsResponse response = new MultipleErrorsResponse("200", "error.validation.failed");

        for (org.springframework.validation.FieldError fieldError : errors.getFieldErrors()) {
            com.ag04.common.web.domain.dto.FieldError error = new com.ag04.common.web.domain.dto.FieldError(fieldError.getCode(), fieldError.getDefaultMessage(), null, fieldError.getField());
            response.addError(error);
        }
        return response;
    }

    @ResponseStatus (HttpStatus.CONFLICT)
    @ExceptionHandler(ConcurrencyException.class)
    @ResponseBody
    public EntityConflictError handleOptimisticLockingException(final Exception exception) {
        ConcurrencyException ex = (ConcurrencyException) exception;
        EntityConflictError error = new EntityConflictError();
        error.setCode("409");
        error.setMessage("error.entity.conflict");
        error.setCurrentVersion(ex.getCurrentVersion());
        error.setVersion(ex.getVersion());
        error.setEntityId(ex.getId());
        return error;
    }

    @ResponseStatus (HttpStatus.NOT_FOUND)
    @ExceptionHandler (EntityNotFoundException.class)
    @ResponseBody
    public EntityError handleEntityNotFoundException(final Exception exception) {
        EntityNotFoundException enf = (EntityNotFoundException) exception;
        EntityError rsp = new EntityError("501", "error.entiity.not-exists", enf.getEntityId());

        return rsp;
    }

}
