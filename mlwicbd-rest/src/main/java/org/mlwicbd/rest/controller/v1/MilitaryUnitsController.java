package org.mlwicbd.rest.controller.v1;

import org.apache.commons.lang3.StringUtils;
import org.mlwicbd.Constants;
import org.mlwicbd.criteria.MilitaryUnitSearchCriteria;
import org.mlwicbd.model.MilitaryUnit;
import org.mlwicbd.rest.RequestParamUtils;
import org.mlwicbd.rest.domain.dto.MilitaryUnitDto;
import org.mlwicbd.rest.service.MilitaryUnitFacade;
import org.mlwicbd.service.MilitaryUnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

/**
 * Created by dmadunic on 17/04/16.
 */
@CrossOrigin
@RestController
@RequestMapping(value = "/v1/militaryunits")
public class MilitaryUnitsController {

    public static final String DEFAULT_SORT_ATTRIBUTE = "name";

    @Autowired
    private MilitaryUnitFacade militaryUnitFacade;

    @RequestMapping(method = RequestMethod.GET)
    public final Page<MilitaryUnitDto> getUnits(
            @RequestParam(value = "unitType", required = false) String unitTypeCode,
            @RequestParam(value = "territory", required = false) String territoryCode,
            @RequestParam(value = "searchParam", required = false) String searchParam,
            @RequestParam(value = "parentUnitId", required = false) Long parentUnitId,
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "size", required = false) Integer size,
            @RequestParam(value = "sort", required = false) String sortAttributes) {

        // 1. crate pageable object from request params ...
        if (size == null) {
            size = Constants.DEFAULT_PAGE_SIZE;
        }
        if (StringUtils.isBlank(sortAttributes)) {
            sortAttributes = DEFAULT_SORT_ATTRIBUTE;
        }
        Pageable pageable = RequestParamUtils.pageable(page, size, sortAttributes);

        // 2. map params to searchCriteria ...
        MilitaryUnitSearchCriteria criteria = new MilitaryUnitSearchCriteria(unitTypeCode, territoryCode, searchParam, parentUnitId);

        // 3. perform search ...
        Page<MilitaryUnitDto> units = militaryUnitFacade.searchByCriteria(criteria, pageable);

        return units;
    }


}
