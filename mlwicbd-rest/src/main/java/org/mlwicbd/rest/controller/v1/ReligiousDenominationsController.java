package org.mlwicbd.rest.controller.v1;

import org.mlwicbd.Constants;
import org.mlwicbd.model.Country;
import org.mlwicbd.model.person.ReligiousDenomination;
import org.mlwicbd.rest.RequestParamUtils;
import org.mlwicbd.service.CountryService;
import org.mlwicbd.service.ReligiousDenominationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

/**
 * Created by dmadunic on 24/04/16.
 */
@CrossOrigin
@RestController
@RequestMapping(value = "/v1/denominations")
public class ReligiousDenominationsController {

    @Autowired
    private ReligiousDenominationService denominationService;

    @RequestMapping(method = RequestMethod.GET)
    public final Page<ReligiousDenomination> findAll(@RequestParam(value = "page", required = false) Integer page,
                                       @RequestParam(value = "size", required = false) Integer size,
                                       @RequestParam(value = "sort", required = false) String sortAttributes) {
        if (size == null) {
            size = Constants.DEFAULT_PAGE_SIZE;
        }

        Pageable pageable = RequestParamUtils.pageable(page, size, sortAttributes);
        Page<ReligiousDenomination> result = denominationService.findAll(pageable);
        return result;
    }

}
