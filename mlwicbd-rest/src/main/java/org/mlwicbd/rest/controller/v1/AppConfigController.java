package org.mlwicbd.rest.controller.v1;

import com.ag04.utils.web.ManifestReader;
import org.joda.time.LocalDateTime;
import org.mlwicbd.model.AppConfig;
import org.mlwicbd.service.AppConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by dmadunic on 24/08/15.
 */
@CrossOrigin
@RestController
@RequestMapping(value = "/v1/configs")
public class AppConfigController {
    private static final Logger LOG = LoggerFactory.getLogger(AppConfigController.class);

    @Autowired
    private ManifestReader manifestReader;

    @Autowired
    private AppConfigService appConfigService;

    @RequestMapping(method = RequestMethod.GET)
    public final List<AppConfig> findAll(final Model model) {
        LOG.debug("Fetching all app configs");
        List<AppConfig> configs = appConfigService.findAll();

        // map manifest version and build time to appConfigs
        configs.add(new AppConfig("buildVersion", manifestReader.getBuildVersion()));
        configs.add(new AppConfig("buildTime", manifestReader.getBuildTime()));

        return configs;
    }

    @RequestMapping(value = "/build", method = RequestMethod.GET)
    public final Map<String, String> findBuildInfo() {
        LOG.debug("Fetching app build info");
        Map<String, String> buildInfo = new HashMap<>();
        String buildVersion = manifestReader.getBuildVersion();
        if (!buildVersion.equals("no MANIFEST file found")) {
            buildInfo.put("buildVersion", buildVersion);
            buildInfo.put("buildTime", manifestReader.getBuildTime());
        }else{
           buildInfo.put("buildVersion", "DEV");
            buildInfo.put("buildTime", LocalDateTime.now().toString());
        }

        return buildInfo;
    }
}
