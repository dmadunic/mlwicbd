package org.mlwicbd.rest.controller.v1;

import com.ag04.common.exception.EntityNotFoundException;
import com.ag04.common.exception.ValidationFailedException;
import org.apache.commons.lang3.StringUtils;
import org.mlwicbd.Constants;
import org.mlwicbd.criteria.MusterListSearchCriteria;
import org.mlwicbd.model.musterlist.MusterList;
import org.mlwicbd.rest.RequestParamUtils;
import org.mlwicbd.rest.domain.dto.MusterListDto;
import org.mlwicbd.rest.domain.dto.MusterListEntryDto;
import org.mlwicbd.rest.domain.form.BaseSearchForm;
import org.mlwicbd.rest.domain.form.musterlist.MlEntryMergeSearchForm;
import org.mlwicbd.rest.domain.form.musterlist.MlEntrySearchForm;
import org.mlwicbd.rest.domain.form.musterlist.MusterListEntryForm;
import org.mlwicbd.rest.domain.form.musterlist.MusterListForm;
import org.mlwicbd.rest.service.MusterListEntryFacade;
import org.mlwicbd.rest.service.MusterListFacade;
import org.mlwicbd.rest.support.csv.MlCsvWriterFormatter;
import org.mlwicbd.rest.validator.MlEntryMergeSearchFormValidator;
import org.mlwicbd.rest.validator.MlEntrySearchFormValidator;
import org.mlwicbd.rest.validator.MusterListEntryFormValidator;
import org.mlwicbd.rest.validator.MusterListFormValidator;
import org.mlwicbd.service.MusterListService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.CsvMapWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.io.ICsvMapWriter;
import org.supercsv.prefs.CsvPreference;

import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.FileWriter;
import java.util.List;

/**
 * Created by dmadunic on 21/04/16.
 */
@CrossOrigin
@RestController
@RequestMapping(value = "/v1/musterlists")
public class MusterListController {
    private static final Logger LOG = LoggerFactory.getLogger(MusterListController.class);

    //TODO dmadunic: move this to constants file
    private static final String DEFAULT_SORT_ATTRIBUTE = "id,year,month,militaryUnit.parentUnit.id,militaryUnit.name";

    @Autowired
    private MusterListFacade musterListFacade;

    @Autowired
    private MusterListEntryFacade musterListEntryFacade;

    @Autowired
    private MusterListFormValidator mlFromValidator;

    @Autowired
    private MusterListEntryFormValidator musterListEntryFormValidator;

    @Autowired
    private MlEntrySearchFormValidator mlEntrySearchFormValidator;

    @Autowired
    private MlEntryMergeSearchFormValidator mlEntryMergeSearchFormValidator;

    @RequestMapping(method = RequestMethod.GET)
    public final Page<MusterListDto> getMusterLists(
            @RequestParam(value = "referenceCode", required = false) String referenceCode,
            @RequestParam(value = "year", required = false) Integer year,
            @RequestParam(value = "month", required = false) Integer month,
            @RequestParam(value = "unitId", required = false) Long unitId,
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "size", required = false) Integer size,
            @RequestParam(value = "sort", required = false) String sortAttributes) {

        // 1. crate pageable object from request params ...
        if (size == null) {
            size = Constants.DEFAULT_PAGE_SIZE;
        }

        Pageable pageable = RequestParamUtils.pageable(page, size, parseSortAttributes(sortAttributes));

        // 2. map params to searchCriteria ...
        MusterListSearchCriteria criteria = new MusterListSearchCriteria(referenceCode, unitId, year, month);

        // 3. perform search ...
        Page<MusterListDto> units = musterListFacade.searchByCriteria(criteria, pageable);

        return units;
    }

    private String parseSortAttributes(final String sortAttributes) {
        String parsedAttributes = DEFAULT_SORT_ATTRIBUTE;
        if ("-id".equals(sortAttributes)){
            parsedAttributes="-id";
        }
        else if ("id".equals(sortAttributes)) {
            parsedAttributes="id";
        }
        else if ("-year-month".equals(sortAttributes)) {
            parsedAttributes = "-year,-month,id";
        }
        else if ("year-month".equals(sortAttributes)) {
            parsedAttributes = "year,month,id";
        }
        else if ("-parentUnitName".equals(sortAttributes)){
            parsedAttributes =  "-militaryUnit.parentUnit.name,-militaryUnit.parentUnit.id,id";
        }
        else if ("parentUnitName".equals(sortAttributes)){
            parsedAttributes =  "militaryUnit.parentUnit.name,militaryUnit.parentUnit.id,id";
        }
        else if ("-unitName".equals(sortAttributes)) {
            parsedAttributes = "-militaryUnit.name,id";
        }
        else if ("unitName".equals(sortAttributes)) {
            parsedAttributes = "militaryUnit.name,id";
        }
        else if ("-unitType".equals(sortAttributes)) {
            parsedAttributes = "-militaryUnit.unitType,id";
        }
        else if ("unitType".equals(sortAttributes)) {
            parsedAttributes = "militaryUnit.unitType,id";
        }
        else if ("-title".equals(sortAttributes)) {
            parsedAttributes = "-title,id";
        }
        else if ("title".equals(sortAttributes)) {
            parsedAttributes = "title,id";
        }
        else if ("-referenceCode".equals(sortAttributes)) {
            parsedAttributes = "-referenceCode,id";
        }
        else if ("referenceCode".equals(sortAttributes)) {
            parsedAttributes = "referenceCode,id";
        }
        return parsedAttributes;
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public MusterListDto create(@Valid @RequestBody final MusterListForm musterListForm, final BindingResult results) {
        if (results.hasErrors()) {
            LOG.info("FAILED [Create] MusterListForm validation ---> rejecting: errors={}", results);
            throw new ValidationFailedException(results);
        }
        MusterListDto dto = musterListFacade.create(musterListForm);
        return dto;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public MusterListDto update(@PathVariable final Long id, @RequestBody final MusterListForm form, final BindingResult results) {
        form.setId(id);
        mlFromValidator.validate(form, results);
        if (results.hasErrors()) {
            LOG.info("FAILED [Update] MusterListForm validation ---> rejecting: errors={}", results);
            throw new ValidationFailedException(results);
        }
        MusterListDto dto = musterListFacade.update(id, form);
        return dto;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public MusterListDto getMusterList(@PathVariable final Long id) {

        MusterListDto musterList = musterListFacade.findOne(id);
        return musterList;
    }

    @RequestMapping(value = "/entries", method = RequestMethod.GET)
    public Page<MusterListEntryDto> searchEntries(@Valid final MlEntrySearchForm mlEntrySearchForm, final BindingResult results) {
        if (results.hasErrors()) {
            LOG.info("FAILED MlEntrySearchForm validation ---> rejecting: errors={}", results);
            throw new ValidationFailedException(results);
        }
        Page<MusterListEntryDto> musterListEntries = musterListEntryFacade.search(mlEntrySearchForm);
        return musterListEntries;
    }

    @RequestMapping(value = "entries/merged", method = RequestMethod.GET)
    public Page<MusterListEntryDto> searchMergedEntries(@Valid final MlEntryMergeSearchForm mlEntryMergeSearchForm, final BindingResult result){
        if (result.hasErrors()) {
            LOG.info("FAILED MlEntrySearchForm validation ---> rejecting: errors={}", result);
            throw new ValidationFailedException(result);
        }
        Page<MusterListEntryDto> musterListEntries = musterListEntryFacade.searchMerged(mlEntryMergeSearchForm);
        return musterListEntries;
    }


    @RequestMapping(value = "/{id}/entries", method = RequestMethod.GET)
    public Page<MusterListEntryDto> getListEntries(@PathVariable final Long id, final BaseSearchForm searchForm) {
        Page<MusterListEntryDto> musterListEntries = musterListEntryFacade.findAllForList(id, searchForm.getPageable());
        return musterListEntries;
    }

    @RequestMapping(value = "/{id}/entries/{entryId}", method = RequestMethod.GET)
    public MusterListEntryDto getEntryDetails(@PathVariable final Long id, @PathVariable final Long entryId) {

        MusterListEntryDto musterListEntry = musterListEntryFacade.findById(entryId);
        return musterListEntry;
    }

    @RequestMapping(value = "/{id}/entries/{entryId}", method = RequestMethod.DELETE)
    public ResponseEntity<MusterListEntryDto> deleteEntryDetails(@PathVariable final Long id,
                                                                 @PathVariable final Long entryId,
                                                                 final boolean forceDelete) {

        LOG.debug("DELETING MusterListEntry id={}", entryId);
        boolean exists = musterListEntryFacade.exists(entryId);

        if (!exists) {
            throw new EntityNotFoundException(entryId.toString());
        }

        if (forceDelete) {
            musterListEntryFacade.forceDelete(entryId);
            return new ResponseEntity<>(HttpStatus.OK);
        }

        musterListEntryFacade.deleteById(entryId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}/entries", method = RequestMethod.POST)
    public ResponseEntity<MusterListEntryDto> createDetailsEntry(@PathVariable final Long id,
                                                                 @Valid @RequestBody final MusterListEntryForm entryForm,
                                                                 final BindingResult bindingResult
    ) {
        //throw new IllegalArgumentException();
        if (bindingResult.hasErrors()) {
            throw new ValidationFailedException(bindingResult);
        }

        entryForm.setMusterListId(id);
        MusterListEntryDto musterListEntryDto = musterListEntryFacade.createNew(entryForm);

        return new ResponseEntity<>(musterListEntryDto, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}/entries/{entryId}", method = RequestMethod.PUT)
    public ResponseEntity<MusterListEntryDto> updateDetailsEntry(@PathVariable final Long id,
                                                                 @PathVariable final Long entryId,
                                                                 @Valid @RequestBody final MusterListEntryForm entryForm,
                                                                 final BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            throw new ValidationFailedException(bindingResult);
        }

        entryForm.setMusterListId(id);

        MusterListEntryDto musterListEntryDto = musterListEntryFacade.update(entryId, entryForm);

        return new ResponseEntity<>(musterListEntryDto, HttpStatus.OK);
    }

    @RequestMapping(value = "/{musterListId}/entries/person/{personId}", method = RequestMethod.GET)
    public ResponseEntity<Boolean> findIfPersonIsRecorded(@PathVariable final Long musterListId,
                                                              @PathVariable final Long personId) {
        MusterListEntryDto musterListEntryDto = musterListEntryFacade.findByMusterListIdAndPersonId(musterListId, personId);
        if (musterListEntryDto != null) {
            return new ResponseEntity<>(true, HttpStatus.OK);
        }

        return new ResponseEntity<>(false,HttpStatus.OK);
    }

    //--- CSV file download ---------------------------------------------------
    @RequestMapping(value = "/{id}/downloadCsv", method = RequestMethod.GET)
    public void downloadCsvMusterListFile(@PathVariable final Long id,
                                          HttpServletResponse response) {
        String csvFileName = "musterlist.csv";
        String headerKey = "Content-Disposition";
        String headerValue = String.format("attachment; filename=\"%s\"", csvFileName);
        response.setHeader(headerKey, headerValue);
        response.setContentType("application/csv");
        response.setCharacterEncoding("UTF-8");

        MusterListDto musterListDto = musterListFacade.findOne(id);
        List<MusterListEntryDto> musterListEntryDtos = musterListEntryFacade.findAllForList(id);

        try (ICsvMapWriter csvWriter = new CsvMapWriter(response.getWriter(), CsvPreference.EXCEL_PREFERENCE)) {
            MlCsvWriterFormatter formatter = new MlCsvWriterFormatter(csvWriter);
            formatter.write(musterListDto, musterListEntryDtos);
        } catch (Exception e) {
            LOG.error("Creating CVS file failed:", e);
        }

    }

    //--- Spring MVC util methods ---------------------------------------------

    @InitBinder("musterListEntryForm")
    private void initMusterListEntryFormValidator(WebDataBinder binder) {
        binder.setValidator(musterListEntryFormValidator);
    }

    @InitBinder("mlEntrySearchForm")
    private void initMlSearchEntryFormValidator(WebDataBinder binder) {
        binder.setValidator(mlEntrySearchFormValidator);
    }

    @InitBinder("musterListForm")
    private void initMusterListFormValidator(WebDataBinder binder) {
        binder.setValidator(mlFromValidator);
    }

    @InitBinder("mlEntryMergeSearchForm")
    private void initMlEntryMergeSearchFormValidator(WebDataBinder binder) {
        binder.setValidator(mlEntryMergeSearchFormValidator);
    }
}


