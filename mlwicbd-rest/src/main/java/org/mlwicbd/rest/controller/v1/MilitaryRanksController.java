package org.mlwicbd.rest.controller.v1;

import org.mlwicbd.Constants;
import org.mlwicbd.criteria.MilitaryRankSearchCriteria;
import org.mlwicbd.model.Country;
import org.mlwicbd.model.MilitaryRank;
import org.mlwicbd.rest.RequestParamUtils;
import org.mlwicbd.rest.domain.dto.BaseMilitaryRankDto;
import org.mlwicbd.rest.service.MilitaryRankFacade;
import org.mlwicbd.service.CountryService;
import org.mlwicbd.service.MilitaryRankService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

/**
 * Created by dmadunic on 24/04/2016.
 */
@CrossOrigin
@RestController
@RequestMapping(value = "/v1/militaryranks")
public class MilitaryRanksController {
    public static final String DEFAULT_SORT_ATTRIBUTE = "territory,type";

    @Autowired
    private MilitaryRankFacade rankService;

    @RequestMapping(method = RequestMethod.GET)
    public final Page<BaseMilitaryRankDto> search(@RequestParam(value = "page", required = false) Integer page,
                                       @RequestParam(value = "size", required = false) Integer size,
                                       @RequestParam(value = "sort", required = false) String sortAttributes,
                                       @RequestParam(value="territoryCode", required = false) String territoryCode,
                                       @RequestParam(value="type", required = false) String rankType) {
        if (size == null) {
            size = Constants.DEFAULT_PAGE_SIZE;
        }

        if (sortAttributes == null) {
            sortAttributes = DEFAULT_SORT_ATTRIBUTE;
        }

        Pageable pageable = RequestParamUtils.pageable(page, size, sortAttributes);
        MilitaryRankSearchCriteria criteria  = new MilitaryRankSearchCriteria(territoryCode, rankType);
        Page<BaseMilitaryRankDto> result = rankService.searchByCriteria(criteria, pageable);

        return result;
    }
}
