package org.mlwicbd.rest.controller.v1;

import com.ag04.common.exception.EntityNotFoundException;
import com.ag04.common.exception.ValidationFailedException;
import org.mlwicbd.Constants;
import org.mlwicbd.criteria.PersonSearchCriteria;
import org.mlwicbd.model.view.PersonMleView;
import org.mlwicbd.rest.RequestParamUtils;
import org.mlwicbd.rest.domain.dto.PersonDto;
import org.mlwicbd.rest.domain.form.PersonForm;
import org.mlwicbd.rest.service.MusterListEntryFacade;
import org.mlwicbd.rest.service.PersonFacade;
import org.mlwicbd.rest.service.PersonMleViewFacade;
import org.mlwicbd.rest.validator.PersonFormValidator;
import org.mlwicbd.service.PersonMleViewService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * @author idekic
 * @since 19-Aug-16.
 */
@CrossOrigin
@RestController
@RequestMapping(value = "/v1/person")
public class PersonController {
    private static final Logger LOG = LoggerFactory.getLogger(PersonController.class);

    private static final String DEFAULT_SORT_ATTRIBUTE = "lastName,firstName";

    @Autowired
    private PersonFacade personFacade;

    @Autowired
    private MusterListEntryFacade musterListEntryFacade;

    @Autowired
    private PersonFormValidator personFormValidator;

    @Autowired
    private PersonMleViewFacade personMleViewFacade;



    @RequestMapping(method = RequestMethod.GET)
    public final Page<PersonDto> getPersons(@RequestParam(value = "firstName", required = false) String firstName,
                                            @RequestParam(value = "lastName", required = false) String lastName,
                                            @RequestParam(value = "placeOfBirth", required = false) String placeOfBirth,
                                            @RequestParam(value = "page", required = false) Integer page,
                                            @RequestParam(value = "size", required = false) Integer size,
                                            @RequestParam(value = "sort", required = false) String sortAttributes) {
        if (size == null) {
            size = Constants.DEFAULT_PAGE_SIZE;
        }
        if (sortAttributes == null) {
            sortAttributes = DEFAULT_SORT_ATTRIBUTE;
        }
        Pageable pageable = RequestParamUtils.pageable(page, size, sortAttributes);

        PersonSearchCriteria criteria = new PersonSearchCriteria(firstName, lastName, placeOfBirth);

        Page<PersonDto> persons = personFacade.searchByCriteria(criteria, pageable);

        List<PersonMleView> personMleViews = personMleViewFacade.findAllByPersonsId(persons.getContent());

        personMleViewFacade.assignMlReferenceToPersons(persons.getContent(), personMleViews);


        return persons;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public PersonDto getOne(@PathVariable final Long id) {

        PersonDto personDto = personFacade.findOne(id);

        return personDto;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public PersonDto update(@PathVariable final Long id,
                            @Valid @RequestBody PersonForm personForm,
                            final BindingResult bindingResult) {
        LOG.debug("--> RECEIVED Update Request for: Person id={} / valid={} / form={}", id, bindingResult.hasErrors(), personForm);
        if (bindingResult.hasErrors()) {
            throw new ValidationFailedException(bindingResult);
        }

        PersonDto personDto = personFacade.update(id, personForm);

        return personDto;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<PersonDto> update(@PathVariable final Long id, final boolean forceDelete) {

        LOG.debug("DELETING Person id={}", id);

        PersonDto entryDto = personFacade.findOne(id);

        if (entryDto == null) {
            throw new EntityNotFoundException(id.toString());
        }

        //to force a delete, corresponding ServiceRecord and
        //MusterListEntry entries must be deleted first
        if (forceDelete) {
            musterListEntryFacade.forceDelete(id);
            return new ResponseEntity<>(HttpStatus.OK);
        }

        personFacade.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<PersonDto> create(@Valid @RequestBody final PersonForm personForm,
                                            final BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            throw new ValidationFailedException(bindingResult);
        }

        PersonDto personDto = personFacade.create(personForm);

        return new ResponseEntity<>(personDto, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{sourceId}/{targetId}", method = RequestMethod.GET)
    public ResponseEntity<PersonDto> mergePersons(@PathVariable Long sourceId, @PathVariable Long targetId){

        personFacade.merge(sourceId, targetId);

        return new ResponseEntity<PersonDto>(HttpStatus.OK);
    }

    @InitBinder("personForm")
    private void initPersonFormValidator(WebDataBinder binder) {
        binder.setValidator(personFormValidator);
    }
}
