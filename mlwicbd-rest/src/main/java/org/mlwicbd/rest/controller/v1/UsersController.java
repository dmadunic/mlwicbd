package org.mlwicbd.rest.controller.v1;

import com.ag04.common.exception.ValidationFailedException;
import org.mlwicbd.rest.domain.dto.UserDto;
import org.mlwicbd.rest.domain.form.user.CreateUserForm;
import org.mlwicbd.rest.domain.form.user.UpdateUserForm;
import org.mlwicbd.rest.service.UserFacade;
import org.mlwicbd.rest.validator.CreateUserFormValidator;
import org.mlwicbd.rest.validator.UpdateUserFormValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Created by dmadunic on 31/08/2015.
 */
@CrossOrigin
@RestController
@RequestMapping(value = "/v1/users")
public class UsersController {
    private static final Logger LOG = LoggerFactory.getLogger(UsersController.class);

    @Autowired
    private UserFacade userFacade;

    @Autowired
    private CreateUserFormValidator createUserFormValidator;

    @Autowired
    private UpdateUserFormValidator updateUserFormValidator;

    @RequestMapping(value="/me", method = RequestMethod.GET)
    public final UserDto getUserDetails() {
        String username = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        LOG.debug("Returning details for currently signed in user={}", username);

        UserDto user = userFacade.findByUsername(username);
        return user;
    }

    @RequestMapping(method = RequestMethod.POST)
    public final UserDto create(@Valid @RequestBody CreateUserForm createUserForm, final BindingResult results) {
        if (results.hasErrors()) {
            LOG.info("FAILED NewUserForm validation ---> rejecting: errors={}", results);
            throw new ValidationFailedException(results);
        }
        UserDto savedUser = userFacade.create(createUserForm);
        LOG.debug("SUCCESSFULLY saved NEW user={}", savedUser);
        return savedUser;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public UserDto update(@PathVariable final Long id, @Valid @RequestBody UpdateUserForm updateUserForm, final BindingResult results) {
        updateUserFormValidator.extraValidations(id, updateUserForm, results);

        if (results.hasErrors()) {
            LOG.info("FAILED UpdateUserForm validation ---> rejecting: errors={}", results);
            throw new ValidationFailedException(results);
        }
        UserDto savedUser = userFacade.update(id, updateUserForm);
        LOG.debug("SUCCESSFULLY updated user={}", savedUser);
        return savedUser;
    }

    //--- util methods --------------------------------------------------------

    @InitBinder("createUserForm")
    private void initNewUserFormValidator(final WebDataBinder binder) {
        binder.setValidator(createUserFormValidator);
    }

    @InitBinder("updateUserForm")
    private void initUpdateUserFormValidator(final WebDataBinder binder) {
        binder.setValidator(updateUserFormValidator);
    }

}
