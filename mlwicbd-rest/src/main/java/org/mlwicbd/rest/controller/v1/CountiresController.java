package org.mlwicbd.rest.controller.v1;

import org.mlwicbd.model.AppConfig;
import org.mlwicbd.model.Country;
import org.mlwicbd.rest.RequestParamUtils;
import org.mlwicbd.service.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by dmadunic on 04/04/16.
 */
@CrossOrigin
@RestController
@RequestMapping(value = "/v1/countries")
public class CountiresController {

    private static final Integer MAX_RESULT_SIZE = 100000;

    @Autowired
    private CountryService countryService;

    @RequestMapping(method = RequestMethod.GET)
    public final Page<Country> findAll(@RequestParam(value = "page", required = false) Integer page,
                                       @RequestParam(value = "size", required = false) Integer size,
                                       @RequestParam(value = "sort", required = false) String sortAttributes) {
        if (size == null) {
            size = MAX_RESULT_SIZE;
        }

        Pageable pageable = RequestParamUtils.pageable(page, size, sortAttributes);
        Page<Country> result = countryService.findAll(pageable);
        return result;
    }

}
