package org.mlwicbd.rest.controller.v1;

import com.ag04.common.exception.ValidationFailedException;
import org.mlwicbd.Constants;
import org.mlwicbd.model.person.Profession;
import org.mlwicbd.rest.RequestParamUtils;
import org.mlwicbd.rest.domain.dto.ProfessionDto;
import org.mlwicbd.rest.domain.form.ProfessionForm;
import org.mlwicbd.rest.mapper.dto.ProfessionMapper;
import org.mlwicbd.rest.service.ProfessionFacade;
import org.mlwicbd.rest.validator.ProfessionFormValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by dmadunic on 22/09/2016.
 */
@CrossOrigin
@RestController
@RequestMapping(value = "/v1/professions")
public class ProfessionsController {

    @Autowired
    private ProfessionMapper professionMapper;

    @Autowired
    private ProfessionFacade professionFacade;

    @Autowired
    private ProfessionFormValidator professionFormValidator;

    @RequestMapping(method = RequestMethod.GET)
    public final Page<ProfessionDto> find(@RequestParam(value = "page", required = false) Integer page,
                                           @RequestParam(value = "size", required = false) Integer size,
                                           @RequestParam(value = "sort", required = false) String sortAttributes,
                                           @RequestParam(value = "code", required = false) String professionsCode,
                                           @RequestParam(value = "name", required = false) String professionName){
        if (size == null) {
            size = Constants.DEFAULT_PAGE_SIZE;
        }

        Pageable pageable = RequestParamUtils.pageable(page, size, sortAttributes);
        Page<Profession> result = professionFacade.filterProfessions(pageable, professionName, professionsCode);
        List<ProfessionDto> professionDtos = professionMapper.mapToDtos(result.getContent());

        return new PageImpl<>(professionDtos, pageable, result.getTotalElements());
    }

    @RequestMapping(method = RequestMethod.POST)
    public final ResponseEntity<ProfessionDto> save(@Valid @RequestBody final ProfessionForm professionForm,
                                                    final BindingResult bindingResult){

        if (bindingResult.hasErrors()){
            throw new ValidationFailedException(bindingResult);
        }
        ProfessionDto savedProfessionDto = professionFacade.save(professionForm);

        return new ResponseEntity<>(savedProfessionDto, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public final ResponseEntity<ProfessionDto> update(@Valid @RequestBody final ProfessionForm professionForm,
                                                      final BindingResult bindingResult,
                                                      @PathVariable final Long id) {
        if (bindingResult.hasErrors()) {
            throw new ValidationFailedException(bindingResult);
        }
        ProfessionDto updatedProfessionDto = professionFacade.update(professionForm, id);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @InitBinder("professionForm")
    private void initProfessionFormValidator(WebDataBinder binder) {
        binder.setValidator(professionFormValidator);
    }


}
