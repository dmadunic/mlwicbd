package org.mlwicbd.rest.config;

import org.mlwicbd.dao.repository.UserRepository;
import org.mlwicbd.rest.service.UserRegistrationService;
import org.mlwicbd.rest.service.impl.DefaultUserRegistrationService;
import org.mlwicbd.rest.service.security.BaseUserDetailsService;
import org.mlwicbd.service.RoleService;
import org.mlwicbd.service.UserService;
import org.mlwicbd.support.audit.UserAuditorAwareimpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

/**
 * Created by dmadunic on 10/09/2016.
 */
@Configuration
@Profile("dbauth")
public class DbAuthConfig {

    @Bean
    public  UserDetailsService userDetailsService(final UserRepository userRepository) {
        UserDetailsService userDetailsService = new BaseUserDetailsService(userRepository);
        return userDetailsService;
    }

    @Bean
    public UserRegistrationService userRegistrationService(@Value("${user.defaultPassword}") final String defaultPassword, @Value("${user.defaultRole}") final String defaultRole, final UserService userService, final RoleService roleService) {
        UserRegistrationService userRegistrationService = new DefaultUserRegistrationService(defaultPassword, defaultRole, userService, roleService);
        return userRegistrationService;
    }

}
