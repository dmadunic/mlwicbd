package org.mlwicbd.rest.config.security;

import org.mlwicbd.support.web.security.RestAuthenticationEntryPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;

/**
 * Created by dmadunic on 31/08/15.
 */
//@Profile({"dbauth"})
@Profile({"httpbasic"})
@Configuration
@EnableWebSecurity
@Order(value=2)
public class DefaultWebSecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Value("${basic-authentication.realm.name}")
    private String realmName;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
            .antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
            .antMatchers(HttpMethod.GET, "/v1/configs").permitAll()
            .antMatchers(HttpMethod.GET, "/v1/countries").permitAll()
            .antMatchers(HttpMethod.POST, "/v1/users").permitAll()
            .antMatchers("/v1/**").authenticated();

          http.httpBasic().authenticationEntryPoint(restAuthenticationEntryPoint());

        http.csrf().disable();
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        // @formatter:off
        web.ignoring()
                .antMatchers(HttpMethod.OPTIONS, "/v1/users")
                .antMatchers(HttpMethod.GET, "/v1/configs")
                .antMatchers(HttpMethod.GET, "/v1/countries")
                .antMatchers(HttpMethod.POST, "/v1/users")
                .antMatchers(HttpMethod.OPTIONS, "/**").and();
        // @formatter:on
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder);
    }

    @Bean
    public AuthenticationEntryPoint restAuthenticationEntryPoint() {
        RestAuthenticationEntryPoint restAuthenticationEntryPoint = new RestAuthenticationEntryPoint();
        restAuthenticationEntryPoint.setRealmName(realmName);
        return restAuthenticationEntryPoint;
    }
}
