package org.mlwicbd.rest.config;

import org.mlwicbd.service.UserService;
import org.mlwicbd.support.audit.UserAuditorAwareimpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;

/**
 * Created by dmadunic on 19/04/17.
 */
@Configuration
public class AuditConfig {

    //@Autowired
    //UserService userService;

    @Bean
    public AuditorAware auditorAware() {
        UserAuditorAwareimpl auditorAware = new UserAuditorAwareimpl();
        //auditorAware.setUserService(userService);
        return auditorAware;
    }
}
