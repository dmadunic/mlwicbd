package org.mlwicbd.rest.config;

import com.ag04.utils.web.ManifestReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.ServletContext;

/**
 * Created by Domagoj on 29/03/16.
 */
@Configuration
public class WebConfiguration {

    @Autowired
    private ServletContext servletContext;

    @Bean
    public ManifestReader manifestReader() {
        ManifestReader manifestReader = new ManifestReader();
        manifestReader.setServletContext(servletContext);
        return manifestReader;
    }
}
