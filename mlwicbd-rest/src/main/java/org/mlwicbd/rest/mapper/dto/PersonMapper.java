package org.mlwicbd.rest.mapper.dto;

import org.mapstruct.Mapper;
import org.mlwicbd.model.person.Person;
import org.mlwicbd.rest.domain.dto.PersonDto;
import org.mlwicbd.rest.domain.form.PersonForm;

import java.util.List;

/**
 * @author idekic
 * @since 22-Aug-16.
 */
@Mapper(componentModel = "spring")
public abstract class PersonMapper {

    public abstract PersonDto personToDto(Person person);

    public abstract List<PersonDto> personListToDtoList(List<Person> persons);

    //-------------------------------------------------------------------------
    //TODO: move all these 3 methods to separate EntityFormMapper!
    public abstract PersonDto personFormToDto(PersonForm personForm);

    public abstract Person personFormToEntity(PersonForm personForm);

    public Person updatePersonFromPersonForm(Person entity, PersonForm personForm) {
        entity.setFirstName(personForm.getFirstName());
        entity.setLastName(personForm.getLastName());
        entity.setSourcesFirstNames(personForm.getSourcesFirstNames());
        entity.setSourcesLastNames(personForm.getSourcesLastNames());
        entity.setPlaceOfBirth(personForm.getPlaceOfBirth());
        entity.setNote(personForm.getNote());

        return entity;
    }
}
