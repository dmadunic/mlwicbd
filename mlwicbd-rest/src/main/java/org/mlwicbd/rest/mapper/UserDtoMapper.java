package org.mlwicbd.rest.mapper;

import com.ag04.common.mapper.EntityMapper;
import org.mlwicbd.model.User;
import org.mlwicbd.rest.domain.dto.UserDto;
import org.springframework.stereotype.Component;
import java.util.Locale;
import java.util.Map;

/**
 * Created by dmadunic on 02/04/16.
 */
@Component
public class UserDtoMapper implements EntityMapper<User, UserDto> {

    @Override
    public UserDto mapFromEntity(User entity, Locale locale) {
        if (entity == null)
            return null;

        UserDto dto = new UserDto();

        dto.setId(entity.getId());
        dto.setUsername(entity.getUsername());
        dto.setFirstName(entity.getFirstName());
        dto.setLastName(entity.getLastName());
        dto.setEmail(entity.getEmail());

        dto.setRoles(entity.getRoles());

        if (entity.getUserProfile() != null) {
            dto.setAddress(entity.getUserProfile().getAddress());
            dto.setPostalCode(entity.getUserProfile().getPostalCode());
            dto.setPlace(entity.getUserProfile().getPlace());
            if (entity.getUserProfile().getCountry() != null) {
                dto.setCountryCode(entity.getUserProfile().getCountry().getA2code());
                dto.setCountryName(entity.getUserProfile().getCountry().getName());
            }
            dto.setPhone(entity.getUserProfile().getPhone());
            dto.setMobile(entity.getUserProfile().getMobile());
        }
        return dto;
    }

    @Override
    public UserDto mapFromEntity(User entity, Locale locale, Map<String, Object> config) {
        throw new UnsupportedOperationException("Method not implemented");
    }

    @Override
    public User mapToEntity(UserDto source, Locale locale) {
        throw new UnsupportedOperationException("Method not implemented");
    }

    @Override
    public User mapToEntity(UserDto source, Locale locale, Map<String, Object> config) {
        throw new UnsupportedOperationException("Method not implemented");
    }
}
