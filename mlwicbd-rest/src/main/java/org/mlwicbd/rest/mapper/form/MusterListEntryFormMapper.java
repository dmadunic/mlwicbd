package org.mlwicbd.rest.mapper.form;

import com.ag04.support.mapper.EntityMapper;
import org.mlwicbd.converter.MaritalStatusConverter;
import org.mlwicbd.model.MilitaryRank;
import org.mlwicbd.model.musterlist.Child;
import org.mlwicbd.model.musterlist.MusterList;
import org.mlwicbd.model.musterlist.MusterListEntry;
import org.mlwicbd.model.musterlist.ServiceRecord;
import org.mlwicbd.model.person.Person;
import org.mlwicbd.model.person.PersonMeasures;
import org.mlwicbd.model.person.Profession;
import org.mlwicbd.model.person.ReligiousDenomination;
import org.mlwicbd.rest.domain.dto.ChildDto;
import org.mlwicbd.rest.domain.dto.ServiceRecordDto;
import org.mlwicbd.rest.domain.form.musterlist.MusterListEntryForm;
import org.mlwicbd.rest.mapper.dto.ChildMapper;
import org.mlwicbd.rest.mapper.dto.ServiceRecordMapper;
import org.mlwicbd.service.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * TODO: write tests for this Mapper.
 *
 * Created by dmadunic on 21/09/2016.
 */
@Component
public class MusterListEntryFormMapper implements EntityMapper<MusterListEntry, MusterListEntryForm> {
    public static final String MLE_KEY = "MLE_KEY";

    private static final String NAME_DELIMITER = ";";
    private static final String NAME_DEFAULT = "";

    @Autowired
    private MilitaryRankService militaryRankService;

    @Autowired
    private MusterListService musterListService;

    @Autowired
    private PersonService personService;

    @Autowired
    private ReligiousDenominationService religiousDenominationService;

    @Autowired
    private ProfessionService professionService;

    @Autowired
    private MaritalStatusConverter maritalStatusConverter;

    @Autowired
    private ServiceRecordMapper serviceRecordMapper;

    @Autowired
    private ChildMapper childMapper;

    @Override
    public MusterListEntryForm mapFromEntity(MusterListEntry entity, Locale locale) {
        throw new UnsupportedOperationException();
    }

    @Override
    public MusterListEntryForm mapFromEntity(MusterListEntry entity, Locale locale, Map<String, Object> config) {
        throw new UnsupportedOperationException();
    }

    @Override
    public MusterListEntry mapToEntity(MusterListEntryForm source, Locale locale) {
        Map<String, Object> config = new HashMap<>();
        MusterListEntry musterList = new MusterListEntry();
        config.put(MLE_KEY, musterList);
        return mapToEntity(source, locale, config);
    }

    @Override
    public MusterListEntry mapToEntity(MusterListEntryForm entryForm, Locale locale, Map<String, Object> config) {

        MusterListEntry musterListEntry = (MusterListEntry) config.get(MLE_KEY);
        MusterListEntry mle = new MusterListEntry();

        BeanUtils.copyProperties(musterListEntry, mle);

        mle.setVersion(entryForm.getVersion());
        mle.setListNumber(entryForm.getListNumber());
        mle.setFirstName(entryForm.getFirstName());
        mle.setLastName(entryForm.getLastName());

        //Assign Person
        if (entryForm.getPersonId() != null) {
            Person person = personService.findOne(entryForm.getPersonId());
            mle.setPerson(person);
        }

        //Assign Rank
        if (entryForm.getRankId() != null) {
            MilitaryRank militaryRank = militaryRankService.findOne(entryForm.getRankId());
            mle.setRank(militaryRank);
        }

        //Assign Profession
        if (entryForm.getProfessionId() != null) {
            Profession profession = professionService.findOne(entryForm.getProfessionId());
            mle.setProfession(profession);
        }
        musterListEntry.setSourceProfession(entryForm.getSourceProfession());

        //Assign ReligiousDenomination
        if (entryForm.getReligionId() != null) {
            ReligiousDenomination religiousDenomination = religiousDenominationService.findById(entryForm.getReligionId());
            mle.setReligion(religiousDenomination);
        }

        //Assign MusterList
        if (entryForm.getMusterListId() != null) {
            MusterList musterList = musterListService.findById(entryForm.getMusterListId());
            mle.setMusterList(musterList);
        }

        mle.setSourceProfession(entryForm.getSourceProfession());
        mle.setPlaceOfBirth(entryForm.getPlaceOfBirth());
        mle.setLocation(entryForm.getLocation());
        mle.setHouseNumber(entryForm.getHouseNumber());
        mle.setAge(entryForm.getAge());

        mle.setMaritalStatus(maritalStatusConverter.convertToEntityAttribute(entryForm.getMaritalStatus()));
        mle.setWifePresent(entryForm.getWifePresent());


        PersonMeasures personMeasures = new PersonMeasures();
        personMeasures.setUnit1(entryForm.getUnit1());
        personMeasures.setUnit2(entryForm.getUnit2());
        personMeasures.setUnit3(entryForm.getUnit3());
        mle.setMeasures(personMeasures);

        mle.setTotalYearsServed(entryForm.getTotalYearsServed());
        mle.setTotalMonthsServed(entryForm.getTotalMonthsServed());

        mle.setServiceRecords(mergeServiceRecords(mle, entryForm.getServiceRecords()));
        mle.setChildren(mergeChildren(mle, entryForm.getChildren()));

        return mle;
    }

    //--- merge service records util methods ----------------------------------

    private List<ServiceRecord> mergeServiceRecords(MusterListEntry entry, List<ServiceRecordDto> dtos) {
        if (dtos == null) {
            return new ArrayList<>();
        }
        List<ServiceRecord> mergedList = new ArrayList<>();
        if (entry.getServiceRecords() != null) {
            //1. update existing and remove missing ones
            for (ServiceRecord sr : entry.getServiceRecords()) {
                ServiceRecord mergedServiceRecord = mergeServiceRecord(sr, dtos);
                if (mergedServiceRecord != null) {
                    mergedServiceRecord.setMusterListEntry(entry);
                    mergedList.add(mergedServiceRecord);
                }
            }
        }

        //2. add new ones
        for (ServiceRecordDto dto : dtos) {
            if (dto.getId() == null) {
                ServiceRecord sr = serviceRecordMapper.map(dto);
                sr.setMusterListEntry(entry);
                mergedList.add(sr);
            }
        }
        return mergedList;
    }

    private ServiceRecord mergeServiceRecord(ServiceRecord sr, List<ServiceRecordDto> dtos) {
        for (ServiceRecordDto dto : dtos) {
            if (sr.getId().equals(dto.getId())) {
                return serviceRecordMapper.map(dto);
            }
        }
        return null;
    }

    //--- merge children util methods -----------------------------------------

    private List<Child> mergeChildren(MusterListEntry entry, List<ChildDto> dtos) {
        if (dtos == null){
            return new ArrayList<>();
        }
        List<Child> mergedList = new ArrayList<>();

        //1. update existing and remove missing ones
        if (entry.getChildren() != null && entry.getChildren().size() > 0) {
            for (Child child : entry.getChildren()) {
                Child mergedChild = mergeChild(child, dtos);
                if (mergedChild != null) {
                    mergedChild.setMusterListEntry(entry);
                    mergedList.add(mergedChild);
                }
            }
        }

        //2. add new ones
        for (ChildDto dto : dtos) {
            if (dto.getId() == null) {
                Child child = childMapper.map(dto);
                child.setMusterListEntry(entry);
                mergedList.add(child);
            }
        }
        return mergedList;
    }

    private Child mergeChild(Child child, List<ChildDto> dtos) {
        for (ChildDto dto : dtos) {
            if (child.getId().equals(dto.getId())) {
                return childMapper.map(dto);
            }
        }
        return null;
    }
}
