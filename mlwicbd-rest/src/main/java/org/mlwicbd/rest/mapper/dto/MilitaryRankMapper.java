package org.mlwicbd.rest.mapper.dto;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mlwicbd.model.MilitaryRank;
import org.mlwicbd.rest.domain.dto.BaseMilitaryRankDto;

/**
 * Created by dmadunic on 21/09/2016.
 */
@Mapper(componentModel = "spring")
public interface MilitaryRankMapper {

    @Mappings({
        @Mapping(target = "territoryCode", source = "territory.code")
    })
    BaseMilitaryRankDto mapToBaseDto(MilitaryRank rank);

}
