package org.mlwicbd.rest.mapper.dto;

import org.mapstruct.Mapper;
import org.mlwicbd.model.musterlist.Child;
import org.mlwicbd.rest.domain.dto.ChildDto;

import java.util.List;

/**
 * Created by dmadunic on 27/09/2016.
 */
@Mapper(componentModel = "spring")
public interface ChildMapper {

    //--- map entity to DTO ---------------------------------------------------

    ChildDto map(Child value);

    List<ChildDto> map(List<Child> value);

    //--- map DTO to entity ---------------------------------------------------

    Child map(ChildDto value);

    List<Child> mapFrom(List<ChildDto> value);
}
