package org.mlwicbd.rest.mapper;

import com.ag04.common.mapper.EntityMapper;
import org.mlwicbd.dao.repository.CountryRepository;
import org.mlwicbd.model.Country;
import org.mlwicbd.model.User;
import org.mlwicbd.model.UserProfile;
import org.mlwicbd.rest.domain.form.user.UpdateUserForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Locale;
import java.util.Map;

/**
 * Created by dmadunic on 10/04/16.
 */
@Component
public class UpdateUserFormMapper implements EntityMapper<User, UpdateUserForm> {
    public static final String USER_KEY = "userKey";

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    protected CountryRepository countryRepository;

    @Override
    public UpdateUserForm mapFromEntity(User entity, Locale locale) {
        return null;
    }

    @Override
    public UpdateUserForm mapFromEntity(User entity, Locale locale, Map<String, Object> config) {
        return null;
    }

    @Override
    public User mapToEntity(UpdateUserForm form, Locale locale) {
        throw new UnsupportedOperationException("Please use method with map as argument!");
    }

    @Override
    public User mapToEntity(UpdateUserForm form, Locale locale, Map<String, Object> config) {
        User user = (User) config.get(USER_KEY);

        if (form.getUsername() != null) {
            user.setUsername(form.getUsername());
        }

        if (form.getFirstName() != null) {
            user.setFirstName(form.getFirstName());
        }

        if (form.getLastName() != null) {
            user.setLastName(form.getLastName());
        }

        if (form.getEmail() != null) {
            user.setEmail(form.getEmail());
        }

        if (form.getPassword() != null) {
            String encodedPassword = passwordEncoder.encode(form.getPassword());
            user.setPassword(encodedPassword);
        }

        mapToUserProfile(form, user.getUserProfile());
        //userProfile.setUser(user);
        //user.setUserProfile(userProfile);

        return user;
    }

    protected UserProfile mapToUserProfile(UpdateUserForm form, UserProfile userProfile) {
        if (form.getMobile() != null) {
            userProfile.setMobile(form.getMobile());
        }

        if (form.getPhone() != null) {
            userProfile.setPhone(form.getPhone());
        }

        if (form.getAddress() != null) {
            userProfile.setAddress(form.getAddress());
        }

        if (form.getPlace() != null) {
            userProfile.setPlace(form.getPlace());
        }

        if (form.getPostalCode() != null) {
            userProfile.setPostalCode(form.getPostalCode());
        }

        if (form.getCountryCode() != null) {
            Country country = countryRepository.findByCode(form.getCountryCode());
            userProfile.setCountry(country);
        }
        return userProfile;
    }
}
