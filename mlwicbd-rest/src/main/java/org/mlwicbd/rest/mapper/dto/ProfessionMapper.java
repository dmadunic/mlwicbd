package org.mlwicbd.rest.mapper.dto;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mlwicbd.model.person.Profession;
import org.mlwicbd.rest.domain.dto.ProfessionDto;
import org.mlwicbd.rest.domain.form.ProfessionForm;

import java.util.List;

/**
 * Created by dmadunic on 21/09/2016.
 */
@Mapper(componentModel = "spring", uses = {UsernameToUserMapper.class})
public interface ProfessionMapper {

    List<ProfessionDto> mapToDtos(List<Profession> professions);

    @Mapping(source = "createdBy.username", target = "createdBy")
    @Mapping(source = "modifiedBy.username", target = "modifiedBy")
    @Mapping(source = "createdDate", target = "createdAt")
    @Mapping(source = "modifiedDate", target = "modifiedAt")
    ProfessionDto mapToDto(Profession value);

    Profession mapForm(ProfessionForm value);
}
