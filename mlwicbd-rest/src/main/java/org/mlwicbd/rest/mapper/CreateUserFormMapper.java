package org.mlwicbd.rest.mapper;

import com.ag04.common.mapper.EntityMapper;
import org.mlwicbd.dao.repository.CountryRepository;
import org.mlwicbd.model.Country;
import org.mlwicbd.model.User;
import org.mlwicbd.model.UserProfile;
import org.mlwicbd.rest.domain.form.user.CreateUserForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Locale;
import java.util.Map;

/**
 * Created by dmadunic on 03/04/16.
 */
@Component
public class CreateUserFormMapper implements EntityMapper<User, CreateUserForm> {

    @Autowired
    protected CountryRepository countryRepository;

    @Override
    public CreateUserForm mapFromEntity(User entity, Locale locale) {
        return null;
    }

    @Override
    public CreateUserForm mapFromEntity(User entity, Locale locale, Map<String, Object> config) {
        return null;
    }

    @Override
    public User mapToEntity(CreateUserForm form, Locale locale) {
        User user = new User();
        user.setUsername(form.getUsername());
        user.setFirstName(form.getFirstName());
        user.setLastName(form.getLastName());
        user.setEmail(form.getEmail());

        UserProfile userProfile = mapToUserProfile(form);
        userProfile.setUser(user);
        user.setUserProfile(userProfile);

        return user;
    }

    @Override
    public User mapToEntity(CreateUserForm source, Locale locale, Map<String, Object> config) {
        return null;
    }

    protected UserProfile mapToUserProfile(CreateUserForm form) {
        UserProfile userProfile = new UserProfile();
        userProfile.setMobile(form.getMobile());
        userProfile.setPhone(form.getPhone());

        userProfile.setAddress(form.getAddress());
        userProfile.setPlace(form.getPlace());
        userProfile.setPostalCode(form.getPostalCode());

        Country country = countryRepository.findByCode(form.getCountryCode());
        userProfile.setCountry(country);

        return userProfile;
    }
}
