package org.mlwicbd.rest.mapper.dto;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mlwicbd.model.MilitaryUnit;
import org.mlwicbd.rest.domain.dto.MilitaryUnitDto;

/**
 *
 * Created by dmadunic on 17/04/16.
 */
@Mapper(componentModel = "spring")
public interface MilitaryUnitMapper {

    //MilitaryUnitMapper INSTANCE = Mappers.getMapper(MilitaryUnitMapper.class);

    @Mappings({
            @Mapping(source = "unitType.name", target = "unitType"),
            @Mapping(source = "territory.code", target = "territoryCode"),
            @Mapping(source = "parentUnit.id", target = "parentUnitId"),
            @Mapping(source = "parentUnit.name", target = "parentUnitName")
    })
    MilitaryUnitDto militaryUnitToMilitaryUnitDto(MilitaryUnit militaryUnit);

}
