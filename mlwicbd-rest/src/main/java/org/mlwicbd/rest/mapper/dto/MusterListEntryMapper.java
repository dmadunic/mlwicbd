package org.mlwicbd.rest.mapper.dto;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mlwicbd.converter.MaritalStatusConverter;
import org.mlwicbd.model.MilitaryRank;
import org.mlwicbd.model.musterlist.MusterList;
import org.mlwicbd.model.musterlist.MusterListEntry;
import org.mlwicbd.model.musterlist.ServiceRecord;
import org.mlwicbd.model.person.*;
import org.mlwicbd.rest.domain.dto.*;
import org.mlwicbd.rest.domain.form.musterlist.MusterListEntryForm;
import org.mlwicbd.service.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 *
 * @author idekic
 * @since 01-Aug-16.
 */
@Mapper(componentModel = "spring", uses = {
    PersonMapper.class,
    ChildMapper.class,
    ServiceRecordMapper.class,
    ProfessionMapper.class,
    MilitaryRankMapper.class,
    ReligiousDenominationMapper.class
})
public interface MusterListEntryMapper {

    @Mappings({
        @Mapping(target = "musterListId", source = "musterList.id"),
        @Mapping(target = "year", source = "musterList.year"),
        @Mapping(target = "month", source = "musterList.month"),
        @Mapping(target = "maritalStatus", source = "maritalStatus.id"),
        @Mapping(target = "unit1", source = "measures.unit1"),
        @Mapping(target = "unit2", source = "measures.unit2"),
        @Mapping(target = "unit3", source = "measures.unit3")
    })
    MusterListEntryDto mapToDto(MusterListEntry musterListEntry);

    List<MusterListEntryDto> mapToDto (List<MusterListEntry> musterListEntryDtos);

}
