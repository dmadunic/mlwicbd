package org.mlwicbd.rest.mapper.dto;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mlwicbd.model.musterlist.MusterList;
import org.mlwicbd.rest.domain.dto.MusterListDto;

/**
 * Created by dmadunic on 21/04/2016.
 */
@Mapper(componentModel = "spring")
public interface MusterListMapper {


    @Mappings({
            @Mapping(source = "militaryUnit.id", target = "unitId"),
            @Mapping(source = "militaryUnit.name", target = "unitName"),
            @Mapping(source = "militaryUnit.unitType.name", target = "unitType"),
            @Mapping(source = "militaryUnit.parentUnit.id", target = "parentUnitId"),
            @Mapping(source = "militaryUnit.parentUnit.name", target = "parentUnitName"),
            @Mapping(source = "militaryUnit.parentUnit.unitType.name", target = "parentUnitType"),
            @Mapping(source = "createdBy.username", target = "createdBy"),
            @Mapping(source = "modifiedBy.username", target = "modifiedBy")
    })
    MusterListDto musterListToDto(MusterList musterList);
}
