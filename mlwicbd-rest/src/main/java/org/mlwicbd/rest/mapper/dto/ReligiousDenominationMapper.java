package org.mlwicbd.rest.mapper.dto;

import org.mapstruct.Mapper;
import org.mlwicbd.model.person.ReligiousDenomination;
import org.mlwicbd.rest.domain.dto.ReligiousDenominationDto;

/**
 * Created by dmadunic on 21/09/2016.
 */
@Mapper(componentModel = "spring")
public interface ReligiousDenominationMapper {

    ReligiousDenominationDto mapToDto(ReligiousDenomination entity);

}
