package org.mlwicbd.rest.mapper.dto;

import org.mlwicbd.dao.repository.UserRepository;
import org.mlwicbd.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by tgreblicki on 6/7/17.
 */
@Component
public class UsernameToUserMapper {

    @Autowired
    private UserRepository userRepository;

    public User usernameToUser(String username) {
        if (username == null){
            return null;
        }

        return userRepository.findByUsername(username);
    }
}
