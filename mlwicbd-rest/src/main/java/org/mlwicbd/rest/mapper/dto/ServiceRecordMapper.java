package org.mlwicbd.rest.mapper.dto;

import org.mapstruct.Mapper;
import org.mlwicbd.model.musterlist.ServiceRecord;
import org.mlwicbd.rest.domain.dto.ServiceRecordDto;

import java.util.List;

/**
 * Created by dmadunic on 21/09/2016.
 */
@Mapper(componentModel = "spring")
public interface ServiceRecordMapper {

    //--- map entity to DTO ------------------

    ServiceRecordDto map(ServiceRecord value);

    List<ServiceRecordDto> map(List<ServiceRecord> value);

    //--- map DTO to entity ------------------

    ServiceRecord map(ServiceRecordDto value);

    List<ServiceRecord> mapFrom(List<ServiceRecordDto> value);

}
