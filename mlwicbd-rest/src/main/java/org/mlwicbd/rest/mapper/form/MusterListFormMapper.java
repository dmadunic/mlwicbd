package org.mlwicbd.rest.mapper.form;

import com.ag04.support.mapper.EntityMapper;
import org.mlwicbd.model.MilitaryUnit;
import org.mlwicbd.model.musterlist.MusterList;
import org.mlwicbd.rest.domain.form.musterlist.MusterListForm;
import org.mlwicbd.service.MilitaryUnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 *
 * Created by dmadunic on 19/09/2016.
 */
@Component
public class MusterListFormMapper implements EntityMapper<MusterList, MusterListForm> {
    public static final String ML_KEY = "MUSTER_LIST_KEY";

    @Autowired
    private MilitaryUnitService militaryUnitService;

    @Override
    public MusterListForm mapFromEntity(MusterList entity, Locale locale) {
        throw new UnsupportedOperationException();
    }

    @Override
    public MusterListForm mapFromEntity(MusterList entity, Locale locale, Map<String, Object> config) {
        throw new UnsupportedOperationException();
    }

    @Override
    public MusterList mapToEntity(MusterListForm source, Locale locale) {
        Map<String, Object> config = new HashMap<>();
        MusterList musterList = new MusterList();
        config.put(ML_KEY, musterList);
        return mapToEntity(source, locale, config);
    }

    /**
     * This method expects to find MusterList entity in the config map under the ML_KEY key.
     *
     * Form is then mapped onto this entity.
     *
     * @param source
     * @param locale
     * @param config
     * @return
     */
    @Override
    public MusterList mapToEntity(MusterListForm source, Locale locale, Map<String, Object> config) {
        MusterList musterList = (MusterList) config.get(ML_KEY);
        musterList.setMonth(source.getMonth());
        musterList.setYear(source.getYear());
        musterList.setTitle(source.getTitle());
        musterList.setReferenceCode(source.getReferenceCode());

        MilitaryUnit unit = militaryUnitService.findById(source.getUnitId());
        musterList.setMilitaryUnit(unit);

        return musterList;
    }
}
