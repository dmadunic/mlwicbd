package org.mlwicbd.rest.utils;

import java.util.List;

/**
 * Created by lovrostrihic on 24.06.16..
 */
public class PageableParams implements com.ag04.support.web.PageableParams {

    private Integer page = 0;
    private Integer size = 25;
    private List<String> sortableAttributes;

    @Override
    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    @Override
    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    @Override
    public List<String> getSortableAttributes() {
        return sortableAttributes;
    }

    public void setSortableAttributes(List<String> sortableAttributes) {
        this.sortableAttributes = sortableAttributes;
    }
}
