package org.mlwicbd.rest.utils;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.ArrayList;

/**
 * Created by lmarasovic, dmadunic on 14/03/16.
 */
public final class PageUtil {

    /**
     * Not intended for instantiation.
     */
    private PageUtil(){
    }

    public static <T> Page<T> emptyPage() {
        return new PageImpl<>(new ArrayList<>());
    }
}

