package org.mlwicbd.rest.domain.dto;

import org.mlwicbd.model.musterlist.ChildGender;

import java.io.Serializable;

/**
 * Created by dmadunic on 27/09/2016.
 */
public class ChildDto implements Serializable {

    private Long id;

    private String name;

    private Integer age;

    private ChildGender gender;

    //--- set / get methods ---------------------------------------------------

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public ChildGender getGender() {
        return gender;
    }

    public void setGender(ChildGender gender) {
        this.gender = gender;
    }
}
