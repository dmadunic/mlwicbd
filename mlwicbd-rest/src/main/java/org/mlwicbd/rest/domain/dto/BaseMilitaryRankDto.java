package org.mlwicbd.rest.domain.dto;

import java.io.Serializable;

/**
 * @author idekic
 * @since 01-Aug-16.
 */
public class BaseMilitaryRankDto implements Serializable {

    private Long id;

    private String code;

    private String territoryCode;

    //--- set / get methods ---------------------------------------------------

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTerritoryCode() {
        return territoryCode;
    }

    public void setTerritoryCode(String territoryCode) {
        this.territoryCode = territoryCode;
    }
}
