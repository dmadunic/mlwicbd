package org.mlwicbd.rest.domain.dto;

import java.io.Serializable;

/**
 * @author idekic
 * @since 28-Jul-16.
 */
public class ServiceRecordDto implements Serializable {

    private Long id;

    private String description;

    private Integer durationYears;

    private Integer durationMonths;

    private String durationDays;

    //--- set  / get methods --------------------------------------------------

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getDurationYears() {
        return durationYears;
    }

    public void setDurationYears(Integer durationYears) {
        this.durationYears = durationYears;
    }

    public Integer getDurationMonths() {
        return durationMonths;
    }

    public void setDurationMonths(Integer durationMonths) {
        this.durationMonths = durationMonths;
    }

    public String getDurationDays() {
        return durationDays;
    }

    public void setDurationDays(String durationDays) {
        this.durationDays = durationDays;
    }
}
