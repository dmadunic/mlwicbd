package org.mlwicbd.rest.domain.dto;

/**
 * Created by dmadunic on 21/09/2016.
 */
public class ReligiousDenominationDto {

    private Long id;

    private String code;

    // --- set  / get methods -------------------------------------------------

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
