package org.mlwicbd.rest.domain.form.musterlist;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.mlwicbd.rest.domain.form.BaseSearchForm;

/**
 * Created by tgreblicki on 22.05.17..
 */
public class MlEntryMergeSearchForm extends BaseSearchForm {
    private Long personId1;
    private Long personId2;

    @Override
    public String toString() {
        final ToStringBuilder toStringBuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
        toStringBuilder.append(super.toString());
        toStringBuilder.append("personId1", personId1);
        toStringBuilder.append("personId2", personId2);
        return toStringBuilder.toString();
    }

    //--- util methods --------------------------------------------------------

    public boolean isEmpty() {
        if (personId1 != null && personId2 != null) {
            return false;
        }
        return true;
    }

    // --- get / set methods --------------------------------------------------

    public Long getPersonId1() {
        return personId1;
    }

    public void setPersonId1(Long personId1) {
        this.personId1 = personId1;
    }

    public Long getPersonId2() {
        return personId2;
    }

    public void setPersonId2(Long personId2) {
        this.personId2 = personId2;
    }
}
