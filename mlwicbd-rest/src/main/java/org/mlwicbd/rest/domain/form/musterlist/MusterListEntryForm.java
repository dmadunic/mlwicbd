package org.mlwicbd.rest.domain.form.musterlist;

import org.mlwicbd.rest.domain.dto.ChildDto;
import org.mlwicbd.rest.domain.dto.ServiceRecordDto;

import java.io.Serializable;
import java.util.List;

/**
 * @author idekic
 * @since 28-Jul-16.
 */
public class MusterListEntryForm implements Serializable {

    private Long version;

    private Integer listNumber;

    //Person
    private Long personId;
    private String firstName;
    private String lastName;
    private String placeOfBirth;

    //Rank
    private Long rankId;
    private String sourceProfession;

    //Profession
    private Long professionId;

    //ReligiousDenomination
    private Long religionId;

    private String location;
    private String houseNumber;
    private Integer age;

    private Integer maritalStatus;
    private Boolean wifePresent;

    private Integer unit1, unit2, unit3;

    private List<ServiceRecordDto> serviceRecords;
    private String totalYearsServed;
    private String totalMonthsServed;

    private List<ChildDto> children;

    //MusterList
    private Long musterListId;

    // --- setters/getters ----------------------------------------------------

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public Integer getListNumber() {
        return listNumber;
    }

    public void setListNumber(Integer listNumber) {
        this.listNumber = listNumber;
    }

    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPlaceOfBirth() {
        return placeOfBirth;
    }

    public void setPlaceOfBirth(String placeOfBirth) {
        this.placeOfBirth = placeOfBirth;
    }

    public Long getRankId() {
        return rankId;
    }

    public void setRankId(Long rankId) {
        this.rankId = rankId;
    }

    public String getSourceProfession() {
        return sourceProfession;
    }

    public void setSourceProfession(String sourceProfession) {
        this.sourceProfession = sourceProfession;
    }

    public Long getProfessionId() {
        return professionId;
    }

    public void setProfessionId(Long professionId) {
        this.professionId = professionId;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(Integer maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public Boolean getWifePresent() {
        return wifePresent;
    }

    public void setWifePresent(Boolean wifePresent) {
        this.wifePresent = wifePresent;
    }

    public Integer getUnit1() {
        return unit1;
    }

    public void setUnit1(Integer unit1) {
        this.unit1 = unit1;
    }

    public Integer getUnit2() {
        return unit2;
    }

    public void setUnit2(Integer unit2) {
        this.unit2 = unit2;
    }

    public Integer getUnit3() {
        return unit3;
    }

    public void setUnit3(Integer unit3) {
        this.unit3 = unit3;
    }

    public Long getMusterListId() {
        return musterListId;
    }

    public void setMusterListId(Long musterListId) {
        this.musterListId = musterListId;
    }

    public List<ServiceRecordDto> getServiceRecords() {
        return serviceRecords;
    }

    public void setServiceRecords(List<ServiceRecordDto> serviceRecords) {
        this.serviceRecords = serviceRecords;
    }

    public String getTotalYearsServed() {
        return totalYearsServed;
    }

    public void setTotalYearsServed(String totalYearsServed) {
        this.totalYearsServed = totalYearsServed;
    }

    public String getTotalMonthsServed() {
        return totalMonthsServed;
    }

    public void setTotalMonthsServed(String totalMonthsServed) {
        this.totalMonthsServed = totalMonthsServed;
    }

    public Long getReligionId() {
        return religionId;
    }

    public void setReligionId(Long religionId) {
        this.religionId = religionId;
    }

    public List<ChildDto> getChildren() {
        return children;
    }

    public void setChildren(List<ChildDto> children) {
        this.children = children;
    }
}
