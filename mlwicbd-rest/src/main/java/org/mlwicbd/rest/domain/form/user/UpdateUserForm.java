package org.mlwicbd.rest.domain.form.user;

/**
 * Created by dmadunic on 10/04/16.
 */
public class UpdateUserForm extends CreateUserForm {

    private String oldPassword;

    private String password;

    private String password2;

    //--- set / get methods ---------------------------------------------------
    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword2() {
        return password2;
    }

    public void setPassword2(String password2) {
        this.password2 = password2;
    }
}
