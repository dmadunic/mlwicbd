package org.mlwicbd.rest.domain.dto;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;
import org.mlwicbd.model.User;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

/**
 * Created by dmadunic on 21/04/16.
 */
public class MusterListDto extends BaseAuditableDto implements Serializable {

    private Long id;

    private Long version;

    private String title;

    private String referenceCode;

    private int year;

    private int month;

    private Long unitId;

    private String unitName;

    private String unitType;

    private Long parentUnitId;

    private String parentUnitName;

    private String parentUnitType;

    private long entriesCount = 0;

    //--- set  / get methods --------------------------------------------------

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public String getReferenceCode() {
        return referenceCode;
    }

    public void setReferenceCode(String referenceCode) {
        this.referenceCode = referenceCode;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public Long getUnitId() {
        return unitId;
    }

    public void setUnitId(Long unitId) {
        this.unitId = unitId;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getUnitType() {
        return unitType;
    }

    public void setUnitType(String unitType) {
        this.unitType = unitType;
    }

    public Long getParentUnitId() {
        return parentUnitId;
    }

    public void setParentUnitId(Long parentUnitId) {
        this.parentUnitId = parentUnitId;
    }

    public String getParentUnitName() {
        return parentUnitName;
    }

    public void setParentUnitName(String parentUnitName) {
        this.parentUnitName = parentUnitName;
    }

    public String getParentUnitType() {
        return parentUnitType;
    }

    public void setParentUnitType(String parentUnitType) {
        this.parentUnitType = parentUnitType;
    }

    public long getEntriesCount() {
        return entriesCount;
    }

    public void setEntriesCount(long entriesCount) {
        this.entriesCount = entriesCount;
    }

}
