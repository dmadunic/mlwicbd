package org.mlwicbd.rest.domain.form.musterlist;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.mlwicbd.rest.domain.form.BaseSearchForm;

/**
 * Created by dmadunic on 05/10/2016.
 */
public class MlEntrySearchForm extends BaseSearchForm {

    private Long personId;

    @Override
    public String toString() {
        final ToStringBuilder toStringBuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
        toStringBuilder.append(super.toString());
        toStringBuilder.append("personId", personId);
        return toStringBuilder.toString();
    }

    //--- util methods --------------------------------------------------------

    public boolean isEmpty() {
        if (personId != null) {
            return false;
        }
        return true;
    }

    //--- set / get methods ---------------------------------------------------

    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }
}
