package org.mlwicbd.rest.domain.form;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * @author idekic
 * @since 01-Aug-16.
 */
public class BaseSearchForm implements com.ag04.support.web.PageableParams {

    private Integer page = 0;
    private Integer size = 25;
    private List<String> sort;

    @Override
    public String toString() {
        final ToStringBuilder toStringBuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
        toStringBuilder.append("page", page);
        toStringBuilder.append("size", size);
        toStringBuilder.append("sort", sort);

        return toStringBuilder.toString();
    }

    //--- set  / get methods --------------------------------------------------

    @Override
    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    @Override
    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    @Override
    public List<String> getSortableAttributes() {
        return sort;
    }

    public void setSort(List<String> sort) {
        this.sort = sort;
    }

    public Pageable getPageable() {
        Pageable pageable = com.ag04.support.web.RequestParamUtils.pageable(this);
        return pageable;
    }
}
