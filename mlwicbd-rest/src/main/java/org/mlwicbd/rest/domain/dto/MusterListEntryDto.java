package org.mlwicbd.rest.domain.dto;

import java.io.Serializable;
import java.util.List;

/**
 * @author idekic
 * @since 29-Jul-16.
 */
public class MusterListEntryDto implements Serializable {

    private Long id;

    private Long version;

    private Integer listNumber;

    private BaseMilitaryRankDto rank;

    private PersonDto person;

    /**
     * Source first name
     */
    private String firstName;

    /**
     * Source last Name
     */
    private String lastName;

    /**
     * Source Von.
     */
    private String placeOfBirth;

    private String location;

    private String houseNumber;

    private Integer age;

    private ReligiousDenominationDto religion;

    private Integer maritalStatus;

    private boolean wifePresent;

    private ProfessionDto profession;

    private String sourceProfession;

    private Integer unit1, unit2, unit3;

    private List<ChildDto> children;

    private List<ServiceRecordDto> serviceRecords;

    private Integer totalYearsServed;

    private Integer totalMonthsServed;

    private Long musterListId;

    private Integer year;

    private Integer month;

    // --- getters/setters ----------------------------------------------------

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public Integer getListNumber() {
        return listNumber;
    }

    public void setListNumber(Integer listNumber) {
        this.listNumber = listNumber;
    }

    public BaseMilitaryRankDto getRank() {
        return rank;
    }

    public void setRank(BaseMilitaryRankDto rank) {
        this.rank = rank;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPlaceOfBirth() {
        return placeOfBirth;
    }

    public void setPlaceOfBirth(String placeOfBirth) {
        this.placeOfBirth = placeOfBirth;
    }

    public PersonDto getPerson() {
        return person;
    }

    public void setPerson(PersonDto person) {
        this.person = person;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public ReligiousDenominationDto getReligion() {
        return religion;
    }

    public void setReligion(ReligiousDenominationDto religion) {
        this.religion = religion;
    }

    public Integer getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(Integer maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public boolean isWifePresent() {
        return wifePresent;
    }

    public void setWifePresent(boolean wifePresent) {
        this.wifePresent = wifePresent;
    }

    public ProfessionDto getProfession() {
        return profession;
    }

    public void setProfession(ProfessionDto profession) {
        this.profession = profession;
    }

    public Integer getUnit1() {
        return unit1;
    }

    public void setUnit1(Integer unit1) {
        this.unit1 = unit1;
    }

    public Integer getUnit2() {
        return unit2;
    }

    public void setUnit2(Integer unit2) {
        this.unit2 = unit2;
    }

    public Integer getUnit3() {
        return unit3;
    }

    public void setUnit3(Integer unit3) {
        this.unit3 = unit3;
    }

    public List<ChildDto> getChildren() {
        return children;
    }

    public void setChildren(List<ChildDto> children) {
        this.children = children;
    }

    public List<ServiceRecordDto> getServiceRecords() {
        return serviceRecords;
    }

    public void setServiceRecords(List<ServiceRecordDto> serviceRecords) {
        this.serviceRecords = serviceRecords;
    }

    public Integer getTotalYearsServed() {
        return totalYearsServed;
    }

    public void setTotalYearsServed(Integer totalYearsServed) {
        this.totalYearsServed = totalYearsServed;
    }

    public Integer getTotalMonthsServed() {
        return totalMonthsServed;
    }

    public void setTotalMonthsServed(Integer totalMonthsServed) {
        this.totalMonthsServed = totalMonthsServed;
    }

    public String getSourceProfession() {
        return sourceProfession;
    }

    public void setSourceProfession(String sourceProfession) {
        this.sourceProfession = sourceProfession;
    }

    public Long getMusterListId() {
        return musterListId;
    }

    public void setMusterListId(Long musterListId) {
        this.musterListId = musterListId;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }
}
