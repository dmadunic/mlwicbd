package org.mlwicbd.rest.domain.form;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;

/**
 * @author idekic
 * @since 19-Aug-16.
 */
public class PersonForm implements Serializable {

    private Long version;

    private String firstName;

    private String lastName;

    private String sourcesFirstNames;

    private String sourcesLastNames;

    private String placeOfBirth;

    private String note;

    @Override
    public String toString() {
        final ToStringBuilder toStringBuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
        toStringBuilder.append("firstName", firstName);
        toStringBuilder.append("lastName", lastName);
        toStringBuilder.append("version", version);
        toStringBuilder.append("sourcesFirstNames", sourcesFirstNames);
        toStringBuilder.append("sourcesLastNames", sourcesLastNames);
        toStringBuilder.append("placeOfBirth", placeOfBirth);
        toStringBuilder.append("note", note);
        return toStringBuilder.toString();
    }

    //--- set  / get methods --------------------------------------------------

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSourcesFirstNames() {
        return sourcesFirstNames;
    }

    public void setSourcesFirstNames(String sourcesFirstNames) {
        this.sourcesFirstNames = sourcesFirstNames;
    }

    public String getSourcesLastNames() {
        return sourcesLastNames;
    }

    public void setSourcesLastNames(String sourcesLastNames) {
        this.sourcesLastNames = sourcesLastNames;
    }

    public String getPlaceOfBirth() {
        return placeOfBirth;
    }

    public void setPlaceOfBirth(String placeOfBirth) {
        this.placeOfBirth = placeOfBirth;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }
}
