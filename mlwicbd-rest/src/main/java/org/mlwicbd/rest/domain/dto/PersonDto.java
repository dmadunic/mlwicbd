package org.mlwicbd.rest.domain.dto;

import java.io.Serializable;
import java.util.Map;

/**
 * @author idekic
 * @since 01-Aug-16.
 */
public class PersonDto implements Serializable {

    private Long id;

    private Long version;

    private String firstName;

    private String lastName;

    //TODO: convert this to array or list
    private String sourcesFirstNames;

    //TODO: convert this to array or list
    private String sourcesLastNames;

    private String placeOfBirth;

    private String note;

    private Map<String, String> firstMusterList;

    private Map<String, String> lastMusterList;

    //--- set / get methods ---------------------------------------------------

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSourcesFirstNames() {
        return sourcesFirstNames;
    }

    public void setSourcesFirstNames(String sourcesFirstNames) {
        this.sourcesFirstNames = sourcesFirstNames;
    }

    public String getSourcesLastNames() {
        return sourcesLastNames;
    }

    public void setSourcesLastNames(String sourcesLastNames) {
        this.sourcesLastNames = sourcesLastNames;
    }

    public String getPlaceOfBirth() {
        return placeOfBirth;
    }

    public void setPlaceOfBirth(String placeOfBirth) {
        this.placeOfBirth = placeOfBirth;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Map<String, String> getFirstMusterList() {
        return firstMusterList;
    }

    public void setFirstMusterList(Map<String, String> firstMusterList) {
        this.firstMusterList = firstMusterList;
    }

    public Map<String, String> getLastMusterList() {
        return lastMusterList;
    }

    public void setLastMusterList(Map<String, String> lastMusterList) {
        this.lastMusterList = lastMusterList;
    }
}
