package org.mlwicbd.rest.support.csv;


import org.mlwicbd.model.MilitaryUnit;
import org.mlwicbd.model.musterlist.MusterList;
import org.mlwicbd.model.person.MaritalStatus;
import org.mlwicbd.rest.domain.dto.*;
import org.supercsv.io.ICsvMapWriter;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by tgreblicki on 28.04.17..
 */
public class MlCsvWriterFormatter {

    private ICsvMapWriter writer;
    final private String[] mlHeaders = {
            "ID",
            "Version",
            "Title",
            "Reference Code",
            "Year",
            "Month",
            "Unit ID",
            "Unit name",
            "Unit type",
            "Parent unit ID",
            "Parent unit name",
            "Parent unit type",
            "Entries count"
    };
    final private String[] mleHeaders = {
            "Muster list ID",
            "Muster list year",
            "Muster list month",
            "ID",
            "Version",
            "List number",
            "Rank",
            "Person",
            "First name",
            "Last name",
            "Place of birth",
            "Location",
            "House number",
            "Age",
            "Religion",
            "Marital status",
            "Wife present",
            "Profession",
            "Source profession",
            "Unit 1",
            "Unit 2",
            "Unit 3",
            "Children",
            "Service Records",
            "Total years served",
            "Total months served"
    };
    final private String[] mleServiceRecordsHeaders = {
            "Muster list entry ID",
            "Service record ID",
            "Service record description",
            "Service record duration (years)",
            "Service record duration (months)",
            "Service record duration (days)"
    };
    final private String[] mleChildHeaders = {
            "Muster list entry ID",
            "Child ID",
            "Child name",
            "Child age",
            "Child gender",
            "Person ID",
            "Person first name",
            "Person last name",
    };
    final private String[] mlePersonHeaders = {
            "ID",
            "Version",
            "First name",
            "Last name",
            "Source first name",
            "Source last name",
            "Note"

    };
    final private String[] mleRankHeaders = {
            "ID",
            "Code",
            "Territory code"
    };
    final private String[] mleReligiousDenominatioHeaders = {
            "ID",
            "Code"
    };

    final private String[] mleProfessionHeaders = {
            "ID",
            "Code"
    };

    final private String[] mleCombinedHeaders = {
            "Muster list ID",//0
            "Muster list year",//1
            "Muster list month",//2
            "ID",//3
            "Version",//4
            "List number",//5
            "Rank ID",//6
            "Rank Code",//7
            "Rank Territory code",//8
            "Person ID",//9
            "Person first name",//10
            "Person last name",//11
            "Person source first name",//12
            "Person source last name",//13
            "Person note",//14
            "First name",//15
            "Last name",//16
            "Place of birth",//17
            "Location",//18
            "House number",//19
            "Age",//20
            "Religious denomination ID",//21
            "Religious denomination code",//22
            "Marital status",//23
            "Wife present",//24
            "Profession ID",//25
            "Profession code",//26
            "Source profession",//27
            "Unit 1",//28
            "Unit 2",//29
            "Unit 3",//30
            "Children count",//31
            "Service records count",//32
            "Service records",//33
            "Total years served",//34
            "Total months served"//35
    };

    public MlCsvWriterFormatter(ICsvMapWriter writer){
        this.writer = writer;
    }



    public void write(MusterListDto entryDto, List<MusterListEntryDto> entryDtos) throws IOException{

        // --- write muster list info -----------------------------------------
        final Map<String, Object> mlMap =  new HashMap<>();
        writer.writeHeader(mlHeaders);
        mlMap.put(mlHeaders[0], entryDto.getId());
        mlMap.put(mlHeaders[1], entryDto.getVersion());
        mlMap.put(mlHeaders[2], entryDto.getTitle());
        mlMap.put(mlHeaders[3], entryDto.getReferenceCode());
        mlMap.put(mlHeaders[4], entryDto.getYear());
        mlMap.put(mlHeaders[5], entryDto.getMonth());
        mlMap.put(mlHeaders[6], entryDto.getUnitId());
        mlMap.put(mlHeaders[7], entryDto.getUnitName());
        mlMap.put(mlHeaders[8], entryDto.getUnitType());
        mlMap.put(mlHeaders[9], entryDto.getParentUnitId());
        mlMap.put(mlHeaders[10], entryDto.getParentUnitName());
        mlMap.put(mlHeaders[11], entryDto.getParentUnitType());
        mlMap.put(mlHeaders[12], entryDto.getEntriesCount());
        writer.write(mlMap, mlHeaders);

        // --- write muster list entries info ---------------------------------
        writer.writeHeader(mleCombinedHeaders);
        for (MusterListEntryDto mleDto: entryDtos){
            final Map<String, Object> mleMap = new HashMap<>();
            mleMap.put(mleCombinedHeaders[0], mleDto.getMusterListId());
            mleMap.put(mleCombinedHeaders[1], entryDto.getYear());
            mleMap.put(mleCombinedHeaders[2], entryDto.getMonth());
            mleMap.put(mleCombinedHeaders[3], mleDto.getId());
            mleMap.put(mleCombinedHeaders[4], mleDto.getVersion());
            mleMap.put(mleCombinedHeaders[5], mleDto.getListNumber());
            mleMap.put(mleCombinedHeaders[6], Optional.of(mleDto)
                    .map(MusterListEntryDto::getRank)
                    .map(BaseMilitaryRankDto::getId)
                    .orElse(null));
            mleMap.put(mleCombinedHeaders[7], Optional.of(mleDto)
                    .map(MusterListEntryDto::getRank)
                    .map(BaseMilitaryRankDto::getCode)
                    .orElse(null));
            mleMap.put(mleCombinedHeaders[8], Optional.of(mleDto)
                    .map(MusterListEntryDto::getRank)
                    .map(BaseMilitaryRankDto::getTerritoryCode)
                    .orElse(null));
            mleMap.put(mleCombinedHeaders[9], Optional.of(mleDto)
                    .map(MusterListEntryDto::getPerson)
                    .map(PersonDto::getId)
                    .orElse(null));
            mleMap.put(mleCombinedHeaders[10], Optional.of(mleDto)
                    .map(MusterListEntryDto::getPerson)
                    .map(PersonDto::getFirstName).orElse(null));
            mleMap.put(mleCombinedHeaders[11], Optional.of(mleDto)
                    .map(MusterListEntryDto::getPerson)
                    .map(PersonDto::getLastName)
                    .orElse(null));
            mleMap.put(mleCombinedHeaders[12], Optional.of(mleDto)
                    .map(MusterListEntryDto::getPerson)
                    .map(PersonDto::getSourcesFirstNames)
                    .orElse(null));
            mleMap.put(mleCombinedHeaders[13], Optional.of(mleDto)
                    .map(MusterListEntryDto::getPerson)
                    .map(PersonDto::getSourcesLastNames)
                    .orElse(null));
            mleMap.put(mleCombinedHeaders[14], Optional.of(mleDto)
                    .map(MusterListEntryDto::getPerson)
                    .map(PersonDto::getNote)
                    .orElse(null));
            mleMap.put(mleCombinedHeaders[15], mleDto.getFirstName());
            mleMap.put(mleCombinedHeaders[16], mleDto.getLastName());
            mleMap.put(mleCombinedHeaders[17], mleDto.getPlaceOfBirth());
            mleMap.put(mleCombinedHeaders[18], mleDto.getLocation());
            mleMap.put(mleCombinedHeaders[19], mleDto.getHouseNumber());
            mleMap.put(mleCombinedHeaders[20], mleDto.getAge());
            mleMap.put(mleCombinedHeaders[21], Optional.of(mleDto)
                    .map(MusterListEntryDto::getReligion)
                    .map(ReligiousDenominationDto::getId)
                    .orElse(null));
            mleMap.put(mleCombinedHeaders[22], Optional.of(mleDto)
                    .map(MusterListEntryDto::getReligion)
                    .map(ReligiousDenominationDto::getCode)
                    .orElse(null));
            mleMap.put(mleCombinedHeaders[23], Arrays.stream(MaritalStatus.values())
                    .filter(ms-> ms.getId().equals(mleDto.getMaritalStatus()))
                    .map(MaritalStatus::getDescription)
                    .collect(Collectors.toList())
                    .get(0));
            mleMap.put(mleCombinedHeaders[24], mleDto.isWifePresent());
            mleMap.put(mleCombinedHeaders[25], Optional.of(mleDto)
                    .map(MusterListEntryDto::getProfession)
                    .map(ProfessionDto::getId)
                    .orElse(null));
            mleMap.put(mleCombinedHeaders[26], Optional.of(mleDto)
                    .map(MusterListEntryDto::getProfession)
                    .map(ProfessionDto::getCode)
                    .orElse(null));
            mleMap.put(mleCombinedHeaders[27], mleDto.getSourceProfession());
            mleMap.put(mleCombinedHeaders[28], mleDto.getUnit1());
            mleMap.put(mleCombinedHeaders[29], mleDto.getUnit2());
            mleMap.put(mleCombinedHeaders[30], mleDto.getUnit3());
            mleMap.put(mleCombinedHeaders[31], Optional.of(mleDto)
                    .map(MusterListEntryDto::getChildren)
                    .map(childDtos -> childDtos.size()).orElse(0));
            mleMap.put(mleCombinedHeaders[32], Optional.of(mleDto)
                    .map(MusterListEntryDto::getServiceRecords)
                    .map(serviceRecordDtos -> serviceRecordDtos.size()).orElse(0));
            mleMap.put(mleCombinedHeaders[33], formatServiceRecordCell(mleDto.getServiceRecords()));
            mleMap.put(mleCombinedHeaders[34], mleDto.getTotalYearsServed());
            mleMap.put(mleCombinedHeaders[35], mleDto.getTotalMonthsServed());

            writer.write(mleMap, mleCombinedHeaders);


        }
        // --- write children info for every muster list entry ----------------
        writer.writeHeader(mleChildHeaders);
        for (MusterListEntryDto mleDto: entryDtos){
            List<ChildDto> children = mleDto.getChildren();
            for (ChildDto childDto : children) {
                final Map<String, Object> childMap = new HashMap<>();
                childMap.put(mleChildHeaders[0], mleDto.getId());
                childMap.put(mleChildHeaders[1], childDto.getId());
                childMap.put(mleChildHeaders[2], childDto.getName());
                childMap.put(mleChildHeaders[3], childDto.getAge());
                childMap.put(mleChildHeaders[4], childDto.getGender());
                childMap.put(mleChildHeaders[5], Optional.of(mleDto.getPerson())
                        .map(PersonDto::getId)
                        .orElse(null));
                childMap.put(mleChildHeaders[6], Optional.of(mleDto.getPerson())
                        .map(PersonDto::getFirstName)
                        .orElse(null));
                childMap.put(mleChildHeaders[7], Optional.of(mleDto.getPerson())
                        .map(PersonDto::getLastName)
                        .orElse(null));
                writer.write(childMap, mleChildHeaders);
            }
        }

        // --- write service records info for every muster list entry ---------
//        writer.writeHeader(mleServiceRecordsHeaders);
//        for (MusterListEntryDto mleDto: entryDtos){
//            List<ServiceRecordDto> serviceRecords = mleDto.getServiceRecords();
//            for(ServiceRecordDto srDto: serviceRecords){
//                final Map<String,Object> srMap = new HashMap<>();
//                srMap.put(mleServiceRecordsHeaders[0], mleDto.getId());
//                srMap.put(mleServiceRecordsHeaders[1], srDto.getId());
//                srMap.put(mleServiceRecordsHeaders[2], srDto.getDescription());
//                srMap.put(mleServiceRecordsHeaders[3], srDto.getDurationYears());
//                srMap.put(mleServiceRecordsHeaders[4], srDto.getDurationMonths());
//                srMap.put(mleServiceRecordsHeaders[5], srDto.getDurationDays());
//                writer.write(srMap, mleServiceRecordsHeaders);
//            }
//        }



    }

    // --- support methods ----------------------------------------------------
    private String formatServiceRecordCell(List<ServiceRecordDto> list){
        StringBuilder stringBuilder = new StringBuilder();
        if (list == null){
            return null;
        }

        for (ServiceRecordDto sr : list) {
            stringBuilder.append(
                    sr.getDescription() +
                            "\t" + sr.getDurationYears() +
                            "\t" + sr.getDurationMonths() +
                            "\t" + sr.getDurationDays() + "\n"
            );
        }

        return stringBuilder.toString();
    }
}
