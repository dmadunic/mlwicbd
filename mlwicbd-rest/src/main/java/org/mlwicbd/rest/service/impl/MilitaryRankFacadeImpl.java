package org.mlwicbd.rest.service.impl;

import org.mlwicbd.criteria.MilitaryRankSearchCriteria;
import org.mlwicbd.model.MilitaryRank;
import org.mlwicbd.model.MilitaryUnit;
import org.mlwicbd.rest.domain.dto.BaseMilitaryRankDto;
import org.mlwicbd.rest.domain.dto.MilitaryUnitDto;
import org.mlwicbd.rest.mapper.dto.MilitaryRankMapper;
import org.mlwicbd.rest.service.MilitaryRankFacade;
import org.mlwicbd.service.MilitaryRankService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dmadunic on 27/02/17.
 */
@Service
public class MilitaryRankFacadeImpl implements MilitaryRankFacade {

    @Autowired
    private MilitaryRankMapper mapper;

    @Autowired
    private MilitaryRankService rankService;

    @Override
    public Page<BaseMilitaryRankDto> searchByCriteria(final MilitaryRankSearchCriteria criteria, final Pageable pageable) {
        Page<MilitaryRank> ranks = rankService.searchByCriteria(criteria, pageable);
        List<BaseMilitaryRankDto> militaryRankDtos = convertToDtos(ranks.getContent());

        return new PageImpl<>(militaryRankDtos, pageable, ranks.getTotalElements());
    }

    private List<BaseMilitaryRankDto> convertToDtos(List<MilitaryRank> ranks) {
        List<BaseMilitaryRankDto> dtos = new ArrayList<>();
        for (MilitaryRank rank : ranks) {
            BaseMilitaryRankDto dto = mapper.mapToBaseDto(rank);
            dtos.add(dto);
        }
        return dtos;
    }
}
