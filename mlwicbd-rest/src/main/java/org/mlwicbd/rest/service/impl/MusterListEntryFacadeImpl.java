package org.mlwicbd.rest.service.impl;

import com.ag04.common.exception.EntityNotFoundException;
import org.mlwicbd.model.musterlist.MusterList;
import org.mlwicbd.model.musterlist.MusterListEntry;
import org.mlwicbd.model.person.Person;
import org.mlwicbd.rest.domain.dto.MusterListEntryDto;
import org.mlwicbd.rest.domain.form.musterlist.MlEntryMergeSearchForm;
import org.mlwicbd.rest.domain.form.musterlist.MlEntrySearchForm;
import org.mlwicbd.rest.domain.form.musterlist.MusterListEntryForm;
import org.mlwicbd.rest.mapper.dto.MusterListEntryMapper;
import org.mlwicbd.rest.mapper.form.MusterListEntryFormMapper;
import org.mlwicbd.rest.service.MusterListEntryFacade;
import org.mlwicbd.service.MusterListEntryService;
import org.mlwicbd.service.PersonService;
import org.mlwicbd.service.ServiceRecordService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collector;

/**
 * @author idekic
 * @since 01-Aug-16.
 */
@Service
public class MusterListEntryFacadeImpl implements MusterListEntryFacade {
    private static final Logger LOG = LoggerFactory.getLogger(MusterListEntryFacadeImpl.class);

    @Autowired
    private MusterListEntryService musterListEntryService;

    @Autowired
    private MusterListEntryMapper musterListEntryMapper;

    @Autowired
    private MusterListEntryFormMapper mleFormMapper;

    @Autowired
    private ServiceRecordService serviceRecordService;

    @Autowired
    private PersonService personService;

    @Override
    public Page<MusterListEntryDto> search(MlEntrySearchForm searchForm) {
        Page<MusterListEntryDto> entries = musterListEntryService.findByPersonId(searchForm.getPersonId(), searchForm.getPageable()).map(musterListEntryMapper::mapToDto);
        return entries;
    }

    @Override
    public Page<MusterListEntryDto> searchMerged(MlEntryMergeSearchForm searchForm) {
        List<Long> ids = new ArrayList<>();
        ids.add(searchForm.getPersonId1());
        ids.add(searchForm.getPersonId2());
        Page<MusterListEntryDto> entries = musterListEntryService.findByPersonId(ids, searchForm.getPageable()).map(musterListEntryMapper::mapToDto);
        return entries;
    }

    @Override
    public Page<MusterListEntryDto> findAllForList(Long musterListId, Pageable pageable) {
        Page<MusterListEntryDto> entries = musterListEntryService.findByMusterListId(musterListId, pageable)
                .map(musterListEntryMapper::mapToDto);

        return entries;
    }

    @Override
    public MusterListEntryDto findById(Long id) {
        MusterListEntryDto entry = musterListEntryMapper.mapToDto(musterListEntryService.findById(id));
        return entry;
    }

    public boolean exists(Long id) {
        return musterListEntryService.exists(id);
    }

    @Transactional
    @Override
    public void forceDelete(Long id) {
        serviceRecordService.deleteByMusterListEntryId(id);
        Long personId = musterListEntryService.findById(id).getPerson().getId();
        musterListEntryService.deleteById(id);
        personService.deleteById(personId);
    }

    @Transactional
    @Override
    public void deleteById(Long id) {
        musterListEntryService.deleteById(id);
    }

    @Override
    @Transactional
    public MusterListEntryDto createNew(MusterListEntryForm entryForm) {
        MusterListEntry entry = mleFormMapper.mapToEntity(entryForm, null);
        if (entry.getPerson() == null) {
            LOG.debug("Saving new person record for name={}/{} from {}", entry.getFirstName(), entry.getLastName(), entry.getPlaceOfBirth());
            Person person = new Person(
                entryForm.getFirstName(),
                entryForm.getLastName(),
                entryForm.getPlaceOfBirth(),
                entryForm.getFirstName(),
                entryForm.getLastName()
            );
            Person savedPerson = personService.save(person);
            entry.setPerson(savedPerson);
            LOG.info("SAVED new person entity --> person={}", savedPerson);
        }
        MusterListEntry savedEntry = musterListEntryService.save(entry);

        return musterListEntryMapper.mapToDto(savedEntry);
    }

    @Override
    public MusterListEntryDto update(Long id, MusterListEntryForm entryForm) throws EntityNotFoundException {
        MusterListEntry entry = musterListEntryService.findByIdAndVersion(id, entryForm.getVersion());

        Map<String, Object> config = new HashMap<>();
        config.put(MusterListEntryFormMapper.MLE_KEY, entry);
        entry = mleFormMapper.mapToEntity(entryForm, null, config);

        MusterListEntry savedPerson = musterListEntryService.save(entry);
        LOG.info("UPDATED person entity --> person={}", savedPerson);
        return musterListEntryMapper.mapToDto(savedPerson);

    }

    @Override
    public List<MusterListEntryDto> findAllForList(Long musterListId) {
        List<MusterListEntryDto> mleDtos = new ArrayList<>();

        musterListEntryService.findByMusterListId(musterListId)
                .stream()
                .forEach(musterListEntry -> mleDtos.add(musterListEntryMapper.mapToDto(musterListEntry)));
        return  mleDtos;
    }

    @Override
    public MusterListEntryDto findByMusterListIdAndPersonId(Long musterListId, Long personId) {
        MusterListEntry musterListEntry = musterListEntryService.findByMusterListIdAndPersonId(musterListId, personId);

        return musterListEntryMapper.mapToDto(musterListEntry);
    }
}
