package org.mlwicbd.rest.service.impl;

import org.mlwicbd.model.User;
import org.mlwicbd.rest.domain.dto.UserDto;
import org.mlwicbd.rest.domain.form.user.CreateUserForm;
import org.mlwicbd.rest.domain.form.user.UpdateUserForm;
import org.mlwicbd.rest.mapper.CreateUserFormMapper;
import org.mlwicbd.rest.mapper.UpdateUserFormMapper;
import org.mlwicbd.rest.mapper.UserDtoMapper;
import org.mlwicbd.rest.service.UserFacade;
import org.mlwicbd.rest.service.UserRegistrationService;
import org.mlwicbd.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by dmadunic on 02/04/16.
 */
@Service
public class UserFacadeImpl implements UserFacade {

    @Autowired
    private UserDtoMapper dtoMapper;

    @Autowired
    private UserRegistrationService userRegistrationService;

    @Autowired
    private CreateUserFormMapper formMapper;

    @Autowired
    private UpdateUserFormMapper updateUserformMapper;

    @Autowired
    private UserService userService;

    @Transactional
    public UserDto create(final CreateUserForm createUserForm) {
        User user = formMapper.mapToEntity(createUserForm, null);
        user.setEnabled(false);

        User regUser = userRegistrationService.registerUser(user);

        UserDto userDto = dtoMapper.mapFromEntity(regUser, null);
        return userDto;
    }

    @Override
    @Transactional(readOnly = true)
    public UserDto findByUsername(String username) {
        User user = userService.findByUsername(username);
        UserDto dto = dtoMapper.mapFromEntity(user, null);

        return dto;
    }

    @Override
    @Transactional(readOnly = true)
    public UserDto findByEmail(String email) {
        User user = userService.findByEmail(email);
        UserDto dto = dtoMapper.mapFromEntity(user, null);

        return dto;
    }

    @Override
    @Transactional(readOnly = true)
    public UserDto findById(Long id) {
        User user = userService.findById(id);
        UserDto dto = dtoMapper.mapFromEntity(user, null);

        return dto;
    }


    @Override
    @Transactional
    public UserDto update(Long id, UpdateUserForm updateUserForm) {
        User user = userService.findById(id);
        Map<String, Object> configs = new HashMap<String, Object>();
        configs.put(updateUserformMapper.USER_KEY, user);

        User mappedUser = updateUserformMapper.mapToEntity(updateUserForm, null, configs);

        User savedUser = userService.save(mappedUser);
        userService.resetCacheOnSave(savedUser.getUsername());

        UserDto dto = dtoMapper.mapFromEntity(savedUser, null);

        return dto;
    }
}
