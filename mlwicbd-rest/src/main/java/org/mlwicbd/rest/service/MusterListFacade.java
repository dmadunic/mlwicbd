package org.mlwicbd.rest.service;

import org.mlwicbd.criteria.MusterListSearchCriteria;
import org.mlwicbd.rest.domain.dto.MusterListDto;
import org.mlwicbd.rest.domain.form.musterlist.MusterListForm;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 *
 * Created by dmadunic on 21/04/16.
 */
public interface MusterListFacade {

    MusterListDto findOne(Long id);

    MusterListDto update(Long id, MusterListForm form);

    MusterListDto create(MusterListForm form);

    Page<MusterListDto> searchByCriteria(MusterListSearchCriteria criteria, Pageable pageable);

}
