package org.mlwicbd.rest.service;

import org.mlwicbd.model.person.Profession;
import org.mlwicbd.rest.domain.dto.ProfessionDto;
import org.mlwicbd.rest.domain.form.ProfessionForm;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Created by tgreblicki on 05.05.17..
 */
public interface ProfessionFacade {
    Page<Profession> filterProfessions(Pageable pageable, String professionName, String professionsCode);

    ProfessionDto save(ProfessionForm professionForm);

    ProfessionDto update(ProfessionForm professionForm, Long id);
}
