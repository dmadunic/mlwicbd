package org.mlwicbd.rest.service;

import org.mlwicbd.model.view.PersonMleView;
import org.mlwicbd.rest.domain.dto.PersonDto;

import java.util.List;

/**
 * Created by tgreblicki on 10.05.17..
 */
public interface PersonMleViewFacade {

    List<PersonMleView> findAllByPersonsId(List<PersonDto> personDtos);

    void assignMlReferenceToPersons(List<PersonDto> personDtos, List<PersonMleView> personMleViews);
}
