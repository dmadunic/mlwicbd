package org.mlwicbd.rest.service.impl;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import org.mlwicbd.model.person.Profession;
import org.mlwicbd.model.person.QProfession;
import org.mlwicbd.rest.domain.dto.ProfessionDto;
import org.mlwicbd.rest.domain.form.ProfessionForm;
import org.mlwicbd.rest.mapper.dto.ProfessionMapper;
import org.mlwicbd.rest.service.ProfessionFacade;
import org.mlwicbd.service.ProfessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

/**
 * Created by tgreblicki on 05.05.17..
 */
@Component
public class ProfessionFacadeImpl implements ProfessionFacade {

    @Autowired
    private ProfessionService professionService;

    @Autowired
    private ProfessionMapper professionMapper;

    @Override
    public Page<Profession> filterProfessions(Pageable pageable, String professionName, String professionsCode) {
        Predicate predicate = PredicateBuilder
                .builder()
                .professionName(professionName)
                .professionCode(professionsCode)
                .build();
        return professionService.findAll(pageable, predicate);
    }

    private static final class PredicateBuilder {
        private QProfession qProfession = QProfession.profession;
        private BooleanBuilder booleanBuilder = new BooleanBuilder();

        public PredicateBuilder() {}
        public Predicate build() {
            return booleanBuilder;
        }
        public static PredicateBuilder builder() {
            return new PredicateBuilder();
        }

        public PredicateBuilder professionName(String value) {
            if (value != null  && !value.isEmpty()){
                booleanBuilder.and(qProfession.name.containsIgnoreCase(value));
            }
            return this;
        }
        public PredicateBuilder professionCode(String value) {
            if (value != null && !value.isEmpty()){
                booleanBuilder.and(qProfession.code.containsIgnoreCase(value));
            }
            return this;
        }
    }

    @Override
    public ProfessionDto save(ProfessionForm professionForm) {
        Profession profession = professionMapper.mapForm(professionForm);
        Profession savedProfession = professionService.save(profession);
        return professionMapper.mapToDto(savedProfession);

    }

    @Override
    public ProfessionDto update(ProfessionForm professionForm, Long id) {
        Profession profession = professionMapper.mapForm(professionForm);
        Profession updatedProfession = professionService.save(profession);
        return professionMapper.mapToDto(updatedProfession);
    }
}
