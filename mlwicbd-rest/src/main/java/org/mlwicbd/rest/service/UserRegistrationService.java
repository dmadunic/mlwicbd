package org.mlwicbd.rest.service;

import org.mlwicbd.model.User;

/**
 * @author idekic
 * @since 26-Jul-16.
 */
public interface UserRegistrationService {
    User registerUser(User user);
}
