package org.mlwicbd.rest.service;

import org.mlwicbd.criteria.PersonSearchCriteria;
import org.mlwicbd.rest.domain.dto.PersonDto;
import org.mlwicbd.rest.domain.form.PersonForm;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * @author idekic
 * @since 22-Aug-16.
 */
public interface PersonFacade {
    Page<PersonDto> searchByCriteria(PersonSearchCriteria criteria, Pageable pageable);

    PersonDto findOne(Long id);

    PersonDto update(Long id, PersonForm personForm);

    void deleteById(Long id);

    PersonDto create(PersonForm personForm);

    void merge(Long sourceId, Long targetId);
}
