package org.mlwicbd.rest.service;

import org.mlwicbd.rest.domain.dto.UserDto;
import org.mlwicbd.rest.domain.form.user.CreateUserForm;
import org.mlwicbd.rest.domain.form.user.UpdateUserForm;

/**
 * Created by dmadunic on 02/04/16.
 */
public interface UserFacade {

    UserDto findById(Long id);

    UserDto findByUsername(String username);

    UserDto findByEmail(String username);

    UserDto create(CreateUserForm createUserForm);

    UserDto update(Long id, UpdateUserForm updateUserForm);

}
