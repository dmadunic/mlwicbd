package org.mlwicbd.rest.service.impl;

import org.mlwicbd.criteria.MusterListSearchCriteria;
import org.mlwicbd.dao.MleCountResult;
import org.mlwicbd.dao.repository.MusterListEntryRepository;
import org.mlwicbd.model.musterlist.MusterList;
import org.mlwicbd.model.musterlist.MusterListEntry;
import org.mlwicbd.rest.domain.dto.MusterListDto;
import org.mlwicbd.rest.domain.form.musterlist.MusterListForm;
import org.mlwicbd.rest.mapper.dto.MusterListMapper;
import org.mlwicbd.rest.mapper.form.MusterListFormMapper;
import org.mlwicbd.rest.service.MusterListFacade;
import org.mlwicbd.service.MusterListEntryService;
import org.mlwicbd.service.MusterListService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * Created by dmadunic on 21/04/16.
 */
@Service
public class MusterListFacadeImpl implements MusterListFacade {
    private static final Logger LOG = LoggerFactory.getLogger(MusterListFacadeImpl.class);

    @Autowired
    private MusterListService service;

    @Autowired
    private MusterListEntryService entriesService;

    @Autowired
    private MusterListEntryRepository musterListEntryRepository;

    @Autowired
    private MusterListMapper mapper;

    @Autowired
    private MusterListFormMapper mlFormMapper;

    @Override
    @Transactional(readOnly = true)
    public Page<MusterListDto> searchByCriteria(MusterListSearchCriteria criteria, Pageable pageable) {
        LOG.debug("Searching musterLists by criteria={}", criteria);
        Page<MusterList> lists = service.searchByCriteria(criteria, pageable);

        LOG.info("Found total of {} musterLists that match criteria={} ---> converting to DTOs {} fetched items", lists.getTotalElements(), criteria, lists.getNumberOfElements());
        List<MusterListDto> listsDtos = convertToDtos(lists.getContent());
        return new PageImpl<>(listsDtos, pageable, lists.getTotalElements());
    }

    @Override
    @Transactional(readOnly = true)
    public MusterListDto findOne(Long id) {
        MusterList musterList = service.findById(id);
        return convertToDto(musterList);
    }

    private MusterListDto convertToDto(MusterList musterList) {
        MusterListDto dto = mapper.musterListToDto(musterList);
        int entries = entriesService.countEntires(musterList.getId());
        dto.setEntriesCount(entries);

        return dto;
    }

    @Override
    public MusterListDto create(final MusterListForm form) {
        Map<String, Object> config = new HashMap<>();
        MusterList ml = new MusterList();
        config.put(MusterListFormMapper.ML_KEY, ml);

        return save(form, config);
    }

    @Override
    public MusterListDto update(final Long id, final MusterListForm form) {
        MusterList ml = service.findByIdAndVersion(id, form.getVersion());
        Map<String, Object> config = new HashMap<>();
        config.put(MusterListFormMapper.ML_KEY, ml);

        return save(form, config);
    }

    private MusterListDto save(MusterListForm form, Map<String, Object> config) {
        MusterList musterList = mlFormMapper.mapToEntity(form, null, config);
        MusterList savedMusterList = service.save(musterList);

        return convertToDto(savedMusterList);
    }

    private List<MusterListDto> convertToDtos(List<MusterList> lists) {
        List<MusterListDto> dtos = new ArrayList<MusterListDto>();
        List<Long> mlids = new ArrayList<Long>();
        for (MusterList musterList : lists) {
            MusterListDto dto = mapper.musterListToDto(musterList);
            dtos.add(dto);
            mlids.add(musterList.getId());
        }

        List<MleCountResult> entryCounts = new ArrayList<>();
        if (mlids.size() > 0) {
            entryCounts = musterListEntryRepository.countEntries(mlids);
        }

        for (MusterListDto dto : dtos) {
            Long count = getEntriesCount(entryCounts, dto.getId());
            dto.setEntriesCount(count);
        }
        return dtos;
    }

    private long getEntriesCount(List<MleCountResult> entryCounts, Long musterListId) {
        if (entryCounts == null) {
            return 0L;
        }
        for (MleCountResult res : entryCounts) {
            if (musterListId.equals(res.getMusterListId())) {
                return res.getEntriesCount();
            }
        }
        return 0L;
    }
}
