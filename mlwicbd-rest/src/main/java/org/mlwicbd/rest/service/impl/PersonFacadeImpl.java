package org.mlwicbd.rest.service.impl;

import com.ag04.common.exception.EntityNotFoundException;
import org.mlwicbd.criteria.PersonSearchCriteria;
import org.mlwicbd.model.musterlist.MusterListEntry;
import org.mlwicbd.model.person.Person;
import org.mlwicbd.rest.domain.dto.PersonDto;
import org.mlwicbd.rest.domain.form.PersonForm;
import org.mlwicbd.rest.mapper.dto.PersonMapper;
import org.mlwicbd.rest.service.PersonFacade;
import org.mlwicbd.service.MusterListEntryService;
import org.mlwicbd.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * @author idekic
 * @since 22-Aug-16.
 */
@Service
public class PersonFacadeImpl implements PersonFacade {

    @Autowired
    private PersonService personService;

    @Autowired
    private PersonMapper personMapper;

    @Autowired
    private MusterListEntryService mleService;

    @Override
    public Page<PersonDto> searchByCriteria(PersonSearchCriteria criteria, Pageable pageable) {
        Page<Person> lists = personService.searchByCriteria(criteria, pageable);
        List<PersonDto> listsDtos = personMapper.personListToDtoList(lists.getContent());

        return new PageImpl<>(listsDtos, pageable, lists.getTotalElements());
    }

    @Override
    public PersonDto findOne(Long id) {
        Person person = personService.findOne(id);
        PersonDto personDto = personMapper.personToDto(person);

        return personDto;
    }

    @Override
    public PersonDto update(Long id, PersonForm personForm) {
        Person entry = personService.findOne(id);

        if (entry == null) {
            throw new EntityNotFoundException(id.toString());
        }

        entry = personMapper.updatePersonFromPersonForm(entry, personForm);
        Person savedEntry = personService.save(entry);

        return personMapper.personToDto(savedEntry);
    }

    @Transactional
    @Override
    public void deleteById(Long id) {
        personService.deleteById(id);
    }

    @Override
    public PersonDto create(PersonForm personForm) {
        Person person = personMapper.personFormToEntity(personForm);
        PersonDto personDto = personMapper.personToDto(personService.save(person));

        return personDto;
    }

    @Transactional
    @Override
    public void merge(Long sourceId, Long targetId) {
        List<MusterListEntry> mles = mleService.findByPersonId(Arrays.asList(sourceId, targetId));

        Person targetPerson= personService.findOne(targetId);

        List<MusterListEntry> mergedEntries = mergePersonMleRecords(mles, targetPerson);

        List<MusterListEntry> savedEntries = mleService.save(mergedEntries);

        mleService.deleteByPersonId(sourceId);
        personService.deleteById(sourceId);

        return ;
    }

    private List<MusterListEntry> mergePersonMleRecords(List<MusterListEntry> mles, Person person) {
        filterDuplicateMusterListEntries(mles, person.getId());

        for (MusterListEntry mle : mles) {
            mle.setPerson(person);
        }

        return mles;
    }

    /**
     * This method searches for possible duplicates of muster list entries while merging.
     * Method compares muster list entries by their muster list(id) and if there is two muster list entries in the same
     * muster list it deletes the one which didn't belong to target person before merge.
     *
     * @param musterListEntries list of muster list entries which are going to be reassigned to target person
     * @param targetPersonId person selected as target of merge process
     */
    private void filterDuplicateMusterListEntries(List<MusterListEntry> musterListEntries, Long targetPersonId) {

        for (int i = 0; i < musterListEntries.size() - 1; i++) {
            MusterListEntry tmp = musterListEntries.get(i);
            int tmpIndex = i;

            for (int j = i+1; j < musterListEntries.size(); j++) {
                if (tmp.getMusterList().getId().equals(musterListEntries.get(j).getMusterList().getId())) {
                    if (tmp.getPerson().getId().equals(targetPersonId)) {
                        musterListEntries.remove(j);
                    } else {
                        musterListEntries.remove(tmpIndex);
                    }
                }
            }
        }
    }
}
