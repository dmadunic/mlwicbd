package org.mlwicbd.rest.service.impl;

import org.mlwicbd.model.Role;
import org.mlwicbd.model.User;
import org.mlwicbd.rest.service.UserRegistrationService;
import org.mlwicbd.service.RoleService;
import org.mlwicbd.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.Set;

/**
 * TODO: check if we can remove this class!
 *
 * @author idekic
 * @since 26-Jul-16.
 */
public class DefaultUserRegistrationService implements UserRegistrationService {
    private static final Logger LOG = LoggerFactory.getLogger(DefaultUserRegistrationService.class);

    String defaultPassword;

    UserService userService;

    String defaultRole;

    RoleService roleService;

    public DefaultUserRegistrationService(final String defaultPassword, final String defaultRole, final UserService userService, final RoleService roleService) {
        super();
        this.defaultPassword = defaultPassword;
        this.userService = userService;
        this.roleService = roleService;
        this.defaultRole = defaultRole;
    }

    @Override
    public User registerUser(User user) {
        user.setPassword(defaultPassword);

        Set<Role> roles = new HashSet<>();
        roles.add(roleService.findById(defaultRole));
        user.setRoles(roles);

        User savedUser = userService.save(user);
        return savedUser;
    }
}
