package org.mlwicbd.rest.service;

import org.mlwicbd.criteria.MilitaryUnitSearchCriteria;
import org.mlwicbd.rest.domain.dto.MilitaryUnitDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Created by dmadunic on 17/04/16.
 */
public interface MilitaryUnitFacade {

    Page<MilitaryUnitDto> searchByCriteria(MilitaryUnitSearchCriteria criteria, Pageable pageable);
}
