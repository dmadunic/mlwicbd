package org.mlwicbd.rest.service.security;

import org.mlwicbd.dao.repository.UserRepository;
import org.mlwicbd.model.Role;
import org.mlwicbd.model.User;
import org.mlwicbd.support.security.UserDetailsCustom;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Transactional;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by dmadunic on 01/09/15.
 */
public class BaseUserDetailsService implements UserDetailsService {
    private static final Logger LOG = LoggerFactory.getLogger(BaseUserDetailsService.class);

    private UserRepository userRepository;

    public BaseUserDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        LOG.info("Searching for user with username={}", username);
        User user = userRepository.findByUsername(username);
        if (user == null) {
            LOG.info("User NOT found --> throwing UsernameNotFoundException");
            throw new UsernameNotFoundException(username);
        }

        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        for (Role role : user.getRoles()) {
            authorities.add(new SimpleGrantedAuthority(role.getId()));
        }
        org.springframework.security.core.userdetails.User userDetails = new UserDetailsCustom(user, authorities);;
        LOG.info("User FOUND --> returning userDetails={}", userDetails);
        return userDetails;
    }
}
