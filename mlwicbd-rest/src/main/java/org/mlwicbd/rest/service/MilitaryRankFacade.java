package org.mlwicbd.rest.service;

import org.mlwicbd.criteria.MilitaryRankSearchCriteria;
import org.mlwicbd.rest.domain.dto.BaseMilitaryRankDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Created by dmadunic on 27/02/17.
 */
public interface MilitaryRankFacade {

    Page<BaseMilitaryRankDto> searchByCriteria(MilitaryRankSearchCriteria criteria, Pageable pageable);

}