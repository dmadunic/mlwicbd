package org.mlwicbd.rest.service.impl;

import org.mlwicbd.model.view.PersonMleView;
import org.mlwicbd.rest.domain.dto.PersonDto;
import org.mlwicbd.rest.service.PersonMleViewFacade;
import org.mlwicbd.service.PersonMleViewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by tgreblicki on 10.05.17..
 */
@Component
public class PersonMleViewFacadeImpl implements PersonMleViewFacade{

    @Autowired
    private PersonMleViewService personMleViewService;

    @Override
    public List<PersonMleView> findAllByPersonsId(List<PersonDto> personDtos) {
        List<Long> personIds = new ArrayList<>();
        for(PersonDto p : personDtos){
            personIds.add(p.getId());
        }

        return personMleViewService.findAllByPersonId(personIds);
    }

    @Override
    public void assignMlReferenceToPersons(List<PersonDto> personDtos, List<PersonMleView> personMleViews) {

        // group views by person id (bypass person sort order)
        Map<Long, List<PersonMleView>> map = new HashMap<>();
        for (PersonMleView pmw : personMleViews){
            if (map.containsKey(pmw.getPersonId())){
                map.get(pmw.getPersonId()).add(pmw);
            }
            else {
                List<PersonMleView> list = new ArrayList<>();
                list.add(pmw);
                map.put(pmw.getPersonId(), list);
            }
        }

        // assign views by person id
        for(PersonDto p : personDtos) {
            Map<String, String> firstMusterList = new HashMap<>();
            Map<String, String> lastMusterList = new HashMap<>();
            if (map.get(p.getId()) == null){
                continue;
            }

            List<PersonMleView> list = map.get(p.getId());
            if (list.size() != 1){
                firstMusterList.put("id", list.get(0).getMlId().toString());
                firstMusterList.put("mlYear", list.get(0).getMlYear().toString());
                firstMusterList.put("mlMonth", list.get(0).getMlMonth().toString());
                p.setFirstMusterList(firstMusterList);

                lastMusterList.put("id", list.get(1).getMlId().toString());
                lastMusterList.put("mlYear", list.get(1).getMlYear().toString());
                lastMusterList.put("mlMonth", list.get(1).getMlMonth().toString());
                p.setLastMusterList(lastMusterList);
            }
            //if person have 1 entry set the same entry as firs and last
            else {
                firstMusterList.put("id", list.get(0).getMlId().toString());
                firstMusterList.put("mlYear", list.get(0).getMlYear().toString());
                firstMusterList.put("mlMonth", list.get(0).getMlMonth().toString());
                p.setFirstMusterList(firstMusterList);

                lastMusterList.put("id", list.get(0).getMlId().toString());
                lastMusterList.put("mlYear", list.get(0).getMlYear().toString());
                lastMusterList.put("mlMonth", list.get(0).getMlMonth().toString());
                p.setLastMusterList(lastMusterList);
            }


        }
    }
}
