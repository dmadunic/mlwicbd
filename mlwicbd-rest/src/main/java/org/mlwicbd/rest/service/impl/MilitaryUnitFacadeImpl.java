package org.mlwicbd.rest.service.impl;

import org.mlwicbd.criteria.MilitaryUnitSearchCriteria;
import org.mlwicbd.model.MilitaryUnit;
import org.mlwicbd.rest.domain.dto.MilitaryUnitDto;
import org.mlwicbd.rest.mapper.dto.MilitaryUnitMapper;
import org.mlwicbd.rest.service.MilitaryUnitFacade;
import org.mlwicbd.service.MilitaryUnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * Created by dmadunic on 17/04/16.
 */
@Service
public class MilitaryUnitFacadeImpl implements MilitaryUnitFacade {

    @Autowired
    private MilitaryUnitService militaryUnitService;

    @Autowired
    private MilitaryUnitMapper militaryUnitMapper;

    @Override
    @Transactional(readOnly = true)
    public Page<MilitaryUnitDto> searchByCriteria(final MilitaryUnitSearchCriteria criteria, final Pageable pageable) {
        Page<MilitaryUnit> units = militaryUnitService.searchByCriteria(criteria, pageable);
        List<MilitaryUnitDto> unitDtos = convertToDtos(units.getContent());

        return new PageImpl<>(unitDtos, pageable, units.getTotalElements());
    }

    private List<MilitaryUnitDto> convertToDtos(List<MilitaryUnit> units) {
        List<MilitaryUnitDto> dtos = new ArrayList<>();
        for (MilitaryUnit unit : units) {
            MilitaryUnitDto dto = militaryUnitMapper.militaryUnitToMilitaryUnitDto(unit);
            dtos.add(dto);
        }
        return dtos;
    }
}
