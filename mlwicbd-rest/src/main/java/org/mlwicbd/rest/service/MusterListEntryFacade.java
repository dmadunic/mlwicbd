package org.mlwicbd.rest.service;

import org.mlwicbd.rest.domain.dto.MusterListEntryDto;
import org.mlwicbd.rest.domain.form.musterlist.MlEntryMergeSearchForm;
import org.mlwicbd.rest.domain.form.musterlist.MlEntrySearchForm;
import org.mlwicbd.rest.domain.form.musterlist.MusterListEntryForm;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * @author idekic
 * @since 01-Aug-16.
 */
public interface MusterListEntryFacade {

    Page<MusterListEntryDto> search(MlEntrySearchForm searchForm);

    Page<MusterListEntryDto> findAllForList(Long musterListId, Pageable pageable);

    MusterListEntryDto findById(Long id);

    boolean exists(Long id);

    void forceDelete(Long id);

    void deleteById(Long id);

    MusterListEntryDto createNew(MusterListEntryForm entryForm);

    MusterListEntryDto update(Long id, MusterListEntryForm entryForm);

    List<MusterListEntryDto> findAllForList(Long musterListId);

    Page<MusterListEntryDto> searchMerged(MlEntryMergeSearchForm mlEntryMergeSearchForm);

    MusterListEntryDto findByMusterListIdAndPersonId(Long musterListId, Long personId);
}
