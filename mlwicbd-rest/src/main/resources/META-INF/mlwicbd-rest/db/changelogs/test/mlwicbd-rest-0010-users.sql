--liquibase formatted sql

--changeset dmadunic:20150829000000-1 context:test
insert into MLWICBD_USER (id, username, first_name, last_name, email, password, enabled) values (1, 'domagoj.madunic', 'Domagoj', 'Madunić', 'domagoj.madunic@gmail.com', '$2a$08$OX/aKKb08DCChvx0HWM6xuRu4BPmH42SuJyoyBW2FNGtEwRIPHhP.', true);
insert into MLWICBD_USER (id, username, first_name, last_name, email, password, enabled) values (2, 'alexander.buczynski', 'Alexander', 'Buczynski', 'alexander.buczynski@gmail.com', '$2a$08$OX/aKKb08DCChvx0HWM6xuRu4BPmH42SuJyoyBW2FNGtEwRIPHhP.', true);
insert into MLWICBD_USER (id, username, first_name, last_name, email, password, enabled) values (3, 'vedran.klauzer', 'Vedran', 'Klaužer', 'vedranklauzer@gmail.com', '$2a$08$OX/aKKb08DCChvx0HWM6xuRu4BPmH42SuJyoyBW2FNGtEwRIPHhP.', true);
insert into MLWICBD_USER (id, username, first_name, last_name, email, password, enabled) values (4, 'juraj.balic', 'Juraj', 'Balić', 'jura.balic@gmail.com', '$2a$08$OX/aKKb08DCChvx0HWM6xuRu4BPmH42SuJyoyBW2FNGtEwRIPHhP.', true);
insert into MLWICBD_USER (id, username, first_name, last_name, email, password, enabled) values (5, 'lovorka.coralic', 'Lovorka', 'Čoralić', 'lovorka@isp.hr', '$2a$08$OX/aKKb08DCChvx0HWM6xuRu4BPmH42SuJyoyBW2FNGtEwRIPHhP.', true);
insert into MLWICBD_USER (id, username, first_name, last_name, email, password, enabled) values (6, 'maja.katusic', 'Maja', 'Katušić', 'majkic1@gmail.com', '$2a$08$OX/aKKb08DCChvx0HWM6xuRu4BPmH42SuJyoyBW2FNGtEwRIPHhP.', true);
insert into MLWICBD_USER (id, username, first_name, last_name, email, password, enabled) values (7, 'nikola.markulin', 'Nikola', 'Markulin', 'nikola.markulin@gmail.com', '$2a$08$OX/aKKb08DCChvx0HWM6xuRu4BPmH42SuJyoyBW2FNGtEwRIPHhP.', true);
insert into MLWICBD_USER (id, username, first_name, last_name, email, password, enabled) values (8, 'ruza.rados', 'Ruža', 'Radoš', 'radosruza@yahoo.com', '$2a$08$OX/aKKb08DCChvx0HWM6xuRu4BPmH42SuJyoyBW2FNGtEwRIPHhP.', true);
insert into MLWICBD_USER (id, username, first_name, last_name, email, password, enabled) values (9, 'stjepan.matkovic', 'Stjepan', 'Matković', 'matkovic@isp.hr', '$2a$08$OX/aKKb08DCChvx0HWM6xuRu4BPmH42SuJyoyBW2FNGtEwRIPHhP.', true);
insert into MLWICBD_USER (id, username, first_name, last_name, email, password, enabled) values (10, 'tado.orsolic', 'Tado', 'Orsolić', 'taorsolic@gmail.com', '$2a$08$OX/aKKb08DCChvx0HWM6xuRu4BPmH42SuJyoyBW2FNGtEwRIPHhP.', true);
