# Stormpath Setup
## Stormpath Admin Console access data

tenant: red-prism
email: croatian.frontier@gmail.com
pass: 2015MlwicB

## Stormpath Authentication Instructions

### Setup
To use Stormpath as your authentication service, turn on the `stormpath` profile.
In your `application.properties` configuration file, set the value of `spring.profiles.active` to `stormpath`.
Note that there shouldn't be any active profiles set unless you are using Stormpath.
Additionally, set the value of `stormpath.enabled` to `true`.

### ApiKey
The ApiKey serves as your means of Stormpath authentication. It is located in `support/stormpath/apiKey.properties`.
This file contains your ID and password and shouldn't be shared with others. It must be valid for authentication to work.
Should you require a new key, you can generate a new one via the Stormpath web console (by clicking on the user menu and choosing 'My Api Keys').
You can then download your new key and use it to replace the existing one in the project.

### Accounts
User accounts are split into Directories, which are further separated into Groups. 
The two directories that are currently in use are `MLWICBD_PUBLIC` and `MLWICBD_TEAM`.
These contain groups as depicted below:

* `MLWICBD_PUBLIC`, which contains the accounts of guest users and users that aren't team members
    * `ROLE_ASSOCIATE` - associate users
    * `ROLE_GUEST` - guest users
* `MLWICBD_TEAM`, which contains the accounts of the team members
    * `ROLE_ADMIN` - users with administrative privileges
    * `ROLE_USER` - standard users
    
#### User Account Structure
An User account must contain the following custom properties:

* `address` - the user's address
* `countryCode` - the country code representing the user's country in the database
* `mobile` - the user's mobile phone number
* `phone` - the user's phone number
* `place` - the user's place of residence
* `postalCode` - the user's postal code