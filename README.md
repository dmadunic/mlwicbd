# Croatian Military Life database (mlwicbd)
## Spring boot Web, Jpa domain and Angular2 application
### Modules

Application consists of three modules:

* mlwicbd-domain - Java (jar packed) module, that contains all models, services and repositories
* mlwicbd-rest - java (war packaged) module that contains backend web application which exposes services as rest endpoints
* mlwicbd-ng2-app - Angular2 frontend SPA (packaged as zip)

### Links

* Deploy application to devhub (DEPLOY_DEVHUB.md)

## Usage
### Requirements
* [Java JDK] (http://www.oracle.com/technetwork/java/javase/downloads/index.html) 
* [NodeJS](http://nodejs.org/) (with [NPM](https://www.npmjs.org/))
* [npm](https://www.npmjs.com/)
* [typescript](http://www.typescriptlang.org) Simplest way is to simply run: `npm install -g typescript`
* [angular-cli](https://github.com/angular/angular-cli)


### Setup (First time)

1. Clone the repository: `git clone git@bitbucket.org:dmadunic/mlwicbd.git`
2. Run gradle to build application:

    ```
    cd mlwicbd/
    ./gradlew clean build
    ```

## Application configuration

Review application properties file: application.properties (in mlwicbd-rest module) and enable profiles suitable for your local environment.

For example to run application with authentication against local db, and with bootrun sprong.profiles.active lne should look as following:
    
    ```
    spring.profiles.active=dbauth,oauth2,bootrun
    ```
    
### Available profiles

The following profiles are supported by mlwicbd-rest application:

* bootrun - embedded Tomcat instance in Spring bootrun task
* appserver - run on localy installed Tomcat that provides database connections as JNDI resource
* devhub - run application on Tomcat (production mode) that provides database connections as JNDI resource on devhub.ag04.com.

## Running mlwicbd application locally

The simplest way to run application is through spring boot and angular-cli live reload server.

1. Run bootRun task inside mlwicbd-rest/ folder

```bash
cd mlwicbd-rest/
../gradlew mlwichd:bootRun
```

2. Inside mlwicbd-ng2-app/ folder run the angular-cli task: `ng serve` . This will build any changes made automatically, and also run a live reload server on [http://localhost:4200](http://localhost:4200).

```bash
cd mlwicbd-ng2-app/
ng serve
```

## Postgres Database setup
1. Edit application.properties and uncomment all postgres related configuration and comment h2 ones.
2. create mlwicbd user (AND set his "search_path" varable to "public" for "mlwicbd" database).
3. Create mlwicbd database and specify mlwicbd user as owner

If you are running application localy with bootrun make sure that configuration of database source in application.bootrun.properties file (in mlwicbd-restmodule)
matches your actual setup.

## Rest Api Documentation
Generated Rest api documentation is available at the following url:

[http://localhost:8080/mlwicbd-rest/swagger-ui.html](http://localhost:8080/mlwicbd-rest/swagger-ui.html)

## Credits
* Domagoj Madunić