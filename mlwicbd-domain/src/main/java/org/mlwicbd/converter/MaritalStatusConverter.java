package org.mlwicbd.converter;

import com.ag04.common.data.converter.EnumTypeConverter;
import org.mlwicbd.model.person.MaritalStatus;
import org.springframework.stereotype.Component;

import javax.persistence.Converter;

/**
 * Created by dmadunic on 24/04/2016.
 */
@Component
@Converter(autoApply = true)
public class MaritalStatusConverter extends EnumTypeConverter<MaritalStatus, Integer> {

    public MaritalStatusConverter() {
        super(MaritalStatus.class);
    }
}
