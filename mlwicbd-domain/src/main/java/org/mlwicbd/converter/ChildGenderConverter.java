package org.mlwicbd.converter;

import com.ag04.common.data.converter.EnumTypeConverter;
import org.mlwicbd.model.musterlist.ChildGender;
import org.springframework.stereotype.Component;

import javax.persistence.Converter;

/**
 * Created by dmadunic on 27/09/2016.
 */
@Component
@Converter(autoApply = true)
public class ChildGenderConverter extends EnumTypeConverter<ChildGender, Integer> {

    public ChildGenderConverter() {
        super(ChildGender.class);
    }
}
