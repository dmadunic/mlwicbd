package org.mlwicbd.model.person;

import com.ag04.common.data.IdProperty;

/**
 * Created by dmadunic on 24/04/16.
 */
public enum MaritalStatus implements IdProperty<Integer> {
    SINGLE(1, "SINGLE"),
    MARRIED(2, "MARRIED"),
    WIDOWED(3, "WIDOWED");

    private final Integer id;

    private final String description;

    MaritalStatus(Integer id, String description) {
        this.id = id;
        this.description = description;
    }

    //--- set / get methods ---------------------------------------------------

    public Integer getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }
}
