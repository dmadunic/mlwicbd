package org.mlwicbd.model.person;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * Created by dmadunic on 24/04/16.
 */
@Embeddable
public class PersonMeasures {

    /**
     *
     */
    @Column(name="MLE_MEASURE_UNIT1", nullable = true)
    Integer unit1;

    /**
     *
     */
    @Column(name="MLE_MEASURE_UNIT2", nullable = true)
    Integer unit2;

    /**
     *
     */
    @Column(name="MLE_MEASURE_UNIT3", nullable = true)
    Integer unit3;

    //--- set / get methods ---------------------------------------------------

    public Integer getUnit1() {
        return unit1;
    }

    public void setUnit1(Integer unit1) {
        this.unit1 = unit1;
    }

    public Integer getUnit2() {
        return unit2;
    }

    public void setUnit2(Integer unit2) {
        this.unit2 = unit2;
    }

    public Integer getUnit3() {
        return unit3;
    }

    public void setUnit3(Integer unit3) {
        this.unit3 = unit3;
    }
}
