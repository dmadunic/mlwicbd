package org.mlwicbd.model.person;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by dmadunic on 24/04/16.
 */
@Entity
@Table(name = "RELIGIOUS_DENOMINATION")
public class ReligiousDenomination implements Serializable {
    @Id
    @Column(name = "RD_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * Unique Code value
     */
    @Column(name = "RD_CODE", nullable = false, unique = true, length = 40)
    private String code;

    @Column(name = "RD_NAME", nullable = false, length = 255)
    private String name;

    //--- set / get methods ---------------------------------------------------

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
