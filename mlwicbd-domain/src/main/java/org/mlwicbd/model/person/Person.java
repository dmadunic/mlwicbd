package org.mlwicbd.model.person;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.mlwicbd.model.AbstractAuditableEntity;

import javax.persistence.*;

/**
 * Created by domagoj on 24/04/2016.
 */
@Entity
@Table(name = "PERSON")
@AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "PRS_ID")) })
public class Person extends AbstractAuditableEntity {

    /**
     * Standardized first name of this person entity.
     */
    @Column(name = "PRS_FIRST_NAME", length = 40, nullable = false)
    private String firstName;

    /**
     * Standardized last name of this person entity.
     */
    @Column(name = "PRS_LAST_NAME", length = 40, nullable = false)
    private String lastName;

    /**
     * Variants of this person's first names as they appear in teh sources.
     */
    @Column(name = "PRS_SRC_FIRST_NAMES", nullable = false)
    private String sourcesFirstNames;

    /**
     * Variants of this person's last names as they appear in teh sources.
     */
    @Column(name = "PRS_SRC_LAST_NAMES", nullable = false)
    private String sourcesLastNames;

    /**
     * ??? maybe a foreign key to places/locations table?
     *
     */
    @Column(name = "PRS_PLACE_OF_BIRTH")
    private String placeOfBirth;

    /**
     * Some note regarding this entity.
     */
    @Column(name = "PRS_NOTE", nullable = true, length=255)
    private String note;

    public Person() {
        //
    }

    public Person(String firstName, String lastName, String placeOfBirth, String firstNames, String lastNames) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.placeOfBirth = placeOfBirth;
        this.sourcesFirstNames = firstNames;
        this.sourcesLastNames = lastNames;
    }

    @Override
    public String toString() {
        final ToStringBuilder sbuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
        sbuilder.append(super.toString());
        sbuilder.append("id", id);
        sbuilder.append("firstName", firstName);
        sbuilder.append("lastName", lastName);
        sbuilder.append("sourcesfirstNames", sourcesFirstNames);
        sbuilder.append("sourceslastNames", sourcesLastNames);
        sbuilder.append("placeOfBirth", placeOfBirth);
        sbuilder.append("note", note);
        return sbuilder.toString();
    }

    //--- set / get methods ---------------------------------------------------

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSourcesFirstNames() {
        return sourcesFirstNames;
    }

    public void setSourcesFirstNames(String sourcesFirstNames) {
        this.sourcesFirstNames = sourcesFirstNames;
    }

    public String getSourcesLastNames() {
        return sourcesLastNames;
    }

    public void setSourcesLastNames(String sourcesLastNames) {
        this.sourcesLastNames = sourcesLastNames;
    }

    public String getPlaceOfBirth() {
        return placeOfBirth;
    }

    public void setPlaceOfBirth(String placeOfBirth) {
        this.placeOfBirth = placeOfBirth;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
