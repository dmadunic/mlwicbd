package org.mlwicbd.model.person;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.mlwicbd.model.AbstractAuditableEntity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by dmadunic on 24/04/16.
 */
@Entity
@Table(name = "PROFESSION")
@AttributeOverride(name = "id", column = @Column(name = "PF_ID"))
public class Profession extends AbstractAuditableEntity implements Serializable {

    @Column(name = "PF_CODE", unique = true, nullable = false, length=40)
    private String code;

    @Column(name = "PF_NAME", nullable = true, length=255)
    private String name;

    @Override
    public String toString() {
        final ToStringBuilder sbuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
        sbuilder.append(super.toString());
        sbuilder.append("id", id);
        sbuilder.append("code", code);
        sbuilder.append("name", name);
        return sbuilder.toString();
    }

    //--- set  / get methods --------------------------------------------------

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
