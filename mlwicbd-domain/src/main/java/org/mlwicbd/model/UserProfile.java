package org.mlwicbd.model;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by dmadunic on 02/04/16.
 */
@Entity
@Table(name="USER_PROFILE")
public class UserProfile implements Serializable {

    @Id
    @Column(name="USER_ID")
    @GeneratedValue(generator="gen")
    @GenericGenerator(name="gen", strategy="foreign",
            parameters=@org.hibernate.annotations.Parameter(name="property", value="user"))
    private Long userId;

    @Column(name = "ADDRESS", nullable = true, length=80)
    private String address;

    @Column(name = "POSTAL_CODE", nullable = true, length=40)
    private String postalCode;

    @Column(name = "PLACE", nullable = true, length=80)
    private String place;

    @ManyToOne
    @Fetch(FetchMode.JOIN)
    @JoinColumn(name = "COUNTRY_ID", nullable = true)
    private Country country;

    @Column(name = "PHONE", nullable = true, length=30)
    private String phone;

    @Column(name = "MOBILE", nullable = true, length=30)
    private String mobile;

    @OneToOne
    @PrimaryKeyJoinColumn
    private User user;

    @Override
    public String toString() {
        final ToStringBuilder sbuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
        sbuilder.append("userId", userId);
        sbuilder.append("address", address);
        sbuilder.append("postalCode", postalCode);
        sbuilder.append("place", place);
        sbuilder.append("phone", phone);
        sbuilder.append("mobile", mobile);
        sbuilder.append("country", country);
        return sbuilder.toString();
    }

    //--- set  / get methods --------------------------------------------------


    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }
}
