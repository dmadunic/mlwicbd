package org.mlwicbd.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by dmadunic on 01/09/15.
 */
@JsonIgnoreProperties({"password"})
@Entity
@Table(name = "MLWICBD_USER")
public class User implements Serializable {

    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "USERNAME", unique = true, nullable = false, length=30)
    private String username;

    @Column(name = "PASSWORD", nullable = false, length=60)
    private String password;

    @Column(name = "FIRST_NAME", nullable = false, length=40)
    private String firstName;

    @Column(name = "LAST_NAME", nullable = false, length=40)
    private String lastName;

    @Column(name = "EMAIL", unique = true, nullable = true, length=80)
    private String email;

    @OneToOne(mappedBy="user", cascade=CascadeType.ALL)
    private UserProfile userProfile;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name="USER_ROLE",
            joinColumns={ @JoinColumn(name="USER_ID", referencedColumnName="ID") },
            inverseJoinColumns={ @JoinColumn(name="ROLE_ID", referencedColumnName="ID") }
    )
    @Fetch(FetchMode.JOIN)
    private Set<Role> roles = new HashSet<Role>();

    @Column(name = "ENABLED", nullable = false)
    private boolean enabled;

    @Override
    public String toString() {
        final ToStringBuilder sbuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
        sbuilder.append("id", id);
        sbuilder.append("username", username);
        sbuilder.append("password", password);
        sbuilder.append("firstName", firstName);
        sbuilder.append("lastName", lastName);
        sbuilder.append("email", email);
        sbuilder.append("enabled", enabled);
        sbuilder.append("userProfile", userProfile);
        sbuilder.append("role", roles);
        return sbuilder.toString();
    }

    //--- set / get methods ---------------------------------------------------


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public UserProfile getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(UserProfile userProfile) {
        this.userProfile = userProfile;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
