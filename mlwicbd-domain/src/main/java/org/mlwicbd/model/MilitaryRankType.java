package org.mlwicbd.model;

import com.ag04.common.data.IdProperty;

/**
 * Created by domagoj on 24/04/16.
 */
public enum MilitaryRankType implements IdProperty<Integer> {
    SOLDIERS(1, "Soldiers"),
    NCO(2, "Non commissioned officers"),
    CO(3, "Commissioned officers");

    private final Integer id;

    private final String description;

    MilitaryRankType(Integer id, String description) {
        this.id = id;
        this.description = description;
    }

    public static final MilitaryRankType getEnum(String name) {
        for (MilitaryRankType enumValue : MilitaryRankType.class.getEnumConstants()) {
            if (enumValue.name().equals(name)) {
                return enumValue;
            }
        }
        return null;
    }

    //--- set / get methods ---------------------------------------------------

    public Integer getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

}
