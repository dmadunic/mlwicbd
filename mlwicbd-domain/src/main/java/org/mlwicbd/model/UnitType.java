package org.mlwicbd.model;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Describes unit type.
 *
 * Created by dmadunic on 16/04/16.
 */
@Entity
@Table(name = "UNIT_TYPE")
public class UnitType implements Serializable {

    @Id
    @Column(name = "UT_ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "UT_NAME", nullable = false, length=80)
    private String name;

    @Column(name = "UT_DESC", nullable = false, length=255)
    private String description;

    @Override
    public String toString() {
        final ToStringBuilder sbuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
        sbuilder.append("id", id);
        sbuilder.append("name", name);
        sbuilder.append("description", name);
        return sbuilder.toString();
    }

    //--- set / get methods ---------------------------------------------------

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
