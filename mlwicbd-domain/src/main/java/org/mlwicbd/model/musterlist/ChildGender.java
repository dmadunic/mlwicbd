package org.mlwicbd.model.musterlist;

import com.ag04.common.data.IdProperty;

/**
 * Created by dmadunic on 27/09/2016.
 */
public enum ChildGender implements IdProperty<Integer> {
    SON(1),
    DAUGHTER(2);

    private final Integer id;

    ChildGender(Integer id) {
        this.id = id;
    }

    public static final ChildGender getEnum(String name) {
        for (ChildGender enumValue : ChildGender.class.getEnumConstants()) {
            if (enumValue.name().equals(name)) {
                return enumValue;
            }
        }
        return null;
    }

    //--- set / get methods ---------------------------------------------------

    public Integer getId() {
        return id;
    }

}
