package org.mlwicbd.model.musterlist;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.mlwicbd.model.AbstractAuditableEntity;
import org.mlwicbd.model.MilitaryUnit;

import javax.persistence.*;
import java.io.Serializable;

/**
 *
 * Created by dmadunic on 19/04/16.
 */
@Entity
@Table(name = "MUSTER_LIST")
@AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "ML_ID")) })
public class MusterList extends AbstractAuditableEntity {

    @Column(name = "ML_REF_CODE", nullable = false, length=80, unique = true)
    private String referenceCode;

    @Column(name = "ML_YEAR", nullable = false)
    private int year;

    @Column(name = "ML_MONTH", nullable = false)
    private int month;

    @Column(name = "ML_TITLE", nullable = false, length = 200)
    private String title;

    @ManyToOne(fetch=FetchType.LAZY, optional = false)
    @JoinColumn(name="ML_UNIT_ID")
    private MilitaryUnit militaryUnit;

    @Override
    public String toString() {
        final ToStringBuilder sbuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
        sbuilder.append(super.toString());
        sbuilder.append("referenceCode", referenceCode);
        sbuilder.append("year", year);
        sbuilder.append("month", month);

        return sbuilder.toString();
    }

    //--- set / get methods ---------------------------------------------------

    public String getReferenceCode() {
        return referenceCode;
    }

    public void setReferenceCode(String referenceCode) {
        this.referenceCode = referenceCode;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public MilitaryUnit getMilitaryUnit() {
        return militaryUnit;
    }

    public void setMilitaryUnit(MilitaryUnit militaryUnit) {
        this.militaryUnit = militaryUnit;
    }

}
