package org.mlwicbd.model.musterlist;

import org.mlwicbd.model.AbstractAuditableEntity;
import org.mlwicbd.model.MilitaryRank;
import org.mlwicbd.model.person.*;

import javax.persistence.*;
import java.util.List;

/**
 *
 * Created by dmadunic on 23/04/16.
 */
@Entity
@Table(name = "MUSTER_LIST_ENTRY")
@AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "MLE_ID")) })
public class MusterListEntry extends AbstractAuditableEntity {

    @Column(name="MLE_LIST_NUMBER")
    private Integer listNumber;

    @ManyToOne(fetch= FetchType.LAZY, optional = false)
    @JoinColumn(name="MLE_RANK_ID")
    private MilitaryRank rank;

    @ManyToOne
    @JoinColumn(name="MLE_PERSON_ID", nullable = true)
    private Person person;

    @Column(name = "MLE_FIRST_NAME", nullable = false, length = 40)
    private String firstName;

    @Column(name = "MLE_LAST_NAME", nullable = false, length = 40)
    private String lastName;

    @Column(name = "MLE_PLACE_OF_BIRTH", nullable = true, length = 80)
    private String placeOfBirth;

    @Column(name = "MLE_LOCATION")
    private String location;

    @Column(name = "MLE_HOUSE_NO", length=50)
    private String houseNumber;

    @Column(name = "MLE_AGE")
    private Integer age;

    @ManyToOne(fetch= FetchType.LAZY, optional = false)
    @JoinColumn(name="MLE_DENOMINATION_ID")
    private ReligiousDenomination religion;

    @Column(name = "MLE_MARITAL_STATUS", nullable = true)
    private MaritalStatus maritalStatus;

    @Column(name = "MLE_WIFE_PRESENT")
    private boolean wifePresent;

    @ManyToOne(fetch= FetchType.LAZY, optional = true)
    @JoinColumn(name="MLE_PROFESSION_ID")
    private Profession profession;

    @Column(name = "MLE_PROFESSION")
    private String sourceProfession;

    @Embedded
    private PersonMeasures measures;

    @ManyToOne(fetch= FetchType.LAZY, optional = false)
    @JoinColumn(name="MLE_MUSTER_LIST_ID")
    private MusterList musterList;

    @OneToMany(mappedBy="musterListEntry", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, orphanRemoval=true)
    private List<Child> children;

    @OneToMany(mappedBy="musterListEntry", cascade = {CascadeType.ALL}, orphanRemoval=true)
    private List<ServiceRecord> serviceRecords;

    @Column(name = "MLE_YEARS_SERVED", length = 50)
    private String totalYearsServed;

    @Column(name = "MLE_MONTHS_SERVED", length = 50)
    private String totalMonthsServed;

    //--- set / get methods ---------------------------------------------------

    public Integer getListNumber() {
        return listNumber;
    }

    public void setListNumber(Integer listNumber) {
        this.listNumber = listNumber;
    }

    public MilitaryRank getRank() {
        return rank;
    }

    public void setRank(MilitaryRank rank) {
        this.rank = rank;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPlaceOfBirth() {
        return placeOfBirth;
    }

    public void setPlaceOfBirth(String placeOfBirth) {
        this.placeOfBirth = placeOfBirth;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public ReligiousDenomination getReligion() {
        return religion;
    }

    public void setReligion(ReligiousDenomination religion) {
        this.religion = religion;
    }

    public MaritalStatus getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(MaritalStatus maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public boolean isWifePresent() {
        return wifePresent;
    }

    public void setWifePresent(boolean wifePresent) {
        this.wifePresent = wifePresent;
    }

    public Profession getProfession() {
        return profession;
    }

    public void setProfession(Profession profession) {
        this.profession = profession;
    }

    public String getSourceProfession() {
        return sourceProfession;
    }

    public void setSourceProfession(String sourceProfession) {
        this.sourceProfession = sourceProfession;
    }

    public PersonMeasures getMeasures() {
        return measures;
    }

    public void setMeasures(PersonMeasures measures) {
        this.measures = measures;
    }

    public MusterList getMusterList() {
        return musterList;
    }

    public void setMusterList(MusterList musterList) {
        this.musterList = musterList;
    }

    public List<ServiceRecord> getServiceRecords() {
        return serviceRecords;
    }

    public void setServiceRecords(List<ServiceRecord> serviceRecords) {
        if (this.serviceRecords != null) {
            this.serviceRecords.clear();
            this.serviceRecords.addAll(serviceRecords);
        } else {
            this.serviceRecords = serviceRecords;
        }

    }

    public String getTotalYearsServed() {
        return totalYearsServed;
    }

    public void setTotalYearsServed(String totalYearsServed) {
        this.totalYearsServed = totalYearsServed;
    }

    public String getTotalMonthsServed() {
        return totalMonthsServed;
    }

    public void setTotalMonthsServed(String totalMonthsServed) {
        this.totalMonthsServed = totalMonthsServed;
    }

    public List<Child> getChildren() {
        return children;
    }

    public void setChildren(List<Child> children) {
        if (this.children != null) {
            this.children.clear();
            this.children.addAll(children);
        } else {
            this.children = children;
        }
    }

}
