package org.mlwicbd.model.musterlist;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author idekic
 * @since 29-Jul-16.
 */
@Entity
@Table(name = "SERVICE_RECORD")
public class ServiceRecord implements Serializable {

    @Id
    @Column(name = "SR_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @ManyToOne(fetch= FetchType.LAZY, optional = false)
    @JoinColumn(name="SR_MLE_ID", referencedColumnName = "MLE_ID")
    private MusterListEntry musterListEntry;

    @Column(name = "SR_DESCRIPTION", nullable = false, length=500)
    private String description;

    @Column(name = "SR_DURATION_YEARS")
    private Integer durationYears;

    @Column(name = "SR_DURATION_MONTHS")
    private Integer durationMonths;

    @Column(name = "SR_DURATION_DAYS", length = 10)
    private String durationDays;

    @Override
    public String toString() {
        final ToStringBuilder toStringBuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
        toStringBuilder.append("id", id);
        toStringBuilder.append("musterListEntry", musterListEntry);
        toStringBuilder.append("description", description);
        toStringBuilder.append("durationYears", durationYears);
        toStringBuilder.append("durationMonths", durationMonths);
        toStringBuilder.append("durationDays", durationDays);
        return toStringBuilder.toString();
    }

    //--- setters/getters -----------------------------------------------------

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public MusterListEntry getMusterListEntry() {
        return musterListEntry;
    }

    public void setMusterListEntry(MusterListEntry musterListEntry) {
        this.musterListEntry = musterListEntry;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getDurationYears() {
        return durationYears;
    }

    public void setDurationYears(Integer durationYears) {
        this.durationYears = durationYears;
    }

    public Integer getDurationMonths() {
        return durationMonths;
    }

    public void setDurationMonths(Integer durationMonths) {
        this.durationMonths = durationMonths;
    }

    public String getDurationDays() {
        return durationDays;
    }

    public void setDurationDays(String durationDays) {
        this.durationDays = durationDays;
    }
}
