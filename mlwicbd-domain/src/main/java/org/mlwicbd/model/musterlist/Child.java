package org.mlwicbd.model.musterlist;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by dmadunic on 27/09/2016.
 */
@Entity
@Table(name = "ML_ENTRY_CHILD")
public class Child implements Serializable {

    @Id
    @Column(name = "CH_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @ManyToOne(fetch= FetchType.LAZY, optional = false)
    @JoinColumn(name="CH_MLE_ID", referencedColumnName = "MLE_ID")
    private MusterListEntry musterListEntry;

    @Column(name = "CH_GENDER", nullable = true)
    private ChildGender gender;

    @Column(name = "CH_NAME", nullable= false, length=40)
    private String name;

    @Column(name = "CH_AGE", nullable= false)
    private Integer age;

    @Override
    public String toString() {
        final ToStringBuilder toStringBuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
        toStringBuilder.append("id", id);
        toStringBuilder.append("musterListEntry", musterListEntry);
        toStringBuilder.append("gender", gender);
        toStringBuilder.append("name", name);
        toStringBuilder.append("age", age);
        return toStringBuilder.toString();
    }

    //--- set / get methods ---------------------------------------------------

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public MusterListEntry getMusterListEntry() {
        return musterListEntry;
    }

    public void setMusterListEntry(MusterListEntry musterListEntry) {
        this.musterListEntry = musterListEntry;
    }

    public ChildGender getGender() {
        return gender;
    }

    public void setGender(ChildGender gender) {
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}
