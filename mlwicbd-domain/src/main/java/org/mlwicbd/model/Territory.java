package org.mlwicbd.model;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by dmadunic on 05/04/16.
 */
@Entity
@Table(name = "MLWICBD_TERRITORY")
public class Territory implements Serializable {

    @Id
    @Column(name = "TER_ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "TER_CODE", unique = true, nullable = false, length=10)
    private String code;

    @Column(name = "TER_NAME", nullable = false, length=255)
    private String name;

    @Override
    public String toString() {
        final ToStringBuilder sbuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
        sbuilder.append("id", id);
        sbuilder.append("code", code);
        sbuilder.append("name", name);
        return sbuilder.toString();
    }

    //--- set / get methods ---------------------------------------------------

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
