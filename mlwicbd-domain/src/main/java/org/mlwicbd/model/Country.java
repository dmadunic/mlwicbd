package org.mlwicbd.model;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by dmadunic on 02/04/16.
 */
@Entity
@Table(name = "COUNTRY")
public class Country implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * Id identifier
     */
    @Id
    @Column(name = "CNT_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * Code value
     */
    @Column(name = "CNT_CODE")
    private String code;

    @Column(name = "CNT_NAME", nullable = false)
    private String name;

    @Column(name = "CNT_A3", nullable = false)
    private String a3code;

    @Column(name = "CNT_A2", nullable = false)
    private String a2code;

    @Column(name = "CNT_ACTIVE", nullable = false)
    private boolean active = true;

    @Override
    public String toString() {
        final ToStringBuilder sbuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
        sbuilder.append(super.toString());
        sbuilder.append("id", id);
        sbuilder.append("code", code);
        sbuilder.append("name", name);
        sbuilder.append("a3code", a3code);
        sbuilder.append("a2code", a2code);
        sbuilder.append("active", active);
        return sbuilder.toString();
    }

    // --- set / get methods ------------------------------------------------------

    public String getA3code() {
        return a3code;
    }

    public void setA3code(String codeA3) {
        this.a3code = codeA3;
    }

    public String getA2code() {
        return a2code;
    }

    public void setA2code(String codeA2) {
        this.a2code = codeA2;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
