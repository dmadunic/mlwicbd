package org.mlwicbd.model;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.persistence.*;
import java.io.Serializable;

/**
 *
 * Created by dmadunic on 23/04/16.
 */
@Entity
@Table(name = "MILITARY_RANK")
public class MilitaryRank implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * Id identifier
     */
    @Id
    @Column(name = "MR_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * Unique code value for this rank.
     */
    @Column(name = "MR_CODE", unique = true, length = 40)
    private String code;

    /**
     * The MOST common source name of this rank.
     */
    @Column(name = "MR_SOURCE_NAME", length=255)
    private String sourceName;

    /**
     * Short description.
     */
    @Column(name = "MR_DESCRIPTION", length=255)
    private String description;

    @Column(name = "MR_TYPE", nullable = true)
    private MilitaryRankType type;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="MR_TERRITORY_ID")
    private Territory territory;

    @Override
    public String toString() {
        final ToStringBuilder sbuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
        sbuilder.append("id", id);
        sbuilder.append("code", code);
        sbuilder.append("sourceName", sourceName);
        sbuilder.append("description", description);
        sbuilder.append("territory", territory);
        return sbuilder.toString();
    }

    //--- set / get methods ---------------------------------------------------

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getSourceName() {
        return sourceName;
    }

    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public MilitaryRankType getType() {
        return type;
    }

    public void setType(MilitaryRankType type) {
        this.type = type;
    }

    public Territory getTerritory() {
        return territory;
    }

    public void setTerritory(Territory territory) {
        this.territory = territory;
    }
}
