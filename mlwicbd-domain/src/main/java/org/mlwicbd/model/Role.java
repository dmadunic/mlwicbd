package org.mlwicbd.model;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by domagoj on 01/09/15.
 */
@Entity
@Table(name = "ROLE")
public class Role implements Serializable {
    @Id
    @Column(name = "ID", nullable = false, length=30)
    private String id;

    @Column(name = "NAME", nullable = false, length=40)
    private String name;

    @Override
    public String toString() {
        final ToStringBuilder sbuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
        sbuilder.append("id", id);
        sbuilder.append("name", name);
        return sbuilder.toString();
    }

    //--- set / get methods ---------------------------------------------------


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
