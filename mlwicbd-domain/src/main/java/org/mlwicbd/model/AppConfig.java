package org.mlwicbd.model;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by domagoj on 24/08/15.
 */
@Entity
@Table(name = "APP_CONFIG")
public class AppConfig implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "APC_KEY", nullable = false, length=80)
    private String key;

    @Column(name = "APC_VALUE", nullable = false, length=255)
    private String value;

    @Column(name = "APC_DESC", nullable = true, length=255)
    private String description;

    public AppConfig() {
        //
    }

    public AppConfig(final String key, final String value) {
        this.key = key;
        this.value = value;
    }

    @Override
    public String toString() {
        final ToStringBuilder sbuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
        sbuilder.append("key", key);
        sbuilder.append("value", value);
        sbuilder.append("description", description);

        return sbuilder.toString();
    }

    //--- set / get methods ---------------------------------------------------

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
