package org.mlwicbd.model.view;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;

import javax.annotation.concurrent.Immutable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by tgreblicki on 10.05.17..
 */
@Entity
@Immutable
public class PersonMleView implements Serializable{
    @Id
    private Long mleId;

    @Column(name = "mle_person_id")
    private Long personId;

    @Column(name = "created_date")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
    private LocalDateTime createdDate;

    @Column(name = "ml_year")
    private Integer mlYear;

    @Column(name = "ml_month")
    private Integer mlMonth;

    @Column(name = "ml_id")
    private Long mlId;

    // --- get / set methods --------------------------------------------------
    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public Long getMleId() {
        return mleId;
    }

    public void setMleId(Long mleId) {
        this.mleId = mleId;
    }

    public Integer getMlYear() {
        return mlYear;
    }

    public void setMlYear(Integer mlYear) {
        this.mlYear = mlYear;
    }

    public Integer getMlMonth() {
        return mlMonth;
    }

    public void setMlMonth(Integer mlMonth) {
        this.mlMonth = mlMonth;
    }

    public Long getMlId() {
        return mlId;
    }

    public void setMlId(Long mlId) {
        this.mlId = mlId;
    }
}
