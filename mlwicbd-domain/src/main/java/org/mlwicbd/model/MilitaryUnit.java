package org.mlwicbd.model;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.persistence.*;

/**
 *
 * Created by dmadunic on 16/04/16.
 */
@Entity
@Table(name = "MILITARY_UNIT")
public class MilitaryUnit {

    @Id
    @Column(name = "MU_ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "MU_NAME", nullable = false, length=80)
    private String name;

    @Column(name = "MU_DESC", nullable = false, length=255)
    private String description;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="UNIT_TYPE_ID")
    private UnitType unitType;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="PARENT_UNIT_ID")
    private MilitaryUnit parentUnit;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="TERRITORY_ID")
    private Territory territory;

    @Override
    public String toString() {
        final ToStringBuilder sbuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
        sbuilder.append("id", id);
        sbuilder.append("name", name);
        sbuilder.append("description", description);
        sbuilder.append("unitType", unitType);
        sbuilder.append("territory", territory);
        return sbuilder.toString();
    }

    //--- set / get methods ---------------------------------------------------

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public UnitType getUnitType() {
        return unitType;
    }

    public void setUnitType(UnitType unitType) {
        this.unitType = unitType;
    }

    public MilitaryUnit getParentUnit() {
        return parentUnit;
    }

    public void setParentUnit(MilitaryUnit parentUnit) {
        this.parentUnit = parentUnit;
    }

    public Territory getTerritory() {
        return territory;
    }

    public void setTerritory(Territory territory) {
        this.territory = territory;
    }
}
