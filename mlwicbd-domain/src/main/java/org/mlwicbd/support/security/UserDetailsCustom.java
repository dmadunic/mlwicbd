package org.mlwicbd.support.security;

import org.mlwicbd.model.User;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 * Created by dmadunic on 24/04/16.
 */
public class UserDetailsCustom extends org.springframework.security.core.userdetails.User {

    private User user;

    public UserDetailsCustom(User user, Collection<? extends GrantedAuthority> authorities) {
        super(user.getUsername(), user.getPassword(), authorities);
        this.user = user;
    }

    public User getUser() {
        return user;
    }
}
