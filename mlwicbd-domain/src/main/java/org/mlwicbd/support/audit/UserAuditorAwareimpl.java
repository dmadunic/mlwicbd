package org.mlwicbd.support.audit;

import org.mlwicbd.model.User;
import org.mlwicbd.service.UserService;
import org.mlwicbd.support.security.UserDetailsCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * Created by dmadunic on 19/04/17.
 */
public class UserAuditorAwareimpl implements AuditorAware<User> {

    @Autowired
    UserService userService;

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    /**
     * Returns the current auditor of the application.
     *
     * @return the current auditor
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public User getCurrentAuditor() {
        String name = SecurityContextHolder.getContext().getAuthentication().getName();
        User user = userService.findByUsername(name);
        return user;
    }
}
