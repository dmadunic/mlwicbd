package org.mlwicbd.dao;

/**
 * Muster list entries count result.
 *
 * Created by dmadunic on 17/09/16.
 */
public class MleCountResult {

    private Long musterListId;

    private Long entriesCount;

    public MleCountResult() {
        //
    }

    public MleCountResult(Long musterListId,  Long entriesCount) {
        this.musterListId = musterListId;
        this.entriesCount = entriesCount;
    }

    //--- set / get methods ---------------------------------------------------

    public Long getMusterListId() {
        return musterListId;
    }

    public void setMusterListId(Long musterListId) {
        this.musterListId = musterListId;
    }

    public Long getEntriesCount() {
        return entriesCount;
    }

    public void setEntriesCount(Long entriesCount) {
        this.entriesCount = entriesCount;
    }

}
