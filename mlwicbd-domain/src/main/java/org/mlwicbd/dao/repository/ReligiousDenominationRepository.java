package org.mlwicbd.dao.repository;

import org.mlwicbd.model.person.ReligiousDenomination;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by dmadunic on 24/04/16.
 */
public interface ReligiousDenominationRepository extends JpaRepository<ReligiousDenomination, Long> {

    ReligiousDenomination findByCode(String code);

}
