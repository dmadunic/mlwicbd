package org.mlwicbd.dao.repository;

import org.mlwicbd.model.view.PersonMleView;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by tgreblicki on 10.05.17..
 */
public interface PersonMleViewRepository extends JpaRepository<PersonMleView, Long> {
    List<PersonMleView> findAllByPersonIdInOrderByPersonIdAscCreatedDateAsc(List<Long> personIds);

    List<PersonMleView> findAllByPersonId(List<Long> personIds);

    List<PersonMleView> findAllByPersonIdInOrderByPersonIdAscMlYearAscMlMonthAsc(List<Long> personIds);
}
