package org.mlwicbd.dao.repository.impl;

import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.PathBuilder;
import com.querydsl.jpa.impl.JPAQuery;
import org.mlwicbd.model.QMilitaryUnit;
import org.mlwicbd.model.QTerritory;
import org.mlwicbd.model.QUnitType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.mlwicbd.criteria.MilitaryUnitSearchCriteria;
import org.mlwicbd.dao.repository.MilitaryUnitRepositoryCustom;
import org.mlwicbd.model.MilitaryUnit;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 *
 * Created by dmadunic on 16/04/16.
 */
public class MilitaryUnitRepositoryImpl implements MilitaryUnitRepositoryCustom {
    private static final Logger LOG = LoggerFactory.getLogger(MilitaryUnitRepositoryImpl.class);

    @PersistenceContext
    private transient EntityManager entityManager;

    @Override
    public Page<MilitaryUnit> searchByCriteria(MilitaryUnitSearchCriteria criteria, Pageable pageable) {
        JPAQuery query = new JPAQuery(entityManager);

        QMilitaryUnit militaryUnit = QMilitaryUnit.militaryUnit;
        QUnitType unitType = QUnitType.unitType;
        QTerritory territory = QTerritory.territory;

        query.from(militaryUnit);
        query.leftJoin(militaryUnit.unitType, unitType).fetch();
        query.leftJoin(militaryUnit.territory, territory).fetch();

        // 1. create Search predicate ...
        BooleanExpression searchPredicate = createSearchPredicate(criteria, militaryUnit, query);
       query.where(searchPredicate);

        // 2. count all prospective results ...
        JPAQuery countQuery = new JPAQuery(entityManager);
        countQuery.from(militaryUnit);
        query.leftJoin(militaryUnit.unitType, unitType);
        query.leftJoin(militaryUnit.territory, territory);

        countQuery.where(searchPredicate);
        final long count = countQuery.fetchCount();

        // 3. set page limits...
        query.limit(pageable.getPageSize());
        query.offset(pageable.getOffset());

        // 4. sort data ...
        if (pageable.getSort() != null) {
            for (Sort.Order order : pageable.getSort()) {
                PathBuilder<MilitaryUnit> orderByExpression = new PathBuilder<MilitaryUnit>(MilitaryUnit.class, "militaryUnit");
                query.orderBy(
                    new OrderSpecifier(
                        order.isAscending() ? com.querydsl.core.types.Order.ASC : com.querydsl.core.types.Order.DESC
                        , orderByExpression.get(order.getProperty())
                    )
                );
            }
        }

        // 5. execute query ...
        final List<MilitaryUnit> resultList = query.fetch();
        return new PageImpl<MilitaryUnit>(resultList, pageable, count);
    }

    private BooleanExpression createSearchPredicate(MilitaryUnitSearchCriteria criteria, QMilitaryUnit militaryUnit, JPAQuery query) {
        BooleanExpression predicate = null;
        if (StringUtils.hasText(criteria.getTerritoryCode())) {
            BooleanExpression teritoryCodePredicate = militaryUnit.territory.code.equalsIgnoreCase(criteria.getTerritoryCode());
            predicate = CustomRepositoryUtils.addAndPredicate(predicate, teritoryCodePredicate);
        }

        if (StringUtils.hasText(criteria.getUnitTypeCode())) {
            BooleanExpression unitTypeCodePredicate = militaryUnit.unitType.name.equalsIgnoreCase(criteria.getUnitTypeCode());
            predicate = CustomRepositoryUtils.addAndPredicate(predicate, unitTypeCodePredicate);
        }

        if (criteria.getParentUnitId() != null) {
            BooleanExpression parentUnitIdPredicate = militaryUnit.parentUnit.id.eq(criteria.getParentUnitId());
            predicate = CustomRepositoryUtils.addAndPredicate(predicate, parentUnitIdPredicate);
        }

        if (StringUtils.hasText(criteria.getName())) {
            BooleanExpression namePredicate = militaryUnit.name.containsIgnoreCase(criteria.getName());
            predicate = CustomRepositoryUtils.addAndPredicate(predicate, namePredicate);
        }

        return predicate;
    }



}
