package org.mlwicbd.dao.repository;

import org.mlwicbd.model.MilitaryRank;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by dmadunic on 24/04/16.
 */
public interface MilitaryRankRepository extends JpaRepository<MilitaryRank, Long>, MilitaryRankRepositoryCustom {

}
