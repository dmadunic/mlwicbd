package org.mlwicbd.dao.repository;

import org.mlwicbd.model.MilitaryUnit;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;

/**
 *
 * Created by dmadunic on 16/04/16.
 */
public interface MilitaryUnitRepository extends JpaRepository<MilitaryUnit, Long>, MilitaryUnitRepositoryCustom {

    Set<MilitaryUnit> findByParentUnitId(Long unitId);
}
