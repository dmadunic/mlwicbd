package org.mlwicbd.dao.repository;

import org.mlwicbd.dao.MleCountResult;
import org.mlwicbd.model.musterlist.MusterListEntry;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Map;

/**
 * Created by dmadunic on 25/04/2016.
 */
public interface MusterListEntryRepository extends JpaRepository<MusterListEntry, Long> {

    void deleteById(Long id);

    Page<MusterListEntry> findByPersonId(Long personId, Pageable pageable);

    Page<MusterListEntry> findByMusterListId(Long mlId, Pageable pageable);

    int countByMusterListId(Long mlId);

    int countByListNumberAndMusterListId(Integer listNumber, Long musterListId);

    @Query("select new org.mlwicbd.dao.MleCountResult(mle.musterList.id as musterListId, count(mle.id) as entriesCount) FROM MusterListEntry mle WHERE mle.musterList.id IN :mlids GROUP BY mle.musterList.id")
    List<MleCountResult> countEntries(@Param("mlids") List<Long> mlids);

    List<MusterListEntry> findByMusterListId(Long mlId);

    List<MusterListEntry> findByPersonIdIn(List<Long> ids);

    Page<MusterListEntry> findByPersonIdIn(List<Long> personId, Pageable pageable);

    MusterListEntry findByMusterListIdAndPersonId(Long musterListId, Long personId);

    void deleteByPersonId(Long personId);
}
