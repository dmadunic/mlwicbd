package org.mlwicbd.dao.repository;

import org.mlwicbd.model.musterlist.ServiceRecord;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author idekic
 * @since 19-Aug-16.
 */
public interface ServiceRecordRepository extends JpaRepository<ServiceRecord, Long> {
    void deleteByMusterListEntryId(Long id);
}
