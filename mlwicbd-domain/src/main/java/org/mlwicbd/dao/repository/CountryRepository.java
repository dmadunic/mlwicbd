package org.mlwicbd.dao.repository;

import org.mlwicbd.model.Country;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by dmadunic on 03/04/16.
 */
public interface CountryRepository extends JpaRepository<Country, Long>  {

    Country findByCode(String code);

    int countByCode(String code);

    Country findByA2code(String a2Code);

    Country findByA3code(String a3Code);
}
