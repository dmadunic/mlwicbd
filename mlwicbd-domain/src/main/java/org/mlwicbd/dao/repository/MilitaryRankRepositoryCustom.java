package org.mlwicbd.dao.repository;

import org.mlwicbd.criteria.MilitaryRankSearchCriteria;
import org.mlwicbd.model.MilitaryRank;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Created by dmadunic on 24/04/16.
 */
public interface MilitaryRankRepositoryCustom {

    Page<MilitaryRank> searchByCriteria(MilitaryRankSearchCriteria criteria, Pageable pageable);

}

