package org.mlwicbd.dao.repository;

import org.mlwicbd.model.person.Profession;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

/**
 *
 * Created by dmadunic on 24/04/16.
 */
public interface ProfessionRepository extends JpaRepository<Profession, Long>, QueryDslPredicateExecutor<Profession> {
    Profession findByCode(String code);
}
