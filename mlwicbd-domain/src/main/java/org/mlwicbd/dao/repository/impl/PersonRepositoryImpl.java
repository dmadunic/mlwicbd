package org.mlwicbd.dao.repository.impl;

import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.PathBuilder;
import com.querydsl.jpa.impl.JPAQuery;
import org.mlwicbd.criteria.PersonSearchCriteria;
import org.mlwicbd.dao.repository.PersonRepositoryCustom;
import org.mlwicbd.model.MilitaryUnit;
import org.mlwicbd.model.person.Person;
import org.mlwicbd.model.person.QPerson;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by dmadunic on 03/10/2016.
 */
public class PersonRepositoryImpl implements PersonRepositoryCustom {

    @PersistenceContext
    private transient EntityManager entityManager;

    @Override
    public Page<Person> searchByCriteria(PersonSearchCriteria criteria, Pageable pageable) {
        JPAQuery query = new JPAQuery(entityManager);

        QPerson person = QPerson.person;
        query.from(person);

        // 1. create Search predicate ...
        BooleanExpression searchPredicate = createSearchPredicate(criteria, person, query);
        query.where(searchPredicate);

        // 2. count all prospective results ...
        JPAQuery countQuery = new JPAQuery(entityManager);
        countQuery.from(person);

        countQuery.where(searchPredicate);
        final long count = countQuery.fetchCount();

        // 3. set page limits ...
        query.limit(pageable.getPageSize());
        query.offset(pageable.getOffset());

        // 4. sort data ...
        if (pageable.getSort() != null) {
            for (Sort.Order order : pageable.getSort()) {
                PathBuilder<Person> orderByExpression = new PathBuilder<Person>(Person.class, "person");
                query.orderBy(
                        new OrderSpecifier(
                                order.isAscending() ? com.querydsl.core.types.Order.ASC : com.querydsl.core.types.Order.DESC
                                , orderByExpression.get(order.getProperty())
                        )
                );
            }
        }

        // 5. execute query ...
        final List<Person> resultList = query.fetch();
        return new PageImpl<Person>(resultList, pageable, count);

    }

    private BooleanExpression createSearchPredicate(PersonSearchCriteria criteria, QPerson person, JPAQuery query) {
        BooleanExpression predicate = null;

        if (StringUtils.hasText(criteria.getLastName())) {
            BooleanExpression namePredicate = person.lastName.containsIgnoreCase(criteria.getLastName());
            BooleanExpression sourceNamePredicate = person.sourcesLastNames.containsIgnoreCase(criteria.getLastName());
            namePredicate = namePredicate.or(sourceNamePredicate);
            predicate = CustomRepositoryUtils.addAndPredicate(predicate, namePredicate);
        }

        if (StringUtils.hasText(criteria.getFirstName())) {
            BooleanExpression namePredicate = person.firstName.containsIgnoreCase(criteria.getFirstName());
            BooleanExpression sourceNamePredicate = person.sourcesFirstNames.containsIgnoreCase(criteria.getFirstName());
            namePredicate = namePredicate.or(sourceNamePredicate);
            predicate = CustomRepositoryUtils.addAndPredicate(predicate, namePredicate);
        }

        if (StringUtils.hasText(criteria.getPlaceOfBirth())) {
            BooleanExpression placePredicate = person.placeOfBirth.equalsIgnoreCase(criteria.getPlaceOfBirth());
            predicate = CustomRepositoryUtils.addAndPredicate(predicate, placePredicate);
        }
        return predicate;
    }
}
