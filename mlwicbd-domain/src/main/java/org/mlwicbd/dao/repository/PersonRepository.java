package org.mlwicbd.dao.repository;

import org.mlwicbd.model.person.Person;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;

/**
 * Created by dmadunic on 24/04/2016.
 */
public interface PersonRepository extends JpaRepository<Person, Long>, PersonRepositoryCustom {

    @Modifying
    void deleteById(Long id);

}
