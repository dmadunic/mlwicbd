package org.mlwicbd.dao.repository.impl;

import com.querydsl.core.types.dsl.BooleanExpression;

/**
 * Created by dmadunic on 20/04/16.
 */
public class CustomRepositoryUtils {

    public static BooleanExpression addAndPredicate(BooleanExpression predicate, BooleanExpression newAndPredicate) {
        if (predicate == null) {
            return newAndPredicate;
        }
        predicate = predicate.and(newAndPredicate);
        return predicate;
    }

    public static BooleanExpression addOrPredicate(BooleanExpression predicate, BooleanExpression newOrPredicate) {
        if (predicate == null) {
            return newOrPredicate;
        }
        predicate = predicate.or(newOrPredicate);
        return predicate;
    }
}
