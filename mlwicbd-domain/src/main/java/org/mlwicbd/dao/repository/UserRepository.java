package org.mlwicbd.dao.repository;

import org.mlwicbd.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by domagoj on 01/09/15.
 */
public interface UserRepository extends JpaRepository<User, Long> {

    User findByUsername(String username);

    int countByUsername(String username);

    User findByEmail(String email);

    int countByEmail(String email);


}
