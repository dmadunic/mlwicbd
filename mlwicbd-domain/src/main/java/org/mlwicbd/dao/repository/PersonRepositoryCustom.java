package org.mlwicbd.dao.repository;

import org.mlwicbd.criteria.PersonSearchCriteria;
import org.mlwicbd.model.person.Person;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Created by dmadunic on 03/10/2016.
 */
public interface PersonRepositoryCustom {

    Page<Person> searchByCriteria(PersonSearchCriteria criteria, Pageable pageable);

}
