package org.mlwicbd.dao.repository;

import org.mlwicbd.model.musterlist.MusterList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * Created by dmadunic on 20/04/16.
 */
public interface MusterListRepository extends JpaRepository<MusterList, Long>, MusterListRepositoryCustom {

    @Query("SELECT ml.id FROM MusterList ml WHERE ml.referenceCode = ?1")
    Long findIdByReferenceCode(String referenceCode);

    @Query("SELECT ml.id FROM MusterList ml WHERE ml.militaryUnit.id = ?1 AND ml.year = ?2 AND ml.month = ?3")
    Long findIdByUnitIdAndYearAndMonth(Long unitId, Integer year, Integer month);
}
