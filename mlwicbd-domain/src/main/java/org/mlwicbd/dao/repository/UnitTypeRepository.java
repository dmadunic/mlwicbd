package org.mlwicbd.dao.repository;

import org.mlwicbd.model.UnitType;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * Created by dmadunic on 24/04/16.
 */
public interface UnitTypeRepository extends JpaRepository<UnitType, Long> {
}
