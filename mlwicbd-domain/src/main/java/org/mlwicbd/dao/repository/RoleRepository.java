package org.mlwicbd.dao.repository;

import org.mlwicbd.model.Role;
import org.springframework.data.repository.CrudRepository;

import java.util.Set;

/**
 * Created by tgreblicki on 18.05.17..
 */
public interface RoleRepository extends CrudRepository<Role, String> {

    Set<Role> findByName(String name);
}
