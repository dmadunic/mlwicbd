package org.mlwicbd.dao.repository;

import org.mlwicbd.model.AppConfig;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by dmadunic on 24/08/15.
 */
public interface AppConfigRepository extends JpaRepository<AppConfig, Long> {

    AppConfig findByKey(String key);
}
