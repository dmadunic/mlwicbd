package org.mlwicbd.dao.repository.impl;

import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.PathBuilder;
import com.querydsl.jpa.impl.JPAQuery;
import org.mlwicbd.criteria.MilitaryRankSearchCriteria;
import org.mlwicbd.dao.repository.MilitaryRankRepositoryCustom;
import org.mlwicbd.model.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by domagoj on 24/04/16.
 */
public class MilitaryRankRepositoryImpl implements MilitaryRankRepositoryCustom {

    @PersistenceContext
    private transient EntityManager entityManager;

    @Override
    public Page<MilitaryRank> searchByCriteria(MilitaryRankSearchCriteria criteria, Pageable pageable) {
        JPAQuery query = new JPAQuery(entityManager);

        QMilitaryRank militaryRank = QMilitaryRank.militaryRank;
        QTerritory territory = QTerritory.territory;

        query.from(militaryRank);
        query.leftJoin(militaryRank.territory, territory).fetch();

        // 1. create Search predicate ...
        BooleanExpression searchPredicate = createSearchPredicate(criteria, militaryRank, query);
        query.where(searchPredicate);

        // 2. count all prospective results ...
        JPAQuery countQuery = new JPAQuery(entityManager);
        countQuery.from(militaryRank);
        query.leftJoin(militaryRank.territory, territory);

        final long count = countQuery.fetchCount();

        // 3. set page limits...
        query.limit(pageable.getPageSize());
        query.offset(pageable.getOffset());

        // 4. sort data ...
        if (pageable.getSort() != null) {
            for (Sort.Order order : pageable.getSort()) {
                PathBuilder<MilitaryRank> orderByExpression = new PathBuilder<>(MilitaryRank.class, "militaryRank");
                query.orderBy(
                        new OrderSpecifier(
                                order.isAscending() ? com.querydsl.core.types.Order.ASC : com.querydsl.core.types.Order.DESC
                                , orderByExpression.get(order.getProperty())
                        )
                );
            }
        }

        // 5. execute query ...
        final List<MilitaryRank> resultList = query.fetch();
        return new PageImpl<>(resultList, pageable, count);
    }

    private BooleanExpression createSearchPredicate(MilitaryRankSearchCriteria criteria, QMilitaryRank militaryRank, JPAQuery query) {
        BooleanExpression predicate = null;
        if (StringUtils.hasText(criteria.getTerritoryCode())) {
            BooleanExpression territoryCodePredicate = militaryRank.territory.code.equalsIgnoreCase(criteria.getTerritoryCode());
            predicate = CustomRepositoryUtils.addAndPredicate(predicate, territoryCodePredicate);
        }

        if (StringUtils.hasText(criteria.getRankType())) {
            MilitaryRankType type = MilitaryRankType.getEnum(criteria.getRankType().toUpperCase());
            BooleanExpression rankTypePredicate = militaryRank.type.eq(type);
            predicate = CustomRepositoryUtils.addAndPredicate(predicate, rankTypePredicate);
        }
        return predicate;
    }
}
