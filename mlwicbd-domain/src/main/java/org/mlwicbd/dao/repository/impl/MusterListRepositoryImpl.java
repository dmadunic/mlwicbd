package org.mlwicbd.dao.repository.impl;

import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.PathBuilder;
import com.querydsl.jpa.impl.JPAQuery;
import org.mlwicbd.dao.repository.MusterListRepositoryCustom;
import org.mlwicbd.model.QMilitaryUnit;
import org.mlwicbd.model.musterlist.MusterList;
import org.mlwicbd.model.musterlist.QMusterList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;
import java.util.List;

/**
 *
 * Created by dmadunic on 20/04/16.
 */
public class MusterListRepositoryImpl implements MusterListRepositoryCustom {
    private static final Logger LOG = LoggerFactory.getLogger(MilitaryUnitRepositoryImpl.class);

    @PersistenceContext
    private transient EntityManager entityManager;

    @Override
    public Page<MusterList> searchByCriteria(String referenceCode, Integer year, Integer month, Collection<Long> unitIds, Pageable pageable) {
        JPAQuery query = new JPAQuery(entityManager);
        QMusterList musterList = QMusterList.musterList;
        QMilitaryUnit militaryUnit = QMilitaryUnit.militaryUnit;

        query.from(musterList);
        query.leftJoin(musterList.militaryUnit, militaryUnit).fetch();

        // 1. create Search predicate ...
        BooleanExpression searchPredicate = createSearchPredicate(referenceCode, year, month, unitIds, musterList, query);
        query.where(searchPredicate);

        // 2. count all prospective results ...
        JPAQuery countQuery = new JPAQuery(entityManager);
        countQuery.from(musterList);
        countQuery.leftJoin(musterList.militaryUnit, militaryUnit);

        countQuery.where(searchPredicate);
        final long count = countQuery.fetchCount();

        // 3. set page limits...
        query.limit(pageable.getPageSize());
        query.offset(pageable.getOffset());

        // 4. sort data ...
        if (pageable.getSort() != null) {
            for (Sort.Order order : pageable.getSort()) {
                PathBuilder<MusterList> orderByExpression = new PathBuilder<>(MusterList.class, "musterList");
                query.orderBy(
                        new OrderSpecifier(
                                order.isAscending() ? com.querydsl.core.types.Order.ASC : com.querydsl.core.types.Order.DESC
                                , orderByExpression.get(order.getProperty())
                        )
                );
            }
        }

        // 5. execute query ...
        final List<MusterList> resultList = query.fetch();
        return new PageImpl<>(resultList, pageable, count);
    }

    private BooleanExpression createSearchPredicate(String referenceCode, Integer year, Integer month, Collection<Long> unitIds, QMusterList musterList, JPAQuery query) {
        BooleanExpression predicate = null;

        // reference Code
        if (StringUtils.hasText(referenceCode)) {
            BooleanExpression referenceCodePredicate = musterList.referenceCode.startsWithIgnoreCase(referenceCode);
            predicate = CustomRepositoryUtils.addAndPredicate(predicate, referenceCodePredicate);
        }

        // year ...
        if (year != null) {
            BooleanExpression yearPredicate = musterList.year.eq(year);
            predicate = CustomRepositoryUtils.addAndPredicate(predicate, yearPredicate);
        }

        // month ...
        if (month != null) {
            BooleanExpression monthPredicate = musterList.month.eq(month);
            predicate = CustomRepositoryUtils.addAndPredicate(predicate, monthPredicate);
        }

        if (unitIds != null && unitIds.size() > 0) {
            BooleanExpression unitIdPredicate;
            if (unitIds.size() == 1) {
                unitIdPredicate = musterList.militaryUnit.id.eq(unitIds.iterator().next());
            } else {
                unitIdPredicate = musterList.militaryUnit.id.in(unitIds);
            }

            predicate = CustomRepositoryUtils.addAndPredicate(predicate, unitIdPredicate);
        }
        return predicate;

    }
}