package org.mlwicbd.dao.repository;

import org.mlwicbd.model.Territory;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by dmadunic on 05/04/16.
 */
public interface TerritoryRepository extends JpaRepository<Territory, Long> {

    Territory findByCode(String code);

}
