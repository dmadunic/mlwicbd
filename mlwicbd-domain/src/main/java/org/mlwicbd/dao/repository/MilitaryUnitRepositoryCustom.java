package org.mlwicbd.dao.repository;

import org.mlwicbd.criteria.MilitaryUnitSearchCriteria;
import org.mlwicbd.model.MilitaryUnit;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 *
 * Created by dmadunic on 16/04/16.
 */
public interface MilitaryUnitRepositoryCustom {

    Page<MilitaryUnit> searchByCriteria(MilitaryUnitSearchCriteria criteria, Pageable pageable);

}
