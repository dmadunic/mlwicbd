package org.mlwicbd.dao.repository;

import org.mlwicbd.criteria.MusterListSearchCriteria;
import org.mlwicbd.model.musterlist.MusterList;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Collection;

/**
 * Created by dmadunic on 20/04/16.
 */
public interface MusterListRepositoryCustom {

    Page<MusterList> searchByCriteria(String referenceCode, Integer year, Integer month, Collection<Long> unitIds, Pageable pageable);

}
