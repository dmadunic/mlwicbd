package org.mlwicbd.criteria;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 *
 * Created by dmadunic on 20/04/16.
 */
public class MusterListSearchCriteria {

    private String referenceCode;

    private Integer year;

    private Integer month;

    private Long militaryUnitId;

    public MusterListSearchCriteria() {
        //
    }

    public MusterListSearchCriteria(String referenceCode, Long militaryUnitId, Integer year, Integer month) {
        this.referenceCode = referenceCode;
        this.militaryUnitId = militaryUnitId;
        this.year = year;
        this.month = month;
    }

    @Override
    public String toString() {
        final ToStringBuilder sbuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
        sbuilder.append(super.toString());
        sbuilder.append("year", year);
        sbuilder.append("month", month);
        sbuilder.append("referenceCode", referenceCode);
        sbuilder.append("militaryUnitId", militaryUnitId);

        return sbuilder.toString();
    }

    //--- set / get methods ---------------------------------------------------

    public String getReferenceCode() {
        return referenceCode;
    }

    public void setReferenceCode(String referenceCode) {
        this.referenceCode = referenceCode;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public Long getMilitaryUnitId() {
        return militaryUnitId;
    }

    public void setMilitaryUnitId(Long militaryUnitId) {
        this.militaryUnitId = militaryUnitId;
    }

}
