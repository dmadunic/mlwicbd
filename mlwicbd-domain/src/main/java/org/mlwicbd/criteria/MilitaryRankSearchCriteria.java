package org.mlwicbd.criteria;

/**
 * Created by domagoj on 24/04/16.
 */
public class MilitaryRankSearchCriteria {

    private String territoryCode;

    private String rankType;

    public MilitaryRankSearchCriteria() {
        // default constructor ...
    }

    public MilitaryRankSearchCriteria(String territoryCode, String rankType) {
        this.territoryCode = territoryCode;
        this.rankType = rankType;
    }

    //--- set / get methods ---------------------------------------------------

    public String getTerritoryCode() {
        return territoryCode;
    }

    public void setTerritoryCode(String territoryCode) {
        this.territoryCode = territoryCode;
    }

    public String getRankType() {
        return rankType;
    }

    public void setRankType(String rankType) {
        this.rankType = rankType;
    }
}
