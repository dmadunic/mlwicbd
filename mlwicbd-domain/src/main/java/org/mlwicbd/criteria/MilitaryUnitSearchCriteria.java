package org.mlwicbd.criteria;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;

/**
 * Created by dmadunic on 16/04/16.
 */
public class MilitaryUnitSearchCriteria implements Serializable {

    String unitTypeCode;

    String territoryCode;

    String name;

    Long parentUnitId;

    public MilitaryUnitSearchCriteria() {
        // default constructor
    }

    public MilitaryUnitSearchCriteria(String unitTypeCode, String territoryCode, String name, Long parentUnitId) {
        this.unitTypeCode = unitTypeCode;
        this.territoryCode = territoryCode;
        this.parentUnitId = parentUnitId;
        this.name = name;
    }

    @Override
    public String toString() {
        final ToStringBuilder sbuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
        sbuilder.append("name", name);
        sbuilder.append("unitTypeCode", unitTypeCode);
        sbuilder.append("territoryCode", territoryCode);
        sbuilder.append("parentUnitId", parentUnitId);
        return sbuilder.toString();
    }

    //--- set / get methods ---------------------------------------------------


    public String getUnitTypeCode() {
        return unitTypeCode;
    }

    public void setUnitTypeCode(String unitTypeCode) {
        this.unitTypeCode = unitTypeCode;
    }

    public String getTerritoryCode() {
        return territoryCode;
    }

    public void setTerritoryCode(String territoryCode) {
        this.territoryCode = territoryCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getParentUnitId() {
        return parentUnitId;
    }

    public void setParentUnitId(Long parentUnitId) {
        this.parentUnitId = parentUnitId;
    }
}
