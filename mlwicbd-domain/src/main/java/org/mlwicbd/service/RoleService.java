package org.mlwicbd.service;

import org.mlwicbd.model.Role;

import java.util.Set;

/**
 * Created by tgreblicki on 18.05.17..
 */
public interface RoleService {

    Set<Role> findByName(String name);

    Role findById(String id);
}
