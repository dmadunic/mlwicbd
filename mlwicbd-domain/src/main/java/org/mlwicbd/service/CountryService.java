package org.mlwicbd.service;

import org.mlwicbd.model.Country;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Created by dmadunic on 04/04/16.
 */
public interface CountryService {

    Page<Country> findAll(Pageable pageable);

}
