package org.mlwicbd.service;

import org.mlwicbd.model.view.PersonMleView;

import java.util.List;

/**
 * Created by tgreblicki on 10.05.17..
 */
public interface PersonMleViewService {

    List<PersonMleView> findAllByPersonId(List<Long> personIds);
}
