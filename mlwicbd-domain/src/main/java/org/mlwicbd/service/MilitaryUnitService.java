package org.mlwicbd.service;

import org.mlwicbd.criteria.MilitaryUnitSearchCriteria;
import org.mlwicbd.model.MilitaryUnit;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Set;

/**
 * Created by dmadunic on 16/04/16.
 */
public interface MilitaryUnitService {

    MilitaryUnit findById(Long id);

    Page<MilitaryUnit> searchByCriteria(MilitaryUnitSearchCriteria criteria, Pageable pageable);

    List<MilitaryUnit> recursiveSearch(MilitaryUnit militaryUnit);

    Set<MilitaryUnit> findByParentId(Long id);

    boolean exists(Long id);

}
