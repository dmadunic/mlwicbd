package org.mlwicbd.service;

import com.querydsl.core.types.Predicate;
import org.mlwicbd.model.person.Profession;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * @author idekic
 * @since 03-Aug-16.
 */
public interface ProfessionService {

    Profession findOne(Long id);

    Page<Profession> findAll(Pageable pageable);

    Page<Profession> findAll(Pageable pageable, Predicate predicate);

    Profession save(Profession profession);

    Profession findByCode(String code);
}
