package org.mlwicbd.service.impl;

import org.mlwicbd.dao.repository.RoleRepository;
import org.mlwicbd.model.Role;
import org.mlwicbd.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

/**
 * Created by tgreblicki on 18.05.17..
 */
@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleRepository roleRepository;

    public Set<Role> findByName(String name){
        return roleRepository.findByName(name);
    }

    @Override
    public Role findById(String id) {
        return roleRepository.findOne(id);
    }
}
