package org.mlwicbd.service.impl;

import org.mlwicbd.dao.repository.TerritoryRepository;
import org.mlwicbd.model.Territory;
import org.mlwicbd.service.TerritoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author idekic
 * @since 03-Aug-16.
 */
@Service
public class TerritoryServiceImpl implements TerritoryService {

    @Autowired
    private TerritoryRepository territoryRepository;

    @Override
    public Territory findOne(Long id) {
        Territory result = territoryRepository.findOne(id);
        return result;
    }
}
