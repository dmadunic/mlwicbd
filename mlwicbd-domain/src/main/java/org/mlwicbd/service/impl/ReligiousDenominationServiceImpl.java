package org.mlwicbd.service.impl;

import org.mlwicbd.dao.repository.ReligiousDenominationRepository;
import org.mlwicbd.model.person.ReligiousDenomination;
import org.mlwicbd.service.ReligiousDenominationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * Created by dmadunic on 24/04/16.
 */
@Service
public class ReligiousDenominationServiceImpl implements ReligiousDenominationService {

    @Autowired
    private ReligiousDenominationRepository repository;

    @Override
    public Page<ReligiousDenomination> findAll(Pageable pageable) {
        return repository.findAll(pageable);
    }


    @Override
    @Cacheable(value = "religiousDenominationService.findByCode")
    public ReligiousDenomination findByCode(String code) {
        return repository.findByCode(code);
    }

    @Override
    @Cacheable(value = "religiousDenominationService.findById")
    public ReligiousDenomination findById(Long id) {
        return repository.findOne(id);
    }

    @Override
    public boolean exists(Long id) {
        return repository.exists(id);
    }
}
