package org.mlwicbd.service.impl;

import org.mlwicbd.criteria.MilitaryUnitSearchCriteria;
import org.mlwicbd.dao.repository.MilitaryUnitRepository;
import org.mlwicbd.model.MilitaryUnit;
import org.mlwicbd.service.MilitaryUnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Created by domagoj on 17/04/16.
 */
@Service
public class MilitaryUnitServiceImpl implements MilitaryUnitService {

    @Autowired
    private MilitaryUnitRepository militaryUnitRepository;


    @Override
    public MilitaryUnit findById(Long id) {
        return militaryUnitRepository.findOne(id);
    }

    @Override
    public Page<MilitaryUnit> searchByCriteria(MilitaryUnitSearchCriteria criteria, Pageable pageable) {
        Page<MilitaryUnit> result = militaryUnitRepository.searchByCriteria(criteria, pageable);
        return result;
    }

    @Override
    public Set<MilitaryUnit> findByParentId(final Long id) {
        return militaryUnitRepository.findByParentUnitId(id);
    }

    @Override
    public List<MilitaryUnit> recursiveSearch(MilitaryUnit militaryUnit) {
        List<MilitaryUnit> out = new ArrayList<MilitaryUnit>();
        out.add(militaryUnit);
        List<MilitaryUnit> children = new ArrayList<MilitaryUnit>(findAllBelongingTo(militaryUnit));
        children.sort(Comparator.comparing(MilitaryUnit::getName));
        for (MilitaryUnit child : children) {
            out.addAll(recursiveSearch(child));
        }
        return out;
    }

    private Set<MilitaryUnit> findAllBelongingTo(final MilitaryUnit parentUnit) {
        Set<MilitaryUnit> result = new HashSet<MilitaryUnit>();
        Set<MilitaryUnit> children = findByParentId(parentUnit.getId());
        result.addAll(children);

        for (MilitaryUnit militarylUnit : children) {
            Set<MilitaryUnit> childrenUnits = findAllBelongingTo(militarylUnit);
            result.addAll(childrenUnits);
        }
        return result;
    }

    @Override
    public boolean exists(Long id) {
        return militaryUnitRepository.exists(id);
    }
}
