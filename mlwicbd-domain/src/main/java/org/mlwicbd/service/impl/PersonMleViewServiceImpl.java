package org.mlwicbd.service.impl;

import org.mlwicbd.dao.repository.PersonMleViewRepository;
import org.mlwicbd.model.view.PersonMleView;
import org.mlwicbd.service.PersonMleViewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by tgreblicki on 10.05.17..
 */
@Service
public class PersonMleViewServiceImpl implements PersonMleViewService {

    @Autowired
    private PersonMleViewRepository personMleViewRepository;

    @Override
    public List<PersonMleView> findAllByPersonId(List<Long> personIds) {
        return personMleViewRepository.findAllByPersonIdInOrderByPersonIdAscMlYearAscMlMonthAsc(personIds);
    }
}
