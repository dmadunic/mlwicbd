package org.mlwicbd.service.impl;

import com.ag04.common.exception.ConcurrencyException;
import com.ag04.common.exception.EntityNotFoundException;
import org.mlwicbd.dao.repository.MusterListEntryRepository;
import org.mlwicbd.model.musterlist.Child;
import org.mlwicbd.model.musterlist.MusterListEntry;
import org.mlwicbd.model.musterlist.ServiceRecord;
import org.mlwicbd.service.MusterListEntryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dmadunic on 04/05/16.
 */
@Service
public class MusterListEntryServiceImpl implements MusterListEntryService {
    private static final Logger LOG = LoggerFactory.getLogger(MusterListEntryServiceImpl.class);

    @Autowired
    private MusterListEntryRepository repository;

    @Override
    public int countEntires(Long musterListId) {
        return repository.countByMusterListId(musterListId);
    }

    @Override
    public boolean existsByListNumber(Integer listNumber, Long musterListId) {
        int c = repository.countByListNumberAndMusterListId(listNumber, musterListId);
        if (c > 0) {
            return true;
        }
        return false;
    }

    @Override
    public boolean exists(Long id) {
        return repository.exists(id);
    }

    @Override
    public Page<MusterListEntry> findByPersonId(final Long personId, final Pageable pageable) {
        Page<MusterListEntry> result = repository.findByPersonId(personId, pageable);

        return result;
    }

    @Override
    public Page<MusterListEntry> findByPersonId(List<Long> personId, Pageable pageable) {
        Page<MusterListEntry> result = repository.findByPersonIdIn(personId, pageable);
        return result;
    }

    @Override
    public Page<MusterListEntry> findByMusterListId(final Long musterListId, final Pageable pageable) {
        Page<MusterListEntry> result = repository.findByMusterListId(musterListId, pageable);

        return result;
    }

    @Override
    public MusterListEntry findById(Long id) {
        MusterListEntry result = repository.findOne(id);
        if (result == null) {
            throw new EntityNotFoundException(id.toString());
        }
        return result;
    }

    @Override
    public MusterListEntry findByIdAndVersion(Long id, Long version) {
        MusterListEntry result = findById(id);
        if (result.getVersion() != version) {
            throw new ConcurrencyException(id.toString(), version, result.getVersion());
        }
        return result;
    }


    @Modifying
    @Override
    public void deleteById(Long id) {
        repository.deleteById(id);
    }

    @Override
    @Transactional
    public MusterListEntry save(MusterListEntry musterListEntry) {
        try {
            MusterListEntry savedMle;
            if (musterListEntry.getId() == null) {
                LOG.debug("Saving NEW MusterListEntry entity --> without children and service record entities");

                savedMle = repository.save(musterListEntry);

            } else {
                savedMle = repository.save(musterListEntry);
            }
            return savedMle;
        } catch (ObjectOptimisticLockingFailureException ex) {
            LOG.debug("FAILED to save MuterListEntry id=" + musterListEntry.getId(), ex);
            throw new ConcurrencyException(musterListEntry.getId().toString(), musterListEntry.getVersion(), null);
        }
    }

    @Override
    public List<MusterListEntry> findByMusterListId(Long musterListId) {
        return repository.findByMusterListId(musterListId);
    }

    @Override
    public List<MusterListEntry> findByPersonId(List<Long> ids) {
        return repository.findByPersonIdIn(ids);
    }

    @Override
    public List<MusterListEntry> save(List<MusterListEntry> mergedEntries) {
        return repository.save(mergedEntries);
    }

    @Override
    public MusterListEntry findByMusterListIdAndPersonId(Long musterListId, Long personId) {
        return repository.findByMusterListIdAndPersonId(musterListId, personId);
    }

    @Override
    public void deleteByPersonId(Long personId) {
        repository.deleteByPersonId(personId);
    }
}
