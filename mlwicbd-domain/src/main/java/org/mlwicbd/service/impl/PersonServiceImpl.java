package org.mlwicbd.service.impl;

import org.mlwicbd.criteria.PersonSearchCriteria;
import org.mlwicbd.dao.repository.PersonRepository;
import org.mlwicbd.model.person.Person;
import org.mlwicbd.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by dmadunic on 24/04/2016.
 */
@Service
public class PersonServiceImpl implements PersonService {

    @Autowired
    private PersonRepository repository;

    @Override
    public Person findOne(Long id) {
        Person person = repository.findOne(id);
        return person;
    }

    @Override
    public void deleteById(Long id) {
        repository.deleteById(id);
    }

    @Override
    public Person save(Person person) {
        return repository.save(person);
    }

    @Override
    public Page<Person> searchByCriteria(PersonSearchCriteria criteria, Pageable pageable) {
        Page<Person> result = repository.searchByCriteria(criteria, pageable);
        return result;
    }

    @Override
    public List<Person> findByIds(List<Long> ids) {
        return repository.findAll(ids);
    }
}
