package org.mlwicbd.service.impl;

import com.ag04.common.exception.ConcurrencyException;
import com.ag04.common.exception.EntityNotFoundException;
import org.mlwicbd.criteria.MusterListSearchCriteria;
import org.mlwicbd.dao.repository.MusterListRepository;
import org.mlwicbd.model.MilitaryUnit;
import org.mlwicbd.model.musterlist.MusterList;
import org.mlwicbd.service.MilitaryUnitService;
import org.mlwicbd.service.MusterListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * Created by domagoj on 21/04/16.
 */
@Service
public class MusterListServiceImpl implements MusterListService {

    @Autowired
    private MusterListRepository repository;

    @Autowired
    private MilitaryUnitService militaryUnitService;

    @Override
    @Transactional(readOnly = true)
    public Page<MusterList> searchByCriteria(MusterListSearchCriteria criteria, Pageable pageable) {
        Collection<Long> unitIds = null;
        if (criteria.getMilitaryUnitId() != null) {
            unitIds = findChildUnitIds(criteria.getMilitaryUnitId());
            if (unitIds == null) {
                unitIds = new ArrayList<Long>();
                unitIds.add(criteria.getMilitaryUnitId());
            }
        }

        Page<MusterList> result = repository.searchByCriteria(criteria.getReferenceCode(), criteria.getYear(), criteria.getMonth(), unitIds, pageable);
        return result;
    }

    @Override
    public MusterList findById(Long id) {
        MusterList result = repository.findOne(id);
        if (result == null) {
            throw new EntityNotFoundException(id.toString());
        }
        return result;
    }

    @Override
    public MusterList findByIdAndVersion(Long id, Long version) {
        MusterList result = findById(id);
        if (result.getVersion() != version) {
            throw new ConcurrencyException(id.toString(), version, result.getVersion());
        }
        return result;
    }

    private Collection<Long> findChildUnitIds(final Long parentUnitId) {
        Set<Long> result = new HashSet<Long>();
        MilitaryUnit parentUnit = militaryUnitService.findById(parentUnitId);

        List<MilitaryUnit> units = militaryUnitService.recursiveSearch(parentUnit);
        for (MilitaryUnit unit : units) {
            result.add(unit.getId());
        }
        return result;
    }

    @Override
    @Transactional
    public MusterList save(MusterList musterList) {
        return repository.save(musterList);
    }

    @Override
    public Long exists(final String referenceCode)  {
        return repository.findIdByReferenceCode(referenceCode);
    }

    @Override
    public Long exists(Long unitId, Integer year, Integer month)  {
        return repository.findIdByUnitIdAndYearAndMonth(unitId, year, month);
    }
}
