package org.mlwicbd.service.impl;

import com.querydsl.core.types.Predicate;
import org.mlwicbd.dao.repository.ProfessionRepository;
import org.mlwicbd.model.person.Profession;
import org.mlwicbd.service.ProfessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * @author idekic
 * @since 03-Aug-16.
 */
@Service
public class ProfessionServiceImpl implements ProfessionService {

    @Autowired
    private ProfessionRepository professionRepository;

    @Override
    public Profession findOne(Long id) {
        return professionRepository.findOne(id);
    }

    @Override
    public Page<Profession> findAll(Pageable pageable) {
        return professionRepository.findAll(pageable);
    }

    @Override
    public Page<Profession> findAll(Pageable pageable, Predicate predicate) {
        return professionRepository.findAll(predicate, pageable);
    }

    @Override
    public Profession save(Profession profession) {
        return professionRepository.save(profession);
    }

    @Override
    public Profession findByCode(String code) {
        return professionRepository.findByCode(code);
    }
}
