package org.mlwicbd.service.impl;

import org.mlwicbd.dao.repository.UserRepository;
import org.mlwicbd.model.User;
import org.mlwicbd.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by dmadunic on 04/09/15.
 */
@Service
public class UserServiceImpl implements UserService {
    private static final Logger LOG = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    private UserRepository userRepository;

    @Override
    public User findById(Long id) {
        User user = userRepository.findOne(id);
        return user;
    }

    @Override
    @Cacheable(value = "userService.findByUsername", key = "#username")
    @Transactional(propagation = Propagation.REQUIRED)
    public User findByUsername(final String username) {
        User user = userRepository.findByUsername(username);
        return user;
    }

    @Override
    public User findByEmail(final String email) {
        User user = userRepository.findByEmail(email);
        return user;
    }

    @Override
    @Transactional
    public User save(final User user) {
        User savedUser = userRepository.save(user);

        return savedUser;
    }

    @CacheEvict(value = "userService.findByUsername", key = "#username")
    public void resetCacheOnSave(String username) {
        LOG.debug("Remved user data from cahce for username='{}'", username);
    }
}
