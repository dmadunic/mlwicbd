package org.mlwicbd.service.impl;

import org.mlwicbd.criteria.MilitaryRankSearchCriteria;
import org.mlwicbd.dao.repository.MilitaryRankRepository;
import org.mlwicbd.model.MilitaryRank;
import org.mlwicbd.service.MilitaryRankService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by dmadunic on 24/04/16.
 */
@Service
public class MilitaryRankServiceImpl implements MilitaryRankService {

    @Autowired
    private MilitaryRankRepository repository;

    @Override
    @Transactional(readOnly = true)
    public Page<MilitaryRank> searchByCriteria(MilitaryRankSearchCriteria criteria, Pageable pageable) {
        Page<MilitaryRank> result = repository.searchByCriteria(criteria, pageable);
        return result;
    }

    @Override
    @Cacheable(value = "militaryRankService.findOne")
    public MilitaryRank findOne(Long id) {
        MilitaryRank result = repository.findOne(id);
        return result;
    }

    @Override
    public boolean exists(Long id) {
        return repository.exists(id);
    }
}
