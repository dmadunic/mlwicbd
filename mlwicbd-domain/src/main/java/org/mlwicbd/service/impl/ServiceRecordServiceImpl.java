package org.mlwicbd.service.impl;

import org.mlwicbd.dao.repository.ServiceRecordRepository;
import org.mlwicbd.service.ServiceRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author idekic
 * @since 19-Aug-16.
 */
@Service
public class ServiceRecordServiceImpl implements ServiceRecordService {

    @Autowired
    private ServiceRecordRepository serviceRecordRepository;

    @Modifying
    @Override
    public void deleteByMusterListEntryId(Long id) {
        serviceRecordRepository.deleteByMusterListEntryId(id);
    }
}
