package org.mlwicbd.service.impl;

import org.mlwicbd.dao.repository.AppConfigRepository;
import org.mlwicbd.model.AppConfig;
import org.mlwicbd.service.AppConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by domagoj on 24/08/15.
 */
@Service
public class AppConfigServiceImpl implements AppConfigService {

    @Autowired
    private AppConfigRepository repository;

    @Override
    @Cacheable(value = "appConfigService.findByKey")
    public AppConfig findByKey(String key) {
        return repository.findByKey(key);
    }

    @Override
    @Cacheable(value = "appConfigService.getValueForKey")
    public String getValueForKey(String key) {
        AppConfig cfg = repository.findByKey(key);
        if (cfg != null) {
            return cfg.getValue();
        }
        return null;
    }

    @Override
    public List<AppConfig> findAll() {
        return repository.findAll();
    }

}
