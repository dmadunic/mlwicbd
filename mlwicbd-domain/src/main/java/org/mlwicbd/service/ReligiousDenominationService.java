package org.mlwicbd.service;

import org.mlwicbd.model.person.ReligiousDenomination;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Created by dmadunic on 24/04/16.
 */
public interface ReligiousDenominationService {

    Page<ReligiousDenomination> findAll(Pageable pageable);

    ReligiousDenomination findByCode(String code);

    ReligiousDenomination findById(Long id);

    boolean exists(Long id);

}
