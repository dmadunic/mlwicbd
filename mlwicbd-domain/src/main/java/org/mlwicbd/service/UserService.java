package org.mlwicbd.service;

import org.mlwicbd.model.User;

/**
 * Created by dmadunic on 04/09/15.
 */
public interface UserService {

    User findById(Long id);

    User findByUsername(String username);

    User findByEmail(String email);

    User save(User user);

    void resetCacheOnSave(String username);

}
