package org.mlwicbd.service;

import org.mlwicbd.criteria.MusterListSearchCriteria;
import org.mlwicbd.model.musterlist.MusterList;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 *
 * Created by dmadunic on 20/04/16.
 */
public interface MusterListService {

    Page<MusterList> searchByCriteria(MusterListSearchCriteria criteria, Pageable pageable);

    MusterList findById(Long id);

    MusterList findByIdAndVersion(Long id, Long version);

    MusterList save(MusterList musterList);

    Long exists(String referenceCode);

    Long exists(Long unitId, Integer year, Integer month);

}
