package org.mlwicbd.service;

import org.mlwicbd.criteria.MilitaryRankSearchCriteria;
import org.mlwicbd.model.MilitaryRank;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Created by dmadunic on 24/04/16.
 */
public interface MilitaryRankService {

    MilitaryRank findOne(Long id);

    Page<MilitaryRank> searchByCriteria(MilitaryRankSearchCriteria criteria, Pageable pageable);

    boolean exists(Long id);
}
