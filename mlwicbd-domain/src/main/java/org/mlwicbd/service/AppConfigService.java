package org.mlwicbd.service;

import org.mlwicbd.model.AppConfig;

import java.util.List;

/**
 * Created by dmadunic on 24/08/15.
 */
public interface AppConfigService {

    AppConfig findByKey(String key);

    String getValueForKey(String key);

    List<AppConfig> findAll();
}
