package org.mlwicbd.service;

import org.mlwicbd.criteria.PersonSearchCriteria;
import org.mlwicbd.model.person.Person;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Created by dmadunic on 24/04/16.
 */
public interface PersonService {

    Page<Person> searchByCriteria(PersonSearchCriteria criteria, Pageable pageable);

    Person findOne(Long id);

    void deleteById(Long id);

    Person save(Person person);

    List<Person> findByIds(List<Long> ids);
}
