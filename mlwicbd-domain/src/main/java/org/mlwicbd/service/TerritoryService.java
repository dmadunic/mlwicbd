package org.mlwicbd.service;

import org.mlwicbd.model.Territory;

/**
 * @author idekic
 * @since 03-Aug-16.
 */
public interface TerritoryService {
    Territory findOne(Long id);
}
