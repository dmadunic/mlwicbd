package org.mlwicbd.service;

import org.mlwicbd.model.musterlist.MusterListEntry;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Created by dmadunic on 04/05/16.
 */
public interface MusterListEntryService {

    int countEntires(Long musterListId);

    boolean exists(Long id);

    boolean existsByListNumber(Integer listNumber, Long musterListId);

    Page<MusterListEntry> findByMusterListId(Long musterListId, Pageable pageable);

    Page<MusterListEntry> findByPersonId(Long personId, Pageable pageable);

    MusterListEntry findById(Long id);

    MusterListEntry findByIdAndVersion(Long id, Long version);

    void deleteById(Long id);

    MusterListEntry save(MusterListEntry musterListEntry);

    List<MusterListEntry> findByMusterListId(Long musterListId);

    List<MusterListEntry> findByPersonId(List<Long> longs);

    List<MusterListEntry> save(List<MusterListEntry> mergedEntries);

    Page<MusterListEntry> findByPersonId(List<Long> personId, Pageable pageable);

    MusterListEntry findByMusterListIdAndPersonId(Long musterListId, Long personId);

    void deleteByPersonId(Long personId);
}
