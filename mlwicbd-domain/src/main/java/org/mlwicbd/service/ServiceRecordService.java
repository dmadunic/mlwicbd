package org.mlwicbd.service;

/**
 * @author idekic
 * @since 19-Aug-16.
 */
public interface ServiceRecordService {
    void deleteByMusterListEntryId(Long id);
}
