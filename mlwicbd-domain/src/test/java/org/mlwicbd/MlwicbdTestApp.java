package org.mlwicbd;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.AuditorAware;
import org.mlwicbd.support.audit.MockUserAuditorAwareImpl;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Created by dmadunic on 10/12/16.
 */
@SpringBootApplication
@EnableTransactionManagement
@EnableJpaAuditing(auditorAwareRef = "mockAuditorAware")
public class MlwicbdTestApp {

    @Bean
    public AuditorAware mockAuditorAware() {
        AuditorAware auditor = new MockUserAuditorAwareImpl();
        return auditor;
    }
}
