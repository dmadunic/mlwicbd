package org.mlwicbd.dao.repository;

import com.ag04.support.springtestdbunit.SysdateReplacementDataSetLoader;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DbUnitConfiguration;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mlwicbd.criteria.MilitaryUnitSearchCriteria;
import org.mlwicbd.model.MilitaryUnit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.fest.assertions.Assertions.assertThat;

/**
 *
 * Created by dmadunic on 16/04/16.
 */
@DataJpaTest
@RunWith(SpringRunner.class)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
@DatabaseSetup(value = {
        "/dbunit/territories-data.xml",
        "/dbunit/unit-types-data.xml",
        "/dbunit/military-units-data.xml"
})
@DbUnitConfiguration(dataSetLoader = SysdateReplacementDataSetLoader.class)
public class MilitaryUnitRepositoryTest {

    @Autowired
    MilitaryUnitRepository repository;

    @Test
    @Transactional(readOnly = true)
    public void findOneWhenUnitExists() {
        // arrange ...

        // act ...
        MilitaryUnit unit = repository.findOne(1L);

        //assert ...
        assertThat(unit).isNotNull();
        assertThat(unit.getId()).isEqualTo(1L);
        assertThat(unit.getName()).isEqualTo("regiment.no.60");
        assertThat(unit.getDescription()).isEqualTo("I. Lička pukovnija / I.  Likaner Regiment No. 60");

        assertThat(unit.getTerritory()).isNotNull();
        assertThat(unit.getTerritory().getCode()).isEqualTo("CS-MIL-FR");

        assertThat(unit.getUnitType()).isNotNull();
        assertThat(unit.getUnitType().getName()).isEqualToIgnoringCase("REGIMENT");
    }

    @Test
    @Transactional(readOnly = true)
    public void findByCriteriaWhenMatchingRecordsAreFound() {
        // arrange ...
        MilitaryUnitSearchCriteria criteria = new MilitaryUnitSearchCriteria();
        criteria.setUnitTypeCode("REGIMENT");
        criteria.setTerritoryCode("CS-MIL-FR");

        PageRequest pageRequest = new PageRequest(2, 5, new Sort(Sort.Direction.DESC, "id").and(new Sort(Sort.Direction.ASC, "name")));

        // act ...
        Page<MilitaryUnit> result = repository.searchByCriteria(criteria, pageRequest);

        // assert ... last page with only one element should be returned
        assertThat(result).isNotNull();
        assertThat(result.getTotalElements()).isEqualTo(11);
        assertThat(result.getTotalPages()).isEqualTo(3);
        assertThat(result.getNumber()).isEqualTo(2);
        assertThat(result.getNumberOfElements()).isEqualTo(1);
        assertThat(result.getSize()).isEqualTo(5);

        List<MilitaryUnit> units = result.getContent();
        assertThat(units).isNotNull();
        assertThat(units.size()).isEqualTo(1);
        assertThat(units.get(0).getId()).isEqualTo(1L);

    }



}
