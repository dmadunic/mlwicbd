package org.mlwicbd.dao.repository;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DbUnitConfiguration;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mlwicbd.MlwicbdTestConstants;
import org.mlwicbd.model.User;
import org.mlwicbd.model.musterlist.MusterList;
import org.mlwicbd.support.audit.MockUserAuditorAwareImpl;
import org.mlwicbd.support.springtestdbunit.SysdateReplacementDataSetLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.AuditorAware;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import static org.fest.assertions.Assertions.assertThat;

/**
 * Created by dmadunic on 18/03/17.
 */
@DataJpaTest
@RunWith(SpringRunner.class)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class })
@DbUnitConfiguration(dataSetLoader = SysdateReplacementDataSetLoader.class)
@DatabaseSetup(value = {
        "/dbunit/roles-data.xml",
        "/dbunit/countries-data.xml",
        "/dbunit/users-data.xml",
        "/dbunit/territories-data.xml",
        "/dbunit/unit-types-data.xml",
        "/dbunit/military-units-data.xml",
        "/dbunit/muster-list-data.xml"
}, type= DatabaseOperation.REFRESH)
public class MusterListRepositoryTest {

    @Autowired
    MusterListRepository repository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    MilitaryUnitRepository unitRepository;

    @Autowired
    AuditorAware auditorAware;

    User defaultAuditUser = null;

    @Before
    public void setup() {
        if (defaultAuditUser == null) {
            defaultAuditUser = userRepository.findOne(MlwicbdTestConstants.DEFAULT_AUDIT_USER_ID);
            ((MockUserAuditorAwareImpl) auditorAware).setAuditUser(defaultAuditUser);
        }
    }

    @Test
    public void testFindOne_whenEntityExists() {
        // act ...
        MusterList ml = repository.findOne(1L);

        //assert ...
        assertThat(ml).isNotNull();
        assertThat(ml.getId()).isEqualTo(1L);
        assertThat(ml.getMonth()).isEqualTo(1);
        assertThat(ml.getYear()).isEqualTo(1710);
        assertThat(ml.getReferenceCode()).isEqualTo("REF-111111111");
        assertThat(ml.getTitle()).isEqualTo("ML regiment.no.60.company.1 1710/1");
        assertThat(ml.getMilitaryUnit()).isNotNull();
        assertThat(ml.getMilitaryUnit().getId()).isEqualTo(12L);

    }

    @Test
    public void testSaveNewMusterList() {
        // arrange ...
        MusterList ml = new MusterList();
        ml.setMilitaryUnit(unitRepository.findOne(12L));
        ml.setYear(1710);
        ml.setMonth(1);
        ml.setTitle("ML title");
        ml.setReferenceCode("ML ReferenceCode");

        // act ...
        MusterList savedMusterList = repository.save(ml);

        // assert
        assertThat(savedMusterList.getId()).isNotNull();
        assertThat(savedMusterList.getVersion()).isEqualTo(0);

        assertThat(savedMusterList.getMonth()).isEqualTo(1);
        assertThat(savedMusterList.getYear()).isEqualTo(1710);
        assertThat(savedMusterList.getReferenceCode()).isEqualTo("ML ReferenceCode");
        assertThat(savedMusterList.getTitle()).isEqualTo("ML title");
        assertThat(savedMusterList.getMilitaryUnit()).isNotNull();

        assertThat(savedMusterList.getCreatedBy()).isNotNull();
        assertThat(savedMusterList.getCreatedBy().getUsername()).isEqualTo(defaultAuditUser.getUsername());
        assertThat(savedMusterList.getModifiedBy()).isNotNull();
        assertThat(savedMusterList.getModifiedBy().getUsername()).isEqualTo(defaultAuditUser.getUsername());
        assertThat(savedMusterList.getCreatedDate()).isNotNull();
        assertThat(savedMusterList.getModifiedDate()).isNotNull();
    }


    @Test(expected = ObjectOptimisticLockingFailureException.class)
    public void testSaveWithOldVersion_whenObjectIsCopied_thenConcurrencyExceptionIsThrown() {
        // arrange ...
        Long id = 2L;
        MusterList mlFromDb = repository.findOne(id);

        MusterList ml = new MusterList();
        ml.setId(mlFromDb.getId());
        ml.setVersion(mlFromDb.getVersion() -1);
        ml.setMilitaryUnit(mlFromDb.getMilitaryUnit());
        ml.setYear(mlFromDb.getYear());
        ml.setMonth(mlFromDb.getMonth());
        ml.setTitle("New title");
        ml.setReferenceCode("NewreferenceCode");

        //act ...
        repository.saveAndFlush(ml);

    }

//    @Test(expected = ObjectOptimisticLockingFailureException.class)
//    public void testSaveWithOldVersion_whenObjectIsmerged_thenConcurrencyExceptionIsThrown() {
//        // arrange ...
//        Long id = 2L;
//        MusterList mlFromDb = repository.findOne(id);
//        mlFromDb.setVersion(mlFromDb.getVersion() -1);
//        mlFromDb.setTitle("New title");
//        mlFromDb.setReferenceCode("NewreferenceCode");
//
//        //act ...
//        MusterList mlSave = repository.saveAndFlush(mlFromDb);
//
//        //assert ...
//        assertThat(mlSave).isNotNull();
//    }

}
