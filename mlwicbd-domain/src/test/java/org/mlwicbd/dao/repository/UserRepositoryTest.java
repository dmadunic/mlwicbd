package org.mlwicbd.dao.repository;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mlwicbd.model.User;
import org.mlwicbd.model.UserProfile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import static org.fest.assertions.Assertions.assertThat;

/**
 * Created by dmadunic on 02/04/16.
 */
@DataJpaTest
@RunWith(SpringRunner.class)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class })
@DatabaseSetup(value = {
        "/dbunit/roles-data.xml",
        "/dbunit/countries-data.xml",
        "/dbunit/users-data.xml"
}, type= DatabaseOperation.REFRESH)
public class UserRepositoryTest {
    public static final Long USER_WITH_PROFILE = 1L;

    @Autowired
    UserRepository repository;

    @Autowired
    CountryRepository countryRepository;

    @Test
    @Transactional(readOnly = true)
    public void findOneWhenUserExists() {
        // arrange ...

        //act...
        User user = repository.findOne(USER_WITH_PROFILE);

        //assert ...
        assertThat(user).isNotNull();
        assertThat(user.getId()).isEqualTo(USER_WITH_PROFILE);
        assertThat(user.getUsername()).isEqualTo("dmadunic");
        assertThat(user.getFirstName()).isEqualTo("Domagoj");
        assertThat(user.getLastName()).isEqualTo("Madunić");
        assertThat(user.getEmail()).isEqualTo("domagoj.madunic@gmail.com");
        assertThat(user.getPassword()).isEqualTo("k7gnu3sdo+ol0wnhqovwhr3g6s1xyv72ol/pe/unols=");
        assertThat(user.getRoles()).isNotNull();
        assertThat(user.getRoles().size()).isEqualTo(4);
        assertThat(user.getUserProfile()).isNotNull();
        assertThat(user.getUserProfile().getMobile()).isEqualTo("+385992821100");
        assertThat(user.getUserProfile().getPhone()).isEqualTo("4616121");
        assertThat(user.getUserProfile().getAddress()).isEqualTo("A. Bauera 29");
        assertThat(user.getUserProfile().getPostalCode()).isEqualTo("10000");
        assertThat(user.getUserProfile().getPlace()).isEqualTo("Zagreb");
        assertThat(user.getUserProfile().getCountry()).isNotNull();
        assertThat(user.getUserProfile().getCountry().getCode()).isEqualTo("191");
    }

    @Test
    @Transactional
    public void saveNewUserWithProfile() {
        // arrange ...
        User user = new User();
        user.setUsername("pperic");
        user.setFirstName("Pero");
        user.setLastName("Perić");
        user.setPassword("k7gnu3sdo+ol0wnhqovwhr3g6s1xyv72ol/pe/unols=");
        user.setEmail("pperic@gmail.com");
        user.setEnabled(false);

        UserProfile userProfile = new UserProfile();
        userProfile.setMobile("+385992821100");
        userProfile.setPhone("4616121");
        userProfile.setAddress("A. Bauera 29");
        userProfile.setPlace("Zagreb");
        userProfile.setPostalCode("10000");
        userProfile.setCountry(countryRepository.findByCode("191"));

        userProfile.setUser(user);
        user.setUserProfile(userProfile);


        //act...
        User savedUser = repository.save(user);

        //assert ...
        assertThat(savedUser).isNotNull();
        assertThat(savedUser.getId()).isNotNull();
        assertThat(savedUser.getUsername()).isEqualTo("pperic");
        assertThat(savedUser.getFirstName()).isEqualTo("Pero");
        assertThat(savedUser.getLastName()).isEqualTo("Perić");
        assertThat(savedUser.getEmail()).isEqualTo("pperic@gmail.com");
        assertThat(savedUser.getPassword()).isEqualTo("k7gnu3sdo+ol0wnhqovwhr3g6s1xyv72ol/pe/unols=");
        assertThat(savedUser.getRoles()).isNotNull();
        assertThat(savedUser.getRoles().size()).isEqualTo(0);
        assertThat(savedUser.getUserProfile()).isNotNull();
        assertThat(savedUser.getUserProfile().getUserId()).isNotNull();

        assertThat(savedUser.getUserProfile().getMobile()).isEqualTo("+385992821100");
        assertThat(savedUser.getUserProfile().getPhone()).isEqualTo("4616121");
        assertThat(savedUser.getUserProfile().getAddress()).isEqualTo("A. Bauera 29");
        assertThat(savedUser.getUserProfile().getPostalCode()).isEqualTo("10000");
        assertThat(savedUser.getUserProfile().getPlace()).isEqualTo("Zagreb");
        assertThat(savedUser.getUserProfile().getCountry()).isNotNull();
        assertThat(savedUser.getUserProfile().getCountry().getCode()).isEqualTo("191");
    }

}
