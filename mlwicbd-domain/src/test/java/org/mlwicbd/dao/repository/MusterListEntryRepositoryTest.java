package org.mlwicbd.dao.repository;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DbUnitConfiguration;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mlwicbd.model.MilitaryRank;
import org.mlwicbd.model.MilitaryRankType;
import org.mlwicbd.model.Territory;
import org.mlwicbd.model.musterlist.*;
import org.mlwicbd.model.person.MaritalStatus;
import org.mlwicbd.model.person.PersonMeasures;
import org.mlwicbd.model.person.Profession;
import org.mlwicbd.model.person.ReligiousDenomination;
import org.mlwicbd.support.springtestdbunit.SysdateReplacementDataSetLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static org.fest.assertions.Assertions.assertThat;
/**
 * Created by tgreblicki on 20.04.17..
 */
@DataJpaTest
@RunWith(SpringRunner.class)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
@DbUnitConfiguration(dataSetLoader = SysdateReplacementDataSetLoader.class)
@DatabaseSetup(value = {
        "/dbunit/roles-data.xml",
        "/dbunit/countries-data.xml",
        "/dbunit/users-data.xml",
        "/dbunit/territories-data.xml",
        "/dbunit/unit-types-data.xml",
        "/dbunit/military-units-data.xml",
        "/dbunit/muster-list-data.xml",
        "/dbunit/profession-data.xml",
        "/dbunit/military-rank-data.xml",
        "/dbunit/religious-denomination-data.xml",
        "/dbunit/person-data.xml",
        "/dbunit/muster-list-entry-data.xml",
        "/dbunit/mle-child-data.xml",
        "/dbunit/service-record-data.xml"
})
public class MusterListEntryRepositoryTest {

    @Autowired
    private MusterListEntryRepository musterListEntryRepository;

    @Autowired
    private MusterListRepository musterListRepository;

    @Autowired
    private MilitaryRankRepository militaryRankRepository;

    @Autowired
    private ReligiousDenominationRepository religiousDenominationRepository;

    @Autowired
    private ProfessionRepository professionRepository;


    @Test
    @Transactional
    public void saveNewMusterListEntry_withChildAndServiceRecordAndNoPerson(){
        // arrange...
        PersonMeasures personMeasures = new PersonMeasures();
        personMeasures.setUnit1(100);
        personMeasures.setUnit2(100);
        personMeasures.setUnit3(100);

        Profession profession = professionRepository.findOne(1L);
//        profession.setId(1L);
//        profession.setCode("prof.mason");
//        profession.setName("Mason builder");

        Territory territory = new Territory();
        territory.setId(1L);
        territory.setCode("CS-MIL-FR");
        territory.setName("Vojna krajina");

        MilitaryRank militaryRank = militaryRankRepository.findOne(1L);
//        militaryRank.setId(1L);
//        militaryRank.setCode("GEMEINE");
//        militaryRank.setDescription("Redov, private, gemeiner: lowest soldier rank");
//        militaryRank.setSourceName("Gemeiner");
//        militaryRank.setTerritory(territory);
//        militaryRank.setType(MilitaryRankType.SOLDIERS);

        ReligiousDenomination religiousDenomination = religiousDenominationRepository.findOne(1L);
//        religiousDenomination.setId(1L);
//        religiousDenomination.setCode("roman.catholic");
//        religiousDenomination.setName("Roman Catholic");

        MusterList musterList = musterListRepository.findOne(1L);

        MusterListEntry musterListEntry = new MusterListEntry();
        musterListEntry.setMusterList(musterList);
        musterListEntry.setAge(25);
        musterListEntry.setFirstName("Petar");
        musterListEntry.setLastName("Karađorđević");
        musterListEntry.setHouseNumber("10");
        musterListEntry.setListNumber(10);
        musterListEntry.setLocation("Location");
        musterListEntry.setMaritalStatus(MaritalStatus.WIDOWED);
        musterListEntry.setMeasures(personMeasures);
        musterListEntry.setPerson(null);
        musterListEntry.setPlaceOfBirth("PLB");
        musterListEntry.setProfession(profession);
        musterListEntry.setRank(militaryRank);
        musterListEntry.setReligion(religiousDenomination);

        Child child = new Child();
        child.setName("Aleksandar");
        child.setAge(10);
        child.setGender(ChildGender.SON);
        child.setMusterListEntry(musterListEntry);
        List<Child> children = new ArrayList<>();
        children.add(child);

        ServiceRecord serviceRecord = new ServiceRecord();
        serviceRecord.setDescription("DescriptionSR");
        serviceRecord.setDurationDays("20");
        serviceRecord.setDurationMonths(2);
        serviceRecord.setDurationYears(10);
        serviceRecord.setMusterListEntry(musterListEntry);
        List<ServiceRecord> serviceRecords = new ArrayList<>();
        serviceRecords.add(serviceRecord);

        musterListEntry.setChildren(children);
        musterListEntry.setServiceRecords(serviceRecords);

        //act...
        MusterListEntry savedMle = musterListEntryRepository.saveAndFlush(musterListEntry);

        //arrange...
        assertThat(savedMle.getId()).isNotNull();
        assertThat(savedMle.getMusterList()).isNotNull();
        assertThat(savedMle.getAge()).isEqualTo(musterListEntry.getAge());
        assertThat(savedMle.getFirstName()).isEqualTo(musterListEntry.getFirstName());
        assertThat(savedMle.getLastName()).isEqualTo(musterListEntry.getLastName());
        assertThat(savedMle.getHouseNumber()).isEqualTo(musterListEntry.getHouseNumber());
        assertThat(savedMle.getListNumber()).isEqualTo(musterListEntry.getListNumber());
        assertThat(savedMle.getLocation()).isEqualTo(musterListEntry.getLocation());
        assertThat(savedMle.getMaritalStatus()).isEqualTo(musterListEntry.getMaritalStatus());
        assertThat(savedMle.getMeasures().getUnit1()).isEqualTo(musterListEntry.getMeasures().getUnit1());
        assertThat(savedMle.getPerson()).isNull();
        assertThat(savedMle.getPlaceOfBirth()).isEqualTo(musterListEntry.getPlaceOfBirth());
        assertThat(savedMle.getProfession()).isNotNull();
        assertThat(savedMle.getRank()).isNotNull();
        assertThat(savedMle.getReligion()).isNotNull();
        assertThat(savedMle.getChildren()).isNotNull();
        assertThat(savedMle.getChildren().size()).isEqualTo(1);
        assertThat(savedMle.getChildren().get(0)).isNotNull();
        assertThat(savedMle.getChildren().get(0).getName()).isEqualTo(child.getName());
        assertThat(savedMle.getServiceRecords()).isNotNull();
        assertThat(savedMle.getServiceRecords().size()).isEqualTo(1);
        assertThat(savedMle.getServiceRecords().get(0)).isNotNull();
        assertThat(savedMle.getServiceRecords().get(0).getDescription()).isEqualTo("DescriptionSR");
    }

    @Test
    @Transactional
    public void updateMusterListEntry_withModifiedChildAndModifiedServiceRecord(){
        //arrange...
        MusterListEntry musterListEntry = musterListEntryRepository.findOne(1L);

        musterListEntry.getChildren().get(0).setName("Franz");
        musterListEntry.getServiceRecords().get(0).setDescription("ModifiedDescription");

        //act...
        MusterListEntry savedMle = musterListEntryRepository.saveAndFlush(musterListEntry);

        //arrange...

        assertThat(savedMle.getChildren().size()).isEqualTo(1);
        assertThat(savedMle.getChildren().get(0).getName()).isEqualTo("Franz");
        assertThat(savedMle.getServiceRecords().size()).isEqualTo(1);
        assertThat(savedMle.getServiceRecords().get(0).getDescription()).isEqualTo("ModifiedDescription");

    }

    @Test
    @Transactional
    public void updateMusterEntryList_withNewChildAndNewServiceRecord(){
        //arrange...
        MusterListEntry musterListEntry = musterListEntryRepository.findOne(1L);

        Child child = new Child();
        child.setGender(ChildGender.SON);
        child.setAge(10);
        child.setName("Georg");
        child.setMusterListEntry(musterListEntry);

        ServiceRecord serviceRecord = new ServiceRecord();
        serviceRecord.setDescription("Description");
        serviceRecord.setDurationYears(25);
        serviceRecord.setDurationMonths(10);
        serviceRecord.setDurationDays("20/31");
        serviceRecord.setMusterListEntry(musterListEntry);

        musterListEntry.getChildren().add(child);
        musterListEntry.getServiceRecords().add(serviceRecord);

        //act...
        MusterListEntry savedMle = musterListEntryRepository.saveAndFlush(musterListEntry);

        //arrange...
        assertThat(savedMle.getChildren().size()).isEqualTo(2);
        assertThat(savedMle.getChildren().get(1).getName()).isEqualTo("Georg");
        assertThat(savedMle.getChildren().get(1).getGender()).isEqualTo(ChildGender.SON);
        assertThat(savedMle.getChildren().get(1).getAge()).isEqualTo(10);

        assertThat(savedMle.getServiceRecords().size()).isEqualTo(2);
        assertThat(savedMle.getServiceRecords().get(1).getDescription()).isEqualTo("Description");
        assertThat(savedMle.getServiceRecords().get(1).getDurationYears()).isEqualTo(25);
        assertThat(savedMle.getServiceRecords().get(1).getDurationMonths()).isEqualTo(10);
        assertThat(savedMle.getServiceRecords().get(1).getDurationDays()).isEqualTo("20/31");


    }

    @Test
    @Transactional
    public void updateMusterEntryList_withRemovedChildAndRemovedServiceRecord(){
        //arrange...
        MusterListEntry musterListEntry = musterListEntryRepository.findOne(1L);

        musterListEntry.getChildren().remove(0);
        musterListEntry.getServiceRecords().remove(0);

        //act...
        MusterListEntry savedMle = musterListEntryRepository.save(musterListEntry);

        //assert...
        assertThat(savedMle.getChildren().size()).isEqualTo(0);
        assertThat(savedMle.getServiceRecords().size()).isEqualTo(0);

    }

    @Test
    @Transactional
    public void updateMusterEntryList_withReplacedChildAndServiceRecord(){
        //arrange...
        MusterListEntry musterListEntry = musterListEntryRepository.findOne(1L);

        musterListEntry.getChildren().remove(0);
        musterListEntry.getServiceRecords().remove(0);

        Child child = new Child();
        child.setGender(ChildGender.SON);
        child.setAge(10);
        child.setName("Georg");
        child.setMusterListEntry(musterListEntry);

        ServiceRecord serviceRecord = new ServiceRecord();
        serviceRecord.setDescription("Description");
        serviceRecord.setDurationYears(25);
        serviceRecord.setDurationMonths(10);
        serviceRecord.setDurationDays("20/31");
        serviceRecord.setMusterListEntry(musterListEntry);

        musterListEntry.getChildren().add(child);
        musterListEntry.getServiceRecords().add(serviceRecord);

        //act...
        MusterListEntry savedMle = musterListEntryRepository.saveAndFlush(musterListEntry);

        //assert...
        assertThat(savedMle.getChildren().size()).isEqualTo(1);
        assertThat(savedMle.getChildren().get(0).getName()).isEqualTo("Georg");
        assertThat(savedMle.getChildren().get(0).getGender()).isEqualTo(ChildGender.SON);
        assertThat(savedMle.getChildren().get(0).getAge()).isEqualTo(10);

        assertThat(savedMle.getServiceRecords().size()).isEqualTo(1);
        assertThat(savedMle.getServiceRecords().get(0).getDescription()).isEqualTo("Description");
        assertThat(savedMle.getServiceRecords().get(0).getDurationYears()).isEqualTo(25);
        assertThat(savedMle.getServiceRecords().get(0).getDurationMonths()).isEqualTo(10);
        assertThat(savedMle.getServiceRecords().get(0).getDurationDays()).isEqualTo("20/31");


    }
}
