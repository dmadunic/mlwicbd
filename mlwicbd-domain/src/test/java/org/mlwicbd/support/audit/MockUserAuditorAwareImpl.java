package org.mlwicbd.support.audit;

import org.mlwicbd.model.User;
import org.springframework.data.domain.AuditorAware;

/**
 * Created by dmadunic on 19/04/17.
 */
public class MockUserAuditorAwareImpl implements AuditorAware<User> {

    private User auditUser;

    public void setAuditUser(User user) {
        this.auditUser = user;
    }

    /**
     * Returns the current auditor of the application.
     *
     * @return the current auditor
     */
    @Override
    public User getCurrentAuditor() {
        return auditUser;
    }
}
