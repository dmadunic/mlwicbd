package org.mlwicbd.support.springtestdbunit;

import com.github.springtestdbunit.dataset.DataSetLoader;
import com.github.springtestdbunit.dataset.FlatXmlDataSetLoader;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ReplacementDataSet;
import org.joda.time.LocalDate;
import org.springframework.util.Assert;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by dmadunic on 18/03/17.
 */
public class SysdateReplacementDataSetLoader implements DataSetLoader {

    private final DataSetLoader dataSetLoader;

    /**
     * Create a new ReplacementDataSetLoader using a FlatXmlDataSetLoader to load the source data.
     */
    public SysdateReplacementDataSetLoader() {
        this(new FlatXmlDataSetLoader());
    }

    /**
     *
     * @param dataSetLoader the source data set loader
     */
    public SysdateReplacementDataSetLoader(DataSetLoader dataSetLoader) {
        Assert.notNull(dataSetLoader, "Delegate must not be null");
        this.dataSetLoader = dataSetLoader;
    }

    public IDataSet loadDataSet(Class<?> testClass, String location) throws Exception {
        IDataSet dataSet = this.dataSetLoader.loadDataSet(testClass, location);
        LocalDate today = new LocalDate();

        Map<String, Object> objectReplacements = new HashMap<String, Object>();
        objectReplacements.put("[NULL]", null);

        objectReplacements.put("[sysdate]", new java.sql.Date(today.toDate().getTime()));
        objectReplacements.put("[sysdate-1d]", new java.sql.Date(today.minusDays(1).toDate().getTime()));
        objectReplacements.put("[sysdate+1d]", new java.sql.Date(today.plusDays(1).toDate().getTime()));

        objectReplacements.put("[sysdate-2d]", new java.sql.Date(today.minusDays(2).toDate().getTime()));
        objectReplacements.put("[sysdate+2d]", new java.sql.Date(today.plusDays(2).toDate().getTime()));

        objectReplacements.put("[sysdate-1w]", new java.sql.Date(today.minusDays(7).toDate().getTime()));
        objectReplacements.put("[sysdate+1w]", new java.sql.Date(today.plusDays(7).toDate().getTime()));
        objectReplacements.put("[sysdate-2w]", new java.sql.Date(today.minusDays(14).toDate().getTime()));
        objectReplacements.put("[sysdate+2w]", new java.sql.Date(today.plusDays(14).toDate().getTime()));

        objectReplacements.put("[sysdate-1m]", new java.sql.Date(today.minusMonths(1).toDate().getTime()));
        objectReplacements.put("[sysdate+1m]", new java.sql.Date(today.plusMonths(1).toDate().getTime()));
        objectReplacements.put("[sysdate-3m]", new java.sql.Date(today.minusMonths(3).toDate().getTime()));
        objectReplacements.put("[sysdate+3m]", new java.sql.Date(today.plusMonths(3).toDate().getTime()));

        objectReplacements.put("[sysdate-1y]", new java.sql.Date(today.minusYears(1).toDate().getTime()));
        objectReplacements.put("[sysdate+1y]", new java.sql.Date(today.plusYears(1).toDate().getTime()));

        ReplacementDataSet replacementDataSet = new ReplacementDataSet(dataSet, objectReplacements, null);
        return replacementDataSet;
    }
}
