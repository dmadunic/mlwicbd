/**
 * Created by dmadunic on 29.8.2015.
 */
(function() {
  'use strict';

  angular
    .module('mlwicbd')
    .controller('AppInitController', AppInitController);

  /** @ngInject */
  function AppInitController($rootScope, $state, $log, AppConfig, Security) {
    var vm = this;

    init();

    function init() {
      if (Security.hasCredentials()) {
        //1. attempt to autologin user ...
        $log.debug("[app-init] FOUND user credentials --> loading user data")
        var autoLoginPromise = Security.getAuthenticatedUser();

        //2. chain loadConfigData
        autoLoginPromise.then(loadAppConfigData(), function(error) {
          $log.debug("[app-init] FAILED loading user data --> cleared credentials from localStorage")
          Security.clear();
        });
      } else {
        loadAppConfigData();
      }
    }

    function loadAppConfigData() {
      $log.debug("[app-init] --> START Loading appConfigData")
      var promise = AppConfig.fetchAppConfigData();

      promise.then(function (response) {
        if (angular.isDefined($rootScope.toState) && $rootScope.toState.name !== 'app-init') {
          $state.go($rootScope.toState, $rootScope.toParams);
        } else {
          $state.go('app.home');
        }
      }).catch(function (error) {
        //console.log(error);
        $log.error(error)
        $state.go('app-init-failed');
      });
    }

    function goDashboard() {
      $state.go('app.home', {}, {reload: true, inherit: false, notify: true });
    }

  }

})();
