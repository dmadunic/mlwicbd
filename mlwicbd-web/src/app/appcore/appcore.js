/**
* Created by dmadunic on 29.8.2015.
*/
(function () {
  'use strict';

  angular
    .module('appcore', [
      'ui.router', 'ui-notification'
    ]);

  angular
    .module('appcore')
    .config(config);

  config.$inject = ['$stateProvider'];

  function config($stateProvider) {
    $stateProvider.state('app-init', {
      url: '/init',
      templateUrl: 'app/appcore/app-init.html',
      controller: 'AppInitController as appInit',
      data: {
        authenticated: false
      },
      resolve: {
        apiConfig: loadApiConfig
      }
    });
    $stateProvider.state('app-init-failed', {
      url: '/init-failed',
      templateUrl: 'app/appcore/app-init-failed.html',
      data: {
        authenticated: false
      }
    });
  }

  function loadApiConfig($log, AppConfig) {
    $log.debug('[appcore] -> START Loading apiConfig data');
    var apiConfig = AppConfig.fetchApiConfig();
    return apiConfig;
  }

})();

