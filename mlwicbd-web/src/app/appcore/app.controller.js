/**
 * Created by dmadunic on 29.8.2015.
 */
(function () {
  'use strict';

  angular
    .module('mlwicbd')
    .controller('AppController', AppController);

  AppController.$inject = ['$rootScope', '$scope', '$state', '$log', '$translate', 'Security', 'Notification', 'AppConfig', 'APP_DATA', 'APP_EVENTS'];

  function AppController($rootScope, $scope, $state, $log, $translate, Security, Notification, AppConfig, APP_DATA, APP_EVENTS) {
    var vm = this;
    vm.title = APP_DATA.NAME;
    vm.version = APP_DATA.VERSION;

    vm.authenticated = false;
    vm.disableControls = false;
    vm.userDisplayName = '';
    vm.backendVersion = '';
    vm.user = undefined;

    vm.signOut = signOut;
    vm.goSignIn = goSignIn;
    vm.goDashboard = goDashboard;
    vm.changeLang = changeLang;

    init();

    //--- register global app event listeners ---------------------------------

    $scope.$on(APP_EVENTS.USER_SIGN_IN, function (event, user) {
      vm.authenticated = true;
      vm.user = user;
      vm.userDisplayName = user.firstName + ' ' + user.lastName;
    });

    $scope.$on(APP_EVENTS.USER_SIGN_OUT, function (event, user) {
      vm.authenticated = false;
      vm.user = undefined;
      $rootScope.toState = undefined;
      vm.disableControls = false;
    });

    //--- implementation ------------------------------------------------------

    function init() {
      if(Security.isAuthenticated()) {
        vm.authenticated = true;
        vm.user = Security.user();
        vm.userDisplayName = vm.user.firstName + ' ' + vm.user.lastName;
      }

      //vm.backendVersion
      var version = '';
      var timestamp = '';

      var appConfig = AppConfig.getAppConfigData();
      var arrayLength = appConfig.length;

      if (arrayLength) {
        for (var i = 0; i < arrayLength; i++) {
          var el = appConfig[i];
          if (el.key) {
            if (el.key === 'buildVersion') {
              version = el.value;
            }
            if (el.key === 'buildTime') {
              timestamp = el.value;
            }
          }
        }
      }
      vm.backendVersion = version + ' :: ' + timestamp;
    }

    function signOut() {
      vm.disableControls = true;
      Security.signOut();
      $state.go(APP_DATA.SIGN_OUT_STATE);
      Notification.success('Successfully signed out - security context cleared');
    }

    function goDashboard() {
      $state.go('app.home', {}, {reload: true, inherit: false, notify: true });
    }

    function goSignIn() {
      $state.go('signIn', {}, {reload: true, inherit: false, notify: true });
    }

    function changeLang(langCode) {
      $translate.use(langCode);
    }

  }
})();
