(function() {
  'use strict';

  angular
    .module('mlwicbd')
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('app', {
        url: '/app',
        abstract: true,
        templateUrl: 'app/layout/layout.html',
        controller: 'AppController as app',
        data: {
          authenticated: false
        },
        resolve: {
          apiConfig: function(AppConfig){
            return AppConfig.fetchApiConfig();
          }
        }
      });

    $urlRouterProvider.otherwise(function ($injector, $location) {
      var $state = $injector.get('$state');
      $state.go('app.home');
    });
  }

})();
