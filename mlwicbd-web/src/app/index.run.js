(function() {
  'use strict';

  angular
    .module('mlwicbd')
    .run(runBlock);

  /** @ngInject */
  function runBlock($rootScope, $state, $http, $log, Security, AppConfig) {
    $log.debug('[index.run] ---> AppInit START');

    $rootScope.$on('$stateChangeStart', function (event, toState, toParams) {
      if (toState.name !== 'signIn' && toState.name !== 'app-init' && toState.name !== 'app-init-failed') {
        $rootScope.toState = toState;
        $rootScope.toParams = toParams;
      }

      if (toState.name !== 'app-init' && toState.name !== 'app-init-failed') {
        if (!AppConfig.isInitialized()) {
          event.preventDefault();
          $state.go('app-init');
        } else if (toState.data.authenticated && !Security.isAuthenticated()) {
          event.preventDefault();
          $state.go('signIn');
        }
      }
    });

    $rootScope.$on('$stateChangeError', function (event, toState, toParams, fromState, fromParams, error) {
      if (error === 'API_ACCESS_DENIED') {
        $log.debug('[index.run] API_ACCESS_DENIED ---> redirecting to signIn state');
        $state.go('signIn');
      } else if (error === 'APP_CONFIG_FAILED') {
        $log.debug('[index.run] APP_CONFIG_FAILED ---> redirecting to app.init-failed state');
        $state.go('app-init-failed');
      }
    });
    $log.debug('[index.run] ---> AppInit END');
  }

})();
