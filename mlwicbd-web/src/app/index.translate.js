/**
 * angular-translate configuration
 *
 * All localization messages should go to this file.
 *
 * For more see: https://angular-translate.gihub.io
 *
 * Author: dmadunic 2016
 */
(function() {
  'use strict';

  angular
    .module('mlwicbd')
    .config(translateConfig);

  function translateConfig($translateProvider) {
    $translateProvider.translations('en', {
      'regiment.no.60': 'I. Likaner Regiment No. 60',
      'regiment.no.61': 'II. Ottochaner Regiment No. 61',
      'regiment.no.62': 'III. Oguliner Regiment No. 62',
      'regiment.no.63': 'IV. Szluiner Regiment 63',
      'regiment.no.64': 'V. Kreutzer Regiment No. 64',
      'regiment.no.65': 'VI. St. Georger Regiment No. 65',
      'regiment.no.66': 'VII. Brooder Regiment No. 66',
      'regiment.no.67': 'VIII. Gradiskaner Regiment No. 67',
      'regiment.no.68': 'IX. Peterwardeiner Regiment No. 68',
      'regiment.no.69': 'X. Erste Banal Regiment No. 69',
      'regiment.no.70': 'XI. Zwiter Banal Regiment No. 70'
    });

    $translateProvider.translations('hr', {
      'regiment.no.60': 'I. Lička pukovnija No. 60',
      'regiment.no.61': 'II. Otočka pukovnija No. 61',
      'regiment.no.62': 'III. Ogulinska pukovnija No. 62',
      'regiment.no.63': 'IV. Slunjska pukovnija 63',
      'regiment.no.64': 'V. Križevačka pukovnija No. 64',
      'regiment.no.65': 'VI. Đurđevačka pukovnija No. 65',
      'regiment.no.66': 'VII. Brodska pukovnija No. 66',
      'regiment.no.67': 'VIII. Gradiška pukovnija  No. 67',
      'regiment.no.68': 'IX. Petrovaradinska pukovnija No. 68',
      'regiment.no.69': 'X. Prva banska pukovnija No. 69',
      'regiment.no.70': 'XI. Druga banksa pukovnija No. 70'
    });
    $translateProvider.preferredLanguage('hr');
  }

})();
