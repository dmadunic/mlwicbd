/**
 * Showcase page for Charts.
 *
 * Created by dmadunic on 14.1.2016.
 */
(function() {
  'use strict';

  angular
    .module('demo-pages')
    .controller('ChartsController', ChartsController);

  /** @ngInject */
  function ChartsController($rootScope, $scope, $state, $timeout) {
    var vm = this;

    vm.line = {
    	    labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
    	    series: ['Series A', 'Series B'],
    	    data: [
    	      [65, 59, 80, 81, 56, 55, 40],
    	      [28, 48, 40, 19, 86, 27, 90]
    	    ],
    	    onClick: function (points, evt) {
    	      console.log(points, evt);
    	    }
        };

    vm.bar = {
    	labels: ['2006', '2007', '2008', '2009', '2010', '2011', '2012'],
    	eries: ['Series A', 'Series B'],

      data: [
    		[65, 59, 80, 81, 56, 55, 40],
    		[28, 48, 40, 19, 86, 27, 90]
      ]
    };

        $scope.donut = {
        	labels: ["Download Sales", "In-Store Sales", "Mail-Order Sales"],
        	data: [300, 500, 100]
        };

        $scope.radar = {
        	labels:["Eating", "Drinking", "Sleeping", "Designing", "Coding", "Cycling", "Running"],

        	data:[
        	    [65, 59, 90, 81, 56, 55, 40],
        	    [28, 48, 40, 19, 96, 27, 100]
        	]
        };

        $scope.pie = {
        	labels : ["Download Sales", "In-Store Sales", "Mail-Order Sales"],
        	data : [300, 500, 100]
        };

        $scope.polar = {
        	labels : ["Download Sales", "In-Store Sales", "Mail-Order Sales", "Tele Sales", "Corporate Sales"],
        	data : [300, 500, 100, 40, 120]
        };

        $scope.dynamic = {
        	labels : ["Download Sales", "In-Store Sales", "Mail-Order Sales", "Tele Sales", "Corporate Sales"],
        	data : [300, 500, 100, 40, 120],
        	type : 'PolarArea',

        	toggle : function ()
        	{
        		this.type = this.type === 'PolarArea' ?
        	    'Pie' : 'PolarArea';
    		}
        };

  }

})();
