/**
* Created by dmadunic on 14.1.2016.
*/
(function () {
  'use strict';

  angular
    .module('demo-pages', [
      'ui.router'
    ]);

  angular
    .module('demo-pages')
    .config(config);

  config.$inject = ['$stateProvider'];

  function config($stateProvider) {
    $stateProvider.state('app.demo-pages-charts', {
      parent: 'app',
      url: '/charts',
      templateUrl: 'app/demo-pages/charts.html',
      controller: 'ChartsController',
      data: {
        authenticated: false
      }
    });
    $stateProvider.state('app.demo-pages-tables', {
      parent: 'app',
      url: '/tables',
      templateUrl: 'app/demo-pages/tables.html',
      data: {
        authenticated: false
      }
    });
    $stateProvider.state('app.demo-pages-forms', {
          parent: 'app',
          url: '/forms',
          templateUrl: 'app/demo-pages/forms.html',
          data: {
            authenticated: true
          }
        });
  }

})();

