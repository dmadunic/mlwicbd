/**
 *
 * Created by dmadunic on 28.03.2016.
 */
(function () {
  'use strict';

  angular
    .module('unknown-source', [
      'ui.router'
    ]);

  angular
    .module('unknown-source')
    .config(config);

  config.$inject = ['$stateProvider'];

  function config($stateProvider) {
    $stateProvider.state('app.source-register', {
      parent: 'app',
      url: '/unknown-source/register',
      templateUrl: 'app/unknown-source/register-form.html',
      controller: 'UnknownSourceController',
      data: {
        authenticated: true
      }
    });
    $stateProvider.state('app.unknown-source-success', {
      parent: 'app',
      url: '/unknown-source/sucess',
      templateUrl: 'app/unknown-source/register-success.html',
      data: {
        authenticated: true
      }
    });
  }

})();
