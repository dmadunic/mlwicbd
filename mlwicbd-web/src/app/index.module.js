(function() {
  'use strict';

  angular
    .module('mlwicbd', [
      'ngMessages',
      'ngAnimate',
      'ngSanitize',
      'ngCookies',
      'ui.router',
      'ui.bootstrap',
      'angular-loading-bar',
      'LocalStorageModule',
      'ui-notification',
      'pascalprecht.translate',
      'appconfig',
      'security',
      'appcore',
      'dataservices',
      'appSignIn',
      'dashboard',
      'unknown-source',
      'demo-pages',
      'userProfile',
      'appRegistration',
      'musterList',
      'person'
    ]);

})();
