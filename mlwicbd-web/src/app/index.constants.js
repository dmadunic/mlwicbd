/* global moment:false */
(function() {
  'use strict';

  angular
    .module('mlwicbd')
    .constant('APP_DATA', {
      NAME: 'Military Life and Warrior Images In Croatian Borderlands',
      VERSION: '0.0.1',
      LS_NAME: 'ml',
      SIGN_OUT_STATE: 'app.home'
      }
    );

  angular
    .module('mlwicbd')
    .constant('APP_EVENTS', {
        USER_SIGN_OUT: 'USER_SIGN_OUT',
        USER_SIGN_IN: 'USER_SIGN_IN'
      }
    );

  angular
    .module('mlwicbd')
    .constant('MLE', {
        PAGE_SIZE: '25'
      }
    );
})();
