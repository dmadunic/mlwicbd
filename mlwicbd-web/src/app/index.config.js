(function() {
  'use strict';

  angular
    .module('mlwicbd')
    .config(config);

  /** @ngInject */
  function config($httpProvider, $provide, $logProvider, localStorageServiceProvider, APP_DATA, APP_EVENTS) {
    // Enable log
    $logProvider.debugEnabled(true);

    localStorageServiceProvider.setPrefix(APP_DATA.LS_NAME);

    // 1. initialize HttpBasicAuthInterceptor
    $provide.factory('HttpBasicAuthInterceptor', HttpBasicAuthInterceptor);
    HttpBasicAuthInterceptor.$inject = ['$injector'];

    function HttpBasicAuthInterceptor($injector) {
      return {
        'request': function(config) {
          if (config.url.indexOf('/apiVersion/') !== -1) {
            var Security = $injector.get('Security');
            if (Security.hasCredentials()) {
              var basicAuth = 'Basic ' + Security.getEncodedCredentials();
              /*jshint -W069 */
              config.headers['Authorization'] = basicAuth;
              /*jshint +W069 */
            }
          }
          return config;
        }
      };
    }
    $httpProvider.interceptors.push('HttpBasicAuthInterceptor');

    // 2. initialize UriPreAppenderInterceptor
    $provide.factory('UriPreAppenderInterceptor', UriPreAppenderInterceptor);
    UriPreAppenderInterceptor.$inject = ['$q', '$injector'];

    function UriPreAppenderInterceptor($q, $injector) {
      return {
        'request': function(config) {
          var AppConfig = $injector.get('AppConfig');

          if (config.url.indexOf('/apiVersion/') !== -1) {
            config.url = AppConfig.getApiBaseUrl().concat(config.url);
            config.url = config.url.replace('/apiVersion/', '/' + AppConfig.getApiVersion() + '/');
          }
          return config;
        }
      };
    }
    $httpProvider.interceptors.push('UriPreAppenderInterceptor');


    // 3. Initialize TimeoutHttpInterceptor
    $provide.factory('TimeoutHttpInterceptor', TimeoutHttpInterceptor);
    TimeoutHttpInterceptor.$inject = [];

    function TimeoutHttpInterceptor() {
      return {
        'request': function(config) {
          config.timeout = 60000;
          return config;
        }
      };
    }
    $httpProvider.interceptors.push('TimeoutHttpInterceptor');


    // 4. Initialize HttpErrorInterceptor
    $provide.factory('HttpErrorInterceptor', HttpErrorInterceptor);

    HttpErrorInterceptor.$inject = ['$q', '$injector'];

    function HttpErrorInterceptor($q, $injector) {
          return {
            'responseError': function (rejection) {
              var AppConfig = $injector.get('AppConfig');
              var ignorable401Urls = [
                AppConfig.getApiUrl().concat('users/me') // TODO: check if this is correct!
              ];
              //var notify = $injector.get('notify');
              var Notification = $injector.get('Notification');

              if (rejection.status === 401) {
                if (ignorable401Urls.indexOf(rejection.config.url) !== -1) {
                  return $q.reject(rejection);
                }
                if (rejection.config.ignoreSecurity) {
                  return $q.reject(rejection);
                }
                var $state = $injector.get('$state'),
                  Security = $injector.get('Security');
                  Security.clear();
                  //$rootScope.$broadcast(APP_EVENTS.USER_SIGN_OUT);
                  $state.go('signIn', {
                    status: 401
                  });
              } else if (rejection.status === 403) {
                // TODO handle unauthorized actions..
                //notify('You are NOT authorized to perform requested operation!');
                Notification.error({message: 'You are NOT authorized to perform requested operation!', title: 'Security Warning'});
              } else if (rejection.status === 404) {
                Notification.error('Server could not be reached! Please contact support for furhter help.');
              } else if (rejection.status === 500) {
                Notification.error('Your request could not be processed: internal server error occured! Please contact support for further help.');
              } else if (rejection.status === -1) {
                //notify('Request Timeout! Please try again or contact support if problem persists for furhter help.');
                Notification.error('Request Timedout! Please try again or contact support if problem persists for furhter help.');
              }
              return $q.reject(rejection);
            }
          };
    }
    $httpProvider.interceptors.push('HttpErrorInterceptor');

  }

})();
