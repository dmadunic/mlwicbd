/**
 * User Profile module definition.
 *
 */
(function () {
  'use strict';

  angular
    .module('userProfile', [
      'ui.router',
      'ui-notification',
      'ui.bootstrap'
    ]);

  angular
    .module('userProfile')
    .config(config);

  config.$inject = ['$stateProvider'];

  function config($stateProvider) {
    $stateProvider.state('app.profile-view', {
      parent: 'app',
      url: '/profile',
      templateUrl: 'app/profile/view.html',
      controller: 'UserProfileController as profileCtrl',
      data: {
        authenticated: true
      },
      resolve: {
        countryData: emptyCountries
      }

    });

    $stateProvider.state('app.profile-edit', {
      parent: 'app',
      url: '/profile/form',
      templateUrl: 'app/profile/form.html',
      controller: 'UserProfileController as profileCtrl',
      data: {
        authenticated: true
      },
      resolve: {
        countryData: fetchCountries
      }
    });
  }


  fetchCountries.$inject = ['$rootScope', 'Countries'];
  function fetchCountries($rootScope, Countries) {
    $rootScope.restDataLoadPromise = Countries.fetchAll();
    return $rootScope.restDataLoadPromise.then(function (response) {
      return response.data;
    });
  }

  function emptyCountries () {
    return [];
  }

})();
