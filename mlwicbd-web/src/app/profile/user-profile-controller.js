(function () {
  'use strict';

  angular
    .module('userProfile')
    .controller('UserProfileController', UserProfileController);

  UserProfileController.$inject = ['$rootScope', '$state', '$scope', '$log', 'localStorageService', 'Security', 'Users', 'Notification', 'countryData'];


  function UserProfileController($rootScope, $state, $scope, $log, localStorageService, Security, Users, Notification, countryData) {
    var vm = this;
    vm.disableControls = false;
    vm.showAlert = false;
    vm.alertMessage = "";
    vm.countries = countryData.content;
    vm.userData = populateUserData();

    vm.validationErrors = {};

    // functions ...
    vm.updateProfile = updateProfile;
    vm.closeAlert = closeAlert;

    function closeAlert() {
      vm.showAlert = false;
      vm.alertMessage = "";
    }

    function updateProfile(){
      $log.debug("---> SAVE user Data invoked");
      vm.disableControls = true;
      var promise = Users.update(vm.userData);

      promise.then(handleSuccessfulPromise)
        .catch(handleFailurePromise)
        .finally(function () {
          vm.disableControls = false;
        });
    }

    //--- private methods -----------------------------------------------------

    function populateUserData() {
      var user = Security.user();

      var data = {
        id: user.id,
        firstName: user.firstName,
        lastName: user.lastName,
        password: undefined,
        password2: undefined,
        address: user.address,
        postalCode: user.postalCode,
        countryCode: user.countryCode,
        place: user.place,
        mobile: user.mobile,
        phone: user.phone,
        email: user.email
      };
      return data;
    }

    function handleSuccessfulPromise(response) {
      Notification.success("User profile changed successfully.");
      var oldUser = Security.user();

      if (angular.isDefined(vm.userData.password) && vm.userData.password !== null) {
        Notification.info("Your credentials have changed please sign in again");
        Security.signOut();
        $state.go('signIn');
      } else {
        $state.go('app.profile-view');
      }
      $scope.userForm.$setPristine();
    }

    function handleFailurePromise(reason) {

      var message = 'Failed to save changes for your user profile!.'
      if (reason.status != 422) {
        Notification.error(message);
      }
      console.log(message + ' Reason: ' + JSON.stringify(reason));
      vm.loading = null;

      if (reason.data) {
        addErrorsToAlert(reason);
      }
    }

    function addErrorsToAlert(reason) {
      vm.validationErrors = {};

      vm.showAlert = true;
      vm.alertMessage = reason.data.message + "\n";

      var n = reason.data.errors.length;
      for (var i=0 ; i<n; i++) {
        var error = reason.data.errors[i];

        if (!(error["field"] in vm.validationErrors)){
          var key = error["field"];
          vm.validationErrors[key] = [];
        }
        vm.validationErrors[key].push(error.code);
      }
    }

    function hasLocalStorageCredentials() {
      var credentials = localStorageService.get('credentials');
      return angular.isDefined(credentials) && credentials !== null;
    }

  }

})();
