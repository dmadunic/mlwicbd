(function () {
  'use strict';

  angular
    .module('dashboard', []);

  angular
    .module('dashboard')
    .config(config);

  config.$inject = ['$stateProvider'];

  function config($stateProvider) {
    $stateProvider.state('app.home', {
        url: '/home',
        templateUrl: 'app/dashboard/dashboard.html',
        controller: 'DashboardController as main',
        data: {
          authenticated: false
        }, resolve: {
          apiConfigData : resolveApiConfigData,
          appConfigData : resolveAppConfigData
        }
    });
  }

  //--- resolve functions -----------------------------------------------------

  resolveApiConfigData.$inject = ['AppConfig'];

  function resolveApiConfigData(AppConfig) {
    return AppConfig.fetchApiConfig();
  }

  resolveAppConfigData.$inject = ['AppConfig'];

  function resolveAppConfigData(AppConfig) {
    return AppConfig.fetchAppConfigData();
  }

})();
