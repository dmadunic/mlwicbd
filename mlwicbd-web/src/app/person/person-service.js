/**
 * Person(s) service.
 * 
 * Created by dmadunic on 1.10.2016.
 */
(function(){
  'use strict';

  angular
    .module('person')
    .factory('Persons', Persons);

  Persons.$inject = ['$http', '$stateParams', 'HttpUtils'];

  function Persons($http, $stateParams, HttpUtils) {

    var _mlSearchParams = {
      firstName: undefined,
      lastName: undefined
    };

    var persons = {
      search: search,
      getPerson: getPerson,
      prepareQueryParams: prepareQueryParams,
      lastSearchParams: lastSearchParams
    };
    return persons;

    //--- implementation ------------------------------------------------------

    function lastSearchParams() {
      return _mlSearchParams;
    }

    function search(searchParams) {
      _mlSearchParams = searchParams;
      var sp = prepareSearchParams(searchParams);
      var queryParams = HttpUtils.toQueryParams(sp);
      return $http({
        method: 'GET',
        url: '/apiVersion/person' + queryParams
      });
    }

    function getPerson(id) {
      return $http({
        method: 'GET',
        url: '/apiVersion/person/' + id
      });
    }

    function prepareQueryParams(stateParams) {
      if ($stateParams.from) {
        stateParams.from = $stateParams.from;
      } else {
        stateParams.from = undefined;
      }
      if ($stateParams.fn) {
        stateParams.fn = $stateParams.fn;
      } else {
        stateParams.fn = undefined;
      }
      if ($stateParams.ln) {
        stateParams.ln = $stateParams.ln;
      } else {
        stateParams.ln = undefined;
      }
      return stateParams;
    }
  
    //--- util methods --------------------------------------------------------

    function prepareSearchParams(searchParams) {
      var sp = [];
      if (angular.isDefined(searchParams.firstName)) {
        sp.push("firstName=" + searchParams.firstName);
      }
      if (angular.isDefined(searchParams.lastName)) {
        sp.push("lastName=" + searchParams.lastName);
      }
      sp.push("page=" + searchParams.page);
      sp.push("size=" + searchParams.size);
      return sp;
    }

  }
})();    