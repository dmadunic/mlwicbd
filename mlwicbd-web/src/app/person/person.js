/**
 * Domagoj Madunić 1/10/2016.
 *
 */
(function () {
  'use strict';

  angular
    .module('person', [
      'ui.router',
      'ui-notification',
      'ui.bootstrap',
      'ngTable'
    ]);

  angular
    .module('person')
    .config(config);

  config.$inject = ['$stateProvider'];

  function config($stateProvider) {
    $stateProvider.state('person-home', {
      parent: 'app',
      url: '/persons?from&fn&ln',
      templateUrl: 'app/person/home.html',
      controller: 'PersonController as prsCtrl',
      resolve: {
        //regiments: fetchRegiments
      }
    });
    $stateProvider.state('person-details', {
      parent: 'app',
      url: '/persons/:id?from&fn&ln',
      templateUrl: 'app/person/details.html',
      controller: 'PersonDetailsController as prsDetailsCtrl',
      resolve: {
        person: resolvePerson
      }
    });
  }

  //--- helper functions ------------------------------------------------------

  resolvePerson.$inject = ['$rootScope', '$stateParams', 'Persons'];

  function resolvePerson($rootScope, $stateParams, Persons) {
    var id = $stateParams.id;
    
    if (!id || 0 === id) {
        //TODO: this is illegal state ... handle it somehow
        return null;
    }
    $rootScope.restDataLoadPromise = Persons.getPerson(id);
    return $rootScope.restDataLoadPromise.then(function (response) {
      return response.data;
    });
  }

  //---
 


})();    