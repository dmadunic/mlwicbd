/**
 *
 * Author: Domagoj Madunić 2016.
 */
(function () {
  'use strict';

  angular.module('person')
    .controller('PersonController', PersonController);

  PersonController.$inject = [ '$rootScope', '$state', '$stateParams', '$uibModal', '$scope', '$log', 'Notification', 'NgTableParams', 'CommonCodes', 'MusterListEntries', 'Persons'];

  function PersonController( $rootScope, $state, $stateParams, $uibModal, $scope, $log, Notification, NgTableParams, CommonCodes, MusterListEntries, Persons) {
    var vm = this;
    vm.dataLoded = false;
    vm.searchParams = {
        firstName: undefined,
        lastName: undefined
    };
    vm.showSelectButton = false;
    vm.showBackToMle = false;
    vm.disableControls = false;

    vm.searchPersons = searchPersons;
    vm.showDetails = showDetails;
    vm.backToMusterListEntryForm = backToMusterListEntryForm;
    vm.selectPerson = selectPerson;

    init();

    function init() {
      var from = $stateParams.from;
      if (from) {
        if (from === 'mleForm' && !(MusterListEntries.getForm() == null)) {
            vm.showBackToMle = true;
            vm.showSelectButton = true;
        }
        //TODO: add other possible origins ...
      }
      vm.searchParams.firstName = $stateParams.fn;
      vm.searchParams.lastName = $stateParams.ln;
    }

    function backToMusterListEntryForm() {
        var mleForm = MusterListEntries.getForm();
        $state.go('musterList-entry', {id: mleForm.musterListId, mlid: mleForm.id, restore: '1' });
    }

    function showDetails(personId) {
      var stateParams = {
        id: personId
      };
      stateParams = prepareQueryParams(stateParams);
      $state.go('person-details', stateParams);
    }

    function searchPersons() {
      var stateParams = {};
      stateParams = prepareQueryParams(stateParams);
      $state.go('person-home', stateParams);
      //vm.tableParams.reload();
    }

    function handleFailurePromise(reason) {
      $log.error("Failed to perform person search!");
      $log.error(reason);
    }

    function selectPerson(person) {
      //Notification.info("This function is not yet implemented!");
      var mleForm = MusterListEntries.getForm();
      mleForm.person = person;
      $state.go('musterList-entry', {id: mleForm.musterListId, mlid: mleForm.id, restore: '1' });
    }

    //---- ngTable configuration ----------------------------------------------

    vm.tableParams = new NgTableParams({ count : 25 }, {
      getData: function(params) {
        
        var results = [];
        if (!searchParamsDefined(vm.searchParams)) {
          return results;
        }
        var searchParams = mapFormSearchParams();
        searchParams.page = (params.page() - 1);
        searchParams.size = params.count();
        
        vm.disableControls = true;
        $rootScope.restDataLoadPromise = Persons.search(searchParams);

        results = $rootScope.restDataLoadPromise.then(function(response) {
            params.total(response.data.totalElements);
            vm.dataLoded = true;
            return response.data.content;
          })
          .catch(handleFailurePromise)
          .finally(function () {
            vm.disableControls = false;
          });
        return results;
      }
    });

    
    // --- helper functions ---------------------------------------------------

    function mapFormSearchParams() {
      var sp = {};
      sp.firstName = vm.searchParams.firstName;
      sp.lastName =  vm.searchParams.lastName;
      return sp;
    }

    function searchParamsDefined(searchParams) {
      if (angular.isDefined(searchParams.firstName)) {
        return true;
      }
      if (angular.isDefined(searchParams.lastName)) {
        return true;
      }
      return false;
    }

    function prepareQueryParams(stateParams) {
      if ($stateParams.from) {
        stateParams.from = $stateParams.from;
      }
      if (vm.searchParams.firstName) {
        stateParams.fn = vm.searchParams.firstName;
      } else {
        stateParams.fn = undefined;
      }
      if (vm.searchParams.lastName) {
        stateParams.ln = vm.searchParams.lastName;
      } else {
        stateParams.ln = undefined;
      } 
      return stateParams;
    }

  }

})();
