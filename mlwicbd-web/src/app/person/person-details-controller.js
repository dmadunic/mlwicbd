/**
 * 
 * Author: Domagoj Madunić 2016.
 */

(function () {
  'use strict';

  angular.module('person')
    .controller('PersonDetailsController', PersonDetailsController);

  PersonDetailsController.$inject = [ 
    '$rootScope', 
    '$state', 
    '$stateParams', 
    '$scope', 
    '$log', 
    '$translate', 
    '$uibModal', 
    'Notification', 
    'NgTableParams', 
    'MusterListEntries', 
    'Persons', 
    'person' 
  ];

  function PersonDetailsController( 
    $rootScope, 
    $state, 
    $stateParams, 
    $scope, 
    $log, 
    $translate, 
    $uibModal, 
    Notification, 
    NgTableParams, 
    MusterListEntries, 
    Persons, 
    person 
  ) {
    var vm = this;
    vm.person = person;
    vm.isEditDisabled = false;
    
    vm.editPerson = editPerson;
    vm.backToSearchResults = backToSearchResults;

    init();

    function init() {
      //TODO: set this based on roles ...
      vm.isEditDisabled = false;
    }

    function backToSearchResults() {
      var stateParams = {};
      stateParams = Persons.prepareQueryParams(stateParams);
      $state.go('person-home', stateParams);
     
    }

    //--- ng-table setup and helper functions ---------------------------------

    vm.tableParams = new NgTableParams({ count : 25}, {
            getData: getTableData
        }
    );

    function getTableData(params) {
        var results = [];
        var searchParams = prepareSearchParams();
        //if (searchParams.length == 0) {
        //  return results;
        //}

        searchParams.push("page=" + (params.page() - 1));
        searchParams.push("size=" + params.count());
        searchParams.push("sort=listNumber");

        $rootScope.restDataLoadPromise = MusterListEntries.searchEntries(searchParams);

        results = $rootScope.restDataLoadPromise.then(function(response) {
            params.total(response.data.totalElements);
            vm.dataLoded = true;
            return response.data.content;
          })
          .catch(handleFailurePromise)
          .finally(function () {
            vm.disableControls = false;
          });
        return results;
    }

    function prepareSearchParams() {
        var searchParams = [];
        searchParams.push("personId=" + vm.person.id);
        return searchParams;
    }

    function handleFailurePromise(reason) {
      $log.error("Failed to fetch muster list entries!");
      $log.error(reason);
    }

    //--- MODAL: Edit Person form ------------------------------------------

    function editPerson() {
      var personId = vm.person.id;
      vm.isEditDisabled = true;

      var mlEditModal = $uibModal.open({
          animation: true,
          templateUrl: 'app/muster-list/ml-edit/muster-list-edit-modal.html',
          controller: 'MusterListEditModalController as mlEditModalCtl',
          size: 'lg',
          resolve:{
            person: function() {
              return vm.person;
            }
          }
        }
      );
      mlEditModal.result.then (function (personDto) {
        vm.person = personDto;
      });
      mlEditModal.result.finally(function() {
          vm.isEditDisabled = false;
        }
      );
    }

    //--- MODAL helper functions ----------------------------------------------


  }
})();
