/**
 *
 */
(function () {
  'use strict';

  angular
    .module('appRegistration', [
      'ngMessages',
      'ngSanitize',
      'ui.router',
      'ui-notification',
      'ui.bootstrap'
    ]);

  angular
    .module('appRegistration')
    .config(config);

  config.$inject = ['$stateProvider'];

  function config($stateProvider) {
    $stateProvider.state('registration-form', {
      parent: 'app',
      url: '/registration',
      templateUrl: 'app/registration/form.html',
      controller: 'RegistrationController as regCtrl',
      resolve: {
        countryData: fetchCountries
      }
    });
    $stateProvider.state('registration-success', {
      parent: 'app',
      url: '/registration-success',
      templateUrl: 'app/registration/success.html',
      controller: 'RegistrationSuccessController as regSuccCtrl',
      params: { newUser: null}
    });
  }

  fetchCountries.$inject = ['$rootScope', 'Countries'];
  function fetchCountries($rootScope, Countries) {
    $rootScope.restDataLoadPromise = Countries.fetchAll();
    return $rootScope.restDataLoadPromise.then(function (response) {
      return response.data;
    });
  }

})();
