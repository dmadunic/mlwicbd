/**
 *
 *
 */
(function () {
  'use strict';

  angular.module('appRegistration')
  	.controller('RegistrationController', RegistrationController);

    RegistrationController.$inject = [ '$rootScope', '$state', '$uibModal', '$scope', '$log', 'Security', 'Users', 'Notification', 'APP_EVENTS', 'countryData' ];

    function RegistrationController( $rootScope, $state, $uibModal, $scope, $log, Security, Users, Notification, APP_EVENTS, countryData) {
      var vm = this;
 		  vm.disableControls = false;
	    vm.showAlert = false;
	    vm.alertMessage = "";
	    vm.countries = countryData.content;
	    vm.userData = emptyUserData();

	    vm.validationErrors = {};

		    // functions ...
	    vm.register = register;
	    vm.closeAlert = closeAlert;
      vm.openTermsOfServiceModal = openTermsOfServiceModal;

	    function closeAlert() {
	      vm.showAlert = false;
	      vm.alertMessage = "";
	    }

	    function register(){
	      $log.debug("---> REGISTER new user invoked");
	      vm.disableControls = true;
	      var promise = Users.create(vm.userData);

	      promise.then(handleSuccessfulPromise)
	        .catch(handleFailurePromise)
	        .finally(function () {
	          vm.disableControls = false;
	        });
	    }

	    //--- private methods -----------------------------------------------------

      function emptyUserData() {
        var data = {
          firstName: undefined,
          lastName: undefined,
          username: undefined,
          address: undefined,
          postalCode: undefined,
          countryCode: undefined,
          place: undefined,
          mobile: undefined,
          phone: undefined,
          email: undefined
        };
        return data;
      }

      function openTermsOfServiceModal() {

        var modalInstance = $uibModal.open({
          animation: $scope.animationsEnabled,
          templateUrl: 'app/registration/termsOfService.html',
          controller: 'TermsOfServiceCtrl',
          size: 'lg'
        });
      }

      function handleSuccessfulPromise(response) {
	      Notification.success("Registration successfully submitted.");
        $scope.userForm.$setPristine();

	      $state.go('registration-success', {newUser: response.data});
	    }

		  function handleFailurePromise(reason) {

	      var message = 'Failed to save changes for your user profile!.';
	      if (reason.status !== 422) {
	        Notification.error(message);
	      }
	      console.log(message + ' Reason: ' + JSON.stringify(reason));
	      vm.loading = null;

	      if (reason.data) {
	        addErrorsToAlert(reason);
	      }
	    }

		  function addErrorsToAlert(reason) {
	      vm.validationErrors = {};

	      vm.showAlert = true;
	      vm.alertMessage = reason.data.message + "\n";

	      var n = reason.data.errors.length;
	      for (var i=0 ; i<n; i++) {
	        var error = reason.data.errors[i];
					var key = error["field"];
	        
					if (!(error["field"] in vm.validationErrors)){
	          vm.validationErrors[key] = [];
	        }
	        vm.validationErrors[key].push(error.code);
	      }
	    }

    }

    //--- modal dialog controller --------------------------------

    angular.module('appRegistration').controller('TermsOfServiceCtrl', TermsOfServiceCtrl);

    TermsOfServiceCtrl.$inject = [ '$scope', '$uibModalInstance' ];

    function TermsOfServiceCtrl($scope, $uibModalInstance) {
      $scope.closeTermsOfServiceModal = closeTermsOfServiceModal;

      function closeTermsOfServiceModal() {
        $uibModalInstance.dismiss();
      };
    }

})();
