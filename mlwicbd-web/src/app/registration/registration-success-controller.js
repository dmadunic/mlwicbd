/**
 *
 *
 */
(function () {
  'use strict';

  angular.module('appRegistration')
    .controller('RegistrationSuccessController', RegistrationSuccessController);

  RegistrationSuccessController.$inject = ['$stateParams'];

  function RegistrationSuccessController($stateParams) {
    var vm = this;
    vm.user = $stateParams.newUser;

  }

})();
