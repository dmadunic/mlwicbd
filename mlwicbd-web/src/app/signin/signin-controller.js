/**
 * Created by dmadunic on 1.09.2015.
 *
 */
(function () {
  'use strict';

  angular
    .module('appSignIn')
    .controller('SignInController', SignInController);

  SignInController.$inject = ['$rootScope', '$state', '$q', '$timeout', 'status', 'Security', 'Notification'];

  function SignInController($rootScope, $state, $q, $timeout, status, Security, Notification) {
    var vm = this;
    vm.rememberMe = true;
    vm.disableControls = false;
    vm.invalidCredentials = false;
    vm.showAlert = status === "401";
    vm.noUsername = false;

    vm.dismissAlert = dismissAlert;
    vm.confirm = confirm;

    function dismissAlert() {
      vm.showAlert = false;
      vm.noUsername = false;
    }

    function confirm(username, password, rememberMe) {
      vm.invalidCredentials = false;
      vm.disableControls = true;
      Security.authenticate(username, password, rememberMe)
        .then(handleSuccessfulAuthentication)
        .catch(handleFailure)
        .finally(function () {
          vm.disableControls = false;
        });
    }

    /**
     * User is successfully authorized.
     *
     * @returns {*}
     */
    function handleSuccessfulAuthentication() {
      vm.invalidCredentials = false;
      handleSuccessfulSignIn();
      //var appConfigDataPromise = AppConfig.fetchAppConfigData();
      // add other initial configuration promisses here ...
      //return $q.all(appConfigDataPromise);
    }

    function handleSuccessfulSignIn() {
      Notification.success("Welcome: Successfully signed in");
      if (angular.isDefined($rootScope.toState) && $rootScope.toState.name !== "registration-form") {
        // go to original target state...
        $state.go($rootScope.toState);
      } else {
        // go to default state ...
        $state.go('app.home');
      }
    }

    function handleFailure(reason) {
      if (reason.status === 401) {
        vm.invalidCredentials = true;
        Security.clear();
      } else if (reason.status === -1) {
        // server could not be reached what now???

      }
      console.log('Sign In failed! Reason: ' + reason);
    }

  }
})();
