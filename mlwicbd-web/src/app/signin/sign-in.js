/**
 *
 */
(function () {
  'use strict';

  angular
    .module('appSignIn', [
      'ngMessages',
      'ngSanitize',
      'ui.router',
      'ui-notification'
    ]);

  angular
    .module('appSignIn')
    .config(config);

  config.$inject = ['$stateProvider'];

  function config($stateProvider) {
    $stateProvider.state('signIn', {
      parent: 'app',
      url: '/sign-in?status',
      templateUrl: 'app/signin/sign-in.html',
      controller: 'SignInController as signIn',
      resolve: {
        status: ['$stateParams', function ($stateParams) {
          return $stateParams.status;
        }]
      },
      onEnter: ['$rootScope', '$state', 'Security', function ($rootScope, $state, Security) {
        if (Security.isAuthenticated()) {
          if (angular.isDefined($rootScope.toState) && $rootScope.toState.name !== 'signIn') {
            $state.go($rootScope.toState, $rootScope.toParams);
          } else {
            $state.go('app.home');
          }
        }
      }]
    });
  }
})();
