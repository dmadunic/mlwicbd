/**
 * Created by dmadunic on 20.8.2015..
 */
(function () {
  'use strict';

  angular
    .module('appconfig')
    .factory('AppConfig', AppConfig);

  AppConfig.$inject = ['$http', '$q', '$log'];

  function AppConfig($http, $q, $log) {

    var _appConfigData = undefined;
    var _apiConfig = undefined;

    //indicates if all attempts to fetch configurations have been made ...
    var resolved = false;

    var appConfig = {
      reset: reset,
      isInitialized: isInitialized,
      isResolved: isResolved,
      fetchApiConfig: fetchApiConfig,
      getApiBaseUrl: getApiBaseUrl,
      getApiVersion: getApiVersion,
      getApiUrl: getApiUrl,
      fetchAppConfigData: fetchAppConfigData,
      getAppConfigData: getAppConfigData,
      getApiConfig: getApiConfig
    };
    return appConfig;

    // implementation ...
    function reset() {
      _appConfigData = undefined;
      _apiConfig = undefined;
      resolved = false;
    }

    function isInitialized() {
      if (angular.isUndefined(_apiConfig) || _apiConfig === null) {
        return false;
      }
      return true;
    }

    function getApiConfig() {
      return _apiConfig;
    }

    function getAppConfigData() {
      return _appConfigData;
    }

    function fetchApiConfig() {
      var deferred = $q.defer();
      if (!!_apiConfig) {
        $log.debug('FOUND already initialized ApiConfig [OK]!');
        deferred.resolve(_apiConfig);
      } else {
        $log.debug('---> FETCHING app-config.json file ...');
        $http.get('app-config.json').then(function (response) {
          _apiConfig = {
            apiBaseUrl: response.data.api_base_url,
            apiVersion: response.data.api_version
          };
          $log.debug('SUCCESSFULLY initialized ApiConfig [OK]!');
          deferred.resolve(_apiConfig);
        }).catch(function (reason) {
          //_apiConfig = undefined;
          $log.debug('FAILED to initialize ApiConfig [NOK]! Reason: ' + reason);
          deferred.reject('API_CONFIG_FAILED');
        }).finally(function () {
          resolved = true;
        });
      }
      return deferred.promise;
    }

    function fetchAppConfigData() {
      var deferred = $q.defer();
      if (angular.isDefined(_appConfigData)) {
        deferred.resolve(_appConfigData);
      } else {
        $log.debug('Loading appConfig data from server ...');
        $http.get('/apiVersion/configs').then(function (response) {
          _appConfigData = response.data;
          $log.debug('LOADED AppConfig data from server [OK]');
          deferred.resolve(_appConfigData);
        }).catch(function (reason) {
          _appConfigData = undefined;
          $log.debug('FAILED to load AppConfig data from server [NOK] / reason=' + reason);
          deferred.reject('APP_CONFIG_FAILED');
        }).finally(function () {
          resolved = true;
        });
      }
      return deferred.promise;
    }

    function getApiBaseUrl() {
      return _apiConfig.apiBaseUrl;
    }

    function getApiVersion() {
      return _apiConfig.apiVersion;
    }

    function getApiUrl() {
      return getApiBaseUrl() + "/" + getApiVersion() + "/";
    }

    function isResolved() {
      return resolved;
    }

  }
})();
