'use strict';

/**
 * @ngdoc directive
 * @name izzyposWebApp.directive:adminPosHeader
 * @description
 * # adminPosHeader
 */
angular.module('mlwicbd')
	.directive('timeline',function() {
    return {
        templateUrl:'app/components/timeline/timeline.html',
        restrict: 'E',
        replace: true,
    }
  });
