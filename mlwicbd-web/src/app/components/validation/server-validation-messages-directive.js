/**
 * Created by Domagoj Madunic on 20/09/2016.
 *
 * Directive that shows server validation errors
 */
(function() {
  'use strict';

angular
  .module('mlwicbd')
  .directive('serverValidationMessages', serverValidationMessages);
  
/** @ngInject */
function serverValidationMessages() {
  return {
    restrict: 'E',
    scope: {
      messages: '='
    },
    template:'<div ng-repeat="message in messages">' +
                '<p class="help-block">{{message}}</p>' +
             '</div>'
  };
}
  
})();
