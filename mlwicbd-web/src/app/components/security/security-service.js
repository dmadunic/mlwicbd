/**
 * Created by dmadunic on 29.8.2015.
 */
(function () {
  'use strict';

  angular
    .module('security', ['base64'])
    .factory('Security', Security);

  Security.$inject = ['$rootScope', '$http', '$q', '$base64', '$state', '$log', 'localStorageService', 'APP_EVENTS'];

  function Security($rootScope, $http, $q, $base64, $state, $log, localStorageService, APP_EVENTS) {
    var _user = undefined;
    var _credentials = undefined;

    var security = {
      user: user,
      setUser: setUser,
      clear: clear,
      authenticate: authenticate,
      isAuthenticated: isAuthenticated,
      getAuthenticatedUser: getAuthenticatedUser,
      getEncodedCredentials: getEncodedCredentials,
      signOut: signOut,
      hasCredentials: hasCredentials,
      storeCredentials: storeCredentials
    };
    return security;

    //--- implementation ------------------------

    /**
     * Returns current user object if one exists, if not attempts to fetch it from REST api
     * using credentials stored in localStorage.
     *
     */
    function user() {
      if (!!_user) {
        return _user;
      }
      var promise = getAuthenticatedUser();

      promise.then(function (response) {
        $log.debug("SUCCES reload user! ");
        return response.data;
      }).catch(function (error) {
        $log.error("FAILED to fetch user! error=", error);
        //TODO: what now - clear and logout?
        // clear()
        // $state.go('signIn');
      });
    }

    function setUser(user) {
      _user = user;
    }

    function isAuthenticated() {
      return !!_user;
    }

    function clear() {
      _user = undefined;
      _credentials = undefined;
      localStorageService.remove('credentials');
    }

    function authenticate(username, password, rememberMe) {
      var credentials = {
        username : username,
        password : password
      };
      _user = undefined;
      storeCredentials(credentials, rememberMe);
      return getAuthenticatedUser();
    }

    function getAuthenticatedUser() {
      var deferred = $q.defer();
      if (!!_user) {
        deferred.resolve(_user);
      } else {
        $http({
          method: 'GET',
          url: '/apiVersion/users/me'
        })
        .then(function (userResponse) {
          _user = userResponse.data;
          $rootScope.$broadcast(APP_EVENTS.USER_SIGN_IN, _user);
          deferred.resolve(_user);
        })
        .catch(function (reason) {
          _user = undefined;
          localStorageService.remove('credentials');
          deferred.reject(reason);
        });
      }
      return deferred.promise;
    }

    function signOut() {
      clear();
      $rootScope.$broadcast(APP_EVENTS.USER_SIGN_OUT);
    }

    function getEncodedCredentials() {
      if (angular.isDefined(_credentials) && _credentials !== null) {
        return _credentials;
      }
      return localStorageService.get('credentials');
    }

    function hasCredentials() {
      if (angular.isDefined(_credentials) && _credentials !== null) {
        return true;
      }
      var credentials = localStorageService.get('credentials');
      return angular.isDefined(credentials) && credentials !== null;
    }

    function storeCredentials(credentials, rememberMe) {
      var encodedCredentials = $base64.encode(credentials.username + ':' + credentials.password);
      _credentials = encodedCredentials;
      if (rememberMe) {
        localStorageService.set('credentials', encodedCredentials);
      }
    }

  }
})();
