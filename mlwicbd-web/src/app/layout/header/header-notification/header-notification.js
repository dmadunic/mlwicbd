'use strict';

/**
 * @ngdoc directive
 * @name izzyposWebApp.directive:adminPosHeader
 * @description
 * # adminPosHeader
 */
angular.module('mlwicbd')
	.directive('headerNotification',function(){
		return {
        templateUrl:'app/layout/header/header-notification/header-notification.html',
        restrict: 'E',
        replace: true,
    	};
	});


