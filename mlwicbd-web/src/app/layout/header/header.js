'use strict';

/**
 * @ngdoc directive
 * @name izzyposWebApp.directive:adminPosHeader
 * @description
 * # adminPosHeader
 */
angular.module('mlwicbd')
	.directive('header',function(){
		return {
        templateUrl:'app/layout/header/header.html',
        restrict: 'E',
        replace: true
    	};
	});


