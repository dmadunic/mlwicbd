/**
 * Controller for ServiceRecord Modal which is used for bith creation of new and update of existing records.
 *  
 * Author: Domagoj Madunić 26.09.2016.
 */
(function () {
  'use strict';

  angular
    .module('musterList')
    .controller('ServiceRecordModalController', ServiceRecordModalController);

  ServiceRecordModalController.$inject = ['$stateParams', '$uibModalInstance', '$log', 'Notification', 'serviceRecord'];

  function ServiceRecordModalController($stateParams, $uibModalInstance, $log, Notification, serviceRecord) {
    var vm = this;
    vm.serviceRecord = undefined;
    vm.isNewRecord = false;
    
    vm.save = save;
    vm.close = close;
    
    if (serviceRecord === null) {
      vm.isNewRecord = true;
      vm.serviceRecord = {
        id: undefined,
        description: undefined,
        durationYears: undefined,
        durationMonths: undefined,
        durationDays: undefined
      };
    } else {
      vm.serviceRecord = angular.copy(serviceRecord);
    }
    
    //-- modal functions ------------------------------------------------------

    function close() {
      $uibModalInstance.dismiss(false);
    }

    function save() {
      $uibModalInstance.close(vm.serviceRecord);
    }

  }
})();
