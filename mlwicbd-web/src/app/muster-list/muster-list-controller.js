/**
 *
 * Author: Domagoj Madunić 2016.
 */
(function () {
  'use strict';

  angular.module('musterList')
    .controller('MusterListController', MusterListController);

  MusterListController.$inject = [ '$rootScope', '$state', '$uibModal', '$scope', '$log', 'Notification', 'NgTableParams', 'CommonCodes', 'MusterLists', 'regiments'];

  function MusterListController( $rootScope, $state, $uibModal, $scope, $log, Notification, NgTableParams, CommonCodes, MusterLists, regiments) {
    var vm = this;
    vm.regiments = regiments;
    vm.dataLoded = false;
    vm.months = CommonCodes.months();

    vm.searchParams = {
      year: undefined,
      unitId: undefined,
      month: undefined
    };
    
    vm.findMusterLists = findMusterLists;
    vm.showDetails = showDetails;

    init();

    function init() {
      vm.searchParams = MusterLists.lastSearchParams();
    }

    function showDetails(musterListId) {
      $state.go('musterList-details', {id: musterListId}, { reload: true });
    }

    function findMusterLists() {
      vm.tableParams.reload();
    }

    function handleFailurePromise(reason) {
      $log.error("Failed to fetch muster lists!");
      $log.error(reason);
    }

    //---- ngTable configuration ----------------------------------------------

    vm.tableParams = new NgTableParams({ count : 25 }, {
      getData: function(params) {

        var results = [];
        if (!searchParamsDefined(vm.searchParams)) {
          return results;
        }
        var searchParams = mapFromSearchParams();
        searchParams.page = (params.page() - 1);
        searchParams.size = params.count();

        
        //searchParams.push("page=" + (params.page() - 1));
        //searchParams.push("size=" + params.count());

        $rootScope.restDataLoadPromise = MusterLists.search(searchParams);

        results = $rootScope.restDataLoadPromise.then(function(response) {
            params.total(response.data.totalElements);
            vm.dataLoded = true;
            return response.data.content;
          })
          .catch(handleFailurePromise)
          .finally(function () {
            vm.disableControls = false;
          });
        return results;
      }
    });

    // --- helper functions ---------------------------------------------------

    function mapFromSearchParams() {
      var sp = {};
      sp.unitId = vm.searchParams.unitId;
      sp.year =  vm.searchParams.year;
      sp.month = vm.searchParams.month;

      return sp;
    }

    function searchParamsDefined(searchParams) {
      if (angular.isDefined(searchParams.year)) {
        return true;
      }
      if (angular.isDefined(searchParams.unitId)) {
        return true;
      }
      return false;
    }    


  }

})();
