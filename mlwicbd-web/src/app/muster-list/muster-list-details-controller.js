/**
 * 
 * Author: Domagoj Madunić 2016.
 */

(function () {
  'use strict';

  angular.module('musterList')
    .controller('MusterListDetailsController', MusterListDetailsController);

  MusterListDetailsController.$inject = [ 
    '$rootScope', 
    '$state', 
    '$scope', 
    '$log', 
    '$translate', 
    '$uibModal', 
    'Notification', 
    'NgTableParams', 
    'MilitaryUnits', 
    'MusterLists', 
    'MusterListEntries', 
    'musterList' 
  ];

  function MusterListDetailsController( 
    $rootScope, 
    $state, 
    $scope, 
    $log, 
    $translate, 
    $uibModal, 
    Notification, 
    NgTableParams, 
    MilitaryUnits, 
    MusterLists, 
    MusterListEntries, 
    musterList 
  ) {
    var vm = this;
    vm.ml = musterList;
    vm.isEditDisabled = false;
    vm.isNewEntryDisabled = false;
    vm.isDownloadCsvDisabled = false;
    vm.dataLoded = true;

    vm.editMusterListDetails = editMusterListDetails;
    vm.editSelectedEntry = editSelectedEntry;
    vm.backToSearchResults = backToSearchResults;
    vm.newMusterListEntry = newMusterListEntry;
    vm.downloadAsCsv = downloadAsCsv;

    init();

    function init() {
      //TODO: set this based on roles ...
      vm.isEditDisabled = false;
      vm.isNewEntryDisabled = false;
      vm.isDownloadCsvDisabled = false;
    }

    function editSelectedEntry(mlEntryId) {
       $state.go('musterList-entry', {id : vm.ml.id, mlid: mlEntryId}); 
    }

    function newMusterListEntry() {
      $state.go('musterList-entry', {id : vm.ml.id, mlid: null}); 
    }

    function backToSearchResults() {
      $state.go('musterList-home'); 
    }

    function downloadAsCsv() {
      Notification.info("This function is not yet implemented");
    }

    //--- ng-table setup and helper functions ---------------------------------

    vm.tableParams = new NgTableParams({ count : 25}, {
            getData: getTableData
        }
    );

    function getTableData(params) {
        var results = [];
        var searchParams = prepareSearchParams();

        searchParams.push("page=" + (params.page() - 1));
        searchParams.push("size=" + params.count());
        searchParams.push("sort=listNumber");

        $rootScope.restDataLoadPromise = MusterListEntries.searchByMusterListId(musterList.id, searchParams);

        results = $rootScope.restDataLoadPromise.then(function(response) {
            params.total(response.data.totalElements);
            vm.dataLoded = true;
            return response.data.content;
          })
          .catch(handleFailurePromise)
          .finally(function () {
            vm.disableControls = false;
          });
        return results;
    }

    function prepareSearchParams() {
        var searchParams = [];
        //TODO: should set default search params ...
        return searchParams;
    }

    function handleFailurePromise(reason) {
      $log.error("Failed to fetch muster list entries!");
      $log.error(reason);
    }

    //--- MODAL: Edit Ml Details form ------------------------------------------

    function editMusterListDetails(index) {
      vm.isEditDisabled = true;
      var mlEditModal = $uibModal.open({
          animation: true,
          templateUrl: 'app/muster-list/ml-edit/muster-list-edit-modal.html',
          controller: 'MusterListEditModalController as mlEditModalCtl',
          size: 'lg',
          resolve:{
            musterList: function() {
              return vm.ml;
            },
            companies: resolveCompanies
          }
        }
      );
      mlEditModal.result.then (function (mlDetailsDto) {
        vm.ml = mlDetailsDto;
      });
      mlEditModal.result.finally(function() {
          vm.isEditDisabled = false;
        }
      );
    }

    //--- MODAL helper functions ----------------------------------------------

    function resolveCompanies() {
      var searchParams = [
        'unitType=COMPANY',
        'territory=CS-MIL-FR',
        'sort=parentUnit.name,name',
        'size=500'
      ];
      $rootScope.restDataLoadPromise = MilitaryUnits.search(searchParams);
      return $rootScope.restDataLoadPromise.then(function (response) {
        return response.data.content;
      });
    }

  }
})();
