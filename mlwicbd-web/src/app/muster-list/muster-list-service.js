/**
 *
 * Created by dmadunic on 21.04.2016.
 */
(function(){
  'use strict';

  angular
    .module('musterList')
    .factory('MusterLists', MusterLists);

  MusterLists.$inject = ['$http', 'HttpUtils'];

  function MusterLists($http, HttpUtils) {

    var _mlSearchParams = {
      year: undefined,
      unitId: undefined,
      month: undefined
    };

    var musterLists = {
      search: search,
      lastSearchParams: lastSearchParams,
      getMusterList: getMusterList,
      getMusterListEntries: getMusterListEntries,
      update: update
    };
    return musterLists;

    //--- implementation ------------------------------------------------------

    function lastSearchParams() {
      return _mlSearchParams;
    }

    function search(searchParams) {
      _mlSearchParams = searchParams;
      var sp = prepareSearchParams(searchParams);
      var queryParams = HttpUtils.toQueryParams(sp);
      return $http({
        method: 'GET',
        url: '/apiVersion/musterlists' + queryParams
      });
    }

    function getMusterList(id) {
      return $http({
        method: 'GET',
        url: '/apiVersion/musterlists/' + id
      });
    }

    //TODO: move to separate service ...
    function getMusterListEntries(id, searchParams) {      
      var queryParams = toQueryParams(searchParams);
      return $http({
        method: 'GET',
        url: '/apiVersion/musterlists/' + id + '/entries' + queryParams
      });
    }

    function update(mlUpdateData) {
      var form = mapToUpdateMusterListForm(mlUpdateData);
      return $http({
        method: 'PUT',
        url: '/apiVersion/musterlists/' + mlUpdateData.id,
        data: form
      });
    }

    //--- util methods --------------------------------------------------------

    function mapToUpdateMusterListForm(mlUpdateData) {
      var restForm = {
        title: mlUpdateData.title,
        referenceCode: mlUpdateData.referenceCode,
        unitId: mlUpdateData.unitId,
        year: mlUpdateData.year,
        month: mlUpdateData.month
      };
      return restForm;
    }

    function prepareSearchParams(searchParams) {
      var sp = [];
      if (angular.isDefined(searchParams.unitId)) {
        sp.push("unitId=" + searchParams.unitId);
      }
      if (angular.isDefined(searchParams.year)) {
        sp.push("year=" + searchParams.year);
      }
      if (angular.isDefined(searchParams.month)) {
        sp.push("month=" + searchParams.month);
      }
      sp.push("page=" + searchParams.page);
      sp.push("size=" + searchParams.size);
      return sp;
    }

  }
})();
