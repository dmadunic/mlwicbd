/**
 *
 * Created by dmadunic on 21.09.2016.
 */
(function(){
  'use strict';

  angular
    .module('musterList')
    .factory('MusterListEntries', MusterListEntries);

  MusterListEntries.$inject = ['$http', 'HttpUtils'];

  function MusterListEntries($http, HttpUtils) {
    var _savedForm = null;

    var musterListEntries = {
      searchByMusterListId: searchByMusterListId,
      searchEntries: searchEntries,
      getEntry: getEntry,
      create: create,
      update: update,
      remove: remove,
      saveForm: saveForm,
      getForm: getForm
    };
    return musterListEntries;

    //--- implementation ------------------------------------------------------

    function searchByMusterListId(id, searchParams) {      
      var queryParams = HttpUtils.toQueryParams(searchParams);
      return $http({
        method: 'GET',
        url: '/apiVersion/musterlists/' + id + '/entries' + queryParams
      });
    }

    function searchEntries(searchParams) {
      var queryParams = HttpUtils.toQueryParams(searchParams);
      return $http({
        method: 'GET',
        url: '/apiVersion/musterlists/entries' + queryParams
      });
    }

    function getEntry(id, mlid) {
      return $http({
        method: 'GET',
        url: '/apiVersion/musterlists/' + id + '/entries/' + mlid
      });
    }

    function create(id, mlEntry) {
      var form = adjustFormForRestApi(mlEntry);

      return $http({
        method: 'POST',
        url: '/apiVersion/musterlists/' + id + '/entries',
        data: form
      });
    }

    function update(id, mlEntry) {
      var form = adjustFormForRestApi(mlEntry);

      return $http({
        method: 'PUT',
        url: '/apiVersion/musterlists/' + id + '/entries/' + mlEntry.id,
        data: form
      });
    }

    function remove(mlEntryId) {

    }

    function saveForm(mleForm) {
      _savedForm = mleForm;
    }

    function getForm() {
      return _savedForm;
    }

    //--- util methods --------------------------------------------------------
    
    function adjustFormForRestApi(mlEntry) {
      var form = angular.copy(mlEntry);
      if (mlEntry.person) {
        form.personId = mlEntry.person.id;
      }
      delete form.id;
      delete form.person;
      delete form.musterListId;
      return form;
    }
    
  }
})();
