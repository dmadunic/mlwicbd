/**
 * Domagoj Madunić 15/04/2016.
 *
 */
(function () {
  'use strict';

  angular
    .module('musterList', [
      'ui.router',
      'ui-notification',
      'ui.bootstrap',
      'ngTable'
    ]);

  angular
    .module('musterList')
    .config(config);

  config.$inject = ['$stateProvider'];

  function config($stateProvider) {
    $stateProvider.state('musterList-home', {
      parent: 'app',
      url: '/muster-lists',
      templateUrl: 'app/muster-list/home.html',
      controller: 'MusterListController as mlCtrl',
      resolve: {
        regiments: fetchRegiments
      }
    });
    $stateProvider.state('musterList-details', {
      parent: 'app',
      url: '/muster-list/:id',
      templateUrl: 'app/muster-list/details.html',
      controller: 'MusterListDetailsController as mlDetailsCtl',
      data: {
        authenticated: true
      },
      resolve: {
        musterList: resolveMusterList
      }
    });
    $stateProvider.state('musterList-entry', {
      parent: 'app',
      url: '/muster-list/:id/entry/:mlid?restore',
      templateUrl: 'app/muster-list/entry-form.html',
      controller: 'MusterListEntryController as mlEntryCtl',
      data: {
        authenticated: true
      },
      resolve: {
        mlEntry: resolveMusterListEntry,
        musterList: resolveMusterList,
        ranks: resolveRanks,
        denominations: resolveDenominations,
        professions: resolveProfessions
      }
    });
  }

  //--- 
  fetchRegiments.$inject = ['$rootScope', 'MilitaryUnits'];

  function fetchRegiments($rootScope, MilitaryUnits) {
    var searchParams = [
      'unitType=REGIMENT',
      'territory=CS-MIL-FR',
      'sort=name'
    ];
    $rootScope.restDataLoadPromise = MilitaryUnits.search(searchParams);
    return $rootScope.restDataLoadPromise.then(function (response) {
      return response.data.content;
    });
  }

  //--- 
  resolveMusterList.$inject = ['$rootScope', '$stateParams', 'MusterLists'];

  function resolveMusterList($rootScope, $stateParams, MusterLists) {
    var id = $stateParams.id;
    
    if (!id || 0 === id) {
        //TODO: this is illegal state ... handle it
        return null;
    }
    $rootScope.restDataLoadPromise = MusterLists.getMusterList(id);
    return $rootScope.restDataLoadPromise.then(function (response) {
      return response.data;
    });
  }

  //---
  resolveMusterListEntry.$inject = ['$rootScope', '$stateParams', '$log', 'MusterListEntries'];

  function resolveMusterListEntry($rootScope, $stateParams, $log, MusterListEntries) {
    var mlid = $stateParams.mlid;
    var id = $stateParams.id;
    
    if (!mlid || 0 === mlid) {
      return null 
    }

    if (!id || 0 === id) {
      $log.debug("Invalid state - missing mandatory param: id (musterList) --> redirecting to home");
      //TODO: redirect to home...
      return null;
    }
    $rootScope.restDataLoadPromise = MusterListEntries.getEntry(id, mlid);
    return $rootScope.restDataLoadPromise.then(function (response) {
      return response.data;
    });
  }

  //--- 
  resolveRanks.$inject = ['$rootScope', 'MilitaryRanks'];

  function resolveRanks($rootScope, MilitaryRanks) {
    var searchParams = [
      'territoryCode=CS-MIL-FR'
    ];
    $rootScope.restDataLoadPromise = MilitaryRanks.search(searchParams);
    return $rootScope.restDataLoadPromise.then(function (response) {
      return response.data.content;
    });
  }

  //---
  resolveDenominations.$inject = ['$rootScope', 'ReligiousDenominations'];

  function resolveDenominations($rootScope, ReligiousDenominations) {
    $rootScope.restDataLoadPromise = ReligiousDenominations.fetchAll();
    return $rootScope.restDataLoadPromise.then(function (response) {
      return response.data.content;
    });
  }

  //---
  resolveProfessions.$inject = ['$rootScope', 'Professions'];

  function resolveProfessions($rootScope, Professions) {
    $rootScope.restDataLoadPromise = Professions.fetchAll();
    return $rootScope.restDataLoadPromise.then(function (response) {
      return response.data.content;
    });
  }

})();
