/**
 * Author: Domagoj Madunić 18.09.2016.
 */

(function () {
  'use strict';

  angular
    .module('musterList')
    .controller('MusterListEditModalController', MusterListEditModalController);

  MusterListEditModalController.$inject = ['$stateParams', '$uibModalInstance', '$log', 'Notification', 'MusterLists', 'CommonCodes', 'Errors', 'musterList', 'companies'];

  function MusterListEditModalController($stateParams, $uibModalInstance, $log, Notification, MusterLists, CommonCodes, Errors, musterList, companies) {
    var vm = this;
    vm.details = {};
    vm.isNewRecord = false;
    vm.companies = companies;
    vm.saveButtonDisabled = false;
    vm.months = CommonCodes.months();
    vm.serverValidationErrors = {};

    vm.save = save;
    vm.close = close;
    
    if (musterList === null) {
      vm.isNewRecord = true;
      vm.details = {
          // set default values for details ....
      };
    } else {
      vm.details = {
        'id': musterList.id,
        'title': musterList.title,
        'year': musterList.year,
        'month': musterList.month,
        'unitId': musterList.unitId,
        'referenceCode': musterList.referenceCode
      };
      vm.update = true;
    }
    
    //-- modal functions ------------------------------------------------------

    function close() {
      $uibModalInstance.dismiss(false);
    }

    function save() {
      vm.saveButtonDisabled = true;
      var mlSavePromise;
      if (vm.isNewRecord === true) {
        mlSavePromise = MusterLists.create(vm.details);
        mlSavePromise.then(handleSuccessfulSave)
          .catch(handleSaveFailed)
          .finally(
            function () {
              vm.saveButtonDisabled = false;
            });
      } else {
        mlSavePromise = MusterLists.update(vm.details);
        mlSavePromise.then(handleSuccessfulSave)
          .catch(handleSaveFailed)
          .finally(
            function () {
              vm.saveButtonDisabled = false;
            });
      }
    }

    function handleSuccessfulSave(mlSavePromise) {
      Notification.success("Succes : Muster list saved");
      $uibModalInstance.close(mlSavePromise.data);
    }

    //TODO: create single util service utils centralized function to handle ALL failure cases! 
    function handleSaveFailed(reason) {
      var message = 'FAILED to save MusterList details.';
      $log.log(message + 'Reason: ' + angular.toJson(reason, true));
      if (reason.status !== 422) {
        Notification.error(message);
      } else {
        if (reason.data && reason.data.errors) {
            vm.serverValidationErrors = Errors.parseValidationErrors(reason.data.errors);
        }
      }
    }
    
  }
})();
