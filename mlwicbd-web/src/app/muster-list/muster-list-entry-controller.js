/**
 * MusterListEntry controller.
 * 
 * Author: Domagoj Madunić 2016.
 */

(function () {
  'use strict';

  angular.module('musterList')
    .controller('MusterListEntryController', MusterListEntryController);

  MusterListEntryController.$inject = [
    '$rootScope', 
    '$state', 
    '$stateParams',
    '$scope', 
    '$log', 
    '$translate', 
    '$uibModal', 
    'Notification', 
    'MusterListEntries', 
    'MilitaryRanks', 
    'CommonCodes',
    'Errors',
    'mlEntry',
    'musterList',
    'ranks', 
    'denominations',
    'professions'
  ];

  function MusterListEntryController(
    $rootScope, 
    $state, 
    $stateParams,
    $scope, 
    $log, 
    $translate, 
    $uibModal, 
    Notification, 
    MusterListEntries, 
    MilitaryRanks,
    CommonCodes,
    Errors,
    mlEntry,
    musterList,
    ranks, 
    denominations,
    professions
  ) {
    var vm = this;
    vm.entryForm = null;
    vm.ml = musterList;
    vm.person = undefined;
    vm.militaryRanks = ranks;
    vm.denominations = denominations;
    vm.maritalStatuses = CommonCodes.maritalStatuses();
    vm.professions = professions;
    
    vm.isSaveDisabled = false;
    vm.isAddNewSrDisabled = false;
    vm.dataLoded = true;
    vm.srIndex = -1;
    vm.serverValidationErrors = {};

    vm.save = save;
    vm.backToMlDetails = backToMlDetails;
    vm.registerNewProfession = registerNewProfession;
    vm.addNewServiceRecord = addNewServiceRecord;
    vm.editServiceRecord = editServiceRecord;
    vm.removeServiceRecord = removeServiceRecord;
    vm.openPersonModal = openPersonModal;
    vm.addNewChildRecord = addNewChildRecord;
    vm.editChildRecord = editChildRecord;
    vm.removeChildRecord = removeChildRecord;

    init();

    function init() {
      vm.isSaveDisabled = false;
      var restoreForm = $stateParams.restore;
      
      if (restoreForm == 1) {
          $log.debug("Attempting to restore form (from service ....)");
          vm.entryForm = MusterListEntries.getForm();
      }
      if (vm.entryForm === null) {
        $log.debug("NO Form found in service ---> proceeding with initialization");
        if (mlEntry === null) {
          vm.entryForm = emptyForm();
        } else {
          vm.entryForm = mapEntryToForm(mlEntry);
        }
      } else {
        $log.debug("Form restored form service");
      }
    }

    //--- 

    function save() {
      var mleSavePromise = undefined;
      vm.isSaveDisabled = true;
      if (mlEntry === null) {
        mleSavePromise = MusterListEntries.create(vm.ml.id, vm.entryForm);
      } else {
        mleSavePromise = MusterListEntries.update(vm.ml.id, vm.entryForm);
      }

      mleSavePromise.then(handleSaveSuccess)
        .catch(handleSaveFailed)
        .finally (function(){
          vm.isSaveDisabled = false;
        });  
    }

    function handleSaveSuccess() {
      Notification.info("Entry saved successfully");
      backToMlDetails();
    }

    function handleSaveFailed(reason) {
      var message = "Entry save failed";
      Notification.error(message);
      $log.log(message + ' Reason: ' + angular.toJson(reason, true));
      if (reason.data) {
        if (reason.data.errors) {
          vm.serverValidationErrors = Errors.parseValidationErrors(reason.data.errors);
        }
      }
    }

    function backToMlDetails() {
      $state.go('musterList-details', {id : vm.entryForm.musterListId});
    }

    //-- person modal ---------------------------------------------------------

    function openPersonModal() {
      MusterListEntries.saveForm(vm.entryForm);
      $state.go('person-home', {from : 'mleForm'});
      //Notification.info("This function is not yet implemented");
    }

    //--- professions modal ---------------------------------------------------

    function registerNewProfession() {
      Notification.info("This function is not yet implemented");
    }

    //--- children modal ------------------------------------------------------
    
    function addNewChildRecord() {
      vm.isAddNewChildDisabled = true;
      var childRecordModal = $uibModal.open({
          animation: true,
          templateUrl: 'app/muster-list/child-record/form-modal.html',
          controller: 'ChildRecordModalController as childModalCtl',
          size: 'md',
          resolve: {
            child: function() {
              return null;
            }
          }
        }
      );
      childRecordModal.result.then (function (child) {
        vm.entryForm.children.push(child);
      });
      childRecordModal.result.finally(function() {
          vm.isAddNewChildDisabled = false;
        }
      );
    }

    function editChildRecord(index) {
      $log.debug("index=" + index);
      vm.chIndex = index;

      var childRecordModal = $uibModal.open({
          animation: true,
          templateUrl: 'app/muster-list/child-record/form-modal.html',
          controller: 'ChildRecordModalController as childModalCtl',
          size: 'md',
          resolve: {
            child: function() {
              return vm.entryForm.children[index];
            }
          }
        }
      );
      childRecordModal.result.then (function (child) {
        vm.entryForm.children[vm.chIndex] = child;
      });
      childRecordModal.result.finally(function() {
          // Nothing to do?
        }
      );
    }

    function removeChildRecord(index) {
      vm.entryForm.children.splice(index, 1);
    }

    //--- service record modal ------------------------------------------------
    
    function addNewServiceRecord() {
      vm.isAddNewSrDisabled = true;
      var serviceRecordModal = $uibModal.open({
          animation: true,
          templateUrl: 'app/muster-list/service-record/form-modal.html',
          controller: 'ServiceRecordModalController as srModalCtl',
          size: 'md',
          resolve: {
            serviceRecord: function() {
              return null;
            }
          }
        }
      );
      serviceRecordModal.result.then (function (serviceRecord) {
        vm.entryForm.serviceRecords.push(serviceRecord);
      });
      serviceRecordModal.result.finally(function() {
          vm.isAddNewSrDisabled = false;
        }
      );
    }

    function editServiceRecord(index) {
      $log.debug("index=" + index);
      vm.srIndex = index;
      
      var serviceRecordModal = $uibModal.open({
          animation: true,
          templateUrl: 'app/muster-list/service-record/form-modal.html',
          controller: 'ServiceRecordModalController as srModalCtl',
          size: 'md',
          resolve: {
            serviceRecord: function() {
              return vm.entryForm.serviceRecords[index];
            }
          }
        }
      );
      serviceRecordModal.result.then (function (serviceRecord) {
        vm.entryForm.serviceRecords[vm.srIndex] = serviceRecord;
      });
      serviceRecordModal.result.finally(function() {
          //
        }
      );
    }

    function removeServiceRecord(index) {
      vm.entryForm.serviceRecords.splice(index, 1);
    }

    //--- form util functions -------------------------------------------------

    function emptyForm() {
      var form = {
        id: undefined,
        musterListId: undefined,
        listNumber: undefined,
        person: undefined,
        firstName: undefined,
        lastName: undefined,
        placeOfBirth: undefined,
        rankId: undefined,
        sourceProfession: undefined,
        professionId: undefined,
        location: undefined,
        houseNumber: undefined,
        age: undefined,
        religionId: undefined,
        maritalStatus: undefined,
        wifePresent: undefined,
        unit1: undefined,
        unit2: undefined,
        unit3: undefined,
        totalYearsServed: undefined, 
        totalMonthsServed: undefined,
        serviceRecords: [],
        children: []
      };
      return form;
    }

    function mapEntryToForm(mlEntry) {
      var form = {
        id: mlEntry.id,
        listNumber: mlEntry.listNumber,
        musterListId: mlEntry.musterListId,
        person: mlEntry.person,
        firstName: mlEntry.firstName,
        lastName: mlEntry.lastName,
        placeOfBirth: mlEntry.placeOfBirth,
        sourceProfession: mlEntry.sourceProfession,
        location: mlEntry.location,
        houseNumber: mlEntry.houseNumber,
        age: mlEntry.age,
        maritalStatus: mlEntry.maritalStatus,
        wifePresent: mlEntry.wifePresent,
        unit1: mlEntry.unit1,
        unit2: mlEntry.unit2,
        unit3: mlEntry.unit3,
        totalYearsServed: mlEntry.totalYearsServed, 
        totalMonthsServed: mlEntry.totalMonthsServed,
        serviceRecords: [],
        children: []
      };

      if (mlEntry.rank) {
        form.rankId = mlEntry.rank.id;
      }
      if (mlEntry.profession) {
        form.professionId = mlEntry.profession.id;
      }
      if (mlEntry.religion) {
        form.religionId = mlEntry.religion.id;
      }
      
      for(var i=0; i < mlEntry.serviceRecords.length; i++) {
        var sr = {
          id: mlEntry.serviceRecords[i].id,
          description: mlEntry.serviceRecords[i].description,
          durationYears: mlEntry.serviceRecords[i].durationYears,
          durationMonths: mlEntry.serviceRecords[i].durationMonths,
          durationDays: mlEntry.serviceRecords[i].durationDays
        }
        form.serviceRecords.push(sr);
      }
      
      for(var i=0; i < mlEntry.children.length; i++) {
        var child = {
          id: mlEntry.children[i].id,
          name: mlEntry.children[i].name,
          age: mlEntry.children[i].age,
          gender: mlEntry.children[i].gender
        }
        form.children.push(child);
      }
      return form;
    }
  }
})();
