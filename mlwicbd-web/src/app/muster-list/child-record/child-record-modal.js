/**
 * Controller for ChildRecord Modal which is used for bith creation of new and update of existing records.
 *  
 * Author: Domagoj Madunić 26.09.2016.
 */
(function () {
  'use strict';

  angular
    .module('musterList')
    .controller('ChildRecordModalController', ChildRecordModalController);

  ChildRecordModalController.$inject = ['$stateParams', '$uibModalInstance', '$log', 'Notification', 'CommonCodes', 'child'];

  function ChildRecordModalController($stateParams, $uibModalInstance, $log, Notification, CommonCodes, child) {
    var vm = this;
    vm.child = undefined;
    vm.childGender = CommonCodes.childGenderCodes();
    vm.isNewRecord = false;
    
    vm.save = save;
    vm.close = close;
    
    if (child === null) {
      vm.isNewRecord = true;
      vm.child = {
        id: undefined,
        name: undefined,
        gender: undefined,
        age: undefined
      };
    } else {
      vm.child = angular.copy(child);
    }
    
    //-- modal functions ------------------------------------------------------

    function close() {
      $uibModalInstance.dismiss(false);
    }

    function save() {
      $uibModalInstance.close(vm.child);
    }

  }
})();
