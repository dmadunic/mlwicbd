/**
 * Http utils functions used by data services.
 * 
 * Created by dmadunic on 21.09.2016.
 */
(function(){
  'use strict';

  angular
    .module('dataservices')
    .factory('HttpUtils', HttpUtils);

  HttpUtils.$inject = [];

  function HttpUtils() {
    var httpUtils = {
      toQueryParams : toQueryParams
    };
    return httpUtils;

    //--- implementation ------------------------------------------------------
    
    /**
     * Transforms array of searchParams each of which is in the form: "paramName=value" into single query string.
     * 
     */
    function toQueryParams(searchParams) {
      var queryParams = '';
      if (searchParams !== null && searchParams.length > 0) {
        queryParams += "?";
        for (var i = 0; i < searchParams.length; i++) {
          if (i > 0) {
            queryParams += '&';
          }
          queryParams += searchParams[i];
        }
      }
      return queryParams;
    }

  }
})();