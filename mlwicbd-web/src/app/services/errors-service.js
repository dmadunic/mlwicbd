/**
 *
 * Created by dmadunic on 20.09.2016.
 */
(function(){
  'use strict';

  angular
    .module('dataservices')
    .factory('Errors', Errors);

  Errors.$inject = [];

  function Errors() {
    var errors = {
      parseValidationErrors : parseValidationErrors
    };
    return errors;

    //--- implementation ------------------------------------------------------
    
    // create serverValidationErrors dictionary. Field names that caused the error are the keys.
    // Values are array of messages for that fields containing strings
    function parseValidationErrors(errors) {
      var serverValidationErrors = {};
      var n = errors.length;
      for (var i = 0; i<n; i++) {
        var error = errors[i];
        if ("field" in error && "message" in error){
          var key = error.field;
          if (!(error.field in serverValidationErrors)){
            serverValidationErrors[key] = [];
          }
          serverValidationErrors[key].push(error.message);
        }
      }
      return serverValidationErrors;
    }

  }
})();