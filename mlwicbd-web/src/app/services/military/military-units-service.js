/**
 *
 * Created by dmadunic on 17.04.2016.
 */
(function(){
  'use strict';

  angular
    .module('dataservices')
    .factory('MilitaryUnits', MilitaryUnits);

  MilitaryUnits.$inject = ['$http'];

  function MilitaryUnits($http) {
    var militaryUnits = {
      search: search,
      getDetails: getDetails
    };
    return militaryUnits;

    // implementation ...
    function search(searchParams) {
      var queryParams = '';
      if (searchParams !== null && searchParams.length > 0) {
        queryParams += "?";
        for (var i = 0; i < searchParams.length; i++) {
          if (i > 0) {
            queryParams += '&';
          }
          queryParams += searchParams[i];
        }
      }
      return $http({
        method: 'GET',
        url: '/apiVersion/militaryunits' + queryParams
      });
    }

    function getDetails(id) {
      return $http({
        method: 'GET',
        url: '/apiVersion/militaryunits/' + id
      });
    }

  }
})();
