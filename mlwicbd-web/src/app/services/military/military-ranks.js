/**
 *
 * Created by dmadunic on 21.09.2016.
 */
(function(){
  'use strict';

  angular
    .module('dataservices')
    .factory('MilitaryRanks', MilitaryRanks);

  MilitaryRanks.$inject = ['$http'];

  function MilitaryRanks($http) {
    var militaryRanks = {
      search: search
    };
    return militaryRanks;

    // implementation ...
    function search(searchParams) {
      var queryParams = '';
      if (searchParams !== null && searchParams.length > 0) {
        queryParams += "?";
        for (var i = 0; i < searchParams.length; i++) {
          if (i > 0) {
            queryParams += '&';
          }
          queryParams += searchParams[i];
        }
      }
      return $http({
        method: 'GET',
        url: '/apiVersion/militaryranks' + queryParams
      });
    }

  }
})();
