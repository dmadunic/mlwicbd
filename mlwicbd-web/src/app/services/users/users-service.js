/**
* Created by dmadunic on 5.9.2015.
*/
(function(){
  'use strict';

  angular
    .module('dataservices')
    .factory('Users', Users);

  Users.$inject = ['$http'];

  function Users($http) {
    var users = {
      search: search,
      getDetails: getDetails,
      create: create,
      update: update,
      delete: _delete,
    };
    return users;

    // implementation ...
    function search(searchParams) {
      var queryParams = '';
      if (searchParams !== null && searchParams.length > 0) {
        queryParams += "?";
        for (var i = 0; i < searchParams.length; i++) {
          if (i > 0) {
            queryParams += '&';
          }
          queryParams += searchParams[i];
        }
      }
      return $http({
        method: 'GET',
        url: '/apiVersion/users' + queryParams,
        params: {
          size: 2000
        }
      });
    }

    function getDetails(id) {
      return $http({
        method: 'GET',
        url: '/apiVersion/users/' + id
      });
    }

    function create(userData) {
      var form = mapToCreateUserForm(userData);

      if (angular.isDefined(userData.username)) {
        form.username = userData.username;
      }

      return $http({
        method: 'POST',
        url: '/apiVersion/users',
        data: form
      });
    }

    function update(userUpdateData) {
      var form = mapToUpdateUserRestForm(userUpdateData);
      return $http({
        method: 'PUT',
        url: '/apiVersion/users/' + userUpdateData.id,
        data: form
      });
    }

    function _delete(id) {
      return $http({
        method: 'DELETE',
        url: '/apiVersion/users/' + id
      });
    }

    function mapToCreateUserForm(userUpdateData) {
      var restForm = {
        password: userUpdateData.password,
        firstName: userUpdateData.firstName,
        lastName: userUpdateData.lastName,
        email: userUpdateData.email,
        address: userUpdateData.address,
        postalCode: userUpdateData.postalCode,
        countryCode: userUpdateData.countryCode,
        place: userUpdateData.place,
        mobile: userUpdateData.mobile,
        phone: userUpdateData.phone
      };
      return restForm;
    }

    function mapToUpdateUserRestForm(userUpdateData) {
      var restForm = mapToCreateUserForm(userUpdateData);
      if (angular.isDefined(userUpdateData.password)) {
        restForm.password = userUpdateData.password;
      }
      if (angular.isDefined(userUpdateData.password2)) {
        restForm.password2 = userUpdateData.password2;
      }
      return restForm;
    }

  }
})();
