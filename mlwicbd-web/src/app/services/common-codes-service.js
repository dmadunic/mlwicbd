/**
 * Common codes service.
 * 
 * Created by dmadunic on 18.09.2016.
 */
(function(){
  'use strict';

  angular
    .module('dataservices')
    .factory('CommonCodes', CommonCodes);

  CommonCodes.$inject = [];

  function CommonCodes() {
    
    var statuses = [
        {id: 1, code: "SINGLE"},
        {id: 2, code: "MARRIED"},
        {id: 3, code: "WIDOWED"}
    ];

    var commonCodes = {
      months: getMonths,
      maritalStatuses: getMaritalStatuses,
      getMartialStatus: getMartialStatus,
      childGenderCodes: getChildGenderCodes
    };
    return commonCodes;

    // implementation ...
        
    function getMonths() {
        var m = [
            {id: 1, code: "JAN"},
            {id: 2, code: "FEB"},
            {id: 3, code: "MAR"},
            {id: 4, code: "APR"},
            {id: 5, code: "MAY"},
            {id: 6, code: "JUN"},
            {id: 7, code: "JUL"},
            {id: 8, code: "AUG"},
            {id: 9, code: "SEP"},
            {id: 10, code: "OCT"},
            {id: 11, code: "NOV"},
            {id: 12, code: "DEC"}
        ];
        return m;
    }

    function getMaritalStatuses() {
      return statuses;
    }

    function getMartialStatus(id) {
        for (var j = 0; j < statuses.length; j++) {
          return(statuses[j]);
        }
    }

    function getChildGenderCodes() {
      var g = [ "SON", "DAUGHTER" ];
      return g;
    }
      
  }
})();