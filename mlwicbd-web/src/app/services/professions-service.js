/**
 * Created by dmadunic on 22.9.2016.
 */
(function(){
  'use strict';

  angular
    .module('dataservices')
    .factory('Professions', Professions);

  Professions.$inject = ['$http'];

  function Professions($http) {
    var professions = {
      fetchAll: fetchAll
    };
    return professions;

    function fetchAll() {
      return $http({
        method: 'GET',
        url: '/apiVersion/professions'
      });
    }

  }

})();
