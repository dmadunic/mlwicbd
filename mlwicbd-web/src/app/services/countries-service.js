/**
 * Created by dmadunic on 7.4.2016.
 */
(function(){
  'use strict';

  angular
    .module('dataservices')
    .factory('Countries', Countries);

  Countries.$inject = ['$http'];

  function Countries($http) {
    var countries = {
      fetchAll: fetchAll
    }
    return countries;

    function fetchAll() {
      return $http({
        method: 'GET',
        url: '/apiVersion/countries'
      });
    }

  }

})();

