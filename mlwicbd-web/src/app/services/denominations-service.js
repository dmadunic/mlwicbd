/**
 * Created by dmadunic on 22.9.2016.
 */
(function(){
  'use strict';

  angular
    .module('dataservices')
    .factory('ReligiousDenominations', ReligiousDenominations);

  ReligiousDenominations.$inject = ['$http'];

  function ReligiousDenominations($http) {
    var denominations = {
      fetchAll: fetchAll
    }
    return denominations;

    function fetchAll() {
      return $http({
        method: 'GET',
        url: '/apiVersion/denominations'
      });
    }

  }

})();
