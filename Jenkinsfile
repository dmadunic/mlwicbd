#!/usr/bin/env groovy

pipeline {
  agent any

  options {
    buildDiscarder(logRotator(numToKeepStr: '5', artifactNumToKeepStr: '5'))
    skipDefaultCheckout()
    ansiColor('xterm')
    timestamps()
  }

  tools {
    jdk '1.8.0_111'
  }

  stages {
    stage('Clean workspace') {
      steps {
        sh 'ls -lah'
        deleteDir()
        sh 'ls -lah'
      }
    }

    stage('Checkout source') {
      steps {
        checkout scm
      }
    }

    stage('Build Gradle') {
      steps {
        timeout(time: 30, unit: 'MINUTES') {
          sh './gradlew build --no-daemon --info --stacktrace'
        }
      }

      post {
        always {
          junit '**/build/test-results/test/*.xml'
        }

        success {
          archiveArtifacts artifacts: '**/build/libs/*.jar, **/gradle_build/distributions/*', fingerprint: true
        }
      }
    }

    stage('Analyze with SonarQube') {
      steps {
        timeout(time: 30, unit: 'MINUTES') {
          withSonarQubeEnv('local') {
            script {
              if (isReleaseBranchBuild()) {
                sh "./gradlew sonarqube --no-daemon --info --stacktrace -Dsonar.branch=${env.BRANCH_NAME}"
              } else if (isPullRequestBuild()) {
                def repositoryDetails = extractRepositoryDetails()

                withCredentials([usernamePassword(credentialsId: 'bitbucket-oauth', usernameVariable: 'OAUTH_CLIENT_KEY', passwordVariable: 'OAUTH_CLIENT_SECRET')]) {
                  sh "./gradlew sonarqube --no-daemon --info --stacktrace \
                        -Dsonar.bitbucket.repoSlug=${repositoryDetails['name']} \
                        -Dsonar.bitbucket.accountName=${repositoryDetails['owner']} \
                        -Dsonar.bitbucket.oauthClientKey=${env.OAUTH_CLIENT_KEY} \
                        -Dsonar.bitbucket.oauthClientSecret=${env.OAUTH_CLIENT_SECRET} \
                        -Dsonar.bitbucket.pullRequestId=${env.CHANGE_ID} \
                        -Dsonar.bitbucket.minSeverity=INFO \
                        -Dsonar.analysis.mode=issues"
                }
              }
            }
          }
        }
      }
    }
  }

  post {
    failure {
      script {
        if (isReleaseBranchBuild()) {
          mattermostSend color: 'danger', message: "**Build failed** :thumbsdown:\n[${currentBuild.displayName}](${currentBuild.absoluteUrl})\n**Job Name**\n[${env.JOB_NAME}](${env.JOB_URL})\n**Branch Name**\n${env.BRANCH_NAME}"
        }
      }
    }

    success {
      script {
        if (isReleaseBranchBuild()) {
          mattermostSend color: 'good', message: "**Build succeded** :thumbsup:\n[${currentBuild.displayName}](${currentBuild.absoluteUrl})\n**Job Name**\n[${env.JOB_NAME}](${env.JOB_URL})\n**Branch Name**\n${env.BRANCH_NAME}"
        }
      }
    }

    unstable {
      script {
        if (isReleaseBranchBuild()) {
          mattermostSend color: 'warning', message: "**Build unstable** :disappointed:\n[${currentBuild.displayName}](${currentBuild.absoluteUrl})\n**Job Name**\n[${env.JOB_NAME}](${env.JOB_URL})\n**Branch Name**\n${env.BRANCH_NAME}"
        }
      }
    }
  }
}

def isReleaseBranchBuild() {
  env.BRANCH_NAME ==~ /^((master)|(develop)|(release-\d\.\d))$/
}

def isPullRequestBuild() {
  if (env.CHANGE_ID) {
    return true
  }

  return false
}

// TODO: remove this after "cloudbees-bitbucket-branch-source" plugin is updated
def extractRepositoryDetails() {
  def matcher = (env.CHANGE_URL =~ /.*\/(.+)\/(.+)\/pull-requests\/\d+/)

  if (matcher.matches()) {
    return [owner: matcher.group(1), name: matcher.group(2)]
  }

  throw new IllegalStateException('Owner and repository should always be extractable from URL')
}